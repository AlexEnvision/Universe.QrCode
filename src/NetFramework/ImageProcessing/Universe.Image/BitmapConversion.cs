﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;

namespace Universe.Image
{
    public static class BitmapConversion
    {
        /// <summary>
        /// Реализует преобразование изображения в массив байтов
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static byte[] BitmapToBytes(Bitmap input)
        {
            MemoryStream ms = new MemoryStream();
            input.Save(ms, ImageFormat.Bmp);
            var picbyte = ms.GetBuffer();

            return picbyte;
        }

        /// <summary>
        /// Реализует преобразование из массива байт в изображение
        /// </summary>
        /// <param name="outputBytes"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        public static Bitmap BytesToBitmap(byte[] outputBytes, int width, int height)
        {        
            using (Bitmap image = new Bitmap(width, height, PixelFormat.Format24bppRgb))
            {
                Rectangle rect = new Rectangle(0, 0, width, height);
                BitmapData bmpData = image.LockBits(rect, ImageLockMode.ReadWrite, image.PixelFormat);
                IntPtr ptr = bmpData.Scan0;
                int bytes = Math.Abs(bmpData.Stride) * height;
                byte[] rgbValues = new byte[bytes];

                for (int counter = 0; counter < rgbValues.Length; counter += 3)
                {
                    rgbValues[counter] = outputBytes[counter];         // blue
                    rgbValues[counter + 1] = outputBytes[counter + 1]; // green
                    rgbValues[counter + 2] = outputBytes[counter + 2]; // red
                }

                Marshal.Copy(rgbValues, 0, ptr, bytes);
                image.UnlockBits(bmpData);
                //image.RotateFlip(RotateFlipType.RotateNoneFlipXY);
                image.Save("image.png", ImageFormat.Png);
            }
            System.Drawing.Image returnImage = System.Drawing.Image.FromFile("image.png");

            return returnImage as Bitmap;
        }
    }
}

﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Text;

namespace Universe.Image
{
    /// <summary>
    /// Считывает значения пикселей монохромного изображения
    /// Позволяет получить битовую маску в виде строкового представления
    /// </summary>
    public class BitsReader
    {
        /// <summary>
        /// 
        /// </summary>
        protected byte[] Bits;

        private readonly int _width;

        private readonly int _height;

        public string BitsStringRepresantation
        {
            get
            {
                var sb = new StringBuilder();
                for (int y = 0; y < _height; y++)
                {
                    var offset = y * _width;
                    var maxIndex = _width * 3;
                    for (int x = 0; x < maxIndex; x += 3)
                    {
                        sb.Append(Bits[offset] == 1 ? "X " : "  ");
                        offset++;
                    }
                    sb.Append("\n");
                }

                string stringRepresantation = sb.ToString();
                return stringRepresantation;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BitsReader"/> class.
        /// </summary>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        protected BitsReader(int width, int height)
        {
            _width = width;
            _height = height;

            Bits = new byte[width * height];
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BitsReader"/> class
        /// На вход принимается только бинаризированное изображение
        /// </summary>
        /// <param name="bitmap">The bitmap.</param>
        public BitsReader(Bitmap bitmap)
        {
            _width = bitmap.Width;
            _height = bitmap.Height;

            Bits = new byte[bitmap.Width * bitmap.Height];
            FetchBitValues(bitmap, Bits);
        }

        /// <summary>
        /// calculates the luminance values for bitmaps
        /// </summary>
        /// <param name="bitmap"></param>
        /// <param name="bits"></param>
        protected static void FetchBitValues(Bitmap bitmap, byte[] bits)
        {
            var height = bitmap.Height;
            var width = bitmap.Width;

            // In order to measure pure decoding speed, we convert the entire image to a greyscale array
            // The underlying raster of image consists of bytes with the luminance values
#if WindowsCE
            var data = bitmap.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
#else
            var data = bitmap.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadOnly, bitmap.PixelFormat);
#endif
            try
            {
                var stride = Math.Abs(data.Stride);
                var pixelWidth = stride / width;

                if (pixelWidth > 4)
                {
                    // old slow way for unsupported bit depth
                    FetchBitValuesSlow(bitmap, bits);
                }
                else
                {
#if !WindowsCE
                    if (bitmap.PixelFormat == PixelFormat.Format32bppArgb ||
                        bitmap.PixelFormat == PixelFormat.Format32bppPArgb)
                    {
                        pixelWidth = 40;
                    }
                    if ((int)bitmap.PixelFormat == 8207 ||
                        (bitmap.Flags & (int)ImageFlags.ColorSpaceCmyk) == (int)ImageFlags.ColorSpaceCmyk)
                    {
                        pixelWidth = 41;
                    }
#endif

                    switch (pixelWidth)
                    {
#if !WindowsCE
                        case 0:
                            if (bitmap.PixelFormat == PixelFormat.Format4bppIndexed)
                                FetchBitValuesForIndexed4Bit(bitmap, data, bits);
                            else
                                FetchBitValuesForIndexed1Bit(bitmap, data, bits);
                            break;
                        case 1:
                            FetchBitValuesForIndexed8Bit(bitmap, data, bits);
                            break;
#endif
                        case 2:
                            // should be RGB565 or RGB555, assume RGB565
                            FetchBitValues565(bitmap, data, bits);
                            break;
                        case 3:
                            FetchBitValues24Bit(bitmap, data, bits);
                            break;
                        case 4:
                            FetchBitValues32BitWithoutAlpha(bitmap, data, bits);
                            break;
                        case 40:
                            FetchBitValues32BitWithAlpha(bitmap, data, bits);
                            break;
                        case 41:
                            FetchBitValues32BitCmyk(bitmap, data, bits);
                            break;
                        default:
                            throw new NotSupportedException();
                    }
                }
            }
            finally
            {
                bitmap.UnlockBits(data);
            }
        }

        /// <summary>
        /// old slow way for unsupported bit depth
        /// </summary>
        /// <param name="bitmap"></param>
        /// <param name="bits"></param>
        protected static void FetchBitValuesSlow(Bitmap bitmap, byte[] bits)
        {
            var height = bitmap.Height;
            var width = bitmap.Width;

            for (int y = 0; y < height; y++)
            {
                int offset = y * width;
                for (int x = 0; x < width; x++)
                {
                    var c = bitmap.GetPixel(x, y);
                    bits[offset + x] = (byte)(c.R);
                }
            }
        }

#if !WindowsCE
        /// <summary>
        /// calculates the luminance values for 1-bit indexed bitmaps
        /// </summary>
        /// <param name="bitmap"></param>
        /// <param name="data"></param>
        /// <param name="bits"></param>
        protected static void FetchBitValuesForIndexed1Bit(Bitmap bitmap, BitmapData data, byte[] bits)
        {
            var height = data.Height;
            var width = data.Width;
            var stride = Math.Abs(data.Stride);
            var pixelWidth = stride / width;
            var strideStep = data.Stride;
            var buffer = new byte[stride];
            var ptrInBitmap = data.Scan0;

            if (pixelWidth != 0)
                throw new InvalidOperationException("Unsupported pixel format: " + bitmap.PixelFormat);

            // prepare palette for 1, 4 and 8 bit indexed bitmaps
            var luminancePalette = new byte[256];
            var luminancePaletteLength = Math.Min(bitmap.Palette.Entries.Length, luminancePalette.Length);
            for (var index = 0; index < luminancePaletteLength; index++)
            {
                var color = bitmap.Palette.Entries[index];
                luminancePalette[index] = (byte)((color.R));
            }

            for (int y = 0; y < height; y++)
            {
                // copy a scanline not the whole bitmap because of memory usage
                Marshal.Copy(ptrInBitmap, buffer, 0, stride);
#if NET40 || NET45 || NET46 || NET47 || NET48
                ptrInBitmap = IntPtr.Add(ptrInBitmap, strideStep);
#else
                ptrInBitmap = new IntPtr(ptrInBitmap.ToInt64() + strideStep);
#endif
                var offset = y * width;
                for (int x = 0; x * 8 < width; x++)
                {
                    var x8 = 8 * x;
                    var offset8 = offset + x8;
                    for (int subX = 0; subX < 8 && x8 + subX < width; subX++)
                    {
                        var index = (buffer[x] >> (7 - subX)) & 1;
                        bits[offset8 + subX] = luminancePalette[index];
                    }
                }
            }
        }

        /// <summary>
        /// calculates the luminance values for 4-bit indexed bitmaps
        /// </summary>
        /// <param name="bitmap"></param>
        /// <param name="data"></param>
        /// <param name="bits"></param>
        protected static void FetchBitValuesForIndexed4Bit(Bitmap bitmap, BitmapData data, byte[] bits)
        {
            if (bitmap.PixelFormat != PixelFormat.Format4bppIndexed)
                throw new InvalidOperationException("Unsupported pixel format: " + bitmap.PixelFormat);

            var height = data.Height;
            var width = data.Width;
            var stride = Math.Abs(data.Stride);
            var pixelWidth = stride / width;
            var strideStep = data.Stride;
            var buffer = new byte[stride];
            var ptrInBitmap = data.Scan0;
            var evenWidth = (width / 2) * 2;

            if (pixelWidth != 0)
                throw new InvalidOperationException("Unsupported pixel format: " + bitmap.PixelFormat);

            // prepare palette for 1, 4 and 8 bit indexed bitmaps
            var luminancePalette = new byte[256];
            var luminancePaletteLength = Math.Min(bitmap.Palette.Entries.Length, luminancePalette.Length);
            for (var index = 0; index < luminancePaletteLength; index++)
            {
                var color = bitmap.Palette.Entries[index];
                luminancePalette[index] = (byte)((color.R));
            }

            for (int y = 0; y < height; y++)
            {
                // copy a scanline not the whole bitmap because of memory usage
                Marshal.Copy(ptrInBitmap, buffer, 0, stride);
#if NET40 || NET45 || NET46 || NET47 || NET48
                ptrInBitmap = IntPtr.Add(ptrInBitmap, strideStep);
#else
                ptrInBitmap = new IntPtr(ptrInBitmap.ToInt64() + strideStep);
#endif
                var offset = y * width;
                int sourceX = 0;
                int destX = 0;
                byte sourceValue = 0;
                for (; destX < evenWidth; sourceX++, destX += 2)
                {
                    sourceValue = buffer[sourceX];
                    var index = sourceValue & 15;
                    bits[offset + destX + 1] = luminancePalette[index];
                    index = (sourceValue >> 4) & 15;
                    bits[offset + destX] = luminancePalette[index];
                }
                if (width > evenWidth)
                {
                    var index = (sourceValue >> 4) & 15;
                    bits[offset + destX] = luminancePalette[index];
                }
            }
        }

        /// <summary>
        /// calculates the luminance values for 8-bit indexed bitmaps
        /// </summary>
        /// <param name="bitmap"></param>
        /// <param name="data"></param>
        /// <param name="bits"></param>
        protected static void FetchBitValuesForIndexed8Bit(Bitmap bitmap, BitmapData data, byte[] bits)
        {
            var height = data.Height;
            var width = data.Width;
            var stride = Math.Abs(data.Stride);
            var pixelWidth = stride / width;
            var strideStep = data.Stride;
            var buffer = new byte[stride];
            var ptrInBitmap = data.Scan0;

            if (pixelWidth != 1)
                throw new InvalidOperationException("Unsupported pixel format: " + bitmap.PixelFormat);

            // prepare palette for 1, 4 and 8 bit indexed bitmaps
            var luminancePalette = new byte[256];
            var luminancePaletteLength = Math.Min(bitmap.Palette.Entries.Length, luminancePalette.Length);
            for (var index = 0; index < luminancePaletteLength; index++)
            {
                var color = bitmap.Palette.Entries[index];
                luminancePalette[index] = (byte)((color.R));
            }

            for (int y = 0; y < height; y++)
            {
                // copy a scanline not the whole bitmap because of memory usage
                Marshal.Copy(ptrInBitmap, buffer, 0, stride);
#if NET40 || NET45 || NET46 || NET47 || NET48
                ptrInBitmap = IntPtr.Add(ptrInBitmap, strideStep);
#else
                ptrInBitmap = new IntPtr(ptrInBitmap.ToInt64() + strideStep);
#endif
                var offset = y * width;
                for (int x = 0; x < width; x++)
                {
                    bits[offset + x] = (byte) (buffer[x] == 255 ? 0 : 1); //luminancePalette[buffer[x]];
                }
            }
        }
#endif

        /// <summary>
        /// calculates the luminance values for 565 encoded bitmaps
        /// </summary>
        /// <param name="bitmap"></param>
        /// <param name="data"></param>
        /// <param name="bits"></param>
        private static void FetchBitValues565(Bitmap bitmap, BitmapData data, byte[] bits)
        {
            var height = data.Height;
            var width = data.Width;
            var stride = Math.Abs(data.Stride);
            var pixelWidth = stride / width;
            var strideStep = data.Stride;
            var buffer = new byte[stride];
            var ptrInBitmap = data.Scan0;

            if (pixelWidth != 2)
#if !WindowsCE
                throw new InvalidOperationException("Unsupported pixel format: " + bitmap.PixelFormat);
#else
                throw new InvalidOperationException("Unsupported pixel format");
#endif

            for (int y = 0; y < height; y++)
            {
                // copy a scanline not the whole bitmap because of memory usage
                Marshal.Copy(ptrInBitmap, buffer, 0, stride);
#if NET40 || NET45 || NET46 || NET47 || NET48
                ptrInBitmap = IntPtr.Add(ptrInBitmap, strideStep);
#else
                ptrInBitmap = new IntPtr(ptrInBitmap.ToInt64() + strideStep);
#endif
                var offset = y * width;
                var maxIndex = 2 * width;
                for (int index = 0; index < maxIndex; index += 2)
                {
                    var byte1 = buffer[index];
                    var byte2 = buffer[index + 1];

                    var b5 = byte1 & 0x1F;
                    var g5 = (((byte1 & 0xE0) >> 5) | ((byte2 & 0x03) << 3)) & 0x1F;
                    var r5 = (byte2 >> 2) & 0x1F;
                    var r8 = (r5 * 527 + 23) >> 6;
                    var g8 = (g5 * 527 + 23) >> 6;
                    var b8 = (b5 * 527 + 23) >> 6;

                    bits[offset] = (byte)(r8);
                    offset++;
                }
            }
        }

        /// <summary>
        /// calculates the luminance values for 24-bit encoded bitmaps
        /// </summary>
        /// <param name="bitmap"></param>
        /// <param name="data"></param>
        /// <param name="bits"></param>
        private static void FetchBitValues24Bit(Bitmap bitmap, BitmapData data, byte[] bits)
        {
            var height = data.Height;
            var width = data.Width;
            var stride = Math.Abs(data.Stride);
            var pixelWidth = stride / width;
            var strideStep = data.Stride;
            var buffer = new byte[stride];
            var ptrInBitmap = data.Scan0;

            if (pixelWidth != 3)
#if !WindowsCE
                throw new InvalidOperationException("Unsupported pixel format: " + bitmap.PixelFormat);
#else
                throw new InvalidOperationException("Unsupported pixel format");
#endif

            for (int y = 0; y < height; y++)
            {
                // Считываем развертку изображения одну за другой в небольшой буффер. Это поможет сэкономить ОЗУ
                Marshal.Copy(ptrInBitmap, buffer, 0, stride);

#if NET40 || NET45 || NET46 || NET47 || NET48
                ptrInBitmap = IntPtr.Add(ptrInBitmap, strideStep);
#else
                ptrInBitmap = new IntPtr(ptrInBitmap.ToInt64() + strideStep);
#endif
                var offset = y * width;
                var maxIndex = width * 3;
                for (int x = 0; x < maxIndex; x += 3)
                {
                    var bit = (byte)(buffer[x] == 255 ? 0 : 1);
                    bits[offset] = bit;
                    offset++;
                }
            }
        }

        /// <summary>
        /// calculates the luminance values for 32-bit encoded bitmaps without respecting the alpha channel
        /// </summary>
        /// <param name="bitmap"></param>
        /// <param name="data"></param>
        /// <param name="bits"></param>
        private static void FetchBitValues32BitWithoutAlpha(Bitmap bitmap, BitmapData data, byte[] bits)
        {
            var height = data.Height;
            var width = data.Width;
            var stride = Math.Abs(data.Stride);
            var pixelWidth = stride / width;
            var strideStep = data.Stride;
            var buffer = new byte[stride];
            var ptrInBitmap = data.Scan0;
            var maxIndex = 4 * width;

            if (pixelWidth != 4)
#if !WindowsCE
                throw new InvalidOperationException("Unsupported pixel format: " + bitmap.PixelFormat);
#else
                throw new InvalidOperationException("Unsupported pixel format");
#endif

            for (int y = 0; y < height; y++)
            {
                // copy a scanline not the whole bitmap because of memory usage
                Marshal.Copy(ptrInBitmap, buffer, 0, stride);
#if NET40 || NET45 || NET46 || NET47 || NET48
                ptrInBitmap = IntPtr.Add(ptrInBitmap, strideStep);
#else
                ptrInBitmap = new IntPtr(ptrInBitmap.ToInt64() + strideStep);
#endif
                var offset = y * width;
                // 4 bytes without alpha channel value
                for (int x = 0; x < maxIndex; x += 4)
                {
                    var bit = (byte)(buffer[x] == 255 ? 0 : 1);

                    bits[offset] = bit;
                    offset++;
                }
            }
        }

        /// calculates the luminance values for 32-bit encoded bitmaps with alpha channel
        private static void FetchBitValues32BitWithAlpha(Bitmap bitmap, BitmapData data, byte[] bits)
        {
            var height = data.Height;
            var width = data.Width;
            var stride = Math.Abs(data.Stride);
            var pixelWidth = stride / width;
            var strideStep = data.Stride;
            var buffer = new byte[stride];
            var ptrInBitmap = data.Scan0;
            var maxIndex = 4 * width;

            if (pixelWidth != 4)
#if !WindowsCE
                throw new InvalidOperationException("Unsupported pixel format: " + bitmap.PixelFormat);
#else
                throw new InvalidOperationException("Unsupported pixel format");
#endif

            for (int y = 0; y < height; y++)
            {
                // copy a scanline not the whole bitmap because of memory usage
                Marshal.Copy(ptrInBitmap, buffer, 0, stride);
#if NET40 || NET45 || NET46 || NET47 || NET48
                ptrInBitmap = IntPtr.Add(ptrInBitmap, strideStep);
#else
                ptrInBitmap = new IntPtr(ptrInBitmap.ToInt64() + strideStep);
#endif
                var offset = y * width;
                // with alpha channel; some barcodes are completely black if you
                // only look at the r, g and b channel but the alpha channel controls
                // the view
                for (int x = 0; x < maxIndex; x += 4)
                {
                    var bit = (byte)(buffer[x] == 255 ? 0 : 1);

                    // calculating the resulting luminance based upon a white background
                    // var alpha = buffer[x * pixelWidth + 3] / 255.0;
                    // luminance = (byte)(luminance * alpha + 255 * (1 - alpha));
                    var alpha = buffer[x + 3];
                    bit = (byte)(((bit * alpha) >> 8) + (255 * (255 - alpha) >> 8) + 1);
                    bits[offset] = bit;
                    offset++;
                }
            }
        }

        /// calculates the luminance values for 32-bit CMYK encoded bitmaps (k is ignored at the momen)
        private static void FetchBitValues32BitCmyk(Bitmap bitmap, BitmapData data, byte[] luminances)
        {
            var height = data.Height;
            var width = data.Width;
            var stride = Math.Abs(data.Stride);
            var pixelWidth = stride / width;
            var strideStep = data.Stride;
            var buffer = new byte[stride];
            var ptrInBitmap = data.Scan0;
            var maxIndex = 4 * width;

            if (pixelWidth != 4)
#if !WindowsCE
                throw new InvalidOperationException("Unsupported pixel format: " + bitmap.PixelFormat);
#else
                throw new InvalidOperationException("Unsupported pixel format");
#endif

            for (int y = 0; y < height; y++)
            {
                // copy a scanline not the whole bitmap because of memory usage
                Marshal.Copy(ptrInBitmap, buffer, 0, stride);
#if NET40 || NET45 || NET46 || NET47 || NET48
                ptrInBitmap = IntPtr.Add(ptrInBitmap, strideStep);
#else
                ptrInBitmap = new IntPtr(ptrInBitmap.ToInt64() + strideStep);
#endif
                var offset = y * width;
                for (int x = 0; x < maxIndex; x += 4)
                {
                    var bit = (byte)(buffer[x] == 255 ? 0 : 1);
                    // Ignore value of k at the moment
                    luminances[offset] = bit;
                    offset++;
                }
            }
        }
    }
}
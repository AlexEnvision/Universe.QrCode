﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace Universe.Image
{
    public static class Binarization
    {
        // Количество уровней интенсивности у изображения.
        // Стандартно для серого изображения -- это 256. От 0 до 255.
        const int IntensityLayerNumber = 256;

        // function is used to compute the q values in the equation
        private static float Px(int init, int end, int[] hist)
        {
            int sum = 0;
            int i;
            for (i = init; i <= end; i++)
                sum += hist[i];

            return sum;
        }

        // function is used to compute the mean values in the equation (mu)
        private static float Mx(int init, int end, int[] hist)
        {
            int sum = 0;
            int i;
            for (i = init; i <= end; i++)
                sum += i * hist[i];

            return sum;
        }

        // finds the maximum element in a vector
        private static int FindMax(float[] vec, int n)
        {
            float maxVec = 0;
            int idx = 0;
            int i;

            for (i = 1; i < n - 1; i++)
            {
                if (vec[i] > maxVec)
                {
                    maxVec = vec[i];
                    idx = i;
                }
            }
            return idx;
        }

        // simply computes the image histogram
        private static unsafe void GetHistogram(byte* p, int w, int h, int ws, int[] hist)
        {
            hist.Initialize();
            for (int i = 0; i < h; i++)
            {
                for (int j = 0; j < w * 3; j += 3)
                {
                    int index = i * ws + j;
                    hist[p[index]]++;
                }
            }
        }

        /// <summary>
        /// find otsu threshold
        /// Первая версия - устаревшая
        /// </summary>
        /// <param name="bmp"></param>
        /// <returns></returns>
        [Obsolete]
        public static int GetOtsuThresholdv1(Bitmap bmp)
        {
            Convert2GrayScaleFast(bmp);
            float[] vet = new float[256];
            int[] hist = new int[256];
            vet.Initialize();

            float p1, p2, p12;
            int k;

            BitmapData bmData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height),
            ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
            unsafe
            {
                //byte* p = (byte*)(void*)bmData.Scan0.ToPointer();
                byte* p = (byte*)bmData.Scan0.ToPointer();

                GetHistogram(p, bmp.Width, bmp.Height, bmData.Stride, hist);

                // loop through all possible t values and maximize between class variance
                for (k = 1; k != 255; k++)
                {
                    p1 = Px(0, k, hist);
                    p2 = Px(k + 1, 255, hist);
                    p12 = p1 * p2;
                    //if (p12 == 0)
                    if (Math.Abs(p12) < 0.001)
                        p12 = 1;
                    float diff = (Mx(0, k, hist) * p2) - (Mx(k + 1, 255, hist) * p1);
                    vet[k] = diff * diff / p12;
                    //vet[k] = (float)Math.Pow((Mx(0, k, hist) * p2) - (Mx(k + 1, 255, hist) * p1), 2) / p12;
                }
            }
            bmp.UnlockBits(bmData);

            var t = (byte)FindMax(vet, 256);

            return t;
        }

        /// <summary>
        /// Возвращает гистограмму по интенсивности изображения от 0 до 255 включительно
        /// </summary>
        /// <param name="p"></param>
        /// <param name="w"></param>
        /// <param name="h"></param>
        /// <param name="ws"></param>
        /// <param name="hist"></param>
        private static unsafe void CalculateHist(byte* p, int w, int h, int ws, int[] hist)
        {
            // Иницилизируем гистограмму нулями
            hist.Initialize();

            // Вычисляем гистограмму
            for (var i = 0; i < h; i++)
            {
                for (var j = 0; j < w * 3; j += 3)
                {
                    var index = i * ws + j;
                    hist[p[index]]++;
                }
            }
        }

        /// <summary>
        /// Посчитать сумму всех интенсивностей
        /// </summary>
        /// <param name="image"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        private static unsafe long CalculateIntensitySum(byte* image, int size)
        {
            long sum = 0;
            for (int i = 0; i < size; ++i)
            {
                sum += image[i];
            }

            return sum;
        }

        /// <summary>
        /// Функция возвращает порог бинаризации для полутонового изображения image с общим числом пикселей size.
        /// const IMAGE *image содержит интенсивность изображения от 0 до 255 включительно.
        /// size -- количество пикселей в image.
        /// </summary>
        /// <param name="image">Указатель на изображение</param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="stride"></param>
        /// <returns></returns>
        public static unsafe int OtsuThreshold(byte* image, int width, int height, int stride)
        {
            var size = width * height;

            int bestThresh = 0;
            double bestSigma = 0.0;

            int firstClassPixelCount = 0;
            int firstClassIntensitySum = 0;

            var hist = new int[IntensityLayerNumber];
            CalculateHist(image, width, height, stride, hist);

            // Необходимы для быстрого пересчета разности дисперсий между классами
            int allPixelCount = size;
            long allIntensitySum = CalculateIntensitySum(image, size);

            // Перебираем границу между классами
            // thresh < INTENSITY_LAYER_NUMBER - 1, т.к. при 255 в ноль уходит знаменатель внутри for
            for (int thresh = 0; thresh < IntensityLayerNumber - 1; ++thresh)
            {
                firstClassPixelCount += hist[thresh];
                firstClassIntensitySum += thresh * hist[thresh];

                double firstClassProb = firstClassPixelCount / (double)allPixelCount;
                double secondClassProb = 1.0 - firstClassProb;

                double firstClassMean = firstClassIntensitySum / (double)firstClassPixelCount;
                double secondClassMean = (allIntensitySum - firstClassIntensitySum)
                                            / (double)(allPixelCount - firstClassPixelCount);

                double meanDelta = firstClassMean - secondClassMean;

                double sigma = firstClassProb * secondClassProb * meanDelta * meanDelta;

                if (sigma > bestSigma)
                {
                    bestSigma = sigma;
                    bestThresh = thresh;
                }
            }

            return bestThresh;
        }

        /// <summary>
        /// Извлечение порога по методу Оцу
        /// Вторая версия. Действующая
        /// </summary>
        /// <param name="bmp"></param>
        /// <returns></returns>
        public static int GetOtsuThreshold(Bitmap bmp)
        {
            Convert2GrayScaleFast(bmp);

            var bmData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height),
                ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);

            int threshold;

            unsafe
            {
                var image = (byte*)bmData.Scan0.ToPointer();
                threshold = OtsuThreshold(image, bmp.Width, bmp.Height, bmData.Stride);
            }
            bmp.UnlockBits(bmData);

            return threshold;
        }

        // Преобразование изображения в черно-белое прямо в ОЗУ 
        private static void Convert2GrayScaleFast(Bitmap bmp)
        {
            // Задаём формат Пикселя.
            PixelFormat pxf = PixelFormat.Format24bppRgb;

            // Получаем данные картинки.
            Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
            //Блокируем набор данных изображения в памяти
            BitmapData bmpData = bmp.LockBits(rect, ImageLockMode.ReadWrite, pxf);

            // Получаем адрес первой линии.
            IntPtr ptr = bmpData.Scan0;

            // Задаём массив из Byte и помещаем в него надор данных.
            // int numBytes = bmp.Width * bmp.Height * 3; 
            //На 3 умножаем - поскольку RGB цвет кодируется 3-мя байтами
            //Либо используем вместо Width - Stride
            int numBytes = bmpData.Stride * bmp.Height;
            //int widthBytes = bmpData.Stride;
            byte[] rgbValues = new byte[numBytes];

            // Копируем значения в массив.
            Marshal.Copy(ptr, rgbValues, 0, numBytes);

            // Перебираем пикселы по 3 байта на каждый и меняем значения
            for (int counter = 0; counter < rgbValues.Length + 10; counter += 3)
            {
                if (counter + 1 < rgbValues.Length - 1 || counter + 2 < rgbValues.Length - 1)
                {
                    int value = rgbValues[counter] + rgbValues[counter + 1] + rgbValues[counter + 2];

                    var colorB = Convert.ToByte(value / 3);

                    rgbValues[counter] = colorB;
                    rgbValues[counter + 1] = colorB;
                    rgbValues[counter + 2] = colorB;
                }
            }
            // Копируем набор данных обратно в изображение
            Marshal.Copy(rgbValues, 0, ptr, numBytes);

            // Разблокируем набор данных изображения в памяти.
            bmp.UnlockBits(bmpData);
        }

        // simple routine for thresholdin
        public static Bitmap Threshold(Bitmap bmp, int thresh)
        {
            BitmapData bmData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height),
            ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            unsafe
            {
                //byte* p = (byte*)(void*)bmData.Scan0.ToPointer();
                byte* p = (byte*)bmData.Scan0.ToPointer();
                int h = bmp.Height;
                int w = bmp.Width;
                int ws = bmData.Stride;

                for (int i = 0; i < h; i++)
                {
                    byte* row = &p[i * ws];
                    for (int j = 0; j < w * 3; j += 3)
                    {
                        row[j] = (byte)((row[j] > (byte)thresh) ? 255 : 0);
                        row[j + 1] = (byte)((row[j + 1] > (byte)thresh) ? 255 : 0);
                        row[j + 2] = (byte)((row[j + 2] > (byte)thresh) ? 255 : 0);
                    }
                }
            }
            bmp.UnlockBits(bmData);

            return bmp;
        }

        /// <summary>
        /// Получает гистограмму изображения для одного канала. Используется для разработки функции 
        /// автоматической регулировки баланса белого.
        /// </summary>
        /// <param name="bmp"></param>
        /// <param name="oneChannel"></param>
        /// <returns></returns>
        public static int[] ImageHistogram(Bitmap bmp, bool oneChannel)
        {
            int[] hist = new int[256];
            BitmapData bmData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height),
            ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
            unsafe
            {
                //byte* p = (byte*)(void*)bmData.Scan0.ToPointer();
                byte* p = (byte*)bmData.Scan0.ToPointer();
                GetHistogram(p, bmp.Width, bmp.Height, bmData.Stride, hist);
            }
            bmp.UnlockBits(bmData);

            return hist;
        }

        /// <summary>
        /// Получает гистограмму изображения для каналов R, G, B. Используется для разработки функции 
        /// автоматической регулировки баланса белого.
        /// </summary>
        /// <param name="bmp"></param>
        /// <returns></returns>
        public static List<int[]> ImageHistogram(Bitmap bmp)
        {
            List<int[]> hist = new List<int[]> { new int[256], new int[256], new int[256] };
            
            BitmapData bmData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height),
            ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
            unsafe
            {
                //byte* p = (byte*)(void*)bmData.Scan0.ToPointer();
                //int ws = bmData.Stride;
                byte* p = (byte*)bmData.Scan0.ToPointer();

                hist[0].Initialize();
                hist[1].Initialize();
                hist[2].Initialize();
                for (int i = 0; i < bmp.Height; i++)
                {
                    for (int j = 0; j < bmp.Width * 3; j += 3)
                    {                        
                        int index = i * bmData.Stride + j;
                        int index1 = i * bmData.Stride + j + 1;
                        int index2 = i * bmData.Stride + j + 2;
                        hist[0][p[index]]++;
                        hist[1][p[index1]]++;
                        hist[2][p[index2]]++;
                    }
                }
            }
            bmp.UnlockBits(bmData);

            return hist;
        }

        /// <summary>
        /// Преобразование изображения в черно-белое прямо в ОЗУ 
        /// </summary>
        /// <param name="bmp"></param>
        public static void Convert2GrayScale(Bitmap bmp) //public static void MakeGray(Bitmap bmp) 
        {
            // Задаём формат Пикселя.
            var pxf = PixelFormat.Format24bppRgb;

            // Получаем данные картинки.
            var rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
            //Блокируем набор данных изображения в памяти
            var bmpData = bmp.LockBits(rect, ImageLockMode.ReadWrite, pxf);

            // Получаем адрес первой линии.
            var ptr = bmpData.Scan0;

            // Задаём массив из Byte и помещаем в него надор данных.
            // int numBytes = bmp.Width * bmp.Height * 3; 
            //На 3 умножаем - поскольку RGB цвет кодируется 3-мя байтами
            //Либо используем вместо Width - Stride
            var stride = bmpData.Stride;
            var numBytes = stride * bmp.Height;
            var rgbValues = new byte[numBytes];

            // Копируем значения в массив.
            Marshal.Copy(ptr, rgbValues, 0, numBytes);

            // Перебираем пикселы по 3 байта на каждый и меняем значения
            for (var counter = 0; counter < rgbValues.Length + 10; counter += 3)
                if (counter + 1 < rgbValues.Length - 1 || counter + 2 < rgbValues.Length - 1)
                {
                    var value = rgbValues[counter] + rgbValues[counter + 1] + rgbValues[counter + 2];

                    var colorB = Convert.ToByte(value / 3);

                    rgbValues[counter] = colorB;
                    rgbValues[counter + 1] = colorB;
                    rgbValues[counter + 2] = colorB;
                }
            // Копируем набор данных обратно в изображение
            Marshal.Copy(rgbValues, 0, ptr, numBytes);

            // Разблокируем набор данных изображения в памяти.
            bmp.UnlockBits(bmpData);
        }

        /// <summary>
        /// Пороговая бинаризация.
        /// Через маршаллинг
        /// </summary>
        /// <param name="bmp">Изображение</param>
        /// <param name="thresh">Порог</param>
        /// <returns></returns>
        public static Bitmap ApplyThreshold(Bitmap bmp, int thresh)
        {
            // Задаём формат Пикселя.
            var pxf = PixelFormat.Format24bppRgb;

            // Получаем данные картинки.
            var rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
            //Блокируем набор данных изображения в памяти
            var bmpData = bmp.LockBits(rect, ImageLockMode.ReadWrite, pxf);

            // Получаем адрес первой линии.
            var ptr = bmpData.Scan0;

            // Задаём массив из Byte и помещаем в него надор данных.
            // int numBytes = bmp.Width * bmp.Height * 3; 
            //На 3 умножаем - поскольку RGB цвет кодируется 3-мя байтами
            //Либо используем вместо Width - Stride
            var ws = bmpData.Stride;
            var numBytes = ws * bmp.Height;
            var rgbValues = new byte[numBytes];

            // Копируем значения в массив.
            Marshal.Copy(ptr, rgbValues, 0, numBytes);

            // Перебираем пикселы по 3 байта на каждый и меняем значения
            for (var counter = 0; counter < rgbValues.Length + 10; counter += 3)
            {
                if (counter + 1 >= rgbValues.Length - 1 && counter + 2 >= rgbValues.Length - 1)
                    continue;

                rgbValues[counter] = (byte)(rgbValues[counter] > (byte)thresh ? 255 : 0);
                rgbValues[counter + 1] = (byte)(rgbValues[counter + 1] > (byte)thresh ? 255 : 0);
                rgbValues[counter + 2] = (byte)(rgbValues[counter + 2] > (byte)thresh ? 255 : 0);
            }

            // Копируем набор данных обратно в изображение
            Marshal.Copy(rgbValues, 0, ptr, numBytes);

            // Разблокируем набор данных изображения в памяти.
            bmp.UnlockBits(bmpData);

            return bmp;
        }
    }
}

﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System.Drawing;
using System.Drawing.Imaging;

namespace Universe.Image.Imaging
{
    public class BitmapProcessor
    {
        private PixelFormat _pixelFormat { get; }

        public static BitmapProcessor Inst => Initialize();

        public static BitmapProcessor Initialize(PixelFormat pixelFormat = PixelFormat.Format32bppArgb) => new BitmapProcessor(pixelFormat);

        protected BitmapProcessor(PixelFormat pixelFormat)
        {
            _pixelFormat = pixelFormat;
        }

        /// <summary>
        /// Повышение резкости
        /// </summary>
        /// <param name="image">Изображение</param>
        /// <returns></returns>
        public Bitmap Sharpen(Bitmap image)
        {
            //матрица повышения резкости
            var kernel = new double[,]
                                 {{0, -1, 0},
                              {-1, 5, -1},
                              {0, -1, 0}};

            return Convolution(image, kernel);
        }

        public Bitmap Convolution(Bitmap img, double[,] matrix)
        {
            return ConvolitionSimple(img, matrix);
        }

        private Bitmap ConvolitionSimple(Bitmap img, double[,] matrix)
        {
            var w = matrix.GetLength(0);
            var h = matrix.GetLength(1);

            using (var wr = new ImageWrapper(img, _pixelFormat) { DefaultColor = Color.Silver })
            {
                foreach (var p in wr)
                {
                    double r = 0d, g = 0d, b = 0d;

                    for (int i = 0; i < w; i++)
                        for (int j = 0; j < h; j++)
                        {
                            var pixel = wr[p.X + i - 1, p.Y + j - 1];
                            r += matrix[j, i] * pixel.R;
                            g += matrix[j, i] * pixel.G;
                            b += matrix[j, i] * pixel.B;
                        }
                    wr.SetPixel(p, r, g, b);
                }
            }

            return img;
        }

        public Bitmap Inversion(Bitmap img)
        {
            using (var wr = new ImageWrapper(img, _pixelFormat) { DefaultColor = Color.Silver })
            {
                foreach (var p in wr)
                {
                    double r = 0d, g = 0d, b = 0d;

                    var pixel = wr[p.X,  p.Y];
                    r += 255 - pixel.R;
                    g += 255 - pixel.G;
                    b += 255 - pixel.B;
                    wr.SetPixel(p, r, g, b);
                }
            }

            return img;
        }

        /// <summary>
        /// Выделение контуров
        /// </summary>
        /// <param name="image">Изображение</param>
        /// <returns></returns>
        public Bitmap Contour(Bitmap image)
        {
            //dilation
            var kernel = new double[,]
                 {{0.1d, 0.1d, 0.1d},
                  {0.1d, 0.1d, 0.1d},
                  {0.1d, 0.1d, 0.1d}};

            var dilated = image.FullCopy();
            dilated = Convolution(dilated, kernel);

            //substraction
            using (var wr0 = new ImageWrapper(image, _pixelFormat, true))
            using (var wr1 = new ImageWrapper(dilated, _pixelFormat, true))
            {
                foreach (var p in wr0)
                    if (wr1[p].G > 0)
                        wr1[p] = wr0[p].G > 128 ? Color.Black : Color.White;
            }

            return dilated;
        }

        /// <summary>
        /// Выделение границ
        /// </summary>
        /// <param name="image">Изображение</param>
        /// <returns></returns>
        public Bitmap Edges(Bitmap image)
        {
            var kernel = new double[,]
                                {{0, -1, 0},
                              {-1, 4, -1},
                              {0, -1, 0}};

            return Convolution(image, kernel);
        }

        /// <summary>
        /// Размытие
        /// </summary>
        /// <param name="image">Изображение</param>
        /// <returns></returns>
        public Bitmap Blur(Bitmap image)
        {
            var kernel = new double[,]
                 {{0.11d, 0.11d, 0.11d},
                  {0.11d, 0.11d, 0.11d},
                  {0.11d, 0.11d, 0.11d}};

            return Convolution(image, kernel);
        }
    }
}
﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;
using Universe.QrCode.Common.Types;

namespace Universe.QrCode.Models
{
    /// <summary>
    ///     This class is the core bitmap class used by ZXing to represent 1 bit data. Reader objects
    ///     accept a BinaryBitmap and attempt to decode it.
    /// </summary>
    /// <author>dswitkin@google.com (Daniel Switkin)</author>
    public sealed class BinaryBitmap
    {
        //private readonly Binarizer binarizer;
        private BitMatrix matrix;

        public BinaryBitmap(BitMatrix matrix)
        {
            if (matrix == null)
                throw new ArgumentException("matrix must be non-null.");
            this.matrix = matrix;
        }

        /// <summary>
        ///     Converts a 2D array of luminance data to 1 bit. As above, assume this method is expensive
        ///     and do not call it repeatedly. This method is intended for decoding 2D barcodes and may or
        ///     may not apply sharpening. Therefore, a row from this matrix may not be identical to one
        ///     fetched using getBlackRow(), so don't mix and match between them.
        /// </summary>
        /// <returns> The 2D array of bits for the image (true means black).</returns>
        public BitMatrix BlackMatrix => matrix; //?? (matrix = binarizer.BlackMatrix);

        /// <summary>
        ///     Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        ///     A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            var blackMatrix = BlackMatrix;
            return blackMatrix?.ToString() ?? string.Empty;
        }
    }
}
﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;
using System.Text;

namespace Universe.QrCode.Common.Types
{
    /// <summary>
    ///     <p>
    ///         Представляет собой двумерную матрицу битов. В аргументах функций ниже и во всех общих
    ///         модуль, x - позиция столбца, а y - позиция строки. Порядок всегда x, y.
    ///         Начало координат находится в верхнем левом углу.
    ///     </p>
    ///     <p>
    ///         Внутренне биты представлены в 1-мерном массиве 32-битных целых чисел. Однако каждая строка начинается
    ///         с новым int. Это сделано намеренно, чтобы мы могли скопировать строку в BitArray очень
    ///         эффективно.
    ///     </p>
    ///     <p>
    ///         Порядок битов - строчный. Внутри каждого int сначала используются младшие биты,
    ///         означает, что они представляют более низкие значения x. Это совместимо с реализацией BitArray.
    ///     </p>
    /// </summary>
    /// <author>Sean Owen</author>
    /// <author>dswitkin@google.com (Daniel Switkin)</author>
    /// <author>Alex Envision</author>
    public sealed class BitMatrix
    {
        private readonly int[] _bits;

        /// <summary>
        ///     Создает пустую квадратную битовую матрицу <see cref="BitMatrix" />.
        /// </summary>
        /// <param name="dimension">Высота и ширина</param>
        public BitMatrix(int dimension)
            : this(dimension, dimension)
        {
        }

        /// <summary>
        ///     Создает пустую квадратную битовую матрицу  <see cref="BitMatrix" />.
        /// </summary>
        /// <param name="width">Ширина битовой матрицы</param>
        /// <param name="height">Высота битовой матрицы</param>
        public BitMatrix(int width, int height)
        {
            if (width < 1 || height < 1)
                throw new ArgumentException("Both dimensions must be greater than 0");
            Width = width;
            Height = height;
            RowSize = (width + 31) >> 5;
            _bits = new int[RowSize * height];
        }

        /// <summary>
        /// Создает квадратную битовую матрицу на основе битов изображения <see cref="BitMatrix" />.
        /// </summary>
        /// <param name="width">Ширина битовой матрицы</param>
        /// <param name="height">Высота битовой матрицы</param>
        /// <param name="rowSize">Размер строки</param>
        /// <param name="bits">Битовая матрица как массив</param>
        internal BitMatrix(int width, int height, int rowSize, int[] bits)
        {
            Width = width;
            Height = height;
            RowSize = rowSize;
            _bits = bits;
        }

        internal BitMatrix(int width, int height, int[] bits)
        {
            Width = width;
            Height = height;
            RowSize = (width + 31) >> 5;
            _bits = bits;
        }

        /// <returns>
        ///     Ширина матрицы
        /// </returns>
        public int Width { get; }

        /// <returns>
        ///     Высота матрицы
        /// </returns>
        public int Height { get; }

        /// <summary>
        ///     Эта функция предназначена для совместимости со старым кодом. Логично только называть, если матрица
        ///     квадратная и выбрасываю исключение, если это не так
        /// </summary>
        /// <returns>
        ///     Измерение строки/столбца этой матрицы
        /// </returns>
        public int Dimension
        {
            get
            {
                if (Width != Height)
                    throw new ArgumentException("Can't call Dimension on a non-square matrix");
                return Width;
            }
        }

        /// <returns>
        ///     Размер строки матрицы
        /// </returns>
        public int RowSize { get; }

        /// <summary>
        ///     <p>Получает запрошенный бит, где true означает черный цвет.</p>
        /// </summary>
        /// <param name="x">
        ///     Горизонтальный компонент (т.е. какой столбец)
        /// </param>
        /// <param name="y">
        ///     Вертикальный компонент (т.е. какая строка)
        /// </param>
        /// <returns>
        ///     Значение данного бита в матрице
        /// </returns>
        public bool this[int x, int y]
        {
            get
            {
                var offset = y * RowSize + (x >> 5);
                return ((int) ((uint) _bits[offset] >> (x & 0x1f)) & 1) != 0;
            }
            set
            {
                if (value)
                {
                    var offset = y * RowSize + (x >> 5);
                    _bits[offset] |= 1 << (x & 0x1f);
                }
                else
                {
                    var offset = y * RowSize + x / 32;
                    _bits[offset] &= ~(1 << (x & 0x1f));
                }
            }
        }

        /// <summary>
        ///     Интерпретирует двумерный массив логических значений как <see cref="BitMatrix" />, где "true" означает что бит "включен".
        /// </summary>
        /// <param name="image">Биты изображения в виде двухмерного массива высотой по строкам. Элементы - это массивы, представляющие строки</param>
        /// <returns><see cref="BitMatrix" /> Представление изображения</returns>
        public static BitMatrix Parse(bool[][] image)
        {
            var height = image.Length;
            var width = image[0].Length;
            var bits = new BitMatrix(width, height);
            for (var i = 0; i < height; i++)
            {
                var imageI = image[i];
                for (var j = 0; j < width; j++)
                    bits[j, i] = imageI[j];
            }
            return bits;
        }

        /// <summary>
        ///     Разбор строковое представления в битовую матрицу
        /// </summary>
        /// <param name="stringRepresentation"></param>
        /// <param name="setString"></param>
        /// <param name="unsetString"></param>
        /// <returns></returns>
        public static BitMatrix Parse(string stringRepresentation, string setString, string unsetString)
        {
            if (stringRepresentation == null)
                throw new ArgumentException();

            var bits = new bool[stringRepresentation.Length];
            var bitsPos = 0;
            var rowStartPos = 0;
            var rowLength = -1;
            var nRows = 0;
            var pos = 0;
            while (pos < stringRepresentation.Length)
                if (stringRepresentation.Substring(pos, 1).Equals("\n") ||
                    stringRepresentation.Substring(pos, 1).Equals("\r"))
                {
                    if (bitsPos > rowStartPos)
                    {
                        if (rowLength == -1)
                            rowLength = bitsPos - rowStartPos;
                        else if (bitsPos - rowStartPos != rowLength)
                            throw new ArgumentException("row lengths do not match");
                        rowStartPos = bitsPos;
                        nRows++;
                    }
                    pos++;
                }
                else if (stringRepresentation.Substring(pos, setString.Length).Equals(setString))
                {
                    pos += setString.Length;
                    bits[bitsPos] = true;
                    bitsPos++;
                }
                else if (stringRepresentation.Substring(pos, unsetString.Length).Equals(unsetString))
                {
                    pos += unsetString.Length;
                    bits[bitsPos] = false;
                    bitsPos++;
                }
                else
                {
                    throw new ArgumentException("illegal character encountered: " +
                                                stringRepresentation.Substring(pos));
                }

            // нет EOL в конце?
            if (bitsPos > rowStartPos)
            {
                if (rowLength == -1)
                    rowLength = bitsPos - rowStartPos;
                //else if (bitsPos - rowStartPos != rowLength)
                //    throw new ArgumentException("row lengths do not match");
                nRows++;
            }

            var matrix = new BitMatrix(rowLength, nRows);
            for (var i = 0; i < bitsPos; i++)
                if (bits[i])
                    matrix[i % rowLength, i / rowLength] = true;
            return matrix;
        }

        /// <summary>
        ///     <p>Отражает данный бит.</p>
        /// </summary>
        /// <param name="x">
        ///     Горизонтальный компонент (т.е. какой столбец)
        /// </param>
        /// <param name="y">
        ///     Вертикальный компонент (т.е. какая строка)
        /// </param>
        public void Flip(int x, int y)
        {
            var offset = y * RowSize + (x >> 5);
            _bits[offset] ^= 1 << (x & 0x1f);
        }

        /// <summary>
        ///     Отражает все биты, если shouldBeFlipped истинно для координат
        /// </summary>
        /// <param name="shouldBeFlipped">Должен возвращать true, если бит в данной координате должен быть перевернут</param>
        public void FlipWhen(Func<int, int, bool> shouldBeFlipped)
        {
            for (var y = 0; y < Height; y++)
            for (var x = 0; x < Width; x++)
                if (shouldBeFlipped(y, x))
                {
                    var offset = y * RowSize + (x >> 5);
                    _bits[offset] ^= 1 << (x & 0x1f);
                }
        }

        /// <summary>
        ///     Исключающее ИЛИ (XOR):  Отражает бит в этом объекте класса <see cref="BitMatrix" />,
        ///     если установлен соответствующий бит маски.
        /// </summary>
        /// <param name="mask">The mask.</param>
        public void XOR(BitMatrix mask)
        {
            if (Width != mask.Width || Height != mask.Height || RowSize != mask.RowSize)
                throw new ArgumentException("input matrix dimensions do not match");
            var rowArray = new BitArray(Width);
            for (var y = 0; y < Height; y++)
            {
                var offset = y * RowSize;
                var row = mask.GetRow(y, rowArray).Array;
                for (var x = 0; x < RowSize; x++)
                    _bits[offset + x] ^= row[x];
            }
        }

        /// <summary> Очищает все биты (устанавливается в false).</summary>
        public void Clear()
        {
            var max = _bits.Length;
            for (var i = 0; i < max; i++)
                _bits[i] = 0;
        }

        /// <summary>
        ///     <p>Устанавливает для квадратной области битовой матрицы значение true.</p>
        /// </summary>
        /// <param name="left">
        ///     Горизонтальное положение, с которого нужно начинать (включительно)
        /// </param>
        /// <param name="top">
        ///     Вертикальное положение для начала (включительно)
        /// </param>
        /// <param name="width">
        ///     Ширина области
        /// </param>
        /// <param name="height">
        ///     Высота региона
        /// </param>
        public void SetRegion(int left, int top, int width, int height)
        {
            if (top < 0 || left < 0)
                throw new ArgumentException("Left and top must be nonnegative");
            if (height < 1 || width < 1)
                throw new ArgumentException("Height and width must be at least 1");
            var right = left + width;
            var bottom = top + height;
            if (bottom > Height || right > Width)
                throw new ArgumentException("The region must fit inside the matrix");
            for (var y = top; y < bottom; y++)
            {
                var offset = y * RowSize;
                for (var x = left; x < right; x++)
                    _bits[offset + (x >> 5)] |= 1 << (x & 0x1f);
            }
        }

        /// <summary>
        ///     Быстрый метод получения одной строки данных из матрицы в виде BitArray.
        /// </summary>
        /// <param name="y">
        ///     Строка для получения
        /// </param>
        /// <param name="row">
        ///     Необязательный BitArray, выделяемый вызывающей стороной, будет выделен, если значение null или слишком мало
        /// </param>
        /// <returns>
        ///     Результирующий BitArray - эта ссылка всегда должна использоваться, даже при передаче вашей собственной строки
        /// </returns>
        public BitArray GetRow(int y, BitArray row)
        {
            if (row == null || row.Size < Width)
                row = new BitArray(Width);
            else
                row.Clear();
            var offset = y * RowSize;
            for (var x = 0; x < RowSize; x++)
                row.SetBulk(x << 5, _bits[offset + x]);
            return row;
        }

        /// <summary>
        ///     Устанавливает строку.
        /// </summary>
        /// <param name="y">Ряд для установки</param>
        /// <param name="row"><see cref="BitMatrix" /> для копирования</param>
        public void SetRow(int y, BitArray row)
        {
            Array.Copy(row.Array, 0, _bits, y * RowSize, RowSize);
        }

        /// <summary>
        ///    Изменяет этот <see cref="BitMatrix" />
        ///    для представления того же самого, но повернутого на 180 градусов
        /// </summary>
        public void Rotate180()
        {
            var topRow = new BitArray(Width);
            var bottomRow = new BitArray(Width);
            var maxHeight = (Height + 1) / 2;
            for (var i = 0; i < maxHeight; i++)
            {
                topRow = GetRow(i, topRow);
                var bottomRowIndex = Height - 1 - i;
                bottomRow = GetRow(bottomRowIndex, bottomRow);
                topRow.Reverse();
                bottomRow.Reverse();
                SetRow(i, bottomRow);
                SetRow(bottomRowIndex, topRow);
            }
        }

        /// <summary>
        ///     Это полезно при обнаружении окружающего прямоугольника «чистого» штрих-кода.
        /// </summary>
        /// <returns>{left, top, width, height}, охватывающий прямоугольник из всех 1 битов, или ноль, если он весь белый</returns>
        public int[] GetEnclosingRectangle()
        {
            var left = Width;
            var top = Height;
            var right = -1;
            var bottom = -1;

            for (var y = 0; y < Height; y++)
            for (var x32 = 0; x32 < RowSize; x32++)
            {
                var theBits = _bits[y * RowSize + x32];
                if (theBits != 0)
                {
                    if (y < top)
                        top = y;
                    if (y > bottom)
                        bottom = y;
                    if (x32 * 32 < left)
                    {
                        var bit = 0;
                        while (theBits << (31 - bit) == 0)
                            bit++;
                        if (x32 * 32 + bit < left)
                            left = x32 * 32 + bit;
                    }
                    if (x32 * 32 + 31 > right)
                    {
                        var bit = 31;
                        while ((int) ((uint) theBits >> bit) == 0) // (theBits >>> bit)
                            bit--;
                        if (x32 * 32 + bit > right)
                            right = x32 * 32 + bit;
                    }
                }
            }

            if (right < left || bottom < top)
                return null;

            return new[] {left, top, right - left + 1, bottom - top + 1};
        }

        /// <summary>
        ///     Это полезно при обнаружении угла «чистого» штрих-кода.
        /// </summary>
        /// <returns>{x, y} координата одного левого верхнего бита или ноль, если он весь белый</returns>
        public int[] GetTopLeftOnBit()
        {
            var bitsOffset = 0;
            while (bitsOffset < _bits.Length && _bits[bitsOffset] == 0)
                bitsOffset++;
            if (bitsOffset == _bits.Length)
                return null;
            var y = bitsOffset / RowSize;
            var x = (bitsOffset % RowSize) << 5;

            var theBits = _bits[bitsOffset];
            var bit = 0;
            while (theBits << (31 - bit) == 0)
                bit++;
            x += bit;
            return new[] {x, y};
        }

        /// <summary>
        ///     Нижний правый
        /// </summary>
        /// <returns></returns>
        public int[] GetBottomRightOnBit()
        {
            var bitsOffset = _bits.Length - 1;
            while (bitsOffset >= 0 && _bits[bitsOffset] == 0)
                bitsOffset--;
            if (bitsOffset < 0)
                return null;

            var y = bitsOffset / RowSize;
            var x = (bitsOffset % RowSize) << 5;

            var theBits = _bits[bitsOffset];
            var bit = 31;

            while ((int) ((uint) theBits >> bit) == 0) // (theBits >>> bit)
                bit--;
            x += bit;

            return new[] {x, y};
        }

        /// <summary>
        ///     Определяет, равен ли указанный <see cref="System.Object" /> этому экземпляру.
        /// </summary>
        /// <param name="obj"><see cref="System.Object" /> для сравнения с этим экземпляром.</param>
        /// <returns>
        ///     <c>true</c> если указанный <see cref="System.Object" /> равно этому экземпляру; иначе, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (!(obj is BitMatrix))
                return false;
            var other = (BitMatrix) obj;
            if (Width != other.Width || Height != other.Height ||
                RowSize != other.RowSize || _bits.Length != other._bits.Length)
                return false;
            for (var i = 0; i < _bits.Length; i++)
                if (_bits[i] != other._bits[i])
                    return false;
            return true;
        }

        /// <summary>
        ///     Возвращает хэш-код для этого экземпляра.
        /// </summary>
        /// <returns>
        ///     Хэш-код для этого экземпляра, подходящий для использования в алгоритмах хеширования и структурах данных, таких как хеш-таблица.
        /// </returns>
        public override int GetHashCode()
        {
            var hash = Width;
            hash = 31 * hash + Width;
            hash = 31 * hash + Height;
            hash = 31 * hash + RowSize;
            foreach (var bit in _bits)
                hash = 31 * hash + bit.GetHashCode();
            return hash;
        }

        /// <summary>
        ///     Возвращает <see cref="System.String" /> представляющий этот экземпляр.
        /// </summary>
        /// <returns>
        ///    <see cref="System.String" />, который представляет этот экземпляр.
        /// </returns>
        public override string ToString()
        {
#if WindowsCE
         return ToString("X ", "  ", "\r\n");
#else
            return ToString("X ", "  ", Environment.NewLine);
#endif
        }

        /// <summary>
        ///     Возвращает <see cref="System.String" /> представляющий этот экземпляр.
        /// </summary>
        /// <param name="setString">Строковое значение, которое соответсвует значению 1</param>
        /// <param name="unsetString">Строковое значение, которое соответсвует значению 0</param>
        /// <returns>
        ///     <see cref="System.String" />, который представляет этот экземпляр.
        /// </returns>
        public string ToString(string setString, string unsetString)
        {
#if WindowsCE
         return buildToString(setString, unsetString, "\r\n");
#else
            return BuildToString(setString, unsetString, Environment.NewLine);
#endif
        }

        /// <summary>
        ///     Возвращает <see cref="System.String" /> представляющий этот экземпляр.
        /// </summary>
        /// <param name="setString">Строковое значение, которое соответсвует значению 1</param>
        /// <param name="unsetString">Строковое значение, которое соответсвует значению 0</param>
        /// <param name="lineSeparator">Разделитель</param>
        /// <returns>
        ///     <see cref="System.String" /> представляющий этот экземпляр.
        /// </returns>
        public string ToString(string setString, string unsetString, string lineSeparator)
        {
            return BuildToString(setString, unsetString, lineSeparator);
        }

        private string BuildToString(string setString, string unsetString, string lineSeparator)
        {
            var result = new StringBuilder(Height * (Width + 1));
            for (var y = 0; y < Height; y++)
            {
                for (var x = 0; x < Width; x++)
                    result.Append(this[x, y] ? setString : unsetString);
                result.Append(lineSeparator);
            }
            return result.ToString();
        }

        /// <summary>
        ///     Клонирует этот экземпляр.
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            return new BitMatrix(Width, Height, RowSize, (int[]) _bits.Clone());
        }
    }
}
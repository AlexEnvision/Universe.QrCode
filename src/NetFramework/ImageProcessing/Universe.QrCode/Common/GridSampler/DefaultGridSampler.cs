﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;
using Universe.QrCode.Common.Types;

namespace Universe.QrCode.Common.GridSampler
{
    /// <author>  
    ///     Sean Owen
    /// </author>
    /// <author>
    ///     Alex Envision
    /// </author>
    /// <author>
    ///     www.Redivivus.in (suraj.supekar@redivivus.in) - Ported from ZXING Java Source
    /// </author>
    public sealed class DefaultGridSampler : GridSampler
    {
        /// <inheritdoc />
        /// <summary>
        /// </summary>
        /// <param name="image"></param>
        /// <param name="dimensionX"></param>
        /// <param name="dimensionY"></param>
        /// <param name="p1ToX"></param>
        /// <param name="p1ToY"></param>
        /// <param name="p2ToX"></param>
        /// <param name="p2ToY"></param>
        /// <param name="p3ToX"></param>
        /// <param name="p3ToY"></param>
        /// <param name="p4ToX"></param>
        /// <param name="p4ToY"></param>
        /// <param name="p1FromX"></param>
        /// <param name="p1FromY"></param>
        /// <param name="p2FromX"></param>
        /// <param name="p2FromY"></param>
        /// <param name="p3FromX"></param>
        /// <param name="p3FromY"></param>
        /// <param name="p4FromX"></param>
        /// <param name="p4FromY"></param>
        /// <returns></returns>
        public override BitMatrix SampleGrid(BitMatrix image, int dimensionX, int dimensionY, float p1ToX, float p1ToY,
            float p2ToX, float p2ToY, float p3ToX, float p3ToY, float p4ToX, float p4ToY, float p1FromX, float p1FromY,
            float p2FromX, float p2FromY, float p3FromX, float p3FromY, float p4FromX, float p4FromY)
        {
            var transform = PerspectiveTransform.QuadrilateralToQuadrilateral(
                p1ToX, p1ToY, p2ToX, p2ToY, p3ToX, p3ToY, p4ToX, p4ToY,
                p1FromX, p1FromY, p2FromX, p2FromY, p3FromX, p3FromY, p4FromX, p4FromY);
            return SampleGrid(image, dimensionX, dimensionY, transform);
        }

        /// <summary>
        /// </summary>
        /// <param name="image"></param>
        /// <param name="dimensionX"></param>
        /// <param name="dimensionY"></param>
        /// <param name="transform"></param>
        /// <returns></returns>
        public override BitMatrix SampleGrid(BitMatrix image, int dimensionX, int dimensionY,
            PerspectiveTransform transform)
        {
            if (dimensionX <= 0 || dimensionY <= 0)
                return null;
            var bits = new BitMatrix(dimensionX, dimensionY);
            var points = new float[dimensionX << 1];
            for (var y = 0; y < dimensionY; y++)
            {
                var max = points.Length;
                //UPGRADE_WARNING: Data types in Visual C# might be different.  Verify the accuracy of narrowing conversions. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1042'"
                var iValue = y + 0.5f;
                for (var x = 0; x < max; x += 2)
                {
                    //UPGRADE_WARNING: Data types in Visual C# might be different.  Verify the accuracy of narrowing conversions. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1042'"
                    points[x] = (x >> 1) + 0.5f;
                    points[x + 1] = iValue;
                }
                transform.TransformPoints(points);
                // Quick check to see if points transformed to something inside the image;
                // sufficient to check the endpoints
                if (!CheckAndNudgePoints(image, points))
                    return null;
                try
                {
                    var imageWidth = image.Width;
                    var imageHeight = image.Height;

                    for (var x = 0; x < max; x += 2)
                    {
                        var imagex = (int) points[x];
                        var imagey = (int) points[x + 1];

                        if (imagex < 0 || imagex >= imageWidth || imagey < 0 || imagey >= imageHeight)
                            return null;

                        bits[x >> 1, y] = image[imagex, imagey];
                    }
                }
                catch (IndexOutOfRangeException)
                {
                    // версия java:
                    //
                    // Это кажется неправильным, но иногда, если шаблоны поиска неверно определены, в результате
                    // преобразование становится "скрученным" таким образом, что прямая линия точек сопоставляется с набором точек
                    // чьи конечные точки находятся в границах, а другие - нет. Вероятно, есть какие-то математические
                    // способ узнать это о преобразовании, которого я еще не знаю.
                    // Это приводит к некрасивому исключению времени выполнения, несмотря на наши умные проверки выше - не может быть
                    // который. Мы могли бы проверить координаты каждой точки, но это похоже на дублирование. Мы соглашаемся на
                    // перехват и упаковка ArrayIndexOutOfBoundsException.
                    return null;
                }
            }
            return bits;
        }
    }
}
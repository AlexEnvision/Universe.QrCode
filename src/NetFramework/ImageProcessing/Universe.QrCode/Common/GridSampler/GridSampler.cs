﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;
using Universe.QrCode.Common.Types;

namespace Universe.QrCode.Common.GridSampler
{
    /// <summary>
    ///     Реализации этого класса могут, учитывая расположение шаблонов поиска для QR-кода в
    ///     изображение, выберите правильные точки на изображении, чтобы восстановить QR-код с учетом
    ///     искажение перспективы. Это абстракция, поскольку это относительно дорого и должно быть разрешено.
    ///     чтобы воспользоваться преимуществами оптимизированных для платформы реализаций, таких как Sun Java Advanced
    ///     библиотека изображений, но которая может быть недоступна в других средах, таких как J2ME и т. Д.
    ///     наоборот.
    ///     Используемой реализацией можно управлять, вызывая {@link #setGridSampler (GridSampler)}
    ///     с экземпляром класса, реализующего этот интерфейс.
    /// </summary>
    /// <author> Sean Owen</author>
    /// <author> Alex Envision</author>
    public abstract class GridSampler
    {
        /// <returns>
        ///     the current implementation of <see cref="GridSampler" />
        /// </returns>
        public static GridSampler Instance { get; private set; } = new DefaultGridSampler();

        /// <summary>
        ///     Устанавливает реализацию <see cref = "GridSampler" />, используемую библиотекой. Один глобальный
        ///     экземпляр сохраняется, что может показаться проблематичным. Но реализация при условии
        ///     должно подходить для всей платформы и всех видов использования этой библиотеки.
        ///     за все время существования JVM. Например, действие Android может быть заменено
        ///     реализация, использующая преимущества собственных библиотек платформы.
        /// </summary>
        /// <param name="newGridSampler">The platform-specific object to install.</param>
        public static void SetGridSampler(GridSampler newGridSampler)
        {
            if (newGridSampler == null)
                throw new ArgumentException();
            Instance = newGridSampler;
        }

        /// <summary>
        ///     <p>
        ///         Образец изображения для квадратной матрицы бит заданного размера. Это используется для извлечения
        ///         черно-белые модули двухмерного штрих-кода, такие как QR-код на изображении. Потому что этот штрих-код
        ///         может быть повернут или искажен в перспективе, вызывающий предоставляет четыре точки в исходном изображении
        ///         которые определяют известные точки в штрих-коде, так что изображение может быть отобрано соответствующим образом.
        ///     </p>
        ///     <p>
        ///         Последние восемь параметров "от" - это четыре пары координат X / Y местоположений точек в
        ///         изображение, определяющее некоторые важные точки изображения, которое будет использоваться в качестве образца. Например,
        ///         это может быть расположение шаблона поиска в QR-коде.
        ///     </p>
        ///     <p>
        ///         Первые восемь параметров "до" - это четыре пары координат X / Y, измеренные в пункте назначения.
        ///         <see cref = "BitMatrix" /> из верхнего левого угла, где известные точки на изображении заданы как "от"
        ///         параметры отображаются в.
        ///     </p>
        ///     <p>Эти 16 параметров определяют преобразование, необходимое для выборки изображения.</p>
        /// </summary>
        /// <param name="image">image to sample</param>
        /// <param name="dimensionX">The dimension X.</param>
        /// <param name="dimensionY">The dimension Y.</param>
        /// <param name="p1ToX">The p1 preimage X.</param>
        /// <param name="p1ToY">The p1 preimage  Y.</param>
        /// <param name="p2ToX">The p2 preimage  X.</param>
        /// <param name="p2ToY">The p2 preimage  Y.</param>
        /// <param name="p3ToX">The p3 preimage  X.</param>
        /// <param name="p3ToY">The p3 preimage  Y.</param>
        /// <param name="p4ToX">The p4 preimage  X.</param>
        /// <param name="p4ToY">The p4 preimage  Y.</param>
        /// <param name="p1FromX">The p1 image X.</param>
        /// <param name="p1FromY">The p1 image Y.</param>
        /// <param name="p2FromX">The p2 image X.</param>
        /// <param name="p2FromY">The p2 image Y.</param>
        /// <param name="p3FromX">The p3 image X.</param>
        /// <param name="p3FromY">The p3 image Y.</param>
        /// <param name="p4FromX">The p4 image X.</param>
        /// <param name="p4FromY">The p4 image Y.</param>
        /// <returns>
        ///     <see cref="BitMatrix" /> representing a grid of points sampled from the image within a region
        ///     defined by the "from" parameters
        /// </returns>
        /// <throws>  ReaderException if image can't be sampled, for example, if the transformation defined </throws>
        public abstract BitMatrix SampleGrid(BitMatrix image, int dimensionX, int dimensionY, float p1ToX, float p1ToY,
            float p2ToX, float p2ToY, float p3ToX, float p3ToY, float p4ToX, float p4ToY, float p1FromX, float p1FromY,
            float p2FromX, float p2FromY, float p3FromX, float p3FromY, float p4FromX, float p4FromY);

        /// <summary>
        /// </summary>
        /// <param name="image"></param>
        /// <param name="dimensionX"></param>
        /// <param name="dimensionY"></param>
        /// <param name="transform"></param>
        /// <returns></returns>
        public virtual BitMatrix SampleGrid(BitMatrix image, int dimensionX, int dimensionY,
            PerspectiveTransform transform)
        {
            throw new NotSupportedException();
        }


        /// <summary>
        ///     <p>
        ///         Проверяет набор точек, которые были преобразованы в точки выборки на изображении, по сравнению с
        ///         размеры изображения, чтобы увидеть, находится ли точка даже внутри изображения.
        ///     </p>
        ///     <p>
        ///         Этот метод фактически "подтолкнет" конечные точки обратно к изображению, если обнаружится, что они
        ///         едва (менее 1 пикселя) от изображения. Это объясняет несовершенное обнаружение искателя.
        ///         шаблоны на изображении, где QR-код проходит до границы изображения.
        ///     </p>
        ///     <p>
        ///         Для эффективности метод будет проверять точки с любого конца строки, пока не будет найдена одна
        ///         быть внутри изображения. Поскольку предполагается, что набор точек является линейным, это действительно так.
        ///     </p>
        /// </summary>
        /// <param name="image">
        ///     image into which the points should map
        /// </param>
        /// <param name="points">
        ///     actual points in x1,y1,...,xn,yn form
        /// </param>
        protected internal static bool CheckAndNudgePoints(BitMatrix image, float[] points)
        {
            var width = image.Width;
            var height = image.Height;
            // Check and nudge points from start until we see some that are OK:
            var nudged = true;
            var maxOffset = points.Length - 1; // points.length must be even
            for (var offset = 0; offset < maxOffset && nudged; offset += 2)
            {
                var x = (int) points[offset];
                var y = (int) points[offset + 1];
                if (x < -1 || x > width || y < -1 || y > height)
                    return false;
                nudged = false;
                if (x == -1)
                {
                    points[offset] = 0.0f;
                    nudged = true;
                }
                else if (x == width)
                {
                    points[offset] = width - 1;
                    nudged = true;
                }
                if (y == -1)
                {
                    points[offset + 1] = 0.0f;
                    nudged = true;
                }
                else if (y == height)
                {
                    points[offset + 1] = height - 1;
                    nudged = true;
                }
            }
            // Check and nudge points from end:
            nudged = true;
            for (var offset = points.Length - 2; offset >= 0 && nudged; offset -= 2)
            {
                //UPGRADE_WARNING: Data types in Visual C# might be different.  Verify the accuracy of narrowing conversions. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1042'"
                var x = (int) points[offset];
                //UPGRADE_WARNING: Data types in Visual C# might be different.  Verify the accuracy of narrowing conversions. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1042'"
                var y = (int) points[offset + 1];
                if (x < -1 || x > width || y < -1 || y > height)
                    return false;
                nudged = false;
                if (x == -1)
                {
                    points[offset] = 0.0f;
                    nudged = true;
                }
                else if (x == width)
                {
                    points[offset] = width - 1;
                    nudged = true;
                }
                if (y == -1)
                {
                    points[offset + 1] = 0.0f;
                    nudged = true;
                }
                else if (y == height)
                {
                    points[offset + 1] = height - 1;
                    nudged = true;
                }
            }

            return true;
        }
    }
}
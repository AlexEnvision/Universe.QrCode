﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;
using System.Diagnostics.CodeAnalysis;

namespace Universe.QrCode.Common.Reedsolomon
{
    /// <summary>
    ///     <p>
    ///         Этот класс содержит служебные методы для выполнения математических операций над
    ///         Поля Галуа. Операции используют данный примитивный многочлен в вычислениях.
    ///     </p>
    ///     <p>
    ///         В этом пакете элементы GF представлены как <see cref="int"/>
    ///         для удобства и скорости (но за счет памяти).
    ///     </p>
    /// </summary>
    /// <author>Sean Owen</author>
    /// <author>Alex Envision</author>
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public sealed class GenericGF
    {
        /// <summary>
        ///     Aztec data 12
        /// </summary>
        public static GenericGF AZTEC_DATA_12 = new GenericGF(0x1069, 4096, 1); // x^12 + x^6 + x^5 + x^3 + 1

        /// <summary>
        ///     Aztec data 10
        /// </summary>
        public static GenericGF AZTEC_DATA_10 = new GenericGF(0x409, 1024, 1); // x^10 + x^3 + 1

        /// <summary>
        ///     Aztec data 6
        /// </summary>
        public static GenericGF AZTEC_DATA_6 = new GenericGF(0x43, 64, 1); // x^6 + x + 1

        /// <summary>
        ///     Aztec param
        /// </summary>
        public static GenericGF AZTEC_PARAM = new GenericGF(0x13, 16, 1); // x^4 + x + 1

        /// <summary>
        ///     QR Code
        /// </summary>
        public static GenericGF QR_CODE_FIELD_256 = new GenericGF(0x011D, 256, 0); // x^8 + x^4 + x^3 + x^2 + 1

        /// <summary>
        ///     Data Matrix
        /// </summary>
        public static GenericGF DATA_MATRIX_FIELD_256 = new GenericGF(0x012D, 256, 1); // x^8 + x^5 + x^3 + x^2 + 1

        /// <summary>
        ///     Aztec data 8
        /// </summary>
        public static GenericGF AZTEC_DATA_8 = DATA_MATRIX_FIELD_256;

        /// <summary>
        ///     Maxicode
        /// </summary>
        public static GenericGF MAXICODE_FIELD_64 = AZTEC_DATA_6;

        /// <summary>
        /// Таблица экспонент / значений возведённых в степень
        /// </summary>
        private readonly int[] _expTable;

        /// <summary>
        /// Таблица логарифмов
        /// </summary>
        private readonly int[] _logTable;

        private readonly int _primitive;

        /// <summary>
        ///     Создание представления GF (размер), используя данный примитивный многочлен.
        /// </summary>
        /// <param name="primitive">
        ///     Неприводимый многочлен (irreducible polynomial), коэффициенты которого представлены
        ///     * битами int, где младший бит представляет константу
        ///     * коэффициентом
        /// </param>
        /// <param name="size">Размер поля</param>
        /// <param name="genBase">
        ///     Базис для генерации.
        ///     Множитель b в генераторе многочленов (generator polynomial) может быть основан на 0 или 1
        ///     * (g(x) = (x + a ^ b) (x + a ^ (b + 1)) ... (x + a ^ (b + 2t-1))).
        ///     * В большинстве случаев это должно быть 1, но для QR-кода он равен 0.
        /// </param>
        public GenericGF(int primitive, int size, int genBase)
        {
            _primitive = primitive;
            Size = size;
            GeneratorBase = genBase;

            _expTable = new int[size];
            _logTable = new int[size];
            var x = 1;
            for (var i = 0; i < size; i++)
            {
                _expTable[i] = x;
                x <<= 1; //х = х * 2; предполагается, что значение альфа для генератора равно 2
                if (x >= size)
                {
                    x ^= primitive;
                    x &= size - 1;
                }
            }
            for (var i = 0; i < size - 1; i++)
                _logTable[_expTable[i]] = i;
            // logTable[0] == 0 однако это значение не следует использовать
            Zero = new GenericGFPoly(this, new[] {0});
            One = new GenericGFPoly(this, new[] {1});
        }

        internal GenericGFPoly Zero { get; }

        internal GenericGFPoly One { get; }

        /// <summary>
        ///     Получает размер.
        /// </summary>
        public int Size { get; }

        /// <summary>
        ///     Получает базис генератора.
        /// </summary>
        public int GeneratorBase { get; }

        /// <summary>
        ///     Строит моном (одночлен)
        /// </summary>
        /// <param name="degree">Степень.</param>
        /// <param name="coefficient">Коэффициент.</param>
        /// <returns>Моном (одночлен), представляющий коэффициент умноженный на x^степень</returns>
        internal GenericGFPoly BuildMonomial(int degree, int coefficient)
        {
            if (degree < 0)
                throw new ArgumentException();
            if (coefficient == 0)
                return Zero;
            var coefficients = new int[degree + 1];
            coefficients[0] = coefficient;
            return new GenericGFPoly(this, coefficients);
        }

        /// <summary>
        ///     Реализует как сложение, так и вычитание -- они схожи по GF(size).
        /// </summary>
        /// <returns>Cумма / разница a и b</returns>
        internal static int AddOrSubtract(int a, int b)
        {
            return a ^ b;
        }

        /// <summary>
        ///     Получает возведённое ранее число в степень сопоставляющийся с указанным числом A. / Exps the specified a.
        /// </summary>
        /// <returns>2 в степени A в GF(size) / 2 to the power of a in GF(size)</returns> 
        internal int Exp(int a)
        {
            return _expTable[a];
        }

        /// <summary>
        ///     Получает ранее вычисленный логарифм сопоставляющийся с указанным числом А. / Logs the specified a.
        /// </summary>
        /// <param name="a">A.</param>
        /// <returns>base 2 log of a in GF(size)</returns>
        internal int Log(int a)
        {
            if (a == 0)
                throw new ArgumentException();
            return _logTable[a];
        }

        /// <summary>
        ///     Инвертирование указанного числа A.
        /// </summary>
        /// <returns>Multiplicative inverse of a</returns>
        internal int Inverse(int a)
        {
            if (a == 0)
                throw new ArithmeticException();
            return _expTable[Size - _logTable[a] - 1];
        }

        /// <summary>
        ///     Умножает указанное число a на b.
        /// </summary>
        /// <param name="a">A.</param>
        /// <param name="b">The b.</param>
        /// <returns>Произведение числа A на B в GF(size) / Product of a and b in GF(size)</returns>
        internal int Multiply(int a, int b)
        {
            if (a == 0 || b == 0)
                return 0;
            return _expTable[(_logTable[a] + _logTable[b]) % (Size - 1)];
        }

        /// <summary>
        ///     Возвращает <see cref="System.String" /> который представляет этот экземпляр.
        /// </summary>
        /// <returns>
        ///     <see cref="System.String" /> который представляет этот экземпляр.
        /// </returns>
        public override string ToString()
        {
            return "GF(0x" + _primitive.ToString("X") + ',' + Size + ')';
        }
    }
}
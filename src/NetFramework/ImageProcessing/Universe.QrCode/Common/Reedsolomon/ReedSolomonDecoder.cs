﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

namespace Universe.QrCode.Common.Reedsolomon
{
    /// <summary>
    ///     <p>Реализует декодирование Рида-Соломона, что, собственно, следует из названия.</p>
    ///     <p>
    ///         Алгоритм здесь не объясняется, но следующие ссылки были полезны
    ///         при создании этой реализации:
    ///     </p>
    ///     <ul>
    ///         <li>
    ///             Bruce Maggs.
    ///             <a href="http://www.cs.cmu.edu/afs/cs.cmu.edu/project/pscico-guyb/realworld/www/rs_decode.ps">
    ///                 "Расшифровка кодов Рида-Соломона"
    ///             </a>
    ///             (см. обсуждение формулы Форни)
    ///         </li>
    ///         <li>
    ///             J.I. Hall.
    ///             <a href="http://www.mth.msu.edu/~jhall/classes/codenotes/GRS.pdf">
    ///                 "Глава 5. Обобщенные коды Рида-Соломона."
    ///             </a>
    ///             (см. обсуждение алгоритма Евклида)
    ///         </li>
    ///     </ul>
    ///     <p>
    ///         Большая заслуга принадлежит Уильяму Раклиджу, поскольку части этого кода являются косвенными
    ///         Это порт его реализации Рида-Соломона на C ++.
    ///     </p>
    /// </summary>
    /// <author>Sean Owen</author>
    /// <author>Alex Envision</author>
    /// <author>William Rucklidge</author>
    /// <author>sanfordsquires</author>
    public sealed class ReedSolomonDecoder
    {
        private readonly GenericGF _field;

        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="field"></param>
        public ReedSolomonDecoder(GenericGF field)
        {
            _field = field;
        }

        /// <summary>
        ///     <p>
        ///     Декодирует заданный набор полученных кодовых слов, который включает как данные, так и исправление ошибок
        ///     кодовые слова. На самом деле это означает, что он использует Рида-Соломона для обнаружения и исправления ошибок на месте,
        ///     во вводе.
        ///     </p>
        /// </summary>
        /// <param name="received">Данные и кодовые слова исправления ошибок</param>
        /// <param name="twoS">Количество доступных кодовых слов для исправления ошибок</param>
        /// <returns>false: декодирование не удалось</returns>
        public bool Decode(int[] received, int twoS)
        {
            var poly = new GenericGFPoly(_field, received);
            var syndromeCoefficients = new int[twoS];
            var noError = true;
            for (var i = 0; i < twoS; i++)
            {
                var eval = poly.EvaluateAt(_field.Exp(i + _field.GeneratorBase));
                syndromeCoefficients[syndromeCoefficients.Length - 1 - i] = eval;
                if (eval != 0)
                    noError = false;
            }
            if (noError)
                return true;
            var syndrome = new GenericGFPoly(_field, syndromeCoefficients);

            var sigmaOmega = RunEuclideanAlgorithm(_field.BuildMonomial(twoS, 1), syndrome, twoS);
            if (sigmaOmega == null)
                return false;

            var sigma = sigmaOmega[0];
            var errorLocations = FindErrorLocations(sigma);
            if (errorLocations == null)
                return false;

            var omega = sigmaOmega[1];
            var errorMagnitudes = FindErrorMagnitudes(omega, errorLocations);
            for (var i = 0; i < errorLocations.Length; i++)
            {
                var position = received.Length - 1 - _field.Log(errorLocations[i]);
                if (position < 0)
                    return false;
                received[position] = GenericGF.AddOrSubtract(received[position], errorMagnitudes[i]);
            }

            return true;
        }

        /// <summary>
        /// Запуск алгоритма Евклида
        /// </summary>
        /// <param name="a">Полином (многочлен) А</param>
        /// <param name="b">Полином (многочлен) B</param>
        /// <param name="rdegree">Степень R. Количество доступных кодовых слов для исправления ошибок</param>
        /// <returns></returns>
        internal GenericGFPoly[] RunEuclideanAlgorithm(GenericGFPoly a, GenericGFPoly b, int rdegree)
        {
            // Предположим, что степень a's = b's
            if (a.Degree < b.Degree)
            {
                var temp = a;
                a = b;
                b = temp;
            }

            var rLast = a;
            var r = b;
            var tLast = _field.Zero;
            var t = _field.One;

            // Aлгоритм Евклида работает, пока степень r не станет меньше R / 2.
            while (r.Degree >= rdegree / 2)
            {
                var rLastLast = rLast;
                var tLastLast = tLast;
                rLast = r;
                tLast = t;

                // Деление rLastLast на rLast, с частным по q и остатком по r
                if (rLast.IsZero)
                    return null;
                r = rLastLast;
                var q = _field.Zero;
                var denominatorLeadingTerm = rLast.GetCoefficient(rLast.Degree);
                var dltInverse = _field.Inverse(denominatorLeadingTerm);
                while (r.Degree >= rLast.Degree && !r.IsZero)
                {
                    var degreeDiff = r.Degree - rLast.Degree;
                    var scale = _field.Multiply(r.GetCoefficient(r.Degree), dltInverse);
                    q = q.AddOrSubtract(_field.BuildMonomial(degreeDiff, scale));
                    r = r.AddOrSubtract(rLast.MultiplyByMonomial(degreeDiff, scale));
                }

                t = q.Multiply(tLast).AddOrSubtract(tLastLast);

                if (r.Degree >= rLast.Degree)
                    return null;
            }

            var sigmaTildeAtZero = t.GetCoefficient(0);
            if (sigmaTildeAtZero == 0)
                return null;

            var inverse = _field.Inverse(sigmaTildeAtZero);
            var sigma = t.Multiply(inverse);
            var omega = r.Multiply(inverse);
            return new[] {sigma, omega};
        }

        /// <summary>
        /// Поиск мест с ошибками/расположений ошибок.
        /// </summary>
        /// <param name="errorLocator"></param>
        /// <returns></returns>
        private int[] FindErrorLocations(GenericGFPoly errorLocator)
        {
            // Это прямое приложение поиска Чиен
            var numErrors = errorLocator.Degree;
            if (numErrors == 1)
                return new[] {errorLocator.GetCoefficient(1)};
            var result = new int[numErrors];
            var e = 0;
            for (var i = 1; i < _field.Size && e < numErrors; i++)
                if (errorLocator.EvaluateAt(i) == 0)
                {
                    result[e] = _field.Inverse(i);
                    e++;
                }
            if (e != numErrors)
                return null;
            return result;
        }

        /// <summary>
        /// Поиск величин ошибок
        /// </summary>
        /// <param name="errorEvaluator">Оценщик ошибки</param>
        /// <param name="errorLocations">Места с ошибками/Расположения ошибок.</param>
        /// <returns></returns>
        private int[] FindErrorMagnitudes(GenericGFPoly errorEvaluator, int[] errorLocations)
        {
            // Это прямое применение формулы Форни
            var s = errorLocations.Length;
            var result = new int[s];
            for (var i = 0; i < s; i++)
            {
                var xiInverse = _field.Inverse(errorLocations[i]);
                var denominator = 1;
                for (var j = 0; j < s; j++)
                    if (i != j)
                    {
                        // denominator = field.multiply(denominator,
                        //    GenericGF.addOrSubtract(1, field.multiply(errorLocations[j], xiInverse)));
                        // Вышеупомянутое должно работать, но не работает на некоторых JDK Apple и Linux из-за Hotspot бага.
                        // Ниже приведен забавный обходной путь (костыль) от Стивена Паркса.
                        var term = _field.Multiply(errorLocations[j], xiInverse);
                        var termPlus1 = (term & 0x1) == 0 ? term | 1 : term & ~1;
                        denominator = _field.Multiply(denominator, termPlus1);

                        // Удален в версии java, не уверен, корректно ли это
                        // denominator = field.multiply(denominator, GenericGF.addOrSubtract(1, field.multiply(errorLocations[j], xiInverse)));
                    }
                result[i] = _field.Multiply(errorEvaluator.EvaluateAt(xiInverse), _field.Inverse(denominator));
                if (_field.GeneratorBase != 0)
                    result[i] = _field.Multiply(result[i], xiInverse);
            }
            return result;
        }
    }
}
﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;

namespace Universe.QrCode.Decoder
{
    /// <summary>
    ///     <p>
    ///         Инкапсулирует блок данных в QR-код. QR-коды могут разбивать свои данные на
    ///         несколько блоков, каждый из которых является единицей данных и кодовых слов исправления ошибок. Каждый
    ///         представлен экземпляром этого класса.
    ///     </p>
    /// </summary>
    /// <author> 
    ///     Sean Owen
    /// </author>
    /// <author>
    ///     Alex Envision
    /// </author>
    /// <author>
    ///     www.Redivivus.in (suraj.supekar@redivivus.in) - Ported from ZXING Java Source
    /// </author>
    internal sealed class DataBlock
    {
        private DataBlock(int numDataCodewords, byte[] codewords)
        {
            NumDataCodewords = numDataCodewords;
            Codewords = codewords;
        }

        internal int NumDataCodewords { get; }

        internal byte[] Codewords { get; }

        /// <summary>
        ///     <p>
        ///         When QR Codes use multiple data blocks, they are actually interleaved.
        ///         That is, the first byte of data block 1 to n is written, then the second bytes, and so on. This
        ///         method will separate the data into original blocks.
        ///     </p>
        /// </summary>
        /// <param name="rawCodewords">
        ///     bytes as read directly from the QR Code
        /// </param>
        /// <param name="version">
        ///     version of the QR Code
        /// </param>
        /// <param name="ecLevel">
        ///     error-correction level of the QR Code
        /// </param>
        /// <returns>
        ///     {@link DataBlock}s containing original bytes, "de-interleaved" from representation in the
        ///     QR Code
        /// </returns>
        internal static DataBlock[] getDataBlocks(byte[] rawCodewords, Version version, ErrorCorrectionLevel ecLevel)
        {
            if (rawCodewords.Length != version.TotalCodewords)
                throw new ArgumentException();

            // Figure out the number and size of data blocks used by this version and
            // error correction level
            var ecBlocks = version.GetEcBlocksForLevel(ecLevel);

            // First count the total number of data blocks
            var totalBlocks = 0;
            var ecBlockArray = ecBlocks.GetEcBlocks();
            foreach (var ecBlock in ecBlockArray)
                totalBlocks += ecBlock.Count;

            // Now establish DataBlocks of the appropriate size and number of data codewords
            var result = new DataBlock[totalBlocks];
            var numResultBlocks = 0;
            foreach (var ecBlock in ecBlockArray)
                for (var i = 0; i < ecBlock.Count; i++)
                {
                    var numDataCodewords = ecBlock.DataCodewords;
                    var numBlockCodewords = ecBlocks.ECCodewordsPerBlock + numDataCodewords;
                    result[numResultBlocks++] = new DataBlock(numDataCodewords, new byte[numBlockCodewords]);
                }

            // All blocks have the same amount of data, except that the last n
            // (where n may be 0) have 1 more byte. Figure out where these start.
            var shorterBlocksTotalCodewords = result[0].Codewords.Length;
            var longerBlocksStartAt = result.Length - 1;
            while (longerBlocksStartAt >= 0)
            {
                var numCodewords = result[longerBlocksStartAt].Codewords.Length;
                if (numCodewords == shorterBlocksTotalCodewords)
                    break;
                longerBlocksStartAt--;
            }
            longerBlocksStartAt++;

            var shorterBlocksNumDataCodewords = shorterBlocksTotalCodewords - ecBlocks.ECCodewordsPerBlock;
            // The last elements of result may be 1 element longer;
            // first fill out as many elements as all of them have
            var rawCodewordsOffset = 0;
            for (var i = 0; i < shorterBlocksNumDataCodewords; i++)
            for (var j = 0; j < numResultBlocks; j++)
                result[j].Codewords[i] = rawCodewords[rawCodewordsOffset++];
            // Fill out the last data block in the longer ones
            for (var j = longerBlocksStartAt; j < numResultBlocks; j++)
                result[j].Codewords[shorterBlocksNumDataCodewords] = rawCodewords[rawCodewordsOffset++];
            // Now add in error correction blocks
            var max = result[0].Codewords.Length;
            for (var i = shorterBlocksNumDataCodewords; i < max; i++)
            for (var j = 0; j < numResultBlocks; j++)
            {
                var iOffset = j < longerBlocksStartAt ? i : i + 1;
                result[j].Codewords[iOffset] = rawCodewords[rawCodewordsOffset++];
            }
            return result;
        }
    }
}
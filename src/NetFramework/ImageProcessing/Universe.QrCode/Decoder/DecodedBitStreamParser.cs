﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;
using System.Collections.Generic;
using System.Text;
using Universe.QrCode.Common;
using Universe.QrCode.Common.DataContracts;
using Universe.QrCode.Models;

namespace Universe.QrCode.Decoder
{
    /// <summary>
    ///     <p>
    ///        QR-коды могут кодировать текст как биты в одном из нескольких режимов и могут использовать несколько режимов
    ///        в одном QR-коде. Этот класс декодирует биты обратно в текст.
    ///     </p>
    ///     <p> См. ISO 18004: 2006, 6.4.3 - 6.4.7 </p>
    ///     <author> Sean Owen</author>
    ///     <author> Alex Envision</author>
    /// </summary>
    internal static class DecodedBitStreamParser
    {
        private const int GB2312_SUBSET = 1;

        /// <summary>
        ///     See ISO 18004:2006, 6.4.4 Table 5
        /// </summary>
        private static readonly char[] ALPHANUMERIC_CHARS =
            "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ $%*+-./:".ToCharArray();

        internal static DecoderResult Decode(byte[] bytes,
            Version version,
            ErrorCorrectionLevel ecLevel,
            IDictionary<DecodeHintType, object> hints)
        {
            var bits = new BitSource(bytes);
            var result = new StringBuilder(50);
            var byteSegments = new List<byte[]>(1);
            var symbolSequence = -1;
            var parityData = -1;

            try
            {
                CharacterSetECI currentCharacterSetEci = null;
                var fc1InEffect = false;
                Mode mode;
                do
                {
                    // While still another segment to read...
                    if (bits.Available() < 4)
                        mode = Mode.TERMINATOR;
                    else
                        try
                        {
                            mode = Mode.forBits(bits.ReadBits(4)); // mode is encoded by 4 bits
                        }
                        catch (ArgumentException)
                        {
                            return null;
                        }
                    switch (mode.Name)
                    {
                        case Mode.Names.TERMINATOR:
                            break;
                        case Mode.Names.FNC1_FIRST_POSITION:
                        case Mode.Names.FNC1_SECOND_POSITION:
                            // We do little with FNC1 except alter the parsed result a bit according to the spec
                            fc1InEffect = true;
                            break;
                        case Mode.Names.STRUCTURED_APPEND:
                            if (bits.Available() < 16)
                                return null;
                            // not really supported; but sequence number and parity is added later to the result metadata
                            // Read next 8 bits (symbol sequence #) and 8 bits (parity data), then continue
                            symbolSequence = bits.ReadBits(8);
                            parityData = bits.ReadBits(8);
                            break;
                        case Mode.Names.ECI:
                            // Count doesn't apply to ECI
                            var value = ParseEciValue(bits);
                            currentCharacterSetEci = CharacterSetECI.getCharacterSetECIByValue(value);
                            if (currentCharacterSetEci == null)
                                return null;
                            break;
                        case Mode.Names.HANZI:
                            // First handle Hanzi mode which does not start with character count
                            //chinese mode contains a sub set indicator right after mode indicator
                            var subset = bits.ReadBits(4);
                            var countHanzi = bits.ReadBits(mode.getCharacterCountBits(version));
                            if (subset == GB2312_SUBSET)
                                if (!DecodeHanziSegment(bits, result, countHanzi))
                                    return null;
                            break;
                        default:
                            // "Normal" QR code modes:
                            // How many characters will follow, encoded in this mode?
                            var count = bits.ReadBits(mode.getCharacterCountBits(version));
                            switch (mode.Name)
                            {
                                case Mode.Names.NUMERIC:
                                    if (!DecodeNumericSegment(bits, result, count))
                                        return null;
                                    break;
                                case Mode.Names.ALPHANUMERIC:
                                    if (!DecodeAlphanumericSegment(bits, result, count, fc1InEffect))
                                        return null;
                                    break;
                                case Mode.Names.BYTE:
                                    if (!DecodeByteSegment(bits, result, count, currentCharacterSetEci, byteSegments,
                                        hints))
                                        return null;
                                    break;
                                case Mode.Names.KANJI:
                                    if (!DecodeKanjiSegment(bits, result, count))
                                        return null;
                                    break;
                                default:
                                    return null;
                            }
                            break;
                    }
                } while (mode != Mode.TERMINATOR);
            }
            catch (ArgumentException)
            {
                // from readBits() calls
                return null;
            }

            return new DecoderResult(bytes,
                result.ToString(),
                byteSegments.Count == 0 ? null : byteSegments,
                ecLevel == null ? null : ecLevel.ToString(),
                symbolSequence, parityData);
        }

        /// <summary>
        ///     See specification GBT 18284-2000
        /// </summary>
        /// <param name="bits">The bits.</param>
        /// <param name="result">The result.</param>
        /// <param name="count">The count.</param>
        /// <returns></returns>
        private static bool DecodeHanziSegment(BitSource bits,
            StringBuilder result,
            int count)
        {
            // Don't crash trying to read more bits than we have available.
            if (count * 13 > bits.Available())
                return false;

            // Each character will require 2 bytes. Read the characters as 2-byte pairs
            // and decode as GB2312 afterwards
            var buffer = new byte[2 * count];
            var offset = 0;
            while (count > 0)
            {
                // Each 13 bits encodes a 2-byte character
                var twoBytes = bits.ReadBits(13);
                var assembledTwoBytes = ((twoBytes / 0x060) << 8) | (twoBytes % 0x060);
                if (assembledTwoBytes < 0x00A00)
                    assembledTwoBytes += 0x0A1A1;
                else
                    assembledTwoBytes += 0x0A6A1;
                buffer[offset] = (byte) ((assembledTwoBytes >> 8) & 0xFF);
                buffer[offset + 1] = (byte) (assembledTwoBytes & 0xFF);
                offset += 2;
                count--;
            }

            try
            {
                result.Append(Encoding.GetEncoding(StringUtils.GB2312).GetString(buffer, 0, buffer.Length));
            }
#if (WINDOWS_PHONE70 || WINDOWS_PHONE71 || SILVERLIGHT4 || SILVERLIGHT5 || NETFX_CORE || MONOANDROID || MONOTOUCH)
         catch (ArgumentException)
         {
            try
            {
               // Silverlight only supports a limited number of character sets, trying fallback to UTF-8
               result.Append(Encoding.GetEncoding("UTF-8").GetString(buffer, 0, buffer.Length));
            }
            catch (Exception)
            {
               return false;
            }
         }
#endif
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        private static bool DecodeKanjiSegment(BitSource bits,
            StringBuilder result,
            int count)
        {
            // Don't crash trying to read more bits than we have available.
            if (count * 13 > bits.Available())
                return false;

            // Each character will require 2 bytes. Read the characters as 2-byte pairs
            // and decode as Shift_JIS afterwards
            var buffer = new byte[2 * count];
            var offset = 0;
            while (count > 0)
            {
                // Each 13 bits encodes a 2-byte character
                var twoBytes = bits.ReadBits(13);
                var assembledTwoBytes = ((twoBytes / 0x0C0) << 8) | (twoBytes % 0x0C0);
                if (assembledTwoBytes < 0x01F00)
                    assembledTwoBytes += 0x08140;
                else
                    assembledTwoBytes += 0x0C140;
                buffer[offset] = (byte) (assembledTwoBytes >> 8);
                buffer[offset + 1] = (byte) assembledTwoBytes;
                offset += 2;
                count--;
            }
            // Shift_JIS may not be supported in some environments:
            try
            {
                result.Append(Encoding.GetEncoding(StringUtils.SHIFT_JIS).GetString(buffer, 0, buffer.Length));
            }
#if (WINDOWS_PHONE70 || WINDOWS_PHONE71 || SILVERLIGHT4 || SILVERLIGHT5 || NETFX_CORE || MONOANDROID || MONOTOUCH)
         catch (ArgumentException)
         {
            try
            {
               // Silverlight only supports a limited number of character sets, trying fallback to UTF-8
               result.Append(Encoding.GetEncoding("UTF-8").GetString(buffer, 0, buffer.Length));
            }
            catch (Exception)
            {
               return false;
            }
         }
#endif
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        private static bool DecodeByteSegment(BitSource bits,
            StringBuilder result,
            int count,
            CharacterSetECI currentCharacterSetEci,
            IList<byte[]> byteSegments,
            IDictionary<DecodeHintType, object> hints)
        {
            // Don't crash trying to read more bits than we have available.
            if (count << 3 > bits.Available())
                return false;

            var readBytes = new byte[count];
            for (var i = 0; i < count; i++)
                readBytes[i] = (byte) bits.ReadBits(8);
            string encoding;
            if (currentCharacterSetEci == null)
                encoding = StringUtils.GuessEncoding(readBytes, hints);
            else
                encoding = currentCharacterSetEci.EncodingName;
            try
            {
                result.Append(Encoding.GetEncoding(encoding).GetString(readBytes, 0, readBytes.Length));
            }
#if (WINDOWS_PHONE70 || WINDOWS_PHONE71 || SILVERLIGHT4 || SILVERLIGHT5 || NETFX_CORE || MONOANDROID || MONOTOUCH)
         catch (ArgumentException)
         {
            try
            {
               // Silverlight only supports a limited number of character sets, trying fallback to UTF-8
               result.Append(Encoding.GetEncoding("UTF-8").GetString(readBytes, 0, readBytes.Length));
            }
            catch (Exception)
            {
               return false;
            }
         }
#endif
#if WindowsCE
         catch (PlatformNotSupportedException)
         {
            try
            {
               // WindowsCE doesn't support all encodings. But it is device depended.
               // So we try here the some different ones
               if (encoding == "ISO-8859-1")
               {
                  result.Append(Encoding.GetEncoding(1252).GetString(readBytes, 0, readBytes.Length));
               }
               else
               {
                  result.Append(Encoding.GetEncoding("UTF-8").GetString(readBytes, 0, readBytes.Length));
               }
            }
            catch (Exception)
            {
               return false;
            }
         }
#endif
            catch (Exception)
            {
                return false;
            }
            byteSegments.Add(readBytes);

            return true;
        }

        private static char ToAlphaNumericChar(int value)
        {
            if (value >= ALPHANUMERIC_CHARS.Length)
                throw new FormatException();
            return ALPHANUMERIC_CHARS[value];
        }

        private static bool DecodeAlphanumericSegment(BitSource bits,
            StringBuilder result,
            int count,
            bool fc1InEffect)
        {
            // Read two characters at a time
            var start = result.Length;
            while (count > 1)
            {
                if (bits.Available() < 11)
                    return false;
                var nextTwoCharsBits = bits.ReadBits(11);
                result.Append(ToAlphaNumericChar(nextTwoCharsBits / 45));
                result.Append(ToAlphaNumericChar(nextTwoCharsBits % 45));
                count -= 2;
            }
            if (count == 1)
            {
                // special case: one character left
                if (bits.Available() < 6)
                    return false;
                result.Append(ToAlphaNumericChar(bits.ReadBits(6)));
            }

            // See section 6.4.8.1, 6.4.8.2
            if (fc1InEffect)
                for (var i = start; i < result.Length; i++)
                    if (result[i] == '%')
                        if (i < result.Length - 1 && result[i + 1] == '%')
                        {
                            // %% is rendered as %
                            result.Remove(i + 1, 1);
                        }
                        else
                        {
                            // In alpha mode, % should be converted to FNC1 separator 0x1D
                            result.Remove(i, 1);
                            result.Insert(i, new[] {(char) 0x1D});
                        }

            return true;
        }

        private static bool DecodeNumericSegment(BitSource bits,
            StringBuilder result,
            int count)
        {
            // Read three digits at a time
            while (count >= 3)
            {
                // Each 10 bits encodes three digits
                if (bits.Available() < 10)
                    return false;
                var threeDigitsBits = bits.ReadBits(10);
                if (threeDigitsBits >= 1000)
                    return false;
                result.Append(ToAlphaNumericChar(threeDigitsBits / 100));
                result.Append(ToAlphaNumericChar(threeDigitsBits / 10 % 10));
                result.Append(ToAlphaNumericChar(threeDigitsBits % 10));

                count -= 3;
            }
            if (count == 2)
            {
                // Two digits left over to read, encoded in 7 bits
                if (bits.Available() < 7)
                    return false;
                var twoDigitsBits = bits.ReadBits(7);
                if (twoDigitsBits >= 100)
                    return false;
                result.Append(ToAlphaNumericChar(twoDigitsBits / 10));
                result.Append(ToAlphaNumericChar(twoDigitsBits % 10));
            }
            else if (count == 1)
            {
                // One digit left over to read
                if (bits.Available() < 4)
                    return false;
                var digitBits = bits.ReadBits(4);
                if (digitBits >= 10)
                    return false;
                result.Append(ToAlphaNumericChar(digitBits));
            }

            return true;
        }

        private static int ParseEciValue(BitSource bits)
        {
            var firstByte = bits.ReadBits(8);
            if ((firstByte & 0x80) == 0)
                return firstByte & 0x7F;
            if ((firstByte & 0xC0) == 0x80)
            {
                // two bytes
                var secondByte = bits.ReadBits(8);
                return ((firstByte & 0x3F) << 8) | secondByte;
            }
            if ((firstByte & 0xE0) == 0xC0)
            {
                // three bytes
                var secondThirdBytes = bits.ReadBits(16);
                return ((firstByte & 0x1F) << 16) | secondThirdBytes;
            }
            throw new ArgumentException("Bad ECI bits starting with byte " + firstByte);
        }
    }
}
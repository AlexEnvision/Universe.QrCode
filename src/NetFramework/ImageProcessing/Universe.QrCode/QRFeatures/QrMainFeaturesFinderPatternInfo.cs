﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

namespace Universe.QrCode.QRFeatures
{
    /// <summary>
    ///     <p>
    ///        Инкапсулирует информацию о шаблонах поиска в изображении, 
    ///        включая расположение трех шаблонов поиска и их предполагаемый размер модуля.
    ///     </p>
    /// </summary>
    /// <author>Alex Envision</author>
    public sealed class QrMainFeaturesFinderPatternInfo
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="QrMainFeaturesFinderPatternInfo" /> class.
        /// </summary>
        /// <param name="patternCenters">The pattern centers.</param>
        public QrMainFeaturesFinderPatternInfo(QrMainFeaturesFinderPattern[] patternCenters)
        {
            BottomLeft = patternCenters[0];
            TopLeft = patternCenters[1];
            TopRight = patternCenters[2];
        }

        /// <summary>
        ///     Gets the bottom left.
        /// </summary>
        public QrMainFeaturesFinderPattern BottomLeft { get; }

        /// <summary>
        ///     Gets the top left.
        /// </summary>
        public QrMainFeaturesFinderPattern TopLeft { get; }

        /// <summary>
        ///     Gets the top right.
        /// </summary>
        public QrMainFeaturesFinderPattern TopRight { get; }
    }
}
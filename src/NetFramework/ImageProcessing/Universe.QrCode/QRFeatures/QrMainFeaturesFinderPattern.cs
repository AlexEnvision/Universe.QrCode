﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;
using Universe.QrCode.Models;

namespace Universe.QrCode.QRFeatures
{
    /// <summary>
    ///     <p>
    ///         Инкапсулирует шаблон поиска, который представляет собой три квадратных шаблона в углах QR-кодов.
    ///         Он также инкапсулирует количество похожих шаблонов поиска для удобства ведения бухгалтерии поисковика.
    ///     </p>
    /// </summary>
    /// <author>Alex Envision</author>
    public sealed class QrMainFeaturesFinderPattern : ResultPoint
    {
        internal QrMainFeaturesFinderPattern(float posX, float posY, float estimatedModuleSize)
            : this(posX, posY, estimatedModuleSize, 1)
        {
            EstimatedModuleSize = estimatedModuleSize;
            Count = 1;
        }

        internal QrMainFeaturesFinderPattern(float posX, float posY, float estimatedModuleSize, int count)
            : base(posX, posY)
        {
            EstimatedModuleSize = estimatedModuleSize;
            Count = count;
        }

        /// <summary>
        ///     Gets the size of the estimated module.
        /// </summary>
        /// <value>
        ///     The size of the estimated module.
        /// </value>
        public float EstimatedModuleSize { get; }

        internal int Count { get; }

        /*
        internal void incrementCount()
        {
           this.count++;
        }
        */

        /// <summary>
        ///     <p>
        ///         Determines if this finder pattern "about equals" a finder pattern at the stated
        ///         position and size -- meaning, it is at nearly the same center with nearly the same size.
        ///     </p>
        /// </summary>
        internal bool AboutEquals(float moduleSize, float i, float j)
        {
            if (Math.Abs(i - Y) <= moduleSize && Math.Abs(j - X) <= moduleSize)
            {
                var moduleSizeDiff = Math.Abs(moduleSize - EstimatedModuleSize);
                return moduleSizeDiff <= 1.0f || moduleSizeDiff <= EstimatedModuleSize;
            }
            return false;
        }

        /// <summary>
        ///     Combines this object's current estimate of a finder pattern position and module size
        ///     with a new estimate. It returns a new {@code FinderPattern} containing a weighted average
        ///     based on count.
        /// </summary>
        /// <param name="i">The i.</param>
        /// <param name="j">The j.</param>
        /// <param name="newModuleSize">New size of the module.</param>
        /// <returns></returns>
        internal QrMainFeaturesFinderPattern CombineEstimate(float i, float j, float newModuleSize)
        {
            var combinedCount = Count + 1;
            var combinedX = (Count * X + j) / combinedCount;
            var combinedY = (Count * Y + i) / combinedCount;
            var combinedModuleSize = (Count * EstimatedModuleSize + newModuleSize) / combinedCount;
            return new QrMainFeaturesFinderPattern(combinedX, combinedY, combinedModuleSize, combinedCount);
        }
    }
}
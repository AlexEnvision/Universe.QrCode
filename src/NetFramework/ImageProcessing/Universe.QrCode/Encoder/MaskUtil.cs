﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;

namespace Universe.QrCode.Encoder
{
    /// <summary>
    ///     Масочные утилиты
    /// </summary>
    /// <author>Satoru Takabayashi</author>
    /// <author>Daniel Switkin</author>
    /// <author>Sean Owen</author>
    /// <author>Alex Envision</author>
    public static class MaskUtil
    {
        // Штрафы из раздела 6.8.2.1
        private const int N1 = 3;

        private const int N2 = 3;
        private const int N3 = 40;
        private const int N4 = 10;

        /// <summary>
        ///     Примение штрафного правила по маске 1 и возврат штрафа. Поиск повторяющихся ячеек одного цвета и
        ///     назначение им штрафа. Пример: 00000 или 11111.
        /// </summary>
        /// <param name="matrix">Матрица.</param>
        /// <returns></returns>
        public static int ApplyMaskPenaltyRule1(ByteMatrix matrix)
        {
            return ApplyMaskPenaltyRule1Internal(matrix, true) + ApplyMaskPenaltyRule1Internal(matrix, false);
        }

        /// <summary>
        ///     Примение штрафного правила 2 по маске и возврат штрафа. Поиск блоков 2x2 одного цвета и выдача
        ///     штрафа им. Это фактически эквивалентно правилу спецификации, которое заключается в том, чтобы найти блоки MxN и
        ///     дать
        ///     штраф пропорциональный (M-1) x (N-1), потому что это количество блоков 2x2 внутри такого блока.
        /// </summary>
        /// <param name="matrix">Матрица.</param>
        /// <returns></returns>
        public static int ApplyMaskPenaltyRule2(ByteMatrix matrix)
        {
            var penalty = 0;
            var array = matrix.Array;
            var width = matrix.Width;
            var height = matrix.Height;
            for (var y = 0; y < height - 1; y++)
            {
                var arrayY = array[y];
                var arrayY1 = array[y + 1];
                for (var x = 0; x < width - 1; x++)
                {
                    int value = arrayY[x];
                    if (value == arrayY[x + 1] && value == arrayY1[x] && value == arrayY1[x + 1])
                        penalty++;
                }
            }
            return N2 * penalty;
        }

        /// <summary>
        ///     Применить штрафного правила по маске 3 и возврат штрафа. Поиск последовательных ячеек 00001011101 или
        ///     10111010000 и назначение им штрафа. Если будут найдены шаблоны типа 000010111010000, то
        ///     выдается двойной штраф (т.е. 40 * 2)
        /// </summary>
        /// <param name="matrix">Матрица.</param>
        /// <returns></returns>
        public static int ApplyMaskPenaltyRule3(ByteMatrix matrix)
        {
            var numPenalties = 0;
            var array = matrix.Array;
            var width = matrix.Width;
            var height = matrix.Height;
            for (var y = 0; y < height; y++)
            for (var x = 0; x < width; x++)
            {
                var arrayY = array[y]; // We can at least optimize this access
                if (x + 6 < width &&
                    arrayY[x] == 1 &&
                    arrayY[x + 1] == 0 &&
                    arrayY[x + 2] == 1 &&
                    arrayY[x + 3] == 1 &&
                    arrayY[x + 4] == 1 &&
                    arrayY[x + 5] == 0 &&
                    arrayY[x + 6] == 1 &&
                    (IsWhiteHorizontal(arrayY, x - 4, x) || IsWhiteHorizontal(arrayY, x + 7, x + 11)))
                    numPenalties++;
                if (y + 6 < height &&
                    array[y][x] == 1 &&
                    array[y + 1][x] == 0 &&
                    array[y + 2][x] == 1 &&
                    array[y + 3][x] == 1 &&
                    array[y + 4][x] == 1 &&
                    array[y + 5][x] == 0 &&
                    array[y + 6][x] == 1 &&
                    (IsWhiteVertical(array, x, y - 4, y) || IsWhiteVertical(array, x, y + 7, y + 11)))
                    numPenalties++;
            }
            return numPenalties * N3;
        }

        private static bool IsWhiteHorizontal(byte[] rowArray, int from, int to)
        {
            from = Math.Max(from, 0);
            to = Math.Min(to, rowArray.Length);
            for (var i = from; i < to; i++)
                if (rowArray[i] == 1)
                    return false;
            return true;
        }

        private static bool IsWhiteVertical(byte[][] array, int col, int from, int to)
        {
            from = Math.Max(from, 0);
            to = Math.Min(to, array.Length);
            for (var i = from; i < to; i++)
                if (array[i][col] == 1)
                    return false;
            return true;
        }

        /// <summary>
        ///     Примение штрафного правило 4 по маске и возврат штрафа. Рассчёт соотношения темных ячеек и выдача
        ///     штрафа, если соотношение далеко от 50%. Дает 10 штрафа за 5% дистанции.
        /// </summary>
        /// <param name="matrix">Матрица.</param>
        /// <returns></returns>
        public static int ApplyMaskPenaltyRule4(ByteMatrix matrix)
        {
            var numDarkCells = 0;
            var array = matrix.Array;
            var width = matrix.Width;
            var height = matrix.Height;
            for (var y = 0; y < height; y++)
            {
                var arrayY = array[y];
                for (var x = 0; x < width; x++)
                    if (arrayY[x] == 1)
                        numDarkCells++;
            }
            var numTotalCells = matrix.Height * matrix.Width;
            var darkRatio = (double) numDarkCells / numTotalCells;
            var fivePercentVariances = (int) (Math.Abs(darkRatio - 0.5) * 20.0); // * 100.0 / 5.0
            return fivePercentVariances * N4;
        }

        /// <summary>
        ///     Возврат бита маски для «getMaskPattern» в «x» и «y». См. Маску в 8.8 JISX0510: 2004.
        ///     в условиях шаблона (pattern conditions).
        /// </summary>
        /// <param name="maskPattern">Маска шаблона.</param>
        /// <param name="x">Координата x.</param>
        /// <param name="y">Координата y.</param>
        /// <returns></returns>
        public static bool GetDataMaskBit(int maskPattern, int x, int y)
        {
            int intermediate, temp;
            switch (maskPattern)
            {
                case 0:
                    intermediate = (y + x) & 0x1;
                    break;

                case 1:
                    intermediate = y & 0x1;
                    break;

                case 2:
                    intermediate = x % 3;
                    break;

                case 3:
                    intermediate = (y + x) % 3;
                    break;

                case 4:
                    intermediate = ((int) ((uint) y >> 1) + x / 3) & 0x1;
                    break;

                case 5:
                    temp = y * x;
                    intermediate = (temp & 0x1) + temp % 3;
                    break;

                case 6:
                    temp = y * x;
                    intermediate = ((temp & 0x1) + temp % 3) & 0x1;
                    break;

                case 7:
                    temp = y * x;
                    intermediate = (temp % 3 + ((y + x) & 0x1)) & 0x1;
                    break;

                default:
                    throw new ArgumentException("Invalid mask pattern: " + maskPattern);
            }
            return intermediate == 0;
        }

        /// <summary>
        ///     Вспомогательная функция для applyMaskPenaltyRule1. Это нужно для выполнения этого расчета в обоих:
        ///     вертикальном и горизонтальном положениях соответственно.
        /// </summary>
        /// <param name="matrix">Матрица.</param>
        /// <param name="isHorizontal">Если установлено <c>true</c> [Горизонтальное положение].</param>
        /// <returns></returns>
        private static int ApplyMaskPenaltyRule1Internal(ByteMatrix matrix, bool isHorizontal)
        {
            var penalty = 0;
            var iLimit = isHorizontal ? matrix.Height : matrix.Width;
            var jLimit = isHorizontal ? matrix.Width : matrix.Height;
            var array = matrix.Array;
            for (var i = 0; i < iLimit; i++)
            {
                var numSameBitCells = 0;
                var prevBit = -1;
                for (var j = 0; j < jLimit; j++)
                {
                    int bit = isHorizontal ? array[i][j] : array[j][i];
                    if (bit == prevBit)
                    {
                        numSameBitCells++;
                    }
                    else
                    {
                        if (numSameBitCells >= 5)
                            penalty += N1 + (numSameBitCells - 5);
                        numSameBitCells = 1; // Include the cell itself.
                        prevBit = bit;
                    }
                }
                if (numSameBitCells >= 5)
                    penalty += N1 + (numSameBitCells - 5);
            }
            return penalty;
        }
    }
}
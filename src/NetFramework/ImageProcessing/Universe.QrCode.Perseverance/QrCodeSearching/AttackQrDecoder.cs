﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;
using System.Drawing;
using Universe.Image;
using Universe.QrCode.Perseverance.Dal.Query;
using Universe.QrCode.Perseverance.Models;
using Universe.QrCode.QRFeatures;
using Universe.QrCode.Types.GraphicTypes;
using Universe.Types.Event;

namespace Universe.QrCode.Perseverance.QrCodeSearching
{
    /// <summary>
    /// Декодировщик QR-кодов использующий разные типы "атак" с помощью фильтров
    /// </summary>
    public class AttackQrDecoder
    {
        public event LogInfoDel LogInfo;

        public event LogErrorDel LogError;

        /// <summary>
        /// "Атака" на изображение.
        /// Базовая, всегда выполняеся первой.
        /// Использует фильтры:
        ///    - автоконтрасности
        ///    - бинаризации/монохромности.
        /// </summary>
        /// <param name="page">Страница в виде Bitmap</param>
        /// <param name="logPrefix">Префикс для записей в лог</param>
        /// <returns>Результат поиска и чтения QR-кода</returns>
        public QrDecodeResult Attack(Bitmap page, string logPrefix = "")
        {
            var preprocessedPage = page.FullCopy()
                                    .ApplyAutocontrast()
                                    .MakeMonoChrome();

            return Decode(preprocessedPage, logPrefix);
        }

        /// <summary>
        /// "Атака" на изображение.
        /// Использует фильтры:
        ///    - бинаризации/монохромности.
        /// </summary>
        /// <param name="page">Страница в виде Bitmap</param>
        /// <param name="logPrefix">Префикс для записей в лог</param>
        /// <returns>Результат поиска и чтения QR-кода</returns>
        public QrDecodeResult AttackOnlyWithMonochroming(Bitmap page, string logPrefix = "")
        {
            var preprocessedPage = page.FullCopy()
                                    .MakeMonoChrome();

            return Decode(preprocessedPage, logPrefix);
        }

        /// <summary>
        /// "Атака" на изображение.
        /// Использует фильтры:
        ///    - Увеличение/уменьшение разрешения
        ///    - автоконтрасности
        ///    - бинаризации/монохромности.
        /// </summary>
        /// <param name="page">Страница в виде Bitmap</param>
        /// <param name="logPrefix">Префикс для записей в лог</param>
        /// <returns>Результат поиска и чтения QR-кода</returns>
        public QrDecodeResult AttackWithDownUpscale(Bitmap page, string logPrefix = "")
        {
            var preprocessedPage = page.FullCopy()
                                    .ApplyDownUpscale()
                                    .ApplyAutocontrast()
                                    .MakeMonoChrome();

            return Decode(preprocessedPage, logPrefix);
        }

        /// <summary>
        /// "Атака" на изображение.
        /// Использует фильтры:
        ///    - размытия
        ///    - автоконтрасности
        ///    - бинаризации/монохромности.
        /// </summary>
        /// <param name="page">Страница в виде Bitmap</param>
        /// <param name="logPrefix">Префикс для записей в лог</param>
        /// <returns>Результат поиска и чтения QR-кода</returns>
        public QrDecodeResult AttackWithBlur(Bitmap page, string logPrefix = "")
        {
            var preprocessedPage = page.FullCopy()
                                    .ApplyBlur()
                                    .ApplyAutocontrast()
                                    .MakeMonoChrome();

            return Decode(preprocessedPage, logPrefix);
        }

        /// <summary>
        /// "Атака" на изображение.
        /// Использует фильтры:
        ///    - размытия
        ///    - бинаризации/монохромности.
        /// </summary>
        /// <param name="page">Страница в виде Bitmap</param>
        /// <param name="logPrefix">Префикс для записей в лог</param>
        /// <returns>Результат поиска и чтения QR-кода</returns>
        public QrDecodeResult AttackOnlyMonochromingButWithBlur(Bitmap page, string logPrefix = "")
        {
            var preprocessedPage = page.FullCopy()
                                    .ApplyBlur()
                                    .MakeMonoChrome();

            return Decode(preprocessedPage, logPrefix);
        }

        /// <summary>
        /// "Атака" на изображение.
        /// Увеличивает разрешение изображения.
        /// Использует фильтры:
        ///    - бинаризации/монохромности.
        /// </summary>
        /// <param name="page">Страница в виде Bitmap</param>
        /// <param name="logPrefix">Префикс для записей в лог</param>
        /// <returns>Результат поиска и чтения QR-кода</returns>
        public QrDecodeResult AttackWithUpscaleOnlyMonochroming(Bitmap page, string logPrefix = "")
        {
            var preprocessedPage = page.FullCopy()
                                    .Upscale()
                                    .MakeMonoChrome();

            return Decode(preprocessedPage, logPrefix).AfterUpscaling();
        }

        /// <summary>
        /// "Атака" на изображение.
        /// Увеличивает разрешение изображения.
        /// Использует фильтры:
        ///    - автоконтрастность
        ///    - бинаризации/монохромности.
        /// </summary>
        /// <param name="page">Страница в виде Bitmap</param>
        /// <param name="logPrefix">Префикс для записей в лог</param>
        /// <returns>Результат поиска и чтения QR-кода</returns>
        public QrDecodeResult AttackWithUpscale(Bitmap page, string logPrefix = "")
        {
            var preprocessedPage = page.FullCopy()
                                    .Upscale()
                                    .ApplyAutocontrast()
                                    .MakeMonoChrome();

            return Decode(preprocessedPage, logPrefix).AfterUpscaling();
        }

        /// <summary>
        /// Применяет комбинацию "атак" на полном изображении
        /// </summary>
        /// <param name="page">Страница в виде Bitmap</param>
        /// <param name="logPrefix">Префикс для записей в лог</param>
        /// <returns>Результат поиска и чтения QR-кода</returns>
        public QrDecodeResult MakeComboWithFullPage(Bitmap page, string logPrefix = "")
        {
            // Сначала атакуем обычным способом с автоконтрастностью и монохромностью
            var result = Attack(page, "Attack. ");

            // Если не найдено - применяем только монохромный фильтр
            if (!result.HasFoundCode)
            {               
                result = AttackOnlyWithMonochroming(page, "AttackOnlyWithMonochroming. ");
            }
            // Если не найдено - применяем: 
            //    - Увеличение/уменьшение разрешения
            //    - автоконтрасность
            //    - монохромность.
            if (!result.HasFoundCode)
            {
                result = AttackWithDownUpscale(page, "AttackWithDownUpscale. ");
            }
            // Если не найдено - применяем: 
            //    - Увеличение разрешения изображения - апскейлинг
            //    - автоконтрасность
            //    - монохромность.
            if (!result.HasReadCode)
            {
                result = AttackWithUpscale(page, "AttackWithUpscale. ");
            }
            // Если не найдено - применяем: 
            //    - Увеличение разрешения изображения - апскейлинг
            //    - монохромность.
            if (!result.HasReadCode)
            {
                result = AttackWithUpscaleOnlyMonochroming(page, "AttackWithUpscaleOnlyMonochroming. ");
            }
            // Если не найдено - применяем: 
            //    - размытие
            //    - автоконтрасность
            //    - монохромность.
            if (!result.HasReadCode)
            {
                result = AttackWithBlur(page, "AttackWithBlur. ");
            }
            // Если по прежнему ничего не найдено - выходим
            if (!result.HasFoundCode)
            {
                return result;
            }

            // Если код уже прочитан, то выходим
            if (result.HasReadCode)
            {
                return result;
            }

            var qrCodeMeta = result.QrCodeMeta;
            var fragment = qrCodeMeta.QrCodeImage;

            return MakeComboWithFragment(fragment, result);
        }

        /// <summary>
        /// "Атака" на фрагмент изображения.
        /// Базовая, всегда выполняеся первой.
        /// Использует фильтры:
        ///    - автоконтрасности
        ///    - бинаризации/монохромности.
        /// </summary>
        /// <param name="page">Страница в виде Bitmap</param>
        /// <param name="result"></param>
        /// <param name="logPrefix">Префикс для записей в лог</param>
        /// <returns>Результат поиска и чтения QR-кода</returns>
        public QrDecodeResult AttackOnFragment(Bitmap page, QrDecodeResult result, string logPrefix = "")
        {
            var preprocessedPage = page.FullCopy()
                                    .ApplyAutocontrast()
                                    .MakeMonoChrome();

            return DecodeFragment(result, preprocessedPage, logPrefix);
        }

        /// <summary>
        /// "Атака" на изображение.
        /// Использует фильтры:
        ///    - бинаризации/монохромности.
        /// </summary>
        /// <param name="page">Страница в виде Bitmap</param>
        /// <param name="result"></param>
        /// <param name="logPrefix">Префикс для записей в лог</param>
        /// <returns>Результат поиска и чтения QR-кода</returns>
        public QrDecodeResult AttackOnFragmentOnlyWithMonochroming(Bitmap page, QrDecodeResult result, string logPrefix = "")
        {
            var preprocessedPage = page.FullCopy()
                                    .MakeMonoChrome();

            return DecodeFragment(result, preprocessedPage, logPrefix);
        }

        /// <summary>
        /// "Атака" на изображение.
        /// Использует фильтры:
        ///    - Увеличение/уменьшение разрешения
        ///    - автоконтрасности
        ///    - бинаризации/монохромности.
        /// </summary>
        /// <param name="page">Страница в виде Bitmap</param>
        /// <param name="result"></param>
        /// <param name="logPrefix">Префикс для записей в лог</param>
        /// <returns>Результат поиска и чтения QR-кода</returns>
        public QrDecodeResult AttackOnFragmentWithDownUpscale(Bitmap page, QrDecodeResult result, string logPrefix = "")
        {
            var preprocessedPage = page.FullCopy()
                                    .ApplyDownUpscale()
                                    .ApplyAutocontrast()
                                    .MakeMonoChrome();

            return DecodeFragment(result, preprocessedPage, logPrefix);
        }

        /// <summary>
        /// "Атака" на изображение.
        /// Использует фильтры:
        ///    - размытия
        ///    - автоконтрасности
        ///    - бинаризации/монохромности.
        /// </summary>
        /// <param name="page">Страница в виде Bitmap</param>
        /// <param name="result"></param>
        /// <param name="logPrefix">Префикс для записей в лог</param>
        /// <returns>Результат поиска и чтения QR-кода</returns>
        public QrDecodeResult AttackOnFragmentWithBlur(Bitmap page, QrDecodeResult result, string logPrefix = "")
        {
            var preprocessedPage = page.FullCopy()
                                    .ApplyBlur()
                                    .ApplyAutocontrast()
                                    .MakeMonoChrome();

            return DecodeFragment(result, preprocessedPage, logPrefix);
        }

        /// <summary>
        /// "Атака" на изображение.
        /// Использует фильтры:
        ///    - размытия
        ///    - бинаризации/монохромности.
        /// </summary>
        /// <param name="page">Страница в виде Bitmap</param>
        /// <param name="result"></param>
        /// <param name="logPrefix">Префикс для записей в лог</param>
        /// <returns>Результат поиска и чтения QR-кода</returns>
        public QrDecodeResult AttackOnFragmentOnlyMonochromingButWithBlur(Bitmap page, QrDecodeResult result, string logPrefix = "")
        {
            var preprocessedPage = page.FullCopy()
                                    .ApplyBlur()
                                    .MakeMonoChrome();

            return DecodeFragment(result, preprocessedPage, logPrefix);
        }

        /// <summary>
        /// "Атака" на изображение.
        /// Увеличивает разрешение изображения.
        /// Использует фильтры:
        ///    - бинаризации/монохромности.
        /// </summary>
        /// <param name="page">Страница в виде Bitmap</param>
        /// <param name="result"></param>
        /// <param name="logPrefix">Префикс для записей в лог</param>
        /// <returns>Результат поиска и чтения QR-кода</returns>
        public QrDecodeResult AttackOnFragmentWithUpscaleOnlyMonochroming(Bitmap page, QrDecodeResult result, string logPrefix = "")
        {
            var preprocessedPage = page.FullCopy()
                                    .Upscale()
                                    .MakeMonoChrome();

            return DecodeFragment(result, preprocessedPage, logPrefix).AfterUpscaling();
        }

        /// <summary>
        /// "Атака" на изображение.
        /// Увеличивает разрешение изображения.
        /// Использует фильтры:
        ///    - автоконтрастность
        ///    - бинаризации/монохромности.
        /// </summary>
        /// <param name="page">Страница в виде Bitmap</param>
        /// <param name="result"></param>
        /// <param name="logPrefix">Префикс для записей в лог</param>
        /// <returns>Результат поиска и чтения QR-кода</returns>
        public QrDecodeResult AttackOnFragmentWithUpscale(Bitmap page, QrDecodeResult result, string logPrefix = "")
        {
            var preprocessedPage = page.FullCopy()
                                    .Upscale()
                                    .ApplyAutocontrast()
                                    .MakeMonoChrome();

            return DecodeFragment(result, preprocessedPage, logPrefix).AfterUpscaling();
        }

        /// <summary>
        /// Применяет комбинацию "атак" на фрагменте изображения
        /// </summary>
        /// <param name="fragment">Фрагмент изображения</param>
        /// <param name="resultPrevStage">Результат предыдущей стадии</param>
        /// <returns>Результат поиска и чтения QR-кода</returns>
        private QrDecodeResult MakeComboWithFragment(Bitmap fragment, QrDecodeResult resultPrevStage)
        {
            if (fragment == null)
                throw new ArgumentException(nameof(fragment));

            var result = resultPrevStage == null
                ? new QrDecodeResult{
                    HasFoundCode = false,
                    FeaturesCoordinates = null,
                    HasReadCode = false,
                    QrCodeText = "",
                    QrCodeMeta = null
                }
                : resultPrevStage;

            // Сначала атакуем обычным способом с автоконтрастностью и монохромностью
            result = AttackOnFragment(fragment, result, "AttackOnFragment. ");

            // Так в данном алгоритме доппускаем, что это уже фрагмент с изображением
            // то в дальненйшем ориентируемся на флаг чтения кода - HadReadCode
            // Если не найдено - применяем только монохромный фильтр
            if (!result.HasReadCode)
            {
                result = AttackOnFragmentOnlyWithMonochroming(fragment, result, "AttackOnFragmentOnlyWithMonochroming. ");
            }
            // Если не найдено - применяем: 
            //    - Увеличение/уменьшение разрешения
            //    - автоконтрасность
            //    - монохромность.
            if (!result.HasReadCode)
            {
                result = AttackOnFragmentWithDownUpscale(fragment, result, "AttackOnFragmentkWithDownUpscale. ");
            }
            // Если не найдено - применяем: 
            //    - Увеличение разрешения изображения - апскейлинг
            //    - автоконтрасность
            //    - монохромность.
            if (!result.HasReadCode)
            {
                result = AttackOnFragmentWithUpscale(fragment, result, "AttackOnFragmentWithUpscale. ");
            }
            // Если не найдено - применяем: 
            //    - Увеличение разрешения изображения - апскейлинг
            //    - монохромность.
            if (!result.HasReadCode)
            {
                result = AttackOnFragmentWithUpscaleOnlyMonochroming(fragment, result, "AttackOnFragmentWithUpscaleOnlyMonochroming. ");
            }
            // Если не найдено - применяем: 
            //    - размытие
            //    - автоконтрасность
            //    - монохромность.
            if (!result.HasReadCode)
            {
                result = AttackOnFragmentWithBlur(fragment, result, "AttackOnFragmentWithBlur. ");
            }
            // Если по прежнему ничего не найдено - выходим
            if (!result.HasReadCode)
            {
                return result;
            }

            return result;
        }

        /// <summary>
        /// Применяет комбинацию "атак" на фрагменте изображения
        /// </summary>
        /// <param name="fragment">Фрагмент изображения</param>
        /// <param name="logPrefix">Префикс для записей в лог</param>
        /// <returns>Результат поиска и чтения QR-кода</returns>
        public QrDecodeResult MakeComboWithFragment(Bitmap fragment, string logPrefix = "")
        {
            if (fragment == null)
                throw new ArgumentException(nameof(fragment));

            var result = new QrDecodeResult {
                    HasFoundCode = false,
                    FeaturesCoordinates = null,
                    HasReadCode = false,
                    QrCodeText = "",
                    QrCodeMeta = null
                };

            QrMainFeaturesFinderPatternInfo featureCoordinates = GetQrMainFeaturesPoints(fragment, false); // Отключено автоприменение монохромности по-умолчанию
            result.FeaturesCoordinates = featureCoordinates;

            var imageFragmentLocation = fragment;
            var qrCodeMetaBorders = new Borders(new SpacePoint(0, 0), imageFragmentLocation.Width, imageFragmentLocation.Height);

            result.QrCodeMeta = new QrCodeMeta
            {
                Borders = qrCodeMetaBorders,
                QrCodeImage = fragment
            };

            // Сначала атакуем обычным способом с автоконтрастностью и монохромностью
            result = AttackOnFragment(fragment, result);

            // Так в данном алгоритме доппускаем, что это уже фрагмент с изображением
            // то в дальненйшем ориентируемся на флаг чтения кода - HadReadCode
            // Если не найдено - применяем только монохромный фильтр
            if (!result.HasReadCode)
            {
                result = AttackOnFragmentOnlyWithMonochroming(fragment, result, "AttackOnlyWithMonochroming. ");
            }
            // Если не найдено - применяем: 
            //    - Увеличение/уменьшение разрешения
            //    - автоконтрасность
            //    - монохромность.
            if (!result.HasReadCode)
            {
                result = AttackOnFragmentWithDownUpscale(fragment, result, "AttackWithDownUpscale. ");
            }
            // Если не найдено - применяем: 
            //    - Увеличение разрешения изображения - апскейлинг
            //    - автоконтрасность
            //    - монохромность.
            if (!result.HasReadCode)
            {
                result = AttackOnFragmentWithUpscale(fragment, result, "AttackWithUpscale. ");
            }
            // Если не найдено - применяем: 
            //    - Увеличение разрешения изображения - апскейлинг
            //    - монохромность.
            if (!result.HasReadCode)
            {
                result = AttackOnFragmentWithUpscaleOnlyMonochroming(fragment, result, "AttackWithUpscaleOnlyMonochroming. ");
            }
            // Если по прежнему ничего не найдено - выходим
            if (!result.HasReadCode)
            {
                return result;
            }

            return result;
        }

        /// <summary>
        /// Получение координат центров меток позиционирования QR-кода
        /// </summary>
        /// <param name="bitmap"></param>
        /// <param name="makesMonoChrome"></param>
        /// <returns></returns>
        public QrMainFeaturesFinderPatternInfo GetQrMainFeaturesPoints(Bitmap bitmap, bool makesMonoChrome = true)
        {
            var featureCoordinates = new GetQrCodeMainFeaturesCoordinatesQuery().Execute(bitmap, true, makesMonoChrome);
            return featureCoordinates;
        }

        /// <summary>
        /// Чтение QR-кода
        /// </summary>
        /// <param name="bitmap">Изображение на котором производится поиск QR-кода</param>
        /// <param name="makesMonoChrome"></param>
        /// <returns></returns>
        public string ReadQrCodeData(Bitmap bitmap, bool makesMonoChrome = true)
        {
            var qrCodeOpenCvReader = new DecodeQrCodeQuery();
            return qrCodeOpenCvReader.Execute(bitmap, makesMonoChrome);
        }

        /// <summary>
        /// Вырезает QR-код
        /// </summary>
        /// <param name="featureCoordinates">Координаты центров меток позиционирования QR-кода</param>
        /// <param name="imgWithCode">Изображение с кодом</param>
        /// <returns></returns>
        public QrCodeMeta QrCodeCut(QrMainFeaturesFinderPatternInfo featureCoordinates, Bitmap imgWithCode)
        {
            var width = (int)(Math.Abs(featureCoordinates.TopRight.X - featureCoordinates.TopLeft.X) + 80);
            var heght = (int)(Math.Abs(featureCoordinates.BottomLeft.Y - featureCoordinates.TopLeft.Y) + 80);
            var startX = (int)featureCoordinates.TopLeft.X - 40;
            var startY = (int)featureCoordinates.TopLeft.Y - 40;

            Rectangle bordersRect = new Rectangle(startX, startY, width, heght);
            var qrCodeCut = imgWithCode.Copy(bordersRect);

            var borders = new Borders(bordersRect.X, bordersRect.Y, bordersRect.Width, bordersRect.Height);

            return new QrCodeMeta
            {
                QrCodeImage = qrCodeCut,
                Borders = borders
            };
        }

        /// <summary>
        /// Декодирование QR-кода
        /// </summary>
        /// <param name="image">Изображение</param>
        /// <param name="logPrefx">Сообщение - префикс для уточнения категории в логах</param>
        /// <returns></returns>
        private QrDecodeResult Decode(
            Bitmap image,
            string logPrefx = "")
        {
            var decodeResult = new QrDecodeResult();
            QrMainFeaturesFinderPatternInfo featureCoordinates = GetQrMainFeaturesPoints(image, false); // Отключено автоприменение монохромности по-умолчанию
            decodeResult.HasFoundCode = featureCoordinates != null;
            decodeResult.FeaturesCoordinates = featureCoordinates;

            if (decodeResult.HasFoundCode)
            {
                if (featureCoordinates != null)
                {
                    var featureCoordinatesAsString =
                        $"{featureCoordinates.TopLeft}; {featureCoordinates.TopRight}; {featureCoordinates.BottomLeft}";

                    WriteToLog($"{logPrefx}Найден QR-код располагающийся по координатам: {featureCoordinatesAsString}.");
                }

                var qrCodeCut = QrCodeCut(featureCoordinates, image);

                var qrCodeText = ReadQrCodeData(qrCodeCut.QrCodeImage, false); // Отключено автоприменение монохромности по-умолчанию
                decodeResult.HasReadCode = !string.IsNullOrEmpty(qrCodeText);
                decodeResult.QrCodeText = qrCodeText;
                decodeResult.QrCodeMeta = qrCodeCut;

                if (!decodeResult.HasReadCode)
                {
                    WriteToLog($"{logPrefx}QR-код был обнаружен, но не удалось прочитать из него информацию.");
                }
            }
            else
            {
                WriteToLog($"{logPrefx}На изображении не распознано/не найдено ни одного QR-кода!");
            }

            return decodeResult;
        }

        /// <summary>
        /// Декодирование фрагмента QR-кода
        /// </summary>
        /// <param name="decodeResultPrevStage"></param>
        /// <param name="image">Изображение</param>
        /// <param name="logPrefx">Сообщение - префикс для уточнения категории в логах</param>
        /// <returns></returns>
        private QrDecodeResult DecodeFragment(
            QrDecodeResult decodeResultPrevStage,
            Bitmap image,
            string logPrefx = "")
        {
            var decodeResult = decodeResultPrevStage;

            var qrCodeText = ReadQrCodeData(image, false); // Отключено автоприменение монохромности по-умолчанию
            decodeResult.HasReadCode = !string.IsNullOrEmpty(qrCodeText);
            decodeResult.QrCodeText = qrCodeText;

            if (!decodeResult.HasReadCode)
            {
                WriteToLog($"{logPrefx}QR-код был обнаружен, но не удалось прочитать из него информацию.");
            }

            return decodeResult;
        }

        private void WriteToLog(string v)
        {
            LogInfo?.Invoke(new LogInfoEventArgs
            {
                AllowedReport = true,
                Message = v
            });
        }

        private void WriteErrorToLog(Exception ex)
        {
            LogError?.Invoke(new LogErrorEventArgs
            {
                AllowReport = true,
                Message = ex.Message,
                Ex = ex
            });
        }
    }

    public class AttackingBitmapRepresentation
    {
        public Bitmap SourceImage { get; set; }

        public Bitmap ProcessedImage { get; set; }
    }
}
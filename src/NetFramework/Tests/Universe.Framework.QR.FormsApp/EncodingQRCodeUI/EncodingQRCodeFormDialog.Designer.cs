﻿namespace Universe.Framework.QR.FormsApp.EncodingQRCodeUI
{
    partial class EncodingQrCodeFormDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label3 = new System.Windows.Forms.Label();
            this.tbCodeAsStringWithSplitters = new System.Windows.Forms.TextBox();
            this.lbBase64OrQrAsText = new System.Windows.Forms.Label();
            this.tbCodeAsBase64String = new System.Windows.Forms.TextBox();
            this.btOK = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.pbQrCodeImage = new Universe.OpenCv.Windows.Forms.Controls.PanAndZoomPictureBoxQrCode();
            this.cmsQrCodeImg = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.resetZoomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToClipBoardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btEncodeDataToQrCode = new System.Windows.Forms.Button();
            this.cbEncodeBase64 = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbQrCodeImage)).BeginInit();
            this.cmsQrCodeImg.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(217, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Исходные данные кодируемые в QR-код:";
            // 
            // tbCodeAsStringWithSplitters
            // 
            this.tbCodeAsStringWithSplitters.Location = new System.Drawing.Point(9, 33);
            this.tbCodeAsStringWithSplitters.Multiline = true;
            this.tbCodeAsStringWithSplitters.Name = "tbCodeAsStringWithSplitters";
            this.tbCodeAsStringWithSplitters.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbCodeAsStringWithSplitters.Size = new System.Drawing.Size(649, 242);
            this.tbCodeAsStringWithSplitters.TabIndex = 9;
            // 
            // lbBase64OrQrAsText
            // 
            this.lbBase64OrQrAsText.AutoSize = true;
            this.lbBase64OrQrAsText.Location = new System.Drawing.Point(9, 295);
            this.lbBase64OrQrAsText.Name = "lbBase64OrQrAsText";
            this.lbBase64OrQrAsText.Size = new System.Drawing.Size(294, 13);
            this.lbBase64OrQrAsText.TabIndex = 8;
            this.lbBase64OrQrAsText.Text = "Данные в виде Base64 строки записываемые в QR-код:";
            // 
            // tbCodeAsBase64String
            // 
            this.tbCodeAsBase64String.Location = new System.Drawing.Point(9, 311);
            this.tbCodeAsBase64String.Multiline = true;
            this.tbCodeAsBase64String.Name = "tbCodeAsBase64String";
            this.tbCodeAsBase64String.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbCodeAsBase64String.Size = new System.Drawing.Size(649, 243);
            this.tbCodeAsBase64String.TabIndex = 7;
            // 
            // btOK
            // 
            this.btOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btOK.Location = new System.Drawing.Point(943, 524);
            this.btOK.Name = "btOK";
            this.btOK.Size = new System.Drawing.Size(101, 33);
            this.btOK.TabIndex = 6;
            this.btOK.Text = "ОК";
            this.btOK.UseVisualStyleBackColor = true;
            this.btOK.Click += new System.EventHandler(this.btOK_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(674, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "QR-код фото:";
            // 
            // pbQrCodeImage
            // 
            this.pbQrCodeImage.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbQrCodeImage.ContextMenuStrip = this.cmsQrCodeImg;
            this.pbQrCodeImage.Location = new System.Drawing.Point(677, 33);
            this.pbQrCodeImage.Name = "pbQrCodeImage";
            this.pbQrCodeImage.PanableAndZoomableEnable = true;
            this.pbQrCodeImage.Size = new System.Drawing.Size(350, 350);
            this.pbQrCodeImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbQrCodeImage.TabIndex = 11;
            this.pbQrCodeImage.TabStop = false;
            // 
            // cmsQrCodeImg
            // 
            this.cmsQrCodeImg.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.resetZoomToolStripMenuItem,
            this.copyToClipBoardToolStripMenuItem});
            this.cmsQrCodeImg.Name = "cmsDocumentPageImg";
            this.cmsQrCodeImg.Size = new System.Drawing.Size(232, 48);
            // 
            // resetZoomToolStripMenuItem
            // 
            this.resetZoomToolStripMenuItem.Name = "resetZoomToolStripMenuItem";
            this.resetZoomToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.resetZoomToolStripMenuItem.Text = "Сбросить масштаб";
            this.resetZoomToolStripMenuItem.Click += new System.EventHandler(this.resetZoomToolStripMenuItem_Click);
            // 
            // copyToClipBoardToolStripMenuItem
            // 
            this.copyToClipBoardToolStripMenuItem.Name = "copyToClipBoardToolStripMenuItem";
            this.copyToClipBoardToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.copyToClipBoardToolStripMenuItem.Text = "Копировать в буфер обмена";
            this.copyToClipBoardToolStripMenuItem.Click += new System.EventHandler(this.copyToClipBoardToolStripMenuItem_Click);
            // 
            // btEncodeDataToQrCode
            // 
            this.btEncodeDataToQrCode.Location = new System.Drawing.Point(677, 524);
            this.btEncodeDataToQrCode.Name = "btEncodeDataToQrCode";
            this.btEncodeDataToQrCode.Size = new System.Drawing.Size(260, 33);
            this.btEncodeDataToQrCode.TabIndex = 13;
            this.btEncodeDataToQrCode.Text = "Закодировать данные в QR-код";
            this.btEncodeDataToQrCode.UseVisualStyleBackColor = true;
            this.btEncodeDataToQrCode.Click += new System.EventHandler(this.btEncodeDataToQrCode_Click);
            // 
            // cbEncodeBase64
            // 
            this.cbEncodeBase64.AutoSize = true;
            this.cbEncodeBase64.Location = new System.Drawing.Point(677, 423);
            this.cbEncodeBase64.Name = "cbEncodeBase64";
            this.cbEncodeBase64.Size = new System.Drawing.Size(171, 17);
            this.cbEncodeBase64.TabIndex = 14;
            this.cbEncodeBase64.Text = "Кодировать в Base64 строку";
            this.cbEncodeBase64.UseVisualStyleBackColor = true;
            this.cbEncodeBase64.CheckedChanged += new System.EventHandler(this.cbEncodeBase64_CheckedChanged);
            // 
            // EncodingQrCodeFormDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1056, 569);
            this.Controls.Add(this.cbEncodeBase64);
            this.Controls.Add(this.btEncodeDataToQrCode);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pbQrCodeImage);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbCodeAsStringWithSplitters);
            this.Controls.Add(this.lbBase64OrQrAsText);
            this.Controls.Add(this.tbCodeAsBase64String);
            this.Controls.Add(this.btOK);
            this.Name = "EncodingQrCodeFormDialog";
            ((System.ComponentModel.ISupportInitialize)(this.pbQrCodeImage)).EndInit();
            this.cmsQrCodeImg.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbCodeAsStringWithSplitters;
        private System.Windows.Forms.Label lbBase64OrQrAsText;
        private System.Windows.Forms.TextBox tbCodeAsBase64String;
        private System.Windows.Forms.Button btOK;
        private System.Windows.Forms.Label label4;
        private Universe.OpenCv.Windows.Forms.Controls.PanAndZoomPictureBoxQrCode pbQrCodeImage;
        private System.Windows.Forms.Button btEncodeDataToQrCode;
        private System.Windows.Forms.CheckBox cbEncodeBase64;
        private System.Windows.Forms.ContextMenuStrip cmsQrCodeImg;
        private System.Windows.Forms.ToolStripMenuItem resetZoomToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToClipBoardToolStripMenuItem;
    }
}
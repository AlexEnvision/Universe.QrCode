﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Universe.Framework.QR.FormsApp.Dal.Query;
using Universe.Framework.QR.FormsApp.Rendering;
using Universe.QrCode;

namespace Universe.Framework.QR.FormsApp.EncodingQRCodeUI
{
    public partial class EncodingQrCodeFormDialog : Form
    {
        public QrCodeDataEncodingModel QrCodeModel { get; set; }

        public EncodingQrCodeFormDialog()
        {
            QrCodeModel = new QrCodeDataEncodingModel();

            InitializeComponent();
        }

        public EncodingQrCodeFormDialog(QrCodeDataEncodingModel qrCodeModel)
        {
            QrCodeModel = qrCodeModel;

            InitializeComponent();
        }

        private void btOK_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void btEncodeDataToQrCode_Click(object sender, System.EventArgs e)
        {
            // Берём исходные данные
            var sourceData = tbCodeAsStringWithSplitters.Text;

            string toQrCodeData;
            string toBase64String = string.Empty;
            // Кодируем их в BASE64 строку если установлен чекбокс на UI в true
            if (cbEncodeBase64.Checked)
            {
                var encoded = Encoding.UTF8.GetBytes(sourceData);
                toBase64String = Convert.ToBase64String(encoded);
                toQrCodeData = toBase64String;
            }
            else
            {
                toQrCodeData = sourceData;
            }

            var qrCodeOpenCvWriter = new EncodeQrCodeMatrixCommand();
            var qrCodeMatrix = qrCodeOpenCvWriter.Execute(toQrCodeData);

            tbCodeAsBase64String.Text = !cbEncodeBase64.Checked ? qrCodeMatrix.ToString() : toBase64String;

            var qrCogeImage = new BitmapRenderer().Render(qrCodeMatrix, BarcodeFormat.QR_CODE, string.Empty);
            pbQrCodeImage.Image = qrCogeImage;

            // Заполняем модель QR-кода
            QrCodeModel.CodeAsStringWithSplitters = sourceData;
            QrCodeModel.CodeAsBase64String = cbEncodeBase64.Checked ? toQrCodeData : string.Empty;
            QrCodeModel.QrCodeTextRepresentation = qrCodeMatrix.ToString();
            QrCodeModel.QrCodePicture = qrCogeImage;
        }

        private void cbEncodeBase64_CheckedChanged(object sender, EventArgs e)
        {
            lbBase64OrQrAsText.Text = cbEncodeBase64.Checked ? "Данные в виде Base64 строки записываемые в QR-код:" : "QR-код в виде текста:";
        }

        /// <summary>
        /// Сброс области отображения QR-кода
        /// </summary>
        /// <param name="sender">Отправитель события</param>
        /// <param name="e">Аргументы события</param>
        private void resetZoomToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pbQrCodeImage.SetZoomScale(1.0, Point.Empty);
            pbQrCodeImage.HorizontalScrollBar.Value = 0;
            pbQrCodeImage.VerticalScrollBar.Value = 0;
        }

        /// <summary>
        /// Копирование изображения кода в буфер обмена
        /// </summary>
        /// <param name="sender">Отправитель события</param>
        /// <param name="e">Аргументы события</param>
        private void copyToClipBoardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var img = pbQrCodeImage.Image;
            Clipboard.SetImage(img);
        }
    }
}
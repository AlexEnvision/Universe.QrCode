﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;
using System.Drawing;
using System.Drawing.Imaging;
using Universe.Image;
using Universe.QrCode.Perseverance.Dal.Query;
using Universe.QrCode.Perseverance.Models;
using Universe.QrCode.QRFeatures;
using Universe.QrCode.Types;
using Universe.QrCode.Types.GraphicTypes;

namespace Universe.Framework.QR.FormsApp
{
    /// <summary>
    /// Выполняет поиск QR-кодов на изображении
    /// </summary>
    public class QrCodeSeeker
    {
        private readonly PdfViewer _pdfViewer;

        public QrCodeModel QrCodeModel { get; set; }

        public PdfViewer PdfViewer => _pdfViewer;

        public QrCodeSeeker()
        {
            _pdfViewer = new PdfViewer();
        }

        public QrCodeSeeker(PdfViewer pdfViewer)
        {
            _pdfViewer = pdfViewer ?? throw new ArgumentNullException(nameof(pdfViewer));
        }

        /// <summary>
        /// Бинаризация
        /// Делает изображение монохромным
        /// </summary>
        /// <param name="bufferBitmap">Обрабатываемое изображение</param>
        public Bitmap MakeMonoChrome(Bitmap bufferBitmap)
        {
            // Создаём холст
            var canvas = new Bitmap(bufferBitmap.Width, bufferBitmap.Height, PixelFormat.Format24bppRgb);
            var startDrawingImagePoint = new Point(0, 0);
            using (var graphic = Graphics.FromImage(canvas))
            {
                var img = new Bitmap(bufferBitmap);
                var monochromed = img.MakeMonochrome();

                graphic.DrawImage(monochromed, startDrawingImagePoint);

                return canvas;
            }
        }

        /// <summary>
        /// Изменяет контрастность изображения в сторону усиления.
        /// Значение на которое производится коррекция: +512 условных единиц
        /// </summary>
        /// <param name="bufferBitmap">Обрабатываемое изображение</param>
        public Bitmap ApplyAutocontrast(Bitmap bufferBitmap)
        {
            var width = bufferBitmap.Width;
            var height = bufferBitmap.Height;

            // Создаём холст
            var canvas = new Bitmap(width, height, PixelFormat.Format24bppRgb);
            var startDrawingImagePoint = new Point(0, 0);
            using (var graphic = Graphics.FromImage(canvas))
            {
                var img = new Bitmap(bufferBitmap);

                var autocontrasted = img.ContrastByCorrection(512);

                graphic.DrawImage(autocontrasted, startDrawingImagePoint);

                return canvas;
            }
        }

        /// <summary>
        /// Применяет медианный фильтр
        /// Радиус фильтра: 2 пикселя
        /// </summary>
        /// <param name="bufferBitmap">Обрабатываемое изображение</param>
        public Bitmap ApplyMedianFilter(Bitmap bufferBitmap)
        {
            var width = bufferBitmap.Width;
            var height = bufferBitmap.Height;

            // Создаём холст
            var canvas = new Bitmap(width, height, PixelFormat.Format24bppRgb);
            var startDrawingImagePoint = new Point(0, 0);
            using (var graphic = Graphics.FromImage(canvas))
            {
                var img = new Bitmap(bufferBitmap);

                var filtered = img.MedianFilter(2);

                graphic.DrawImage(filtered, startDrawingImagePoint);

                return canvas;
            }
        }

        /// <summary>
        /// Получение координат центров меток позиционирования QR-кода
        /// </summary>
        /// <param name="bitmap"></param>
        /// <param name="makesMonoChrome"></param>
        /// <returns></returns>
        public QrMainFeaturesFinderPatternInfo GetQrMainFeaturesPoints(Bitmap bitmap, bool makesMonoChrome = true)
        {
            var featureCoordinates = new GetQrCodeMainFeaturesCoordinatesQuery().Execute(bitmap, true, makesMonoChrome);
            return featureCoordinates;
        }

        /// <summary>
        /// Чтение QR-кода
        /// </summary>
        /// <param name="bitmap">Изображение на котором производится поиск QR-кода</param>
        /// <param name="makesMonoChrome"></param>
        /// <returns></returns>
        public string ReadQrCodeData(Bitmap bitmap, bool makesMonoChrome = true)
        {
            var qrCodeOpenCvReader = new DecodeQrCodeQuery();
            return qrCodeOpenCvReader.Execute(bitmap, makesMonoChrome);
        }

        /// <summary>
        /// Вырезает QR-код
        /// </summary>
        /// <param name="featureCoordinates">Координаты центров меток позиционирования QR-кода</param>
        /// <param name="imgWithCode">Изображение с кодом</param>
        /// <returns></returns>
        public QrCodeMeta QrCodeCut(QrMainFeaturesFinderPatternInfo featureCoordinates, Bitmap imgWithCode)
        {
            var width = (int)(Math.Abs(featureCoordinates.TopRight.X - featureCoordinates.TopLeft.X) + 40);
            var heght = (int)(Math.Abs(featureCoordinates.BottomLeft.Y - featureCoordinates.TopLeft.Y) + 40);
            var startX = (int)featureCoordinates.TopLeft.X - 20;
            var startY = (int)featureCoordinates.TopLeft.Y - 20;

            Rectangle bordersRect = new Rectangle(startX, startY, width, heght);
            var qrCodeCut = imgWithCode.Copy(bordersRect);

            var borders = new Borders(bordersRect.X, bordersRect.Y, bordersRect.Width, bordersRect.Height);

            return new QrCodeMeta{
                QrCodeImage = qrCodeCut,
                Borders = borders
            };
        }

        /// <summary>
        /// Вырезает QR-код
        /// </summary>
        /// <param name="area">Координаты рамки вокруг QR-кода</param>
        /// <param name="imgWithCode">Изображение с кодом</param>
        /// <returns></returns>
        public QrCodeMeta QrCodeCut(Rectangle area, Bitmap imgWithCode)
        {
            Rectangle bordersRect = area;
            var qrCodeCut = imgWithCode.Copy(bordersRect);

            var borders = new Borders(bordersRect.X, bordersRect.Y, bordersRect.Width, bordersRect.Height);

            return new QrCodeMeta
            {
                QrCodeImage = qrCodeCut,
                Borders = borders
            };
        }

        /// <summary>
        /// Обводка рамкой QR-кода
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public OutlineImage OutlineQrCodeOnImage(Bitmap image)
        {
            if (QrCodeModel == null)
                throw new ArgumentException(nameof(QrCodeModel));

            if (QrCodeModel.QrCodeBorders == null)
                throw new ArgumentException(nameof(QrCodeModel.QrCodeBorders));

            var offset = 30;

            var outline = new Rectangle(
                (int)QrCodeModel.QrCodeBorders.TopLeft.X - offset,
                (int)QrCodeModel.QrCodeBorders.TopLeft.Y - offset, 
                (int)QrCodeModel.QrCodeBorders.Width + offset * 2,
                (int)QrCodeModel.QrCodeBorders.Height + offset * 2);

            var bordersOverQrCode = new Borders(outline.X, outline.Y, outline.Width, outline.Height);

            using (var graphic = Graphics.FromImage(image))
            {
                graphic.DrawRectangle(new Pen(Color.Black, 5), outline);
                graphic.Save();
            }

            return new OutlineImage {
                Borders = outline,
                OutlinedImage = image,
                BordersOverQrCode = bordersOverQrCode
            };
        }

        public class OutlineImage
        {
            public Rectangle Borders { get; set; }

            public Borders BordersOverQrCode { get; set; }

            public Bitmap OutlinedImage { get; set; }

            public static OutlineImage Empty => new OutlineImage{ Borders = Rectangle.Empty };
        }
    }
}
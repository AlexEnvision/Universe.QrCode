﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;
using System.Drawing;
using System.Windows.Forms;

namespace Universe.Framework.QR.FormsApp.DecodedQRCodeUI
{
    public partial class DecodedQrCodeFormDialog : Form
    {
        public QrCodeFormModel QrCodeModel { get; set; }

        public DecodedQrCodeFormDialog()
        {
            QrCodeModel = new QrCodeFormModel();

            InitializeComponent();
        }

        public DecodedQrCodeFormDialog(QrCodeFormModel qrCodeModel)
        {
            QrCodeModel = qrCodeModel;

            InitializeComponent();
        }

        private void btOK_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void DecodedQrCodeForm_Load(object sender, EventArgs e)
        {
            LoadOnForm();
        }

        public void LoadOnForm()
        {
            tbCodeAsBase64String.Text = QrCodeModel.CodeAsBase64String;
            tbCodeAsStringWithSplitters.Text = QrCodeModel.CodeAsStringWithSplitters;

            lbTopLeft.Text = QrCodeModel.QrMainFeaturesFinderPatternInfo.TopLeft.ToString();
            lbTopRight.Text = QrCodeModel.QrMainFeaturesFinderPatternInfo.TopRight.ToString();
            lbBottonLeft.Text = QrCodeModel.QrMainFeaturesFinderPatternInfo.BottomLeft.ToString();

            pbQrCodeImage.Image = QrCodeModel.QrCodePicture;
        }

        /// <summary>
        /// Сброс области отображения QR-кода
        /// </summary>
        /// <param name="sender">Отправитель события</param>
        /// <param name="e">Аргументы события</param>
        private void resetZoomToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pbQrCodeImage.SetZoomScale(1.0, Point.Empty);
            pbQrCodeImage.HorizontalScrollBar.Value = 0;
            pbQrCodeImage.VerticalScrollBar.Value = 0;
        }

        /// <summary>
        /// Копирование изображения кода в буфер обмена
        /// </summary>
        /// <param name="sender">Отправитель события</param>
        /// <param name="e">Аргументы события</param>
        private void copyToClipboardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var img = pbQrCodeImage.Image;
            Clipboard.SetImage(img);
        }
    }
}
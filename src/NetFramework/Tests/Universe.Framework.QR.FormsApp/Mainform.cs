﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Universe.Framework.QR.FormsApp.DataContracts;
using Universe.Framework.QR.FormsApp.DecodedQRCodeUI;
using Universe.Framework.QR.FormsApp.EncodingQRCodeUI;
using Universe.Image;
using Universe.QrCode.Perseverance.Models;
using Universe.QrCode.Perseverance.QrCodeSearching;
using Universe.QrCode.QRFeatures;
using Universe.QrCode.Types.GraphicTypes;
using Universe.QrCode.Types.GraphicTypes.Extensions;
using Universe.Types.Event;
using Universe.Windows.Forms.Controls;

namespace Universe.Framework.QR.FormsApp
{
    public partial class Mainform : Form
    {
        /// <summary>
        /// Слой-исходник
        /// </summary>
        protected Bitmap SourceLayoutBitmap;

        /// <summary>
        /// Операционный слой.
        /// Отображаемый. На нем отражаются все операции
        /// </summary>
        protected Bitmap OperationLayoutBitmap;

        /// <summary>
        /// Слой под фрагмент изображения для последующего наложения
        /// </summary>
        protected Bitmap ImageFragmentLayoutBitmap;

        private FileType _fileType;

        private DocumentViewState _documentFormState;

        private PdfViewer _pdfViewer;

        private QrCodeSeeker _qrCodeSeeker;

        private Rectangle _outlineBorder;

        private bool _manualOutlineBorderPaint;

        /// <summary>
        /// Отредактированные страницы PDF-документа
        /// </summary>
        protected Dictionary<int, EditedPage> EditedPages;

        public Mainform()
        {
            InitializeComponent();

            pbDocumentPage.AllowDrop = true;

            Initialize();
        }

        private void Initialize()
        {
            _documentFormState = new DocumentViewState();
            _manualOutlineBorderPaint = false;
            _outlineBorder = Rectangle.Empty;
            _qrCodeSeeker = new QrCodeSeeker();
            EditedPages = new Dictionary<int, EditedPage>();
        }

        private void openDocumentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFile();
        }

        private void btLoadImage_Click(object sender, EventArgs e)
        {
            OpenFile();
        }

        private void pbDocumentPage_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop, false))
            {
                e.Effect = DragDropEffects.All;
            }
        }

        private void pbDocumentPage_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            var firstfile = files[0];

            OpenFile(firstfile);
        }

        private void btDecode_Click(object sender, EventArgs e)
        {
            try
            {
                Decode();
            }
            catch (Exception ex)
            {
                WriteErrorToLog(ex);
            }
        }

        /// <summary>
        /// Сформировать QR-код
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void createQrCodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                using (var decodedQrCodeFormDialog = new EncodingQrCodeFormDialog())
                {
                    decodedQrCodeFormDialog.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                WriteErrorToLog(ex);
            }
        }

        /// <summary>
        /// Прочитать QR-код
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btScanCodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Decode();
            }
            catch (Exception ex)
            {
                WriteErrorToLog(ex);
            }
        }

        private void btOutLineCode_Click(object sender, EventArgs e)
        {
            OutlineAroundCodeRendering();
        }

        /// <summary>
        /// Обвести рамкой QR-код
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btOutLineCodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OutlineAroundCodeRendering();
        }

        private void cbAutoContrast_CheckedChanged(object sender, EventArgs e)
        {
            if (!cbAutoContrast.Checked)
                return;

            if (OperationLayoutBitmap == null)
                return;

            // Если рамки нет - применяем автоконтрастность по всему изображению
            // Если есть, то только к участку изображения c QR-кодом
            if (_documentFormState.OutlineQrCodeBorder == null)
            {
                ApplyLayoutAutocontrast(OperationLayoutBitmap);
            }
            else
            {
                ImageFragmentLayoutBitmap = ApplyAutocontrast(ImageFragmentLayoutBitmap);
                DrawFragment(_documentFormState.OutlineQrCodeBorder, ImageFragmentLayoutBitmap, OperationLayoutBitmap);
            }

            pbDocumentPage.Image = OperationLayoutBitmap;
        }

        private void cbShowBinarized_CheckedChanged(object sender, EventArgs e)
        {
            if (!cbShowBinarized.Checked)
                return;

            if (OperationLayoutBitmap == null)
                return;

            // Если рамки нет - применяем бинаризацию по всему изображению
            // Если есть, то только к участку изображения c QR-кодом
            if (_documentFormState.OutlineQrCodeBorder == null)
            {
                MakesLayoutMonoChrome(OperationLayoutBitmap);
            }
            else
            {
                ImageFragmentLayoutBitmap = MakesMonoChrome(ImageFragmentLayoutBitmap);
                DrawFragment(_documentFormState.OutlineQrCodeBorder, ImageFragmentLayoutBitmap, OperationLayoutBitmap);
            }

            pbDocumentPage.Image = OperationLayoutBitmap;
        }


        private void cbMedianFilter_CheckedChanged(object sender, EventArgs e)
        {
            if (!cbMedianFilter.Checked)
                return;

            if (OperationLayoutBitmap == null)
                return;

            // Если рамки нет - применяем медианный фильтр по всему изображению
            // Если есть, то только к участку изображения c QR-кодом
            if (_documentFormState.OutlineQrCodeBorder == null)
            {
                ApplyMedianFilter(OperationLayoutBitmap);
            }
            else
            {
                ImageFragmentLayoutBitmap = ApplyMedianFilter(ImageFragmentLayoutBitmap);
                DrawFragment(_documentFormState.OutlineQrCodeBorder, ImageFragmentLayoutBitmap, OperationLayoutBitmap);
            }

            pbDocumentPage.Image = OperationLayoutBitmap;
        }

        private void btPrvPage_Click(object sender, EventArgs e)
        {
            try
            {
                var prevPage = _pdfViewer.PreviousPageAsImg(_documentFormState);
                SourceLayoutBitmap = prevPage.PageAsBitmap;
                OperationLayoutBitmap = SourceLayoutBitmap.FullCopy();

                Preprocessing();
                pbDocumentPage.Image = OperationLayoutBitmap;

                // Сбрасываем модель распознанного QR-кода
                _qrCodeSeeker.QrCodeModel = null;
                _documentFormState.OutlineQrCodeBorder = null;
            }
            catch (Exception ex)
            {
                WriteErrorToLog(ex);
            }
        }

        private void btNextPage_Click(object sender, EventArgs e)
        {
            try
            {
                var nextPage = _pdfViewer.NextPageAsImg(_documentFormState);
                SourceLayoutBitmap = nextPage.PageAsBitmap;
                OperationLayoutBitmap = SourceLayoutBitmap.FullCopy();

                Preprocessing();
                pbDocumentPage.Image = OperationLayoutBitmap;

                // Сбрасываем модель распознанного QR-кода
                _qrCodeSeeker.QrCodeModel = null;
                _documentFormState.OutlineQrCodeBorder = null;
            }
            catch (Exception ex)
            {
                WriteErrorToLog(ex);
            }
        }

        private void pbDocumentPage_Paint(object sender, PaintEventArgs e)
        {
            if (!_manualOutlineBorderPaint)
                return;

            e.Graphics.DrawRectangle(Pens.Black, _outlineBorder);
        }

        private void pbDocumentPage_MouseDown(object sender, MouseEventArgs e)
        {
            if (OperationLayoutBitmap == null)
                return;

            if (!_manualOutlineBorderPaint)
                return;

            _outlineBorder = new Rectangle(0, 0, 0, 0);

            if (e.Button != MouseButtons.Left)
                return;

            var zoomScale = pbDocumentPage.ZoomScale;
            var horizontalScrollbar = pbDocumentPage.HorizontalScrollBar;
            var verticalScrollbar = pbDocumentPage.VerticalScrollBar;

            _outlineBorder.X = (int)(Convert.ToInt32(e.X) / zoomScale) + horizontalScrollbar.Value;
            _outlineBorder.Y = (int)(Convert.ToInt32(e.Y) / zoomScale) + verticalScrollbar.Value;

            pbDocumentPage.Invalidate();
        }

        private void pbDocumentPage_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (OperationLayoutBitmap == null)
                    return;

                if (!_manualOutlineBorderPaint)
                    return;

                if (e.Button != MouseButtons.Left)
                    return;

                var zoomScale = pbDocumentPage.ZoomScale;
                var horizontalScrollbar = pbDocumentPage.HorizontalScrollBar;
                var verticalScrollbar = pbDocumentPage.VerticalScrollBar;

                _outlineBorder.Width = -_outlineBorder.X + (int)(Convert.ToInt32(e.X) / zoomScale) + horizontalScrollbar.Value;
                _outlineBorder.Height = -_outlineBorder.Y + (int)(Convert.ToInt32(e.Y) / zoomScale) + verticalScrollbar.Value;

                pbDocumentPage.Invalidate();
            }
            catch (InvalidOperationException ex)
            {
                WriteErrorToLog(ex);
            }
        }

        private void pbDocumentPage_MouseUp(object sender, MouseEventArgs e)
        {
            if (OperationLayoutBitmap == null)
                return;

            if (!_manualOutlineBorderPaint)
                return;

            if (e.Button != MouseButtons.Left)
                return;

            var zoomScale = pbDocumentPage.ZoomScale;
            var horizontalScrollbar = pbDocumentPage.HorizontalScrollBar;
            var verticalScrollbar = pbDocumentPage.VerticalScrollBar;

            _outlineBorder.Width = -_outlineBorder.X + (int)(Convert.ToInt32(e.X) / zoomScale) + horizontalScrollbar.Value;
            _outlineBorder.Height = -_outlineBorder.Y + (int)(Convert.ToInt32(e.Y) / zoomScale) + verticalScrollbar.Value;

            using (var dialog = new PrintWithoutFilters())
            {
                var transferedBorder = new Borders(_outlineBorder.X, _outlineBorder.Y, _outlineBorder.Width, _outlineBorder.Height);
                var outlineBorderCorr = FromImageBoxToSurfaceImage(
                    pbDocumentPage.Image,
                    transferedBorder,
                    pbDocumentPage.Width,
                    pbDocumentPage.Height);

                var spacePoints = new[]
                    {outlineBorderCorr.TopLeft, outlineBorderCorr.TopRight, outlineBorderCorr.BottomRight, outlineBorderCorr.BottomLeft};

                var points = new Point[spacePoints.Length];
                for (var index = 0; index < spacePoints.Length; index++)
                    points[index] = new Point((int)spacePoints[index].X, (int)spacePoints[index].Y);

                if (dialog.ShowDialog() == DialogResult.Yes)
                {
                    OperationLayoutBitmap = SourceLayoutBitmap.FullCopy();
                    pbDocumentPage.Image = OperationLayoutBitmap;

                    using (var graphic = Graphics.FromImage(OperationLayoutBitmap))
                    {
                        graphic.DrawPolygon(new Pen(Color.Black, 5), points);
                    }

                    UncheckPostprocessingFlags();
                }
                else
                {
                    using (var graphic = Graphics.FromImage(OperationLayoutBitmap))
                    {
                        graphic.DrawPolygon(new Pen(Color.Black, 5), points);
                    }
                }

                _outlineBorder = new Rectangle((int)outlineBorderCorr.X, (int)outlineBorderCorr.Y, (int)outlineBorderCorr.Width, (int)outlineBorderCorr.Height);
                pbDocumentPage.Invalidate();

                _documentFormState.OutlineQrCodeBorder = outlineBorderCorr;
                // Вырезаем изображение и копируем на слой для фрагментов
                CutFragmentIfOutlineQrCodeBorderIsntNull();

                // Очищаем прямоугольную область т.к она нужна лишь для операций визуализации 
                // и формирования основы для отрисовки, в дальнейшем не должна использоватся
                _outlineBorder = Rectangle.Empty;
                PrepareForSave();
            }
        }

        private void cancelDrawingBorderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CancelOutlineDrawing();
        }

        private void saveResultsAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var dialog = new SaveFileDialog())
            {
                try
                {
                    dialog.Filter = @"PDF-documents(*.pdf)|*.pdf";
                    if (dialog.ShowDialog() == DialogResult.OK)
                    {
                        SaveFileOperation(_documentFormState.DocumentPath, dialog.FileName);
                    }
                }
                catch (Exception ex)
                {
                    WriteErrorToLog(ex);
                }
            }
        }

        private void saveResultToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                var tempAppFolder = Directory.GetCurrentDirectory() + "\\TemporaryAppFiles\\";
                if (!Directory.Exists(tempAppFolder))
                    Directory.CreateDirectory(tempAppFolder);

                var fileInfo = new FileInfo(_documentFormState.DocumentPath);
                var temporaryFilePath = $"{tempAppFolder}{fileInfo.Name}{fileInfo.Extension}";
                File.Copy(fileInfo.FullName, temporaryFilePath, true);

                SaveFileOperation(temporaryFilePath, _documentFormState.DocumentPath);
            }
            catch (Exception ex)
            {
                WriteErrorToLog(ex);
            }
        }

        private void saveCurrentPageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var dialog = new SaveFileDialog())
            {
                dialog.Filter = @"Images(*.bmp)|*.bmp";
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    OperationLayoutBitmap.Save(dialog.FileName);
                }
            }
        }

        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pbDocumentPage.SetZoomScale(1.0, Point.Empty);
            pbDocumentPage.HorizontalScrollBar.Value = 0;
            pbDocumentPage.VerticalScrollBar.Value = 0;

            CancelOutlineDrawing();
        }

        private void resetFiltersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OperationLayoutBitmap = SourceLayoutBitmap.FullCopy();
            pbDocumentPage.Image = OperationLayoutBitmap;

            // Обнуляем модель QR-кода
            _qrCodeSeeker.QrCodeModel = null;

            DrawOutlineIfOutlineQrCodeBorderIsntNull();
            CutFragmentIfOutlineQrCodeBorderIsntNull();

            UncheckPostprocessingFlags();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
       
        /// <summary>
        /// Выполняет операцию открытия файла
        /// </summary>
        private void OpenFile()
        {
            using (OpenFileDialog dialog = new OpenFileDialog())
            {
                dialog.Filter = @"PDF-documents(*.pdf)|*.pdf|Images(*.bmp)|*.bmp";
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    var imagePath = dialog.FileName;

                    OpenFile(imagePath);
                }
            }
        }

        /// <summary>
        /// Выполняет операцию открытия файла
        /// </summary>
        /// <param name="imagePath"></param>
        private void OpenFile(string imagePath)
        {
            Initialize();

            var fileExtension = Path.GetExtension(imagePath);
            switch (fileExtension)
            {
                case ".bmp":
                    _fileType = FileType.Bmp;
                    break;
                case ".pdf":
                    _fileType = FileType.Pdf;
                    break;

                default:
                    _fileType = FileType.NotSupported;
                    break;
            }

            if (_fileType == FileType.Bmp)
            {
                SourceLayoutBitmap = new Bitmap(imagePath ?? throw new ArgumentNullException(nameof(imagePath)));
                OperationLayoutBitmap = SourceLayoutBitmap.FullCopy();

                Preprocessing();
                pbDocumentPage.Image = OperationLayoutBitmap;
            }

            if (_fileType == FileType.Pdf)
            {
                _documentFormState = new DocumentViewState
                {
                    DocumentPath = imagePath,
                    CurrentPage = 0
                };
                _pdfViewer = new PdfViewer();
                _pdfViewer.LogInfo += WriteToLog;
                _pdfViewer.LogError += WriteErrorToLog;
                _pdfViewer.Initialize(_documentFormState);

                var firstPage = _pdfViewer.NextPageAsImg(_documentFormState);

                SourceLayoutBitmap = firstPage.PageAsBitmap;
                OperationLayoutBitmap = SourceLayoutBitmap.FullCopy();

                Preprocessing();
                pbDocumentPage.Image = OperationLayoutBitmap;
            }

            btNextPage.Enabled = _fileType == FileType.Pdf;
            btPrvPage.Enabled = _fileType == FileType.Pdf;
        }

        /// <summary>
        /// Сохранение файла
        /// </summary>
        /// <param name="sourceFileName"></param>
        /// <param name="destFileName"></param>
        private void SaveFileOperation(string sourceFileName, string destFileName)
        {
            for (int i = 1; i <= _pdfViewer.TotalPages; i++)
            {
                if (!EditedPages.ContainsKey(i))
                {
                    var page = _pdfViewer.OpenDocumentAndGetPage(sourceFileName, i);
                    EditedPages.Add(i, new EditedPage
                    {
                        PageNumber = i,
                        EditedPageAsBitmapForSaving = page.PageAsBitmap
                    });
                }
            }

            var ordered = EditedPages.Values.ToList().OrderBy(x => x.PageNumber).ToList();

            _pdfViewer.SavePages(destFileName, ordered);
            EditedPages = new Dictionary<int, EditedPage>(); // TODO -> DisposeAndClear
        }

        /// <summary>
        /// Рисует рамку, если в объекте состояния есть границы, на OperationBufferBitmap
        /// </summary>
        private void DrawOutlineIfOutlineQrCodeBorderIsntNull()
        {
            if (_documentFormState.OutlineQrCodeBorder != null)
            {
                var outlineQrCodeBorderRectangle = _documentFormState.OutlineQrCodeBorder.ToRectangle();
                using (var graphic = Graphics.FromImage(OperationLayoutBitmap))
                {
                    graphic.DrawRectangle(new Pen(Color.Black, 5), outlineQrCodeBorderRectangle);
                }
            }
        }

        /// <summary>
        /// Вырезает фрагмент изображения на отдельный слой, если в объекте состояния есть границы, на OperationBufferBitmap
        /// </summary>
        private void CutFragmentIfOutlineQrCodeBorderIsntNull()
        {
            if (_documentFormState.OutlineQrCodeBorder != null)
            {
                // Вырезаем изображение и копируем на слой для фрагментов
                ImageFragmentLayoutBitmap = _qrCodeSeeker
                    .QrCodeCut(_documentFormState.OutlineQrCodeBorder.ToRectangle(), SourceLayoutBitmap.FullCopy()).QrCodeImage;
            }
        }

        /// <summary>
        /// Рендерит изображение на OperationBufferBitmap
        /// </summary>
        /// <param name="borders">Границы изображения</param>
        /// <param name="fragment">Рендерещийся фрагмент</param>
        /// <param name="canvas">Холст</param>
        private void DrawFragment(Borders borders, Bitmap fragment, Bitmap canvas)
        {
            if (_documentFormState.OutlineQrCodeBorder != null)
            {
                var outlineQrCodeBorderRectangle = borders.ToRectangle();
                using (var graphic = Graphics.FromImage(canvas))
                {
                    graphic.DrawImage(fragment, outlineQrCodeBorderRectangle);
                }
            }
        }

        /// <summary>
        /// Предообработка
        /// </summary>
        private void Preprocessing()
        {
            if (cbAutoContrast.Checked)
                ApplyLayoutAutocontrast(OperationLayoutBitmap);

            if (cbShowBinarized.Checked)
                MakesLayoutMonoChrome(OperationLayoutBitmap);
        }

        /// <summary>
        /// Предообработка
        /// </summary>
        private Bitmap Preprocessing(Bitmap bufferBitmap, bool ignoreUserSettings = false)
        {
            if (cbAutoContrast.Checked || ignoreUserSettings)
                ApplyLayoutAutocontrast(bufferBitmap);

            if (cbShowBinarized.Checked || ignoreUserSettings)
                MakesLayoutMonoChrome(bufferBitmap);

            return bufferBitmap;
        }

        /// <summary>
        /// Бинаризация
        /// Делает изображение монохромным и выводит в операционный слой
        /// </summary>
        /// <param name="bufferBitmap"></param>
        public void MakesLayoutMonoChrome(Bitmap bufferBitmap)
        {           
            OperationLayoutBitmap = _qrCodeSeeker.MakeMonoChrome(bufferBitmap);
        }

        /// <summary>
        /// Изменяет контрастность изображения в сторону усиления и выводит в операционный слой
        /// Значение на которое производится коррекция: +512 условных единиц
        /// </summary>
        /// <param name="bufferBitmap">Обрабатываемое изображение</param>
        public void ApplyLayoutAutocontrast(Bitmap bufferBitmap)
        {
            OperationLayoutBitmap = _qrCodeSeeker.ApplyAutocontrast(bufferBitmap);
        }

        /// <summary>
        /// Применяет медианный фильтр и выводит в операционный слой
        /// Радиус фильтра: 3 пикселя
        /// </summary>
        /// <param name="bufferBitmap">Обрабатываемое изображение</param>
        public void ApplyLayoutMedianFilter(Bitmap bufferBitmap)
        {
            OperationLayoutBitmap = _qrCodeSeeker.ApplyMedianFilter(bufferBitmap);
        }

        /// <summary>
        /// Бинаризация
        /// Делает изображение монохромным
        /// </summary>
        /// <param name="bufferBitmap"></param>
        public Bitmap MakesMonoChrome(Bitmap bufferBitmap)
        {
            return _qrCodeSeeker.MakeMonoChrome(bufferBitmap);
        }

        /// <summary>
        /// Изменяет контрастность изображения в сторону усиления.
        /// Значение на которое производится коррекция: +512 условных единиц
        /// </summary>
        /// <param name="bufferBitmap">Обрабатываемое изображение</param>
        public Bitmap ApplyAutocontrast(Bitmap bufferBitmap)
        {
            return _qrCodeSeeker.ApplyAutocontrast(bufferBitmap);
        }

        /// <summary>
        /// Применяет медианный фильтр
        /// Радиус фильтра: 2 пикселя
        /// </summary>
        /// <param name="bufferBitmap">Обрабатываемое изображение</param>
        public Bitmap ApplyMedianFilter(Bitmap bufferBitmap)
        {
            return _qrCodeSeeker.ApplyMedianFilter(bufferBitmap);
        }

        /// <summary>
        /// Декодирование QR-кода. 
        /// Используется контурный анализ для поиска рамки в которую заключён QR-код
        /// </summary>
        /// <param name="allowReport"></param>
        /// <returns></returns>
        private bool Decode(bool allowReport = true)
        {
            //return Decode(OperationLayoutBitmap, allowReport);

            Bitmap sourceLayoutBitmap = OperationLayoutBitmap.FullCopy();
            Bitmap operationLayoutBitmap = sourceLayoutBitmap.FullCopy();

            var quartusArea = new Rectangle(0, 0, sourceLayoutBitmap.Width, sourceLayoutBitmap.Height / 4);
            var quartusFragment = operationLayoutBitmap.Copy(quartusArea);

            return DecodeAutoWithPerfMetrics(_documentFormState, FileType.Pdf, quartusFragment, allowReport);
        }

        /// <summary>
        /// Декодирование QR-кода. 
        /// Используется контурный анализ для поиска рамки в которую заключён QR-код
        /// </summary>
        /// <param name="image"></param>
        /// <param name="allowReport"></param>
        /// <param name="documentFormState"></param>
        /// <param name="fileType"></param>
        /// <param name="logPrefx"></param>
        /// <returns></returns>
        private bool DecodeAutoWithPerfMetrics(
            DocumentViewState documentFormState,
            FileType fileType,
            Bitmap image,
            bool allowReport = true,
            string logPrefx = "")
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            var result = DecodeAuto(documentFormState, fileType, image, allowReport, logPrefx);

            stopwatch.Stop();
            var elapsed = stopwatch.Elapsed;
            WriteToLog($"Поиск и чтение QR-кода выполнено за {elapsed.Hours}:{elapsed.Minutes}:{elapsed.Seconds}:{elapsed.Milliseconds}.");

            return result;
        }

        /// <summary>
        /// Декодирование QR-кода. 
        /// Используется контурный анализ для поиска рамки в которую заключён QR-код
        /// </summary>
        /// <param name="image"></param>
        /// <param name="allowReport"></param>
        /// <param name="documentFormState"></param>
        /// <param name="fileType"></param>
        /// <param name="logPrefx"></param>
        /// <returns></returns>
        private bool DecodeAuto(
            DocumentViewState documentFormState,
            FileType fileType,
            Bitmap image,
            bool allowReport = true,
            string logPrefx = "")
        {
            var attackDecoder = new AttackQrDecoder();
            attackDecoder.LogInfo += WriteToLog;
            attackDecoder.LogError += WriteErrorToLog;

            QrDecodeResult result = attackDecoder.MakeComboWithFullPage(image);

            if (result.HasFoundCode)
            {
                var featureCoordinatesAsString =
                   $"{result.FeaturesCoordinates.TopLeft}; {result.FeaturesCoordinates.TopRight}; {result.FeaturesCoordinates.BottomLeft}";

                if (fileType == FileType.Pdf)
                    WriteToLog(
                        $"{logPrefx}Найден QR-код на странице {documentFormState.CurrentPage} располагающийся по координатам: {featureCoordinatesAsString}.");

                if (fileType == FileType.Bmp)
                    WriteToLog($"{logPrefx}Найден QR-код располагающийся по координатам: {featureCoordinatesAsString}.");


                if (result.HasReadCode)
                {
                    QrCodeReadingAndPrintInformatiom(
                        result,
                        "DocumentPage",
                        result.QrCodeMeta,
                        allowReport,
                        logPrefx);
                }
                else
                {
                    WriteToLog($"{logPrefx}QR-код был обнаружен, но не удалось прочитать из него информацию.");
                }
            }
            else
            {
                if (fileType == FileType.Bmp)
                    WriteToLog($"{logPrefx}На изображении не распознано/не найдено ни одного QR-кода!");

                if (fileType == FileType.Pdf)
                    WriteToLog(
                        $"{logPrefx}На странице документа {documentFormState.CurrentPage} не распознано/не найдено ни одного QR-кода!");
            }

            return result.HasFoundCode;
        }

        /// <summary>
        /// Считывает QR-код и выводит из него информацию
        /// </summary>
        /// <param name="result">Данные из QR-кода</param>
        /// <param name="areaName">Название области</param>
        /// <param name="qrCodeCut">Вырезанный QR-код</param>
        /// <param name="allowReport">Разрешить вывод информации о QR-коде</param>
        /// <param name="logPrefx">Сообщение - префикс для уточнения категории в логах</param>
        private void QrCodeReadingAndPrintInformatiom(
            QrDecodeResult result,
            string areaName,
            QrCodeMeta qrCodeCut,
            bool allowReport = true,
            string logPrefx = "")
        {
            if (result.HasReadCode)
            {
                var qrCodeModel = DecodeToQrCodeFormModel(
                    areaName,
                    result.FeaturesCoordinates,
                    qrCodeCut,
                    result.QrCodeText);

                _qrCodeSeeker.QrCodeModel = qrCodeModel;

                if (!allowReport)
                    return;

                using (var decodedQrCodeFormDialog = new DecodedQrCodeFormDialog())
                {
                    decodedQrCodeFormDialog.QrCodeModel = qrCodeModel;
                    decodedQrCodeFormDialog.ShowDialog();
                }
            }
            else
            {
                WriteToLog($"{logPrefx}QR-код был обнаружен, но не удалось прочитать из него информацию.");
                // Если QR-код обнаружен записываем информацию без дешифровки и естественно не выводим её на форму
                var qrCodeModel = new QrCodeFormModel
                {
                    ImageAreaName = areaName,
                    CodeAsBase64String = result.QrCodeText,
                    CodeAsStringWithSplitters = string.Empty,
                    QrCodePicture = qrCodeCut.QrCodeImage,
                    QrCodeBorders = qrCodeCut.Borders,
                    QrMainFeaturesFinderPatternInfo = result.FeaturesCoordinates
                };
                _qrCodeSeeker.QrCodeModel = qrCodeModel;
            }
        }

        /// <summary>
        /// Декодирование QR-кода
        /// </summary>
        /// <param name="image">Изображение</param>
        /// <param name="allowReport">Разрешить вывод информации о QR-коде</param>
        /// <param name="logPrefx">Сообщение - префикс для уточнения категории в логах</param>
        /// <returns></returns>
        private bool Decode(Bitmap image, bool allowReport = true, string logPrefx = "")
        {
            var hasFoundCode = false;

            var featureCoordinates = _qrCodeSeeker.GetQrMainFeaturesPoints(image, false); // Отключено автоприменение монохромности по-умолчанию
            if (featureCoordinates != null)
            {
                var featureCoordinatesAsString =
                    $"{featureCoordinates.TopLeft}; {featureCoordinates.TopRight}; {featureCoordinates.BottomLeft}";

                if (_fileType == FileType.Pdf)
                    WriteToLog(
                        $"{logPrefx}Найден QR-код на странице {_documentFormState.CurrentPage} располагающийся по координатам: {featureCoordinatesAsString}.");

                if (_fileType == FileType.Bmp)
                    WriteToLog($"{logPrefx}Найден QR-код располагающийся по координатам: {featureCoordinatesAsString}.");

                var qrCodeCut = _qrCodeSeeker.QrCodeCut(featureCoordinates, image);

                QrCodeReadingAndPrintInformatiom(image, 
                    "DocumentPage", 
                    featureCoordinates,
                    qrCodeCut,
                    allowReport,
                    logPrefx);
                hasFoundCode = true;
            }
            else
            {
                if (_fileType == FileType.Bmp)
                    WriteToLog($"{logPrefx}На изображении не распознано/не найдено ни одного QR-кода!");

                if (_fileType == FileType.Pdf)
                    WriteToLog(
                        $"{logPrefx}На странице документа {_documentFormState.CurrentPage} не распознано/не найдено ни одного QR-кода!");
            }

            return hasFoundCode;
        }

        /// <summary>
        /// Считывает QR-код и выводит из него информацию
        /// </summary>
        /// <param name="researchingFragment">Исследуемый фрагмент</param>
        /// <param name="areaName">Название области</param>
        /// <param name="featureCoordinates">Координаты признака</param>
        /// <param name="qrCodeCut">Вырезанный QR-код</param>
        /// <param name="allowReport">Разрешить вывод информации о QR-коде</param>
        /// <param name="logPrefx">Сообщение - префикс для уточнения категории в логах</param>
        private void QrCodeReadingAndPrintInformatiom(
            Bitmap researchingFragment,
            string areaName,
            QrMainFeaturesFinderPatternInfo featureCoordinates,
            QrCodeMeta qrCodeCut,
            bool allowReport = true,
            string logPrefx = "")
        {
            var qrCodeText = _qrCodeSeeker.ReadQrCodeData(researchingFragment, false); // Отключено автоприменение монохромности по-умолчанию
            if (!string.IsNullOrEmpty(qrCodeText))
            {
                var qrCodeModel = DecodeToQrCodeFormModel(
                    areaName, 
                    featureCoordinates,
                    qrCodeCut,
                    qrCodeText);

                _qrCodeSeeker.QrCodeModel = qrCodeModel;

                if(!allowReport)
                    return;

                using (var decodedQrCodeFormDialog = new DecodedQrCodeFormDialog())
                {
                    decodedQrCodeFormDialog.QrCodeModel = qrCodeModel;
                    decodedQrCodeFormDialog.ShowDialog();
                }
            }
            else
            {
                WriteToLog($"{logPrefx}QR-код был обнаружен, но не удалось прочитать из него информацию.");
                // Если QR-код обнаружен записываем информацию без дешифровки и естественно не выводим её на форму
                var qrCodeModel = new QrCodeFormModel{
                    ImageAreaName = areaName,
                    CodeAsBase64String = qrCodeText,
                    CodeAsStringWithSplitters = string.Empty,
                    QrCodePicture = qrCodeCut.QrCodeImage,
                    QrCodeBorders = qrCodeCut.Borders,
                    QrMainFeaturesFinderPatternInfo = featureCoordinates
                };
                _qrCodeSeeker.QrCodeModel = qrCodeModel;
            }
        }

        /// <summary>
        /// Декодирует информацию из QR-кода
        /// </summary>
        /// <param name="areaName"></param>
        /// <param name="featureCoordinates"></param>
        /// <param name="qrCodeCut"></param>
        /// <param name="qrCodeText"></param>
        /// <returns></returns>
        private QrCodeFormModel DecodeToQrCodeFormModel(
            string areaName,
            QrMainFeaturesFinderPatternInfo featureCoordinates, 
            QrCodeMeta qrCodeCut,
            string qrCodeText)
        {
            // Строка декодируемая из QR-кода является Base64 строкой
            var decoded = FromBase64Safe(qrCodeText);

            var qrCodeModel = new QrCodeFormModel
            {
                ImageAreaName = areaName,
                CodeAsBase64String = qrCodeText,
                CodeAsStringWithSplitters = decoded,
                QrCodePicture = qrCodeCut.QrCodeImage,
                QrCodeBorders = qrCodeCut.Borders,
                QrMainFeaturesFinderPatternInfo = featureCoordinates
            };
            return qrCodeModel;
        }

        /// <summary>
        /// Безопасный парсин Base64 строки
        /// </summary>
        /// <param name="base64String">Base64 строка</param>
        /// <returns>
        ///     Распарсенная строка. 
        ///     Если распарсить не удалось - возвращается первоначальное значение
        /// </returns>
        private string FromBase64Safe(string base64String)
        {
            try
            {
                var fromBase64String = Convert.FromBase64String(base64String);
                var decoded = Encoding.UTF8.GetString(fromBase64String);
                return decoded;
            }
            catch (Exception)
            {
                return base64String;
            }          
        }

        private void WriteToLog(string text)
        {
            tbLog.SafeCall(() =>
            {
                var curentDate = DateTime.Now;

                var msg = $"[{curentDate}] {text}";
                tbLog.AppendText(msg + Environment.NewLine);
                //Log.Info(msg);
            });
        }

        private void WriteErrorToLog(Exception ex)
        {
            tbLog.SafeCall(() =>
            {
                var curentDate = DateTime.Now;
                var msg = !ex.Message.Contains("Thread was being aborted")
                    ? $"[{curentDate}] Произошла ошибка: " + ex.Message
                    : $"[{curentDate}] Выполнение операции было прервано пользователем.";
                tbLog.AppendText(msg + Environment.NewLine);
                //Log.Error(ex, msg);
            });
        }

        private void WriteToLog(LogInfoEventArgs e)
        {
            tbLog.SafeCall(() =>
            {
                var curentDate = DateTime.Now;
                var msg = $"[{curentDate}] {e.Message}";
                if (e.AllowedReport)
                    tbLog.AppendText(msg + Environment.NewLine);
                //Log.Info(msg);
            });
        }

        private void WriteErrorToLog(LogErrorEventArgs e)
        {
            tbLog.SafeCall(() =>
            {
                var curentDate = DateTime.Now;
                var msg = $"[{curentDate}] Произошла ошибка: " + e.Ex.Message + Environment.NewLine + "StackTrace:" + e.Ex.StackTrace;
                if (e.AllowReport)
                    tbLog.AppendText(msg + Environment.NewLine);
                //Log.Error(e.Ex, msg);
            });
        }

        /// <summary>
        /// Обрамление QR-кода
        /// </summary>
        private void OutlineAroundCodeRendering()
        {
            if (_qrCodeSeeker == null)
                return;

            pbDocumentPage.PanableAndZoomableEnable = true;

            try
            {
                if (_qrCodeSeeker.QrCodeModel == null)
                {
                    if (!Decode(false))
                    {
                        using (var dialog = new AllowManualOutlineCodeDialog())
                        {
                            if (dialog.ShowDialog() == DialogResult.Yes)
                            {
                                pbDocumentPage.PanableAndZoomableEnable = false;
                                cancelDrawingBorderToolStripMenuItem.Enabled = true;
                                _manualOutlineBorderPaint = true;

                                float k = Math.Min(pbDocumentPage.Image.Width / pbDocumentPage.Width,
                                    pbDocumentPage.Image.Height / pbDocumentPage.Height);
                                pbDocumentPage.SetZoomScale(k, Point.Empty);
                                pbDocumentPage.HorizontalScrollBar.Visible = true;
                                pbDocumentPage.VerticalScrollBar.Visible = true;
                            }
                        }

                        WriteToLog("На изображении нет ни одного QR-кода. Обвести не удалось.");
                        return;
                    }
                }

                var bufferBitmap = OperationLayoutBitmap;

                using (var dialog = new PrintWithoutFilters())
                {
                    if (dialog.ShowDialog() == DialogResult.Yes)
                    {
                        bufferBitmap = SourceLayoutBitmap.FullCopy();
                        UncheckPostprocessingFlags();
                    }
                }

                QrCodeSeeker.OutlineImage result = _qrCodeSeeker.OutlineQrCodeOnImage(bufferBitmap);
                OperationLayoutBitmap = result.OutlinedImage;
                pbDocumentPage.Image = OperationLayoutBitmap;

                _documentFormState.OutlineQrCodeBorder = result.BordersOverQrCode;
                WriteToLog($"По координатам {result.BordersOverQrCode} установлена рамка вокруг QR-кода.");

                // Вырезаем изображение и копируем на слой для фрагментов
                CutFragmentIfOutlineQrCodeBorderIsntNull();

                PrepareForSave();
            }
            catch (Exception ex)
            {
                WriteErrorToLog(ex);
            }
        }

        private void PrepareForSave()
        {
            var editedPage = new EditedPage{
                PageNumber = _documentFormState.CurrentPage,
                EditedPageAsBitmapForSaving = OperationLayoutBitmap.FullCopy()
            };
            if (!EditedPages.ContainsKey(_documentFormState.CurrentPage))
                EditedPages.Add(_documentFormState.CurrentPage, editedPage);
            else
                EditedPages[_documentFormState.CurrentPage] = editedPage;
        }

        private Borders FromImageBoxToSurfaceImage(
            System.Drawing.Image surfaceImage,
            Borders transferedBorder,
            int imageBoxWidth,
            int imageBoxHeight)
        {
            var surfaceImageWidth = surfaceImage.Width;
            var surfaceImageHeight = surfaceImage.Height;

            var startPoint = new Point(0, 0);

            var outlineBorder = new Borders(transferedBorder.X, transferedBorder.Y, transferedBorder.Width, transferedBorder.Height);

            double w = imageBoxWidth;
            double h = imageBoxHeight;
            //float k = (float)Math.Min(surfaceImageWidth / w, surfaceImageHeight / h);
            float kx = (float) (surfaceImageWidth / w);
            float ky = (float) (surfaceImageHeight / h);

            int dx = (int)(Math.Abs(imageBoxWidth - (int)(surfaceImageWidth / kx)) / 2.0);
            int dy = (int)(Math.Abs(imageBoxHeight - (int)(surfaceImageHeight / ky)) / 2.0);

            int ddx = startPoint.X;
            int ddy = startPoint.Y;
            int dddx = (int)(transferedBorder.X * kx);
            int dddy = (int)(transferedBorder.Y * ky);

            outlineBorder = new Borders(dddx - ddx - dx, dddy - ddy - dy, (int)(outlineBorder.Width * kx), (int)(outlineBorder.Height * ky));

            return outlineBorder;
        }

        /// <summary>
        /// Снятие флажков с фильтров постпроцессинга
        /// </summary>
        private void UncheckPostprocessingFlags()
        {
            cbShowBinarized.Checked = false;
            cbAutoContrast.Checked = false;
        }

        /// <summary>
        /// Прерывание операции отрисовки рамки
        /// </summary>
        private void CancelOutlineDrawing()
        {
            pbDocumentPage.PanableAndZoomableEnable = true;
            cancelDrawingBorderToolStripMenuItem.Enabled = false;
            _manualOutlineBorderPaint = false;

            pbDocumentPage.SetZoomScale(1.0, Point.Empty);
            pbDocumentPage.SizeMode = PictureBoxSizeMode.Zoom;             
        }
    }
}
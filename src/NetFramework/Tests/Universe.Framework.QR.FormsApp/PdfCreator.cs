﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.QrCode                                                ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;
using System.Drawing.Imaging;
using System.IO;
using iText.IO.Image;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using Universe.Framework.QR.FormsApp.DataContracts;
using Universe.Types;
using Universe.Types.Event;

namespace Universe.Framework.QR.FormsApp
{
    /// <summary>
    /// Создает pdf-файл и записывает страницы в него
    /// </summary>
    public class PdfCreator : DisposableObject
    {
        public event LogInfoDel LogInfo;

        public event LogErrorDel LogError;

        private int _totalPages;

        private PdfDocument _pdfDocument;

        private Document _document;

        public int TotalPages => _totalPages;

        public void Initialize(DocumentViewState documentViewState, int width, int height)
        {
            var filePath = documentViewState.DocumentPath;
            if (string.IsNullOrWhiteSpace(filePath))
                throw new ArgumentException(nameof(filePath));

            LogInfoMessage($"Created file: {filePath}");

            var pdfDoc = new PdfDocument(new PdfWriter(filePath));
            Document document = new Document(pdfDoc, new PageSize(width, height));

            _pdfDocument = pdfDoc;
            _document = document;
        }

        /// <summary>
        /// Добавление страницы в документ
        /// </summary>
        /// <param name="editedPage"></param>
        public void AddPage(PageModel editedPage)
        {
            if (_document == null)
                return;

            using (var ms = new MemoryStream())
            {
                editedPage.PageAsBitmap.Save(ms, ImageFormat.Bmp);

                var pageAsImage = new iText.Layout.Element.Image(ImageDataFactory.Create(ms.GetBuffer()));
                var paragraph = new Paragraph()
                    .Add(pageAsImage);

                _document.Add(paragraph);
            }
        }

        public void LogInfoMessage(string message)
        {
            LogInfo?.Invoke(new LogInfoEventArgs
            {
                AllowedReport = true,
                Message = message
            });
        }

        public void LogErrorMessage(Exception ex, string message)
        {
            LogError?.Invoke(new LogErrorEventArgs
            {
                Message = message,
                AllowReport = true,
                Ex = ex
            });
        }

        protected override void Dispose(bool disposing)
        {
            _document.Close();

            ((IDisposable)_pdfDocument)?.Dispose();
            ((IDisposable)_document)?.Dispose();
        }
    }
}

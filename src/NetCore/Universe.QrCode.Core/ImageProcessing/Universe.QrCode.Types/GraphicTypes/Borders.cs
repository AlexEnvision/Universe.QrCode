﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

namespace Universe.QrCode.Types.GraphicTypes
{
    /// <summary>
    /// Границы
    /// </summary>
    public class Borders
    {
        /// <summary>
        /// Стартовая точка
        /// </summary>
        public SpacePoint StartPoint { get; protected set; }

        /// <summary>
        /// Координата X стартовой точки
        /// </summary>
        public long X => StartPoint.X;

        /// <summary>
        /// Координата Y стартовой точки
        /// </summary>
        public long Y => StartPoint.Y;

        /// <summary>
        /// Ширина
        /// </summary>
        public long Width { get; protected set; }

        /// <summary>
        /// Высота
        /// </summary>
        public long Height { get; protected set; }

        /// <summary>
        /// Левая верхняя точка
        /// </summary>
        public SpacePoint TopLeft => StartPoint;

        /// <summary>
        /// Правая верхняя точка
        /// </summary>
        public SpacePoint TopRight => new SpacePoint(StartPoint.X + Width, StartPoint.Y);

        /// <summary>
        /// Левая нижняя точка
        /// </summary>
        public SpacePoint BottomLeft => new SpacePoint(StartPoint.X, StartPoint.Y + Height);

        /// <summary>
        /// Правая нижняя точка
        /// </summary>
        public SpacePoint BottomRight => new SpacePoint(StartPoint.X + Width, StartPoint.Y + Height);

        /// <summary>
        ///  Получает координату x левого верхнего угла прямоугольной области, определенной этим.
        /// </summary>
        public long Left => X;

        /// <summary>
        /// Получает y-координату левого верхнего угла прямоугольной области, определяемой этим.
        /// </summary>
        public long Top => Y;

        /// <summary>
        /// Получает координату x правого нижнего угла прямоугольной области, определенной этим.
        /// </summary>
        public long Right => X + Width;

        /// <summary>
        ///  Получает y-координату нижнего правого угла прямоугольной области, определенной этим.
        /// </summary>
        public long Bottom => Y + Height;

        /// <summary>
        ///  Инициализирует экземпляр класса <see cref="Borders"/>
        /// </summary>
        /// <param name="x">X стартовой точки</param>
        /// <param name="y">Y стартовой точки</param>
        /// <param name="width">Ширина</param>
        /// <param name="height">Высота</param>
        public Borders(long x, long y, long width, long height)
        {
            StartPoint = new SpacePoint(x, y);
            Width = width;
            Height = height;
        }

        /// <summary>
        /// Инициализирует экземпляр класса <see cref="Borders"/>
        /// </summary>
        /// <param name="startPoint">Стартовая точка</param>
        /// <param name="width">Ширина</param>
        /// <param name="height">Высота</param>
        public Borders(SpacePoint startPoint, long width, long height)
        {
            StartPoint = startPoint;
            Width = width;
            Height = height;
        }

        /// <summary>
        /// Отображает границы как строку
        /// Обход производится по часовой стрелке.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"[{TopLeft}, {TopRight}, {BottomRight}. {BottomLeft}]";
        }

        /// <summary>
        /// Расширяет границы
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public void Inflate(int width, int height)
        {
            StartPoint.X -= width;
            StartPoint.Y -= height;
            this.Width += 2 * width;
            this.Height += 2 * height;
        }
    }
}
﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System.Drawing;
using System.Drawing.Imaging;
using Universe.Image;
using Universe.Image.Imaging;

namespace Universe.QrCode.Perseverance.QrCodeSearching
{
    public static class AttackQrDecoderExtensions
    {
        /// <summary>
        /// Бинаризация
        /// Делает изображение монохромным
        /// </summary>
        /// <param name="bufferBitmap">Обрабатываемое изображение</param>
        public static Bitmap MakeMonoChrome(this Bitmap bufferBitmap)
        {
            // Создаём холст
            var canvas = new Bitmap(bufferBitmap.Width, bufferBitmap.Height, PixelFormat.Format24bppRgb);
            var startDrawingImagePoint = new Point(0, 0);
            using (var graphic = Graphics.FromImage(canvas))
            {
                var img = new Bitmap(bufferBitmap);
                var monochromed = img.MakeMonochrome();

                graphic.DrawImage(monochromed, startDrawingImagePoint);

                return canvas;
            }
        }

        /// <summary>
        /// Изменяет контрастность изображения в сторону усиления.
        /// Значение на которое производится коррекция: +512 условных единиц
        /// </summary>
        /// <param name="bufferBitmap">Обрабатываемое изображение</param>
        public static Bitmap ApplyAutocontrast(this Bitmap bufferBitmap)
        {
            var width = bufferBitmap.Width;
            var height = bufferBitmap.Height;

            // Создаём холст
            var canvas = new Bitmap(width, height, PixelFormat.Format24bppRgb);
            var startDrawingImagePoint = new Point(0, 0);
            using (var graphic = Graphics.FromImage(canvas))
            {
                var img = new Bitmap(bufferBitmap);

                var autocontrasted = img.ContrastByCorrection(512);

                graphic.DrawImage(autocontrasted, startDrawingImagePoint);

                return canvas;
            }
        }

        /// <summary>
        /// Применяет медианный фильтр
        /// Радиус фильтра: 2 пикселя
        /// </summary>
        /// <param name="bufferBitmap">Обрабатываемое изображение</param>
        public static Bitmap ApplyMedianFilter(this Bitmap bufferBitmap)
        {
            var width = bufferBitmap.Width;
            var height = bufferBitmap.Height;

            // Создаём холст
            var canvas = new Bitmap(width, height, PixelFormat.Format24bppRgb);
            var startDrawingImagePoint = new Point(0, 0);
            using (var graphic = Graphics.FromImage(canvas))
            {
                var img = new Bitmap(bufferBitmap);

                var filtered = img.MedianFilter(2);

                graphic.DrawImage(filtered, startDrawingImagePoint);

                return canvas;
            }
        }

        /// <summary>
        /// Апскейлинг разрешения изображения. Двухкратный.
        /// </summary>
        /// <param name="processingImage">Обрабатываемое изображение</param>
        public static Bitmap Upscale(this Bitmap processingImage)
        {
            var width = (int)(processingImage.Width * 2.0);
            var height = (int)(processingImage.Height * 2.0);

            return processingImage.ResizeImg(width, height);
        }

        /// <summary>
        /// Уменьшение и увеличение разрешения.
        /// Выступает как альтернатива размытию.
        /// </summary>
        /// <param name="processingImage"></param>
        /// <returns></returns>
        public static Bitmap ApplyDownUpscale(this Bitmap processingImage)
        {
            var width = (int)(processingImage.Width / 2.0);
            var height = (int)(processingImage.Height / 2.0);

            processingImage = processingImage.ResizeImg(width, height);
            processingImage = processingImage.ResizeImg(width * 2, height * 2);

            return processingImage;
        }

        // TODO -> После добавления поддержки OpenCv (EmguCv) - расскомментировать
        ///// <summary>
        ///// Размытие изображения при помощи OpenCV (EmpuCV)
        ///// </summary>
        ///// <param name="processingImage">Обрабатываемое изображение</param>
        //public static Bitmap ApplyOpenCvBlur(this Bitmap processingImage)
        //{
        //    //return BitmapProcessor.Inst.Blur(processingImage);
        //    return OpenCvProcessor.Inst.ApplyCvBlur(processingImage);
        //}

        /// <summary>
        /// Размытие изображения.
        /// </summary>
        /// <param name="processingImage">Обрабатываемое изображение</param>
        public static Bitmap ApplyBlur(this Bitmap processingImage)
        {
            return BitmapProcessor.Inst.Blur(processingImage);
        }

        /// <summary>
        /// Увеличение резкости.
        /// </summary>
        /// <param name="processingImage">Обрабатываемое изображение</param>
        public static Bitmap ApplySharpen(this Bitmap processingImage)
        {
            return BitmapProcessor.Inst.Sharpen(processingImage);
        }
    }
}
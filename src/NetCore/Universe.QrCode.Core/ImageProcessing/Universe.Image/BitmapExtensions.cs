﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using Universe.Algorithm.Sorting;

namespace Universe.Image
{
    /// <summary>
    /// Расширяет функциональный набор класса Bitmap
    /// </summary>
    public static class BitmapExtensions
    {
        /// <summary>
        /// Полное копирование изображения
        /// </summary>
        /// <param name="bmp">Изображение</param>
        /// <returns></returns>
        public static Bitmap FullCopy(this Bitmap bmp)
        {
            if (bmp == null)
                throw new ArgumentNullException(nameof(bmp));

            // Задаём формат Пикселя.
            var pxf = bmp.PixelFormat;

            var bmpCopy = new Bitmap(bmp.Width, bmp.Height, pxf);

            // Получаем данные картинки.
            var rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
            //Блокируем набор данных изображения в памяти
            //При этом исходное изображение только для чтения
            var bmpDataSource = bmp.LockBits(rect, ImageLockMode.ReadOnly, pxf);
            var bmpDataCopy = bmpCopy.LockBits(rect, ImageLockMode.ReadWrite, pxf);

            var stride = bmpDataSource.Stride;
            var numBytes = stride * bmp.Height;
            var rgbValues = new byte[numBytes];
            var ptr = bmpDataSource.Scan0;
            Marshal.Copy(ptr, rgbValues, 0, numBytes);

            var strideCopy = bmpDataCopy.Stride;
            var numBytesCopy = strideCopy * bmpCopy.Height;
            var rgbValuesCopy = new byte[numBytesCopy];
            var ptrCopy = bmpDataCopy.Scan0;
            Marshal.Copy(ptrCopy, rgbValuesCopy, 0, rgbValuesCopy.Length);

            for(var index = 0; index < numBytes; index++)
            {
                rgbValuesCopy[index] = rgbValues[index];
            }

            // Копируем набор данных обратно в изображение, но только копии
            Marshal.Copy(rgbValuesCopy, 0, ptrCopy, numBytesCopy);

            bmp.UnlockBits(bmpDataSource);
            bmpCopy.UnlockBits(bmpDataCopy);
            return bmpCopy;
        }

        /// <summary>
        /// Полное копирование изображения
        /// </summary>
        /// <param name="sourceBitmap"></param>
        /// <param name="pixelFormat"></param>
        /// <returns></returns>
        public static Bitmap FullCopy(this Bitmap sourceBitmap, PixelFormat pixelFormat)
        {
            //return new Bitmap(sourceBitmap, sourceBitmap.Width, sourceBitmap.Height);
            var section = new Rectangle(0, 0, sourceBitmap.Width, sourceBitmap.Height);
            var canvas = new Bitmap(sourceBitmap.Width, sourceBitmap.Height, pixelFormat);
            using (Graphics g = Graphics.FromImage(canvas))
            {
                g.DrawImage(sourceBitmap, 0, 0, section, GraphicsUnit.Pixel);
                g.Save();
            }

            return canvas;
        }

        /// <summary>
        /// Вырезание области изображения
        /// </summary>
        /// <param name="sourceBitmap"></param>
        /// <param name="section"></param>
        /// <param name="pixelFormat"></param>
        /// <returns></returns>
        public static Bitmap Copy(this Bitmap sourceBitmap, Rectangle section, PixelFormat pixelFormat = PixelFormat.Format24bppRgb)
        {
            // Вырезаем выбранный кусок картинки
            Bitmap bmp = new Bitmap(section.Width, section.Height, pixelFormat);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.DrawImage(sourceBitmap, 0, 0, section, GraphicsUnit.Pixel);
            }
            //Возвращаем кусок картинки.
            return bmp;
        }

        /// <summary>
        /// Функция бинаризации изображения
        /// </summary>
        /// <param name="sourceBitmap">Обрабатываемое изображение</param>
        /// <returns></returns>
        public static Bitmap MakeMonochrome(this Bitmap sourceBitmap)
        {
            // Автоматически извлекаем пороговое значение
            int threshold = Binarization.GetOtsuThreshold(sourceBitmap);

            // Конвертируем в оттенки серого
            Binarization.Convert2GrayScale(sourceBitmap);
            //Бинаризируем по порогу
            return Binarization.ApplyThreshold(sourceBitmap, threshold);
        }

        /// <summary>
        /// Функция бинаризации изображения
        /// </summary>
        /// <param name="sourceBitmap">Обрабатываемое изображение</param>
        /// <param name="threshold">Пороговое значение от 0 до 255</param>
        /// <returns></returns>
        public static Bitmap MakeMonochrome(this Bitmap sourceBitmap, int threshold)
        {
            // Конвертируем в оттенки серого
            Binarization.Convert2GrayScale(sourceBitmap);
            //Бинаризируем по порогу
            return Binarization.ApplyThreshold(sourceBitmap, threshold);
        }

        /// <summary>
        /// Функция сравнения изображений
        /// </summary>
        /// <param name="inputImageA">Изображение A</param>
        /// <param name="inputImageB">Изображение B</param>
        /// <param name="tollerance">Пороговое значение после которого, субпиксель считается отличным от эталона.
        ///  Задаётся в величине интенсивности от 0 до 255</param>
        /// <returns>Возращает расхождение отличия эталонного изображения в процентах: 
        /// 100% - изображения абсолютно разные.
        /// 0 % - изображения полностью идентичны.</returns>
        public static double CompareImages(Bitmap inputImageA, Bitmap inputImageB, int tollerance)
        {
            if (inputImageA == null)
                throw new ArgumentNullException(nameof(inputImageA));
            if (inputImageB == null)
                throw new ArgumentNullException(nameof(inputImageB));

            Bitmap imageA = new Bitmap(inputImageA, new Size(128, 128));
            Bitmap imageB = new Bitmap(inputImageB, new Size(128, 128));
            int imageASize = imageA.Width * imageA.Height;
            int imageBSize = imageB.Width * imageB.Height;
            Bitmap imageC;
            if (imageASize > imageBSize)
            {
                imageA = new Bitmap(imageA, imageB.Size);
                imageC = new Bitmap(imageB.Width, imageB.Height);
            }
            else
            {
                imageA = new Bitmap(imageA, imageB.Size);
                imageC = new Bitmap(imageB.Width, imageB.Height);
            }

            for (int x = 0; x < imageA.Width; x++)
            {
                for (int y = 0; y < imageA.Height; y++)
                {
                    Color color1 = imageA.GetPixel(x, y);
                    Color color2 = imageB.GetPixel(x, y);
                    int r = color1.R > color2.R ? color1.R - color2.R : color2.R - color1.R;
                    int g = color1.G > color2.G ? color1.G - color2.G : color2.G - color1.G;
                    int b = color1.B > color2.B ? color1.B - color2.B : color2.B - color1.B;
                    imageC.SetPixel(x, y, Color.FromArgb(r, g, b));
                }
            }
            int difference = 0;
            for (int x = 0; x < imageA.Width; x++)
            {
                for (int y = 0; y < imageA.Height; y++)
                {
                    Color color1 = imageC.GetPixel(x, y);
                    int media = (color1.R + color1.G + color1.B) / 3;
                    if (media > tollerance)
                        difference++;
                }
            }
            double usedSize = imageASize > imageBSize ? imageBSize : imageASize;
            double result = difference * 100 / usedSize;

            return result;
        }


        /// <summary>
        /// Функция сравнения изображений. С учетом цвета
        /// Через маршаллинг
        /// </summary>
        /// <param name="inputImageA">Изображение A</param>
        /// <param name="inputImageB">Изображение B</param>
        /// <param name="tollerance">Пороговое значение после которого, субпиксель считается отличным от эталона.
        ///  Задаётся в величине интенсивности от 0 до 255</param>
        /// <param name="upscalingSize">Значение в пикселях, до которого выравнивается изображение. По-умолчанию 128</param>
        /// <returns>Возращает расхождение отличия эталонного изображения в процентах: 
        /// 100% - изображения абсолютно разные.
        /// 0 % - изображения полностью идентичны.</returns>
        public static double CompareImagesColor(Bitmap inputImageA, Bitmap inputImageB, int tollerance, int upscalingSize = 128)
        {
            if (inputImageA == null)
                throw new ArgumentNullException(nameof(inputImageA));
            if (inputImageB == null)
                throw new ArgumentNullException(nameof(inputImageB));

            Bitmap imageA = new Bitmap(inputImageA, new Size(upscalingSize, upscalingSize));
            Bitmap imageB = new Bitmap(inputImageB, new Size(upscalingSize, upscalingSize));
            int imageASize = imageA.Width * imageA.Height;
            int imageBSize = imageB.Width * imageB.Height;
            Bitmap imageC;
            if (imageASize > imageBSize)
            {
                imageA = new Bitmap(imageA, imageB.Size);
                imageC = new Bitmap(imageB.Width, imageB.Height);
            }
            else
            {
                imageA = new Bitmap(imageA, imageB.Size);
                imageC = new Bitmap(imageB.Width, imageB.Height);
            }

            var bmpDataA = GetBitmapBytes(imageA, out var rgbValuesA);
            var ptrA = bmpDataA.Scan0;
            Marshal.Copy(ptrA, rgbValuesA, 0, rgbValuesA.Length);

            var bmpDataB = GetBitmapBytes(imageB, out var rgbValuesB);
            var ptrB = bmpDataB.Scan0;
            Marshal.Copy(ptrB, rgbValuesB, 0, rgbValuesB.Length);

            var bmpDataC = GetBitmapBytes(imageC, out var rgbValuesC);
            var ptrC = bmpDataC.Scan0;
            Marshal.Copy(ptrC, rgbValuesC, 0, rgbValuesC.Length);

            // Перебираем пикселы по 3 байта на каждый и меняем значения
            for (var counter = 0; counter < rgbValuesA.Length + 10; counter += 3)
                if (counter + 1 < rgbValuesA.Length - 1 || counter + 2 < rgbValuesA.Length - 1)
                {
                    int r = rgbValuesA[counter] > rgbValuesB[counter] ? rgbValuesA[counter] - rgbValuesB[counter] : rgbValuesB[counter] - rgbValuesA[counter];
                    int g = rgbValuesA[counter + 1] > rgbValuesB[counter + 1] ? rgbValuesA[counter + 1] - rgbValuesB[counter + 1] : rgbValuesB[counter + 1] - rgbValuesA[counter + 1];
                    int b = rgbValuesA[counter + 2] > rgbValuesB[counter + 2] ? rgbValuesA[counter + 2] - rgbValuesB[counter + 2] : rgbValuesB[counter + 2] - rgbValuesA[counter + 2];

                    rgbValuesC[counter] = (byte)r;
                    rgbValuesC[counter + 1] = (byte)g;
                    rgbValuesC[counter + 2] = (byte)b;
                }

            int difference = 0;
            for (var counter = 0; counter < rgbValuesC.Length + 10; counter += 3)
                if (counter + 1 < rgbValuesC.Length - 1 || counter + 2 < rgbValuesC.Length - 1)
                {
                    int media = (rgbValuesC[counter] + rgbValuesC[counter + 1] + rgbValuesC[counter + 2]) / 3;
                    if (media > tollerance)
                        difference++;
                    else
                    {
                        //ignored
                    }
                }

            double usedSize = imageASize > imageBSize ? imageBSize : imageASize;
            double result = difference * 100 / usedSize;

            // Копируем набор данных обратно в изображение
            Marshal.Copy(rgbValuesA, 0, ptrA, rgbValuesA.Length);
            Marshal.Copy(rgbValuesB, 0, ptrB, rgbValuesB.Length);
            Marshal.Copy(rgbValuesC, 0, ptrC, rgbValuesC.Length);

            imageA.UnlockBits(bmpDataA);
            imageB.UnlockBits(bmpDataB);
            imageC.UnlockBits(bmpDataC);

            return result;
        }

        /// <summary>
        /// Функция сравнения изображений. Монохромное
        /// Через маршаллинг
        /// </summary>
        /// <param name="inputImageA">Изображение A</param>
        /// <param name="inputImageB">Изображение B</param>
        /// <param name="tollerance">Пороговое значение после которого, субпиксель считается отличным от эталона.
        ///  Задаётся в величине интенсивности от 0 до 255</param>
        /// <param name="upscalingSize">Значение в пикселях, до которого выравнивается изображение. По-умолчанию 128</param>
        /// <returns>Возращает расхождение отличия эталонного изображения в процентах: 
        /// 100% - изображения абсолютно разные.
        /// 0 % - изображения полностью идентичны.</returns>
        public static double CompareImagesMonochrome(Bitmap inputImageA, Bitmap inputImageB, int tollerance, int upscalingSize = 128)
        {
            if (inputImageA == null)
                throw new ArgumentNullException(nameof(inputImageA));
            if (inputImageB == null)
                throw new ArgumentNullException(nameof(inputImageB));

            Bitmap imageA = new Bitmap(inputImageA, new Size(upscalingSize, upscalingSize));
            Bitmap imageB = new Bitmap(inputImageB, new Size(upscalingSize, upscalingSize));
            int imageASize = imageA.Width * imageA.Height;
            int imageBSize = imageB.Width * imageB.Height;
            Bitmap imageC;
            if (imageASize > imageBSize)
            {
                imageA = new Bitmap(imageA, imageB.Size);
                imageC = new Bitmap(imageB.Width, imageB.Height);
            }
            else
            {
                imageA = new Bitmap(imageA, imageB.Size);
                imageC = new Bitmap(imageB.Width, imageB.Height);
            }

            // Производим бинаризацию убирая краевые эффекты
            // Повышает точность сравнения в случаях, когда цветовая информация не важна
            imageA = MakeMonochrome(imageA);
            imageB = MakeMonochrome(imageB);

            var bmpDataA = GetBitmapBytes(imageA, out var rgbValuesA);
            var ptrA = bmpDataA.Scan0;
            Marshal.Copy(ptrA, rgbValuesA, 0, rgbValuesA.Length);

            var bmpDataB = GetBitmapBytes(imageB, out var rgbValuesB);
            var ptrB = bmpDataB.Scan0;
            Marshal.Copy(ptrB, rgbValuesB, 0, rgbValuesB.Length);

            var bmpDataC = GetBitmapBytes(imageC, out var rgbValuesC);
            var ptrC = bmpDataC.Scan0;
            Marshal.Copy(ptrC, rgbValuesC, 0, rgbValuesC.Length);

            // Перебираем пикселы по 3 байта на каждый и меняем значения
            for (var counter = 0; counter < rgbValuesA.Length + 10; counter += 3)
                if (counter + 1 < rgbValuesA.Length - 1 || counter + 2 < rgbValuesA.Length - 1)
                {
                    int r = rgbValuesA[counter] > rgbValuesB[counter] ? rgbValuesA[counter] - rgbValuesB[counter] : rgbValuesB[counter] - rgbValuesA[counter];
                    int g = rgbValuesA[counter + 1] > rgbValuesB[counter + 1] ? rgbValuesA[counter + 1] - rgbValuesB[counter + 1] : rgbValuesB[counter + 1] - rgbValuesA[counter + 1];
                    int b = rgbValuesA[counter + 2] > rgbValuesB[counter + 2] ? rgbValuesA[counter + 2] - rgbValuesB[counter + 2] : rgbValuesB[counter + 2] - rgbValuesA[counter + 2];

                    rgbValuesC[counter] = (byte)r;
                    rgbValuesC[counter + 1] = (byte)g;
                    rgbValuesC[counter + 2] = (byte)b;
                }

            int difference = 0;
            for (var counter = 0; counter < rgbValuesC.Length + 10; counter += 3)
                if (counter + 1 < rgbValuesC.Length - 1 || counter + 2 < rgbValuesC.Length - 1)
                {
                    int media = (rgbValuesC[counter] + rgbValuesC[counter + 1] + rgbValuesC[counter + 2]) / 3;
                    if (media > tollerance)
                        difference++;
                    else
                    {
                        //ignored
                    }
                }

            double usedSize = imageASize > imageBSize ? imageBSize : imageASize;
            double result = difference * 100 / usedSize;

            // Копируем набор данных обратно в изображение
            Marshal.Copy(rgbValuesA, 0, ptrA, rgbValuesA.Length);
            Marshal.Copy(rgbValuesB, 0, ptrB, rgbValuesB.Length);
            Marshal.Copy(rgbValuesC, 0, ptrC, rgbValuesC.Length);

            imageA.UnlockBits(bmpDataA);
            imageB.UnlockBits(bmpDataB);
            imageC.UnlockBits(bmpDataC);

            // Раскомментировать для отладки
            //imageA.Save("imageA.bmp");
            //imageB.Save("imageB.bmp");
            //imageC.Save("imageC-diff.bmp");

            return result;
        }

        private static BitmapData GetBitmapBytes(Bitmap bmp, out byte[] rgbValues)
        {
            // Задаём формат Пикселя.
            var pxf = PixelFormat.Format24bppRgb;

            // Получаем данные картинки.
            var rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
            //Блокируем набор данных изображения в памяти
            var bmpData = bmp.LockBits(rect, ImageLockMode.ReadWrite, pxf);

            // Задаём массив из Byte и помещаем в него надор данных.
            // int numBytes = bmp.Width * bmp.Height * 3; 
            //На 3 умножаем - поскольку RGB цвет кодируется 3-мя байтами
            //Либо используем вместо Width - Stride
            var stride = bmpData.Stride;
            var numBytes = stride * bmp.Height;
            rgbValues = new byte[numBytes];

            return bmpData;
        }

        /// <summary>
        /// Средняя яркость
        /// </summary>
        /// <param name="inputImage"></param>
        /// <returns></returns>
        public static long AverageLumination(this Bitmap inputImage)
        {
            if (inputImage == null)
                throw new ArgumentNullException(nameof(inputImage));

            var bmpData = GetBitmapBytes(inputImage, out var rgbValues);
            var ptr = bmpData.Scan0;
            Marshal.Copy(ptr, rgbValues, 0, rgbValues.Length);

            long lAb = 0;

            // Перебираем пикселы по 3 байта на каждый и меняем значения
            // Находим среднюю яркость lAB
            for (var counter = 0; counter < rgbValues.Length + 10; counter += 3)
            {
                if (counter + 1 < rgbValues.Length - 1 || counter + 2 < rgbValues.Length - 1)
                {
                    var valueB = (long)rgbValues[counter];
                    var valueG = (long)rgbValues[counter + 1];
                    var valueR = (long)rgbValues[counter + 2];

                    lAb += (long)(valueR * 0.299 + valueG * 0.587 + valueB * 0.114);
                }
            }

            //средняя яркость 
            lAb /= inputImage.Width * inputImage.Height;

            inputImage.UnlockBits(bmpData);

            return lAb;
        }

        /// <summary>
        /// Функция регулировки контрастности посредством значения
        /// </summary>
        /// <param name="inputImage">Изображение</param>
        /// <param name="correction">Коррекция</param>
        /// <returns></returns>
        public static Bitmap ContrastByCorrection(this Bitmap inputImage, int correction = 255)
        {
            if (inputImage == null)
                throw new ArgumentNullException(nameof(inputImage));

            const int l = 256;

            var bmpData = GetBitmapBytes(inputImage, out var rgbValues);
            var ptr = bmpData.Scan0;
            Marshal.Copy(ptr, rgbValues, 0, rgbValues.Length);

            long lAb = 0;

            //палитра
            long[] b = new long[l];

            // Перебираем пикселы по 3 байта на каждый и меняем значения
            // Находим среднюю яркость lAB
            for (var counter = 0; counter < rgbValues.Length + 10; counter += 3)
            {
                if (counter + 1 < rgbValues.Length - 1 || counter + 2 < rgbValues.Length - 1)
                {
                    var valueB = rgbValues[counter];
                    var valueG = rgbValues[counter + 1];
                    var valueR = rgbValues[counter + 2];

                    lAb += (long)(valueR * 0.299 + valueG * 0.587 + valueB * 0.114);
                }
            }

            //средняя яркость 
            lAb /= inputImage.Width * inputImage.Height;

            //Коэффициент коррекции
            double k = 1.0 + correction / 100.0;

            //RGB алгоритм изменения контраста	
            for (int i = 0; i < l; i++)
            {
                long delta = i - lAb;
                long temp = (long)(lAb + k * delta);

                if (temp < 0)
                    temp = 0;

                if (temp >= 255)
                    temp = 255;
                b[i] = temp;
            }

            for (var counter = 0; counter < rgbValues.Length; counter ++)
            {
                var value = rgbValues[counter];
                rgbValues[counter] = (byte)b[value];
            }

            // Копируем набор данных обратно в изображение
            Marshal.Copy(rgbValues, 0, ptr, rgbValues.Length);

            inputImage.UnlockBits(bmpData);
            return inputImage;
        }

        /// <summary>
        /// Медианный фильтр. //TODO -> работает не совсем корректно. Исправить в будущем
        /// </summary>
        /// <param name="inputImage">Изображение</param>
        /// <param name="rad">Радиус фильтра</param>
        /// <returns></returns>
        public static Bitmap MedianFilter(this Bitmap inputImage, int rad = 1)
        {
            if (inputImage == null)
                throw new ArgumentNullException(nameof(inputImage));

            int wB = inputImage.Width;
            int hB = inputImage.Height;

            var bmpData = GetBitmapBytes(inputImage, out var rgbValues);
            var ptr = bmpData.Scan0;
            Marshal.Copy(ptr, rgbValues, 0, rgbValues.Length);

            for (int x = rad + 1; x < wB - rad; x++)
            {
                for (int y = rad + 1; y < hB - rad; y++)
                {
                    ApplyMedianFilterToPoint(rgbValues, x, y, wB, rad);
                }
            }

            // Копируем набор данных обратно в изображение
            Marshal.Copy(rgbValues, 0, ptr, rgbValues.Length);

            inputImage.UnlockBits(bmpData);
            return inputImage;
        }    

        private static void ApplyMedianFilterToPoint(
            byte[] rgbValues, 
            int x, 
            int y,
            int width,
            int rad = 1)
        {
            int n;
            int cR_, cB_, cG_;
            int k = 1;

            n = (2 * rad + 1) * (2 * rad + 1);

            int[] cR = new int[n + 1];
            int[] cB = new int[n + 1];
            int[] cG = new int[n + 1];

            for (int i = 0; i < n + 1; i++)
            {
                cR[i] = 0;
                cG[i] = 0;
                cB[i] = 0;
            }

            for (int i = x - rad; i < x + rad + 1; i++)
            {
                for (int j = y - rad; j < y + rad + 1; j++)
                {
                    /*    //if (k < n + 1)
                        if (i != w_b - 1 && j != h_b - 1)
                        {
                     */
                    var r = rgbValues[(i * x + rad + 1) + j];
                    var g = rgbValues[(i * x + rad + 1) + (j + 1)];
                    var b = rgbValues[(i * x + rad + 1) + (j + 2)];
                    cR[k] = r;
                    cG[k] = g;
                    cB[k] = b;
                    k++;
                    /*   
                    }
                        //this.Text = System.Convert.ToString(n) + "   " + System.Convert.ToString(k);

                     */
                }
            }

            SortingAlgorithm.Sort(cR, SortingType.Quck);
            SortingAlgorithm.Sort(cG, SortingType.Quck);
            SortingAlgorithm.Sort(cB, SortingType.Quck);
            //QuickSortingAlg.Sort(cR, 0, n - 1);
            //QuickSortingAlg.Sort(cG, 0, n - 1);
            //QuickSortingAlg.Sort(cB, 0, n - 1);

            int dn = n / 2 + 1;

            cR_ = cR[dn];
            cG_ = cG[dn];
            cB_ = cB[dn];

            rgbValues[x * width + y] = (byte)cR_;
            rgbValues[x * width + y + 1] = (byte)cG_;
            rgbValues[x * width + y + 2] = (byte)cB_;

            /*for (int i = x - rad; i < x + rad + 1; x++)
            {
                for (int j = y - rad; j < y + rad + 1; y++)
                {
                    my_bitmap.SetPixel(i, j, System.Drawing.Color.FromArgb(cR_, cG_, cB_));
                }
            }
            */
        }

        /// <summary>
        /// Изменяет разрешение изображения
        /// </summary>
        /// <param name="sourceBitmap"></param>
        /// <param name="nWidth"></param>
        /// <param name="nHeight"></param>
        /// <returns></returns>
        public static Bitmap ResizeImg(this Bitmap sourceBitmap, int nWidth, int nHeight, PixelFormat pixelFormat = PixelFormat.Format24bppRgb)
        {
            Bitmap result = new Bitmap(nWidth, nHeight, pixelFormat);
            using (Graphics g = Graphics.FromImage(result))
            {
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.DrawImage(sourceBitmap, 0, 0, nWidth, nHeight);
                g.Dispose();
            }
            return result;
        }
    }
}
﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;

namespace Universe.Image
{
    /// <summary>
    /// Расширяет функциональный набор класса Bitmap
    /// </summary>
    public static class ExtBitmap
    {
        private struct Pixel : IEquatable<Pixel>
        {          
            // ReSharper disable UnassignedField.Compiler
            public byte Blue;
            public byte Green;
            public byte Red;
 
            public bool Equals(Pixel other)
            {
                return Red == other.Red && Green == other.Green && Blue == other.Blue;
            }
        }
 
        public static Color[,] GetPixels(this Bitmap bmp)
        {
            return ProcessBitmap(bmp, pixel => Color.FromArgb(pixel.Red, pixel.Green, pixel.Blue));
        }
 
        public static float[,] GetBrightness(this Bitmap bmp)
        {
            return ProcessBitmap(bmp, pixel => Color.FromArgb(pixel.Red, pixel.Green, pixel.Blue).GetBrightness());
        }

        private static unsafe T[,] ProcessBitmap<T>(this Bitmap bitmap, Func<Pixel, T> func)
        {
            var lockBits = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly,
                                       bitmap.PixelFormat);
            int padding = lockBits.Stride - (bitmap.Width*sizeof (Pixel));
 
            int width = bitmap.Width;
            int height = bitmap.Height;
 
            var result = new T[height, width];
 
            var ptr = (byte*) lockBits.Scan0;
 
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    var pixel = (Pixel*) ptr;
                    result[i, j] = func(*pixel);
                    ptr += sizeof (Pixel);
                }
                ptr += padding;
            }
 
            bitmap.UnlockBits(lockBits);
 
            return result;
        }  

        /// <summary>
        /// Уменьшает разрешение изображения, и число пикселов, а также окружает изображение границей из "пустых" пикселей
        /// </summary>
        /// <param name="sourceBitmap"></param>
        /// <param name="canvasWidthLenght"></param>
        /// <returns></returns>
        public static Bitmap CopyToSquareCanvas(this Bitmap sourceBitmap, int canvasWidthLenght)
        {
            float ratio;
            int maxSide = sourceBitmap.Width > sourceBitmap.Height ?
                          sourceBitmap.Width : sourceBitmap.Height;

            ratio = (float)maxSide / canvasWidthLenght;

            Bitmap bitmapResult = sourceBitmap.Width > sourceBitmap.Height ?
                                    new Bitmap(canvasWidthLenght, (int)(sourceBitmap.Height / ratio))
                                    : new Bitmap((int)(sourceBitmap.Width / ratio), canvasWidthLenght);

            using (Graphics graphicsResult = Graphics.FromImage(bitmapResult))
            {
                graphicsResult.CompositingQuality = CompositingQuality.HighQuality;
                graphicsResult.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphicsResult.PixelOffsetMode = PixelOffsetMode.HighQuality;

                graphicsResult.DrawImage(sourceBitmap,
                                        new Rectangle(0, 0,
                                            bitmapResult.Width, bitmapResult.Height),
                                        new Rectangle(0, 0,
                                            sourceBitmap.Width, sourceBitmap.Height),
                                            GraphicsUnit.Pixel);
                graphicsResult.Flush();
            }

            return bitmapResult;
        }

        /// <summary>
        /// Реализует фильтрацию изображения методом свертки
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sourceBitmap"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public static Bitmap ConvolutionFilter<T>(this Bitmap sourceBitmap, T filter)
                                        where T : ConvolutionFilterBase
        {

            int width = sourceBitmap.Width,
                height = sourceBitmap.Height;

            byte[, ,] resultBuffer = new byte[3, height, width];

            BitmapData sourceData = sourceBitmap.LockBits(new Rectangle(0, 0,
                                     sourceBitmap.Width, sourceBitmap.Height),
                                     ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
            unsafe
            {
                try
                {
                    double blue;
                    double green;
                    double red;

                    int filterWidth = filter.FilterMatrix.GetLength(1);   // 3
                    int filterHeight = filter.FilterMatrix.GetLength(0);  // 3

                    int filterOffset = (filterWidth - 1) / 2;

                    double factor = filter.Factor;

                    int stride = sourceData.Stride;
                    byte* scan0 = (byte*)sourceData.Scan0;

                    byte* paspos;
                    byte* curpos;
                    byte* futpos;
                    fixed (byte* res = resultBuffer)
                    {
                        byte* r = res, g = res + width * height, b = res + 2 * width * height;

                        //==========================================================
                        for (int offsetY = filterOffset; offsetY <
                            height - filterOffset - 1; offsetY += filterHeight)
                        {
                            paspos = scan0 + (offsetY - 1) * stride;
                            curpos = scan0 + (offsetY + 0) * stride; //Это одномерный массив
                            futpos = scan0 + (offsetY + 1) * stride;

                            for (int offsetX = filterOffset; offsetX <
                                width - filterOffset - 1; offsetX += filterWidth)
                            {
                                blue = 0;
                                green = 0;
                                red = 0;

                                //paspos++;
                                //curpos++;
                                //futpos++;
                                //byteOffset = offsetY *
                                //             sourceData.Stride;

                                for (int filterY = -filterOffset;
                                    filterY <= filterOffset; filterY++)
                                {                                   

                                    //for (int filterX = -filterOffset;
                                    //    filterX <= filterOffset; filterX++)
                                    //{

                                        //calcOffset = byteOffset +
                                        //           (filterY * sourceData.Stride);

                                        blue += (double)*(paspos++) *
                                                filter.FilterMatrix[filterY + filterOffset,
                                                                    0]
                                              + (double)*(curpos++) *
                                               filter.FilterMatrix[filterY + filterOffset,
                                                                    1]
                                              + (double)*(futpos++) *
                                               filter.FilterMatrix[filterY + filterOffset,
                                                                    2];

                                        green += (double)*(paspos++) *
                                                 filter.FilterMatrix[filterY + filterOffset,
                                                                    0]
                                               + (double)*(curpos++) *
                                                 filter.FilterMatrix[filterY + filterOffset,
                                                                    1]
                                               + (double)*(futpos++) *
                                                 filter.FilterMatrix[filterY + filterOffset,
                                                                    2];

                                        red += (double)*(paspos++) *
                                               filter.FilterMatrix[filterY + filterOffset,
                                                                    0]
                                             + (double)*(curpos++) *
                                               filter.FilterMatrix[filterY + filterOffset,
                                                                    1]
                                             + (double)*(futpos++) *
                                               filter.FilterMatrix[filterY + filterOffset,
                                                                    2];
                                    //}
                                }

                                blue = blue * factor + filter.Bias;
                                green = green * factor + filter.Bias;
                                red = red * factor + filter.Bias;

                                if (blue > 255)
                                { blue = 255; }
                                else if (blue < 0)
                                { blue = 0; }

                                if (green > 255)
                                { green = 255; }
                                else if (green < 0)
                                { green = 0; }

                                if (red > 255)
                                { red = 255; }
                                else if (red < 0)
                                { red = 0; }

                                *b = (byte)(blue);
                                *g = (byte)(green);
                                *r = (byte)(red);
                            }
                        }
                        //==========================================================
                    }
                }
                finally
                {
                    sourceBitmap.UnlockBits(sourceData);
                }
            }

            // Вторая стадия

            Bitmap resultBitmap = new Bitmap(sourceBitmap.Width, sourceBitmap.Height);

            BitmapData resultData = resultBitmap.LockBits(new Rectangle(0, 0,
                                     resultBitmap.Width, resultBitmap.Height),
                                     ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
            unsafe
            {
                try
                {
                    byte* curpos;
                    fixed (byte* res = resultBuffer)
                    {
                        byte* r = res, g = res + width * height, b = res + 2 * width * height;
                        for (int h = 0; h < height; h++)
                        {
                            curpos = ((byte*)resultData.Scan0) + h * resultData.Stride;
                            for (int w = 0; w < width; w++)
                            {
                                *(curpos++) = *b; ++b;
                                *(curpos++) = *g; ++g;
                                *(curpos++) = *r; ++r;
                            }
                        }
                    }
                }
                finally
                {
                    resultBitmap.UnlockBits(resultData);
                }
            }

            return resultBitmap;
        }

        /// <summary>
        /// Перерисовка Bitmap
        /// </summary>
        /// <param name="bitmap"></param>
        /// <returns></returns>
        public static Bitmap Redraw(this Bitmap bitmap)
        {
            var bmpNew = new Bitmap(bitmap.Width, bitmap.Height);
            using (var graphic = Graphics.FromImage(bmpNew))
            {
                graphic.DrawImage(bitmap, new Rectangle(0, 0, bmpNew.Width, bmpNew.Height), 0, 0, bitmap.Width, bitmap.Height, GraphicsUnit.Pixel);
            }
            bitmap.Dispose();
            bitmap = bmpNew;
            return bitmap;
        }

        /// <summary>
        /// Изменяет яркость
        /// </summary>
        /// <param name="bmp"></param>
        /// <param name="changeFactor">Фактор изменения от -100 до 100</param>
        /// <returns></returns>
        public static Bitmap ChangeBrigness(this Bitmap bmp, float changeFactor)
        {
            if (Math.Abs(changeFactor) > 100)
                throw new ArgumentException("Фактор изменения должен быть от -100 до 100!");

            var factor = (100f + changeFactor) / 100f;

            BitmapData bmData = bmp.LockBits(
                new Rectangle(0, 0, bmp.Width, bmp.Height),
                ImageLockMode.ReadWrite,
                PixelFormat.Format24bppRgb);
            unsafe
            {
                //byte* p = (byte*)(void*)bmData.Scan0.ToPointer();
                byte* p = (byte*)bmData.Scan0.ToPointer();
                int h = bmp.Height;
                int w = bmp.Width;
                int ws = bmData.Stride;

                for (int i = 0; i < h; i++)
                {
                    byte* row = &p[i * ws];
                    for (int j = 0; j < w * 3; j += 3)
                    {
                        row[j] = (byte)(row[j] * factor < 255 ? (byte)(row[j] * factor) : 255);
                        row[j + 1] = (byte)(row[j + 1] * factor < 255 ? (byte)(row[j + 1] * factor) : 255);
                        row[j + 2] = (byte)(row[j + 2] * factor < 255 ? (byte)(row[j + 2] * factor) : 255);
                    }
                }
            }
            bmp.UnlockBits(bmData);

            return bmp;
        }

        /// <summary>
        /// Изменяет яркость
        /// </summary>
        /// <param name="bmp"></param>
        /// <param name="changeFactor">Фактор изменения от -100 до INF</param>
        /// <returns></returns>
        public static Bitmap ChangeContrast(this Bitmap bmp, float changeFactor)
        {
            if (changeFactor < -100)
                throw new ArgumentException("Фактор изменения может быть от -100!");

            var factor = (100f + changeFactor) / 100f;

            int redVal, greenVal, blueVal;

            double pixel;
            double contrast = factor; //Вычисляем общее значение контраста 
            contrast = contrast * contrast;

            BitmapData bmData = bmp.LockBits(
                new Rectangle(0, 0, bmp.Width, bmp.Height),
                ImageLockMode.ReadWrite,
                PixelFormat.Format24bppRgb);
            unsafe
            {
                //byte* p = (byte*)(void*)bmData.Scan0.ToPointer();
                byte* p = (byte*)bmData.Scan0.ToPointer();
                int h = bmp.Height;
                int w = bmp.Width;
                int ws = bmData.Stride;

                for (int i = 0; i < h; i++)
                {
                    byte* row = &p[i * ws];
                    for (int j = 0; j < w * 3; j += 3)
                    {
                        blueVal = row[j]; //B 
                        greenVal = row[j + 1]; //G 
                        redVal = row[j + 2]; //R

                        pixel = redVal / 255.0;
                        pixel = pixel - 0.5;
                        pixel = pixel * contrast;
                        pixel = pixel + 0.5;
                        pixel = pixel * 255;
                        if (pixel < 0) pixel = 0;
                        if (pixel > 255) pixel = 255;
                        row[j + 2] = Convert.ToByte(pixel);

                        pixel = greenVal / 255.0;
                        pixel = pixel - 0.5;
                        pixel = pixel * contrast;
                        pixel = pixel + 0.5;
                        pixel = pixel * 255;
                        if (pixel < 0) pixel = 0;
                        if (pixel > 255) pixel = 255;
                        row[j + 1] = Convert.ToByte(pixel);

                        pixel = blueVal / 255.0;
                        pixel = pixel - 0.5;
                        pixel = pixel * contrast;
                        pixel = pixel + 0.5;
                        pixel = pixel * 255;
                        if (pixel < 0) pixel = 0;
                        if (pixel > 255) pixel = 255;
                        row[j] = Convert.ToByte(pixel);
                    }
                }
            }
            bmp.UnlockBits(bmData);

            return bmp;
        }

        /// <summary>
        /// Излекает данные из 3-х канального иассива изображения в одноканальный
        /// </summary>
        /// <param name="imageData">3-х канальное изображение</param>
        /// <returns>Одномерный массив изображения</returns>
        public static float[] SingleChannelGet(float[, ,] imageData)
        {
            int height = imageData.GetLength(0);
            int width = imageData.GetLength(1);
            int channels = imageData.GetLength(2);
            float[] sourceBitmap = new float[height * width * (channels + 1)];
            int p = 0;
            for (int i = 0; i < height; i++)
                for (int j = 0; j < width; j++)
                    for (int c = 0; c < channels; c++)
                    {
                        sourceBitmap[p] = imageData[i, j, c];
                        p++;
                        if (c == 2) { sourceBitmap[p] = 255; p++; }
                    }
            return sourceBitmap;
        }

        /// <summary>
        /// Применение фильтра свертки
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sourceBitmap">Исходное изображенние</param>
        /// <param name="filter">Фильтр свертки</param>
        /// <param name="width">Ширина в пикселях</param>
        /// <param name="height">Высота в пикселях</param>
        /// <param name="channels">Число каналов исходного изображения</param>
        /// <returns>"Свернутое" изображение</returns>
        public static float[] ConvolutionFilter<T>(float[] sourceBitmap, T filter, int width, int height, int channels)
                                         where T : ConvolutionFilterBase
        {
            channels++;
            //Width -= filter.FilterMatrix.GetLength(1);
            //Height -= filter.FilterMatrix.GetLength(0);
            //float[] resultBuffer = new float[Convert.ToInt32(Height * Width * Channels/ 2.0)];
            float[] resultBuffer = new float[width * height * channels];

            var pixelBuffer = sourceBitmap;

            float blue;
            float green;
            float red;

            int filterWidth = filter.FilterMatrix.GetLength(1);

            int filterOffset = (filterWidth - 1) / 2;
            int calcOffset;

            int byteOffset;

            int step = 1;
            for (int offsetY = filterOffset; offsetY <
                height - filterOffset; offsetY += step)
            {
                for (int offsetX = filterOffset; offsetX <
                    width - filterOffset; offsetX += step)
                {
                    blue = 0;
                    green = 0;
                    red = 0;

                    byteOffset = offsetY *
                                   (width * channels) +
                                   offsetX * 4;

                    for (int filterY = -filterOffset;
                        filterY <= filterOffset; filterY++)
                    {
                        for (int filterX = -filterOffset;
                            filterX <= filterOffset; filterX++)
                        {

                            calcOffset = byteOffset + 
                                         (filterX * 4) +   
                                         (filterY * (width * channels));

                            blue += pixelBuffer[calcOffset] *
                                    filter.FilterMatrix[filterY + filterOffset,
                                                        filterX + filterOffset];

                            green += pixelBuffer[calcOffset + 1] *
                                     filter.FilterMatrix[filterY + filterOffset,
                                                        filterX + filterOffset];

                            red += pixelBuffer[calcOffset + 2] *
                                   filter.FilterMatrix[filterY + filterOffset,
                                                      filterX + filterOffset];
                        }
                    }

                    blue = filter.Factor * blue + filter.Bias;
                    green = filter.Factor * green + filter.Bias;
                    red = filter.Factor * red + filter.Bias;

                    resultBuffer[byteOffset] = (blue);
                    resultBuffer[byteOffset + 1] = (green);
                    resultBuffer[byteOffset + 2] = (red);
                    resultBuffer[byteOffset + 3] = 255;
                }
            }

            return resultBuffer;
        }

        public static float[, ,] SplitChannel(float[] sourceBitmap, int height, int width, int channels)
        {
            width = Convert.ToInt32(width);
            //height = Convert.ToInt32(height/2.0);
            float[, ,] imageData = new float[height, width, channels];  //Размер изображения после свертки изменяется на единицу
            int p = 0;
            for (int i = 0; i < height; i++)
                for (int j = 0; j < width; j++)
                    for (int c = 0; c < channels; c++)
                    {
                        imageData[i, j, c] = sourceBitmap[p];
                        p++;
                        if (c == 2) { p++; }
                    }
            return imageData;
        }

        /// <summary>
        /// Дабавляет функцию бинаризации изображения
        /// </summary>
        /// <param name="sourceBitmap"></param>
        /// <returns></returns>
        public static Bitmap Binarisation(this Bitmap sourceBitmap)
        {
            //Извлекаем пороговое значение
            int threshold = Binarization.GetOtsuThreshold(sourceBitmap);
            //Бинаризируем по порогу
            return Binarization.Threshold(sourceBitmap, threshold);
        }      

        /// <summary>
        /// Вырезание области изображения
        /// </summary>
        /// <param name="sourceBitmap"></param>
        /// <param name="section"></param>
        /// <returns></returns>
        public static Bitmap Copy(this Bitmap sourceBitmap, Rectangle section)
        {
            // Вырезаем выбранный кусок картинки
            Bitmap bmp = new Bitmap(section.Width, section.Height);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.DrawImage(sourceBitmap, 0, 0, section, GraphicsUnit.Pixel);
            }
            //Возвращаем кусок картинки.
            return bmp;
        }

        /// <summary>
        /// Делит изображуху на части
        /// </summary>
        /// <param name="sourceBitmap"></param>
        /// <param name="section"></param>
        /// <returns></returns>
        public static Bitmap Divide(this Bitmap sourceBitmap, Rectangle section)
        {
            int width = sourceBitmap.Width,
               height = sourceBitmap.Height;
            byte[, ,] res = new byte[3, height, width];

            Bitmap exBitmap = (Bitmap)sourceBitmap.Clone();

            BitmapData bd = exBitmap.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadOnly,
            PixelFormat.Format24bppRgb);

            unsafe
            {
                try
                {
                    byte* curpos;
                    fixed (byte* resDirectMemory = res)
                    {
                        byte* r = resDirectMemory, g = resDirectMemory + width * height, b = resDirectMemory + 2 * width * height;
                        for (int h = 0; h < height; h++)
                        {
                            curpos = ((byte*)bd.Scan0) + h * bd.Stride;
                            for (int w = 0; w < width; w++)
                            {
                                *b = *(curpos++); ++b;
                                *g = *(curpos++); ++g;
                                *r = *(curpos++); ++r;
                            }
                        }
                    }
                }
                finally
                {
                    exBitmap.UnlockBits(bd);
                }
            }

            Bitmap bmp = new Bitmap(section.Width, section.Height);
            bd = bmp.LockBits(new Rectangle(0, 0, section.Width, section.Height), ImageLockMode.ReadWrite,
           PixelFormat.Format24bppRgb);

            unsafe
            {
                try
                {
                    byte* curpos;
                    fixed (byte* resDirectMemory = res)
                    {
                        byte* r = resDirectMemory, g = resDirectMemory + width * height, b = resDirectMemory + 2 * width * height;
                        for (int h = section.Y; h < section.Height; h++)
                        {
                            curpos = ((byte*)bd.Scan0) + h * bd.Stride;
                            for (int w = section.X; w < section.Width; w++)
                            {
                                *(curpos++) = *b; ++b;
                                *(curpos++) = *g; ++g;
                                *(curpos++) = *r; ++r;
                            }
                        }
                    }
                }
                finally
                {
                    bmp.UnlockBits(bd);
                }
            }

            //byte[] superres = new byte[3 * section.Width * section.Height];
            //int supercount = 0;
            //for (int h = section.Y; h < section.Height; h++)
            //{
            //    for (int w = section.X; w < section.Width; w++)
            //    {
            //        superres[supercount] = res[0, h, w];
            //        superres[supercount + 1] = res[1, h, w];
            //        superres[supercount + 2] = res[2, h, w];
            //        supercount+=3;
            //    }
            //}
            //BitmapData bmpData = bmp.LockBits(new Rectangle(0, 0, section.Width, section.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            //Marshal.Copy(superres, 0, bmpData.Scan0, bmpData.Stride * bmp.Height);
            //bmp.UnlockBits(bmpData);

            return bmp;
        }

        /// <summary>
        /// Фильтрация каналов (На указателях)
        /// </summary>
        /// <param name="sourceBitmap"></param>
        /// <param name="filterR"> Фильтр по красному цвету</param>
        /// <param name="filterG"> Фильтр по зеленому цвету</param>
        /// <param name="filterB"> Фильтр по голубому цвету</param>
        /// <returns></returns>
        public static Bitmap FilterColor(this Bitmap sourceBitmap, byte filterR, byte filterG, byte filterB)
        {
            Bitmap bmp = new Bitmap(sourceBitmap.Width, sourceBitmap.Height);
            int width = sourceBitmap.Width,
               height = sourceBitmap.Height;
            byte[, ,] res = new byte[3, height, width];

            BitmapData bd = sourceBitmap.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadOnly,
            PixelFormat.Format24bppRgb);

            unsafe
            {
                try
                {
                    byte* curpos;
                    fixed (byte* resDirectMemory = res)
                    {
                        byte* r = resDirectMemory, g = resDirectMemory + width * height, b = resDirectMemory + 2 * width * height,
                            _fR = &filterR, _fG = &filterG, _fB = &filterB;
                        for (int h = 0; h < height; h++)
                        {
                            curpos = ((byte*)bd.Scan0) + h * bd.Stride;
                            for (int w = 0; w < width; w++)
                            {
                                *b = *(curpos++);
                                if (*g > *_fB) { *b -= *_fB; ++b; }
                                else { ++b; }
                                *g = *(curpos++);
                                if (*g > *_fG) { *g -= *_fG; ++g; }
                                else { ++g; }
                                *r = *(curpos++);
                                if (*r > *_fR) { *r -= *_fR; ++r; }
                                else { ++r; }
                            }
                        }
                    }
                }
                finally
                {
                    sourceBitmap.UnlockBits(bd);
                }
            }

            bd = bmp.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite,
            PixelFormat.Format24bppRgb);

            unsafe
            {
                try
                {
                    byte* curpos;
                    fixed (byte* resDirectMemory = res)
                    {
                        byte* r = resDirectMemory, g = resDirectMemory + width * height, b = resDirectMemory + 2 * width * height;
                        for (int h = 0; h < height; h++)
                        {
                            curpos = ((byte*)bd.Scan0) + h * bd.Stride;
                            for (int w = 0; w < width; w++)
                            {
                                *(curpos++) = *b; ++b;
                                *(curpos++) = *g; ++g;
                                *(curpos++) = *r; ++r;
                            }
                        }
                    }
                }
                finally
                {
                    bmp.UnlockBits(bd);
                }
            }

            return bmp;
        }

        public static Color DominatingColor(byte[,] sample)
        {
            byte[] hist = new byte[256];

            int length = sample.GetLength(0);
            int[] y = new int[length];
            for (int i = 0; i < length; i++)
            {
                y[i] = Convert.ToInt32(0.299 * sample[i, 2] + 0.587 * sample[i, 1] + 0.114 * sample[i, 0]);
                hist[y[i]]++;
            }

            int t = Array.FindIndex(hist, v => v.Equals(hist.Max()));
            int p = Array.FindIndex(y, v => v.Equals(t));

            return Color.FromArgb(sample[p, 2], sample[p, 1], sample[p, 0]); 
        }
        //Как ей пользоваться:
        //Образец изображения
        //Bitmap Ti = image.Bitmap;
        //Image<Bgr, byte> BGR = new Image<Bgr, byte>(Ti);
        //byte[,] ImageHUE = BGR.Sample(new LineSegment2D(new Point(0, Convert.ToInt32(Ti.Height / 2)), new Point(Ti.Width, Convert.ToInt32(Ti.Height / 2))));
        //Color Domning = ExtBitmap.DominatingColor(ImageHUE);
        //BGR = new Image<Bgr, byte>(Ti.filterColor(Domning.R, Domning.G, Domning.B));
    }   
}
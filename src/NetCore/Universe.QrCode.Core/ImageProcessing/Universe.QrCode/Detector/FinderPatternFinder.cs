﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;
using System.Collections.Generic;
using Universe.QrCode.Common.Types;
using Universe.QrCode.Models;

namespace Universe.QrCode.Detector
{
    /// <summary>
    ///     <p>
    ///         Этот класс пытается найти шаблоны поиска в QR-коде. Шаблоны Finder - это квадратные маркеры в трех углах QR-кода.
    ///     </p>
    ///     <p>Этот класс является потокобезопасным, но не реентерабельным. Каждый поток должен выделить свой собственный объект.</p>
    /// </summary>
    /// <author>Sean Owen</author>
    /// <author>Alex Envision</author>
    public class FinderPatternFinder
    {
        private const int CENTER_QUORUM = 2;

        /// <summary>
        ///     1 pixel/module times 3 modules/center
        /// </summary>
        protected internal const int MIN_SKIP = 3;

        /// <summary>
        ///     support up to version 20 for mobile clients
        /// </summary>
        protected internal const int MAX_MODULES = 97;

        private const int INTEGER_MATH_SHIFT = 8;
        private static readonly EstimatedModuleComparator moduleComparator = new EstimatedModuleComparator();
        private readonly int[] _crossCheckStateCount;

        private readonly BitMatrix _image;
        private readonly List<FinderPattern> _possibleCenters;
        private readonly ResultPointCallback _resultPointCallback;
        private bool _hasSkipped;

        /// <summary>
        ///     <p>Creates a finder that will search the image for three finder patterns.</p>
        /// </summary>
        /// <param name="image">image to search</param>
        public FinderPatternFinder(BitMatrix image)
            : this(image, null)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="FinderPatternFinder" /> class.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <param name="resultPointCallback">The result point callback.</param>
        public FinderPatternFinder(BitMatrix image, ResultPointCallback resultPointCallback)
        {
            this._image = image;
            _possibleCenters = new List<FinderPattern>();
            _crossCheckStateCount = new int[5];
            this._resultPointCallback = resultPointCallback;
        }

        /// <summary>
        ///     Gets the image.
        /// </summary>
        protected internal virtual BitMatrix Image => _image;

        /// <summary>
        ///     Gets the possible centers.
        /// </summary>
        protected internal virtual List<FinderPattern> PossibleCenters => _possibleCenters;

        private int[] CrossCheckStateCount
        {
            get
            {
                DoClearCounts(_crossCheckStateCount);
                return _crossCheckStateCount;
            }
        }

        internal virtual FinderPatternInfo Find(IDictionary<DecodeHintType, object> hints)
        {
            var tryHarder = hints != null && hints.ContainsKey(DecodeHintType.TRY_HARDER);
            var maxI = _image.Height;
            var maxJ = _image.Width;
            // Ищем модули черный / белый / черный / белый / черный в соотношении 
            // 1: 1: 3: 1: 1; это позволяет отследить количество таких модулей, замеченных на данный момент.
            // Предположим, что максимальная версия QR-кода, которую мы поддерживаем, занимает 1 / 4 высоты изображения,
            // а затем учтем, что размер центра составляет 3 модуля. Это дает наименьшее количество пикселей в центре, 
            // поэтому часто пропускайте это. Когда действует форсированно, ищутся все версии QR, независимо от того, насколько они плотно расположены.
            var iSkip = 3 * maxI / (4 * MAX_MODULES);
            if (iSkip < MIN_SKIP || tryHarder)
                iSkip = MIN_SKIP;

            var done = false;
            var stateCount = new int[5];
            for (var i = iSkip - 1; i < maxI && !done; i += iSkip)
            {
                // Получаем ряд черных / белых значений
                DoClearCounts(stateCount);
                var currentState = 0;
                for (var j = 0; j < maxJ; j++)
                    if (_image[j, i])
                    {
                        // Черный пиксель
                        if ((currentState & 1) == 1)
                            currentState++;
                        stateCount[currentState]++;
                    }
                    else
                    {
                        // Белый пиксель
                        if ((currentState & 1) == 0)
                            if (currentState == 4)
                                if (FoundPatternCross(stateCount))
                                {
                                    // Подтверждаем
                                    var confirmed = HandlePossibleCenter(stateCount, i, j);
                                    if (confirmed)
                                    {
                                        // Начинаем изучать каждую вторую строку. 
                                        // Проверять каждую строку оказывается слишком дорого и это не улучшает производительность.
                                        iSkip = 2;
                                        if (_hasSkipped)
                                        {
                                            done = HaveMultiplyConfirmedCenters();
                                        }
                                        else
                                        {
                                            var rowSkip = FindRowSkip();
                                            if (rowSkip > stateCount[2])
                                            {
                                                // Пропускаем строки между строкой нижнего подтвержденного центра 
                                                // и верхней частью предполагаемого третьего подтвержденного центра, 
                                                // но немного сделаем резервную копию, чтобы получить полный шанс его обнаружения,
                                                // по всей ширине центра шаблона искателя. Пропустить по rowSkip, 
                                                // но отступить на stateCount [2] (размер последнего центра шаблона,
                                                // который мы видели), чтобы будет более традиционно, 
                                                // а также отступить с помощью iSkip, который будет повторно передобавлен
                                                i += rowSkip - stateCount[2] - iSkip;
                                                j = maxJ - 1;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        DoShiftCounts2(stateCount);
                                        currentState = 3;
                                        continue;
                                    }
                                    // Очистка состояние, чтобы снова начать поиск
                                    currentState = 0;
                                    DoClearCounts(stateCount);
                                }
                                else
                                {
                                    // Нет, сдвиг производится на два
                                    DoShiftCounts2(stateCount);
                                    currentState = 3;
                                }
                            else
                                stateCount[++currentState]++;
                        else
                            stateCount[currentState]++;
                    }
                if (FoundPatternCross(stateCount))
                {
                    var confirmed = HandlePossibleCenter(stateCount, i, maxJ);
                    if (confirmed)
                    {
                        iSkip = stateCount[0];
                        if (_hasSkipped)
                            done = HaveMultiplyConfirmedCenters();
                    }
                }
            }

            var patternInfo = SelectBestPatterns();
            if (patternInfo == null)
                return null;

            ResultPoint.OrderBestPatterns(patternInfo);

            return new FinderPatternInfo(patternInfo);
        }

        /// <summary>
        ///     Учитывает количество только что увиденных черных / белых / черных / белых / черных пикселей и конечное положение,
        ///     вычисляет местоположение центра во время прохождения..
        /// </summary>
        private static float? CenterFromEnd(int[] stateCount, int end)
        {
            var result = end - stateCount[4] - stateCount[3] - stateCount[2] / 2.0f;
            if (float.IsNaN(result))
                return null;
            return result;
        }

        /// <summary>
        ///     Поиск шаблона по горизональному срезу с соотношением 1/1/3/1/1
        /// </summary>
        /// <param name="stateCount">
        ///     количество только что прочитанных черных / белых / черных / белых / черных пикселей
        /// </param>
        /// <returns>
        ///     true, если пропорции подсчетов достаточно близки к соотношениям 1/1/3/1/1
        ///     используемые шаблонами поиска, чтобы считаться совпадением
        /// </returns>
        protected internal static bool FoundPatternCross(int[] stateCount)
        {
            var totalModuleSize = 0;
            for (var i = 0; i < 5; i++)
            {
                var count = stateCount[i];
                if (count == 0)
                    return false;
                totalModuleSize += count;
            }
            if (totalModuleSize < 7)
                return false;
            var moduleSize = (totalModuleSize << INTEGER_MATH_SHIFT) / 7;
            var maxVariance = moduleSize / 2;
            // Допускается отклонение менее 50% от пропорций 1-1-3-1-1
            return Math.Abs(moduleSize - (stateCount[0] << INTEGER_MATH_SHIFT)) < maxVariance &&
                   Math.Abs(moduleSize - (stateCount[1] << INTEGER_MATH_SHIFT)) < maxVariance &&
                   Math.Abs(3 * moduleSize - (stateCount[2] << INTEGER_MATH_SHIFT)) < 3 * maxVariance &&
                   Math.Abs(moduleSize - (stateCount[3] << INTEGER_MATH_SHIFT)) < maxVariance &&
                   Math.Abs(moduleSize - (stateCount[4] << INTEGER_MATH_SHIFT)) < maxVariance;
        }

        /// <summary>
        ///     Поиск шаблона по диагональному срезу с соотношением 1/1/3/1/1
        /// </summary>
        /// <param name="stateCount">количество только что прочитанных черных / белых / черных / белых / черных пикселей</param>
        /// <returns>
        ///     true, если пропорции подсчетов достаточно близки к соотношениям 1/1/3/1/1 по шаблонам поиска, 
        ///     чтобы считаться совпадением
        /// </returns>
        protected static bool FoundPatternDiagonal(int[] stateCount)
        {
            var totalModuleSize = 0;
            for (var i = 0; i < 5; i++)
            {
                var count = stateCount[i];
                if (count == 0)
                    return false;
                totalModuleSize += count;
            }
            if (totalModuleSize < 7)
                return false;
            var moduleSize = totalModuleSize / 7.0f;
            var maxVariance = moduleSize / 1.333f;
            // Допускаются отклонения менее 75% от пропорций 1-1-3-1-1
            return
                Math.Abs(moduleSize - stateCount[0]) < maxVariance &&
                Math.Abs(moduleSize - stateCount[1]) < maxVariance &&
                Math.Abs(3.0f * moduleSize - stateCount[2]) < 3 * maxVariance &&
                Math.Abs(moduleSize - stateCount[3]) < maxVariance &&
                Math.Abs(moduleSize - stateCount[4]) < maxVariance;
        }

        /// <summary>
        ///     Устанавливает все на 0
        /// </summary>
        /// <param name="counts"></param>
        [Obsolete]
        protected void ClearCounts(int[] counts)
        {
            DoClearCounts(counts);
        }

        /// <summary>
        ///     Сдвигает влево на 2 индекса
        /// </summary>
        /// <param name="stateCount"></param>
        [Obsolete]
        protected void ShiftCounts2(int[] stateCount)
        {
            DoShiftCounts2(stateCount);
        }

        /// <summary>
        ///     Устанавливает все на 0
        /// </summary>
        /// <param name="counts"></param>
        protected static void DoClearCounts(int[] counts)
        {
            SupportClass.Fill(counts, 0);
        }

        /// <summary>
        ///     Сдвигает влево на 2 индекса
        /// </summary>
        /// <param name="stateCount"></param>
        protected static void DoShiftCounts2(int[] stateCount)
        {
            stateCount[0] = stateCount[2];
            stateCount[1] = stateCount[3];
            stateCount[2] = stateCount[4];
            stateCount[3] = 1;
            stateCount[4] = 0;
        }

        /// <summary>
        ///       После того, как вертикальное и горизонтальное сканирование обнаруживает потенциальный шаблон искателя,
        ///       этот метод т.н «пересечение-пересечение-проверка» выполняется путем сканирования вниз по диагонали 
        ///       через центр возможного шаблона искателя, для того чтобы увидеть, обнаруживается ли такая же пропорция.
        ///       «Пересечение-пересечение-проверка» в данном случае выполняется по диагонали.
        /// </summary>
        /// <param name="centerI">строка, в которой был обнаружен образец искателя</param>
        /// <param name="centerJ">центр секции, которая, кажется, пересекает шаблон искателя</param>
        /// <returns>true, если пропорции находятся в ожидаемых пределах</returns>
        private bool CrossCheckDiagonal(int centerI, int centerJ)
        {
            var stateCount = CrossCheckStateCount;

            // Начало отсчета, слева от центра с нахождением черного центр массы
            var i = 0;
            while (centerI >= i && centerJ >= i && _image[centerJ - i, centerI - i])
            {
                stateCount[2]++;
                i++;
            }
            if (stateCount[2] == 0)
                return false;

            // Продолжаем движение вверх, налево, пока не будет найдено пустое пространство
            while (centerI >= i && centerJ >= i && !_image[centerJ - i, centerI - i])
            {
                stateCount[1]++;
                i++;
            }
            if (stateCount[1] == 0)
                return false;

            // Продолжаем движение вверх, налево пока не будет найдена черная рамка
            while (centerI >= i && centerJ >= i && _image[centerJ - i, centerI - i])
            {
                stateCount[0]++;
                i++;
            }
            if (stateCount[0] == 0)
                return false;

            var maxI = _image.Height;
            var maxJ = _image.Width;

            // Теперь также обратный отсчет, прямо из центра
            i = 1;
            while (centerI + i < maxI && centerJ + i < maxJ && _image[centerJ + i, centerI + i])
            {
                stateCount[2]++;
                i++;
            }

            while (centerI + i < maxI && centerJ + i < maxJ && !_image[centerJ + i, centerI + i])
            {
                stateCount[3]++;
                i++;
            }
            if (stateCount[3] == 0)
                return false;

            while (centerI + i < maxI && centerJ + i < maxJ && _image[centerJ + i, centerI + i])
            {
                stateCount[4]++;
                i++;
            }
            if (stateCount[4] == 0)
                return false;

            return FoundPatternDiagonal(stateCount);
        }

        /// <summary>
        ///     <p>
        ///         После того, как горизонтальное сканирование находит потенциальный искомый образец, этот метод выполняется по типу
        ///         «пересеклись-проверили» путем сканирования вниз по вертикали через центр возможного шаблона искателя, 
        ///         чтобы увидеть, обнаружена ли такая же пропорция.
        ///     </p>
        /// </summary>
        /// <param name="startI">строка, в которой был обнаружен ли искомый образец</param>
        /// <param name="centerJ">центр секции, которая, кажется, пересекает искомый шаблон</param>
        /// <param name="maxCount">
        ///     максимальное разумное количество модулей, которое должно наблюдаться в любом состоянии чтения, 
        ///     по результатам горизонтальной развертки
        /// </param>
        /// <param name="originalStateCountTotal">The original state count total.</param>
        /// <returns>
        ///     вертикальный центр шаблона искателя или ноль, если не найден
        /// </returns>
        private float? CrossCheckVertical(int startI, int centerJ, int maxCount, int originalStateCountTotal)
        {
            var maxI = _image.Height;
            var stateCount = CrossCheckStateCount;

            // Начать отсчет от центра
            var i = startI;
            while (i >= 0 && _image[centerJ, i])
            {
                stateCount[2]++;
                i--;
            }
            if (i < 0)
                return null;
            while (i >= 0 && !_image[centerJ, i] && stateCount[1] <= maxCount)
            {
                stateCount[1]++;
                i--;
            }
            //Если в этом состоянии уже слишком много модулей или они вышли за границу:
            if (i < 0 || stateCount[1] > maxCount)
                return null;
            while (i >= 0 && _image[centerJ, i] && stateCount[0] <= maxCount)
            {
                stateCount[0]++;
                i--;
            }
            if (stateCount[0] > maxCount)
                return null;

            // Теперь также ведём обратный отсчет от центра
            i = startI + 1;
            while (i < maxI && _image[centerJ, i])
            {
                stateCount[2]++;
                i++;
            }
            if (i == maxI)
                return null;
            while (i < maxI && !_image[centerJ, i] && stateCount[3] < maxCount)
            {
                stateCount[3]++;
                i++;
            }
            if (i == maxI || stateCount[3] >= maxCount)
                return null;
            while (i < maxI && _image[centerJ, i] && stateCount[4] < maxCount)
            {
                stateCount[4]++;
                i++;
            }
            if (stateCount[4] >= maxCount)
                return null;

            // Если мы нашли раздел, похожий на шаблон поиска, 
            // но его размер более чем на 40% отличается от исходного, предположим, что это ложное срабатывание.
            var stateCountTotal = stateCount[0] + stateCount[1] + stateCount[2] + stateCount[3] + stateCount[4];
            if (5 * Math.Abs(stateCountTotal - originalStateCountTotal) >= 2 * originalStateCountTotal)
                return null;

            return FoundPatternCross(stateCount) ? CenterFromEnd(stateCount, i) : null;
        }

        /// <summary>
        ///     <p>
        ///          Подобно {CrossCheckVertical(int, int, int, int)} и фактически идентичен,
        ///          за исключением того, что он читается по горизонтали, а не по вертикали. 
        ///          Это используется для перекрестной проверки по горизонтали и 
        ///          определения реального центра образца выравнивания.
        ///     </p>
        /// </summary>
        private float? CrossCheckHorizontal(int startJ, int centerI, int maxCount, int originalStateCountTotal)
        {
            var maxJ = _image.Width;
            var stateCount = CrossCheckStateCount;

            var j = startJ;
            while (j >= 0 && _image[j, centerI])
            {
                stateCount[2]++;
                j--;
            }
            if (j < 0)
                return null;
            while (j >= 0 && !_image[j, centerI] && stateCount[1] <= maxCount)
            {
                stateCount[1]++;
                j--;
            }
            if (j < 0 || stateCount[1] > maxCount)
                return null;
            while (j >= 0 && _image[j, centerI] && stateCount[0] <= maxCount)
            {
                stateCount[0]++;
                j--;
            }
            if (stateCount[0] > maxCount)
                return null;

            j = startJ + 1;
            while (j < maxJ && _image[j, centerI])
            {
                stateCount[2]++;
                j++;
            }
            if (j == maxJ)
                return null;
            while (j < maxJ && !_image[j, centerI] && stateCount[3] < maxCount)
            {
                stateCount[3]++;
                j++;
            }
            if (j == maxJ || stateCount[3] >= maxCount)
                return null;
            while (j < maxJ && _image[j, centerI] && stateCount[4] < maxCount)
            {
                stateCount[4]++;
                j++;
            }
            if (stateCount[4] >= maxCount)
                return null;

            // Если мы нашли раздел, похожий на шаблон поиска, но его размер значительно отличается от оригинала,
            // предположим, что это ложное срабатывание.
            var stateCountTotal = stateCount[0] + stateCount[1] + stateCount[2] + stateCount[3] + stateCount[4];
            if (5 * Math.Abs(stateCountTotal - originalStateCountTotal) >= originalStateCountTotal)
                return null;

            return FoundPatternCross(stateCount) ? CenterFromEnd(stateCount, j) : null;
        }

        /// <summary>
        ///     @see #handlePossibleCenter(int[], int, int)
        /// </summary>
        /// <param name="stateCount">reading state module counts from horizontal scan</param>
        /// <param name="i">row where finder pattern may be found</param>
        /// <param name="j">end of possible finder pattern in row</param>
        /// <param name="pureBarcode">ignored</param>
        /// <returns>true if a finder pattern candidate was found this time</returns>
        [Obsolete("only exists for backwards compatibility")]
        protected bool HandlePossibleCenter(int[] stateCount, int i, int j, bool pureBarcode)
        {
            return HandlePossibleCenter(stateCount, i, j);
        }

        /// <summary>
        ///     <p>
        ///         Это вызывается, когда горизонтальное сканирование обнаруживает возможный образец выравнивания. 
        ///         Он выполнит перекрестную проверку с вертикальной разверткой, и в случае успеха, произведет перекрестную проверку 
        ///         с другой горизонтальной разверткой.
        ///         Это необходимо в первую очередь для определения реального горизонтального положения.
        ///         центр рисунка в случае сильного перекоса.
        ///         Затем мы проводим перекрестную проверку с другим диагональным сканированием.
        ///     </p>
        ///     Если это удается, местоположение шаблона поиска добавляется в список, который отслеживает, 
        ///     сколько раз каждое местоположение почти совпадало в качестве шаблона поиска.
        ///     Каждая дополнительная находка является еще одним доказательством того, 
        ///     что это место на самом деле является центром образца искателя.
        /// </summary>
        /// <param name="stateCount">отсчет модуля состояния чтения от горизонтальной развертки</param>
        /// <param name="i">строка, в которой можно найти искомый шаблон</param>
        /// <param name="j">конец возможного искомого шаблона в строке</param>
        /// <returns>
        ///     true, если на этот раз был найден кандидат в шаблон поиска
        /// </returns>
        protected bool HandlePossibleCenter(int[] stateCount, int i, int j)
        {
            var stateCountTotal = stateCount[0] + stateCount[1] + stateCount[2] + stateCount[3] +
                                  stateCount[4];
            var centerJ = CenterFromEnd(stateCount, j);
            if (centerJ == null)
                return false;
            var centerI = CrossCheckVertical(i, (int) centerJ.Value, stateCount[2], stateCountTotal);
            if (centerI != null)
            {
                // Повторная перекрестная проверка
                centerJ = CrossCheckHorizontal((int) centerJ.Value, (int) centerI.Value, stateCount[2],
                    stateCountTotal);
                if (centerJ != null && CrossCheckDiagonal((int) centerI, (int) centerJ))
                {
                    var estimatedModuleSize = stateCountTotal / 7.0f;
                    var found = false;
                    for (var index = 0; index < _possibleCenters.Count; index++)
                    {
                        var center = _possibleCenters[index];
                        // Ищется примерно такой же центр и размер модуля:
                        if (center.AboutEquals(estimatedModuleSize, centerI.Value, centerJ.Value))
                        {
                            _possibleCenters.RemoveAt(index);
                            _possibleCenters.Insert(index,
                                center.CombineEstimate(centerI.Value, centerJ.Value, estimatedModuleSize));

                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        var point = new FinderPattern(centerJ.Value, centerI.Value, estimatedModuleSize);

                        _possibleCenters.Add(point);
                        if (_resultPointCallback != null)
                            _resultPointCallback(point);
                    }
                    return true;
                }
            }
            return false;
        }

        /// <returns>
        ///      количество строк, которые мы могли безопасно пропустить во время сканирования,
        ///      на основе первых двух найденных шаблонов поиска. В некоторых случаях их положение позволяет нам сделать вывод,
        ///      что третий узор должен находиться ниже определенной точки ниже на изображении.
        /// </returns>
        private int FindRowSkip()
        {
            var max = _possibleCenters.Count;
            if (max <= 1)
                return 0;
            ResultPoint firstConfirmedCenter = null;
            foreach (var center in _possibleCenters)
                if (center.Count >= CENTER_QUORUM)
                    if (firstConfirmedCenter == null)
                    {
                        firstConfirmedCenter = center;
                    }
                    else
                    {
                        // У нас есть два подтвержденных центра. Как далеко мы можем пропустить, 
                        // прежде чем продолжить поиск следующего?
                        // шаблон? В худшем случае только разница между
                        // разница в координатах x / y двух центров.
                        // Это тот случай, когда вы находите верхний левый край последним.
                        _hasSkipped = true;
                        //UPGRADE_WARNING: Data types in Visual C# might be different.  Verify the accuracy of narrowing conversions. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1042'"
                        return (int) (Math.Abs(firstConfirmedCenter.X - center.X) -
                                      Math.Abs(firstConfirmedCenter.Y - center.Y)) / 2;
                    }
            return 0;
        }

        /// <returns>
        ///     true, если мы нашли не менее 3 шаблонов поиска, каждый из которых был обнаружен не менее N раз, 
        ///     и предполагаемый размер модуля кандидатов "очень похож"
        /// </returns>
        private bool HaveMultiplyConfirmedCenters()
        {
            var confirmedCount = 0;
            var totalModuleSize = 0.0f;
            var max = _possibleCenters.Count;
            foreach (var pattern in _possibleCenters)
                if (pattern.Count >= CENTER_QUORUM)
                {
                    confirmedCount++;
                    totalModuleSize += pattern.EstimatedModuleSize;
                }
            if (confirmedCount < 3)
                return false;
            // OK,у нас есть по крайней мере 3 подтвержденных центра, но, возможно, один из них является «ложноположительным»
            // и нам нужно продолжать поиски. Мы обнаруживаем это, спрашивая, есть ли предполагаемые размеры модулей
            // очень много. Мы условно говорим, что когда общее отклонение от среднего превышает 
            // 5 % от общей оценки размера модуля, это слишком много.
            float average = totalModuleSize / max;
            var totalDeviation = 0.0f;
            for (var i = 0; i < max; i++)
            {
                var pattern = _possibleCenters[i];
                totalDeviation += Math.Abs(pattern.EstimatedModuleSize - average);
            }
            return totalDeviation <= 0.05f * totalModuleSize;
        }

        /// <summary>
        ///    Получение квадрата расстояния между a и b.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        private static double SquaredDistance(FinderPattern a, FinderPattern b)
        {
            double x = a.X - b.X;
            double y = a.Y - b.Y;
            return x * x + y * y;
        }

        /// <returns>
        ///     3 лучших из нашего списка кандидатов. «Лучшими» считаются те,
        ///     которые имеют аналогичный размер модуля и образуют форму,
        ///     близкую к равнобедренному прямоугольному треугольнику.
        /// </returns>
        private FinderPattern[] SelectBestPatterns()
        {
            var startSize = _possibleCenters.Count;
            if (startSize < 3)
                return null;

            _possibleCenters.Sort(moduleComparator);

            var distortion = double.MaxValue;
            var bestPatterns = new FinderPattern[3];

            for (var i = 0; i < _possibleCenters.Count - 2; i++)
            {
                var fpi = _possibleCenters[i];
                var minModuleSize = fpi.EstimatedModuleSize;

                for (var j = i + 1; j < _possibleCenters.Count - 1; j++)
                {
                    var fpj = _possibleCenters[j];
                    var squares0 = SquaredDistance(fpi, fpj);

                    for (var k = j + 1; k < _possibleCenters.Count; k++)
                    {
                        var fpk = _possibleCenters[k];
                        var maxModuleSize = fpk.EstimatedModuleSize;
                        if (maxModuleSize > minModuleSize * 1.4f)
                            continue;

                        var a = squares0;
                        var b = SquaredDistance(fpj, fpk);
                        var c = SquaredDistance(fpi, fpk);

                        // сортировка по возрастанию - линейная
                        if (a < b)
                        {
                            if (b > c)
                                if (a < c)
                                {
                                    var temp = b;
                                    b = c;
                                    c = temp;
                                }
                                else
                                {
                                    var temp = a;
                                    a = c;
                                    c = b;
                                    b = temp;
                                }
                        }
                        else
                        {
                            if (b < c)
                            {
                                if (a < c)
                                {
                                    var temp = a;
                                    a = b;
                                    b = temp;
                                }
                                else
                                {
                                    var temp = a;
                                    a = b;
                                    b = c;
                                    c = temp;
                                }
                            }
                            else
                            {
                                var temp = a;
                                a = c;
                                c = temp;
                            }
                        }

                        // a^2 + b^2 = c^2 (теорема Пифагора) и a = b (равнобедренный треугольник).
                        // Поскольку любой прямоугольный треугольник удовлетворяет формуле c^2 - b^2 - a^2 = 0,
                        // нам нужно проверить обе две равные стороны по отдельности.
                        // Значение | c^2 - 2 * b^2 | + | c^2 - 2*a^2 | увеличивается как несходство
                        // из равнобедренного прямоугольного треугольника.
                        var d = Math.Abs(c - 2 * b) + Math.Abs(c - 2 * a);
                        if (d < distortion)
                        {
                            distortion = d;
                            bestPatterns[0] = fpi;
                            bestPatterns[1] = fpj;
                            bestPatterns[2] = fpk;
                        }
                    }
                }
            }

            if (distortion == double.MaxValue)
                return null;

            return bestPatterns;
        }

        /// <summary>
        ///     Компаратор для сортировки по размеру оцененных модулей
        /// </summary>
        private sealed class EstimatedModuleComparator : IComparer<FinderPattern>
        {
            public int Compare(FinderPattern center1, FinderPattern center2)
            {
                if (center1.EstimatedModuleSize == center2.EstimatedModuleSize)
                    return 0;
                if (center1.EstimatedModuleSize < center2.EstimatedModuleSize)
                    return -1;
                return 1;
            }
        }
    }
}
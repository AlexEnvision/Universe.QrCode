﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;
using System.Collections.Generic;
using Universe.QrCode.Common;
using Universe.QrCode.Common.DataContracts;
using Universe.QrCode.Common.Extensions;
using Universe.QrCode.Common.GridSampler;
using Universe.QrCode.Common.Types;
using Universe.QrCode.Models;
using Version = Universe.QrCode.Decoder.Version;

namespace Universe.QrCode.Detector
{
    /// <summary>
    ///     <p>
    ///         Инкапсулирует логику, которая может обнаруживать QR-код на изображении, даже если QR-код
    ///         повернут, перекошены или частично скрыты.
    ///     </p>
    /// </summary>
    /// <author>Sean Owen</author>
	/// <author>Alex Envision</author>
    public class Detector
    {
        private readonly BitMatrix _image;
        private ResultPointCallback _resultPointCallback;

        /// <summary>
        ///     Initializes a new instance of the <see cref="Detector" /> class.
        /// </summary>
        /// <param name="image">The image.</param>
        public Detector(BitMatrix image)
        {
            _image = image;
        }

        /// <summary>
        ///     Gets the image.
        /// </summary>
        protected internal virtual BitMatrix Image => _image;

        /// <summary>
        ///     Gets the result point callback.
        /// </summary>
        protected internal virtual ResultPointCallback ResultPointCallback => _resultPointCallback;

        /// <summary>
        ///     <p>Detects a QR Code in an image.</p>
        /// </summary>
        /// <returns>
        ///     <see cref="DetectorResult" /> encapsulating results of detecting a QR Code
        /// </returns>
        public virtual DetectorResult Detect()
        {
            return Detect(null);
        }

        /// <summary>
        ///     <p>Обнаруживает QR-код на изображении.</p>
        /// </summary>
        /// <param name="hints">optional hints to detector</param>
        /// <returns>
        ///     <see cref="DetectorResult" /> encapsulating results of detecting a QR Code
        /// </returns>
        public virtual DetectorResult Detect(IDictionary<DecodeHintType, object> hints)
        {
            _resultPointCallback = hints == null || !hints.ContainsKey(DecodeHintType.NEED_RESULT_POINT_CALLBACK)
                ? null
                : (ResultPointCallback) hints[DecodeHintType.NEED_RESULT_POINT_CALLBACK];

            var finder = new FinderPatternFinder(_image, _resultPointCallback);
            var info = finder.Find(hints);
            if (info == null)
                return null;

            return ProcessFinderPatternInfo(info);
        }

        /// <summary>
        ///     Обрабатывает информацию о шаблоне поиска.
        /// </summary>
        /// <param name="info">The info.</param>
        /// <returns></returns>
        protected internal virtual DetectorResult ProcessFinderPatternInfo(FinderPatternInfo info)
        {
            var topLeft = info.TopLeft;
            var topRight = info.TopRight;
            var bottomLeft = info.BottomLeft;

            var moduleSize = CalculateModuleSize(topLeft, topRight, bottomLeft);
            if (moduleSize < 1.0f)
                return null;
            int dimension;
            if (!ComputeDimension(topLeft, topRight, bottomLeft, moduleSize, out dimension))
                return null;
            var provisionalVersion = Version.GetProvisionalVersionForDimension(dimension);
            if (provisionalVersion == null)
                return null;
            var modulesBetweenFpCenters = provisionalVersion.DimensionForVersion - 7;

            AlignmentPattern alignmentPattern = null;
            // Anything above version 1 has an alignment pattern
            if (provisionalVersion.AlignmentPatternCenters.Length > 0)
            {
                // Guess where a "bottom right" finder pattern would have been
                var bottomRightX = topRight.X - topLeft.X + bottomLeft.X;
                var bottomRightY = topRight.Y - topLeft.Y + bottomLeft.Y;

                // Estimate that alignment pattern is closer by 3 modules
                // from "bottom right" to known top left location
                //UPGRADE_WARNING: Data types in Visual C# might be different.  Verify the accuracy of narrowing conversions. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1042'"
                var correctionToTopLeft = 1.0f - 3.0f / modulesBetweenFpCenters;
                //UPGRADE_WARNING: Data types in Visual C# might be different.  Verify the accuracy of narrowing conversions. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1042'"
                var estAlignmentX = (int) (topLeft.X + correctionToTopLeft * (bottomRightX - topLeft.X));
                //UPGRADE_WARNING: Data types in Visual C# might be different.  Verify the accuracy of narrowing conversions. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1042'"
                var estAlignmentY = (int) (topLeft.Y + correctionToTopLeft * (bottomRightY - topLeft.Y));

                // Kind of arbitrary -- expand search radius before giving up
                for (var i = 4; i <= 16; i <<= 1)
                {
                    alignmentPattern = FindAlignmentInRegion(moduleSize, estAlignmentX, estAlignmentY, i);
                    if (alignmentPattern == null)
                        continue;
                    break;
                }
                // If we didn't find alignment pattern... well try anyway without it
            }

            var transform = CreateTransform(topLeft, topRight, bottomLeft, alignmentPattern, dimension);

            var bits = SampleGrid(_image, transform, dimension);
            if (bits == null)
                return null;

            ResultPoint[] points;
            if (alignmentPattern == null)
                points = new ResultPoint[] {bottomLeft, topLeft, topRight};
            else
                points = new ResultPoint[] {bottomLeft, topLeft, topRight, alignmentPattern};
            return new DetectorResult(bits, points);
        }

        private static PerspectiveTransform CreateTransform(ResultPoint topLeft, ResultPoint topRight,
            ResultPoint bottomLeft, ResultPoint alignmentPattern, int dimension)
        {
            //UPGRADE_WARNING: Data types in Visual C# might be different.  Verify the accuracy of narrowing conversions. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1042'"
            var dimMinusThree = dimension - 3.5f;
            float bottomRightX;
            float bottomRightY;
            float sourceBottomRightX;
            float sourceBottomRightY;
            if (alignmentPattern != null)
            {
                bottomRightX = alignmentPattern.X;
                bottomRightY = alignmentPattern.Y;
                sourceBottomRightX = sourceBottomRightY = dimMinusThree - 3.0f;
            }
            else
            {
                // Don't have an alignment pattern, just make up the bottom-right point
                bottomRightX = topRight.X - topLeft.X + bottomLeft.X;
                bottomRightY = topRight.Y - topLeft.Y + bottomLeft.Y;
                sourceBottomRightX = sourceBottomRightY = dimMinusThree;
            }

            return PerspectiveTransform.QuadrilateralToQuadrilateral(
                3.5f,
                3.5f,
                dimMinusThree,
                3.5f,
                sourceBottomRightX,
                sourceBottomRightY,
                3.5f,
                dimMinusThree,
                topLeft.X,
                topLeft.Y,
                topRight.X,
                topRight.Y,
                bottomRightX,
                bottomRightY,
                bottomLeft.X,
                bottomLeft.Y);
        }

        private static BitMatrix SampleGrid(BitMatrix image, PerspectiveTransform transform, int dimension)
        {
            var sampler = GridSampler.Instance;
            return sampler.SampleGrid(image, dimension, dimension, transform);
        }

        /// <summary>
        ///     <p>
        ///         Computes the dimension (number of modules on a size) of the QR Code based on the position
        ///         of the finder patterns and estimated module size.
        ///     </p>
        /// </summary>
        private static bool ComputeDimension(ResultPoint topLeft, ResultPoint topRight, ResultPoint bottomLeft,
            float moduleSize, out int dimension)
        {
            var tltrCentersDimension = MathUtils.Round(ResultPoint.Distance(topLeft, topRight) / moduleSize);
            var tlblCentersDimension = MathUtils.Round(ResultPoint.Distance(topLeft, bottomLeft) / moduleSize);
            dimension = ((tltrCentersDimension + tlblCentersDimension) >> 1) + 7;
            switch (dimension & 0x03)
            {
                // mod 4
                case 0:
                    dimension++;
                    break;
                // 1? do nothing
                case 2:
                    dimension--;
                    break;
                case 3:
                    return true;
            }
            return true;
        }

        /// <summary>
        ///     <p>
        ///         Computes an average estimated module size based on estimated derived from the positions
        ///         of the three finder patterns.
        ///     </p>
        /// </summary>
        /// <param name="topLeft">detected top-left finder pattern center</param>
        /// <param name="topRight">detected top-right finder pattern center</param>
        /// <param name="bottomLeft">detected bottom-left finder pattern center</param>
        /// <returns>estimated module size</returns>
        protected internal virtual float CalculateModuleSize(ResultPoint topLeft, ResultPoint topRight,
            ResultPoint bottomLeft)
        {
            // Take the average
            return (CalculateModuleSizeOneWay(topLeft, topRight) + CalculateModuleSizeOneWay(topLeft, bottomLeft)) /
                   2.0f;
        }

        /// <summary>
        ///     <p>
        ///         Estimates module size based on two finder patterns -- it uses
        ///         {@link #sizeOfBlackWhiteBlackRunBothWays(int, int, int, int)} to figure the
        ///         width of each, measuring along the axis between their centers.
        ///     </p>
        /// </summary>
        private float CalculateModuleSizeOneWay(ResultPoint pattern, ResultPoint otherPattern)
        {
            //UPGRADE_WARNING: Data types in Visual C# might be different.  Verify the accuracy of narrowing conversions. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1042'"
            var moduleSizeEst1 = SizeOfBlackWhiteBlackRunBothWays((int) pattern.X, (int) pattern.Y,
                (int) otherPattern.X, (int) otherPattern.Y);
            //UPGRADE_WARNING: Data types in Visual C# might be different.  Verify the accuracy of narrowing conversions. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1042'"
            var moduleSizeEst2 = SizeOfBlackWhiteBlackRunBothWays((int) otherPattern.X, (int) otherPattern.Y,
                (int) pattern.X, (int) pattern.Y);
            if (float.IsNaN(moduleSizeEst1))
                return moduleSizeEst2 / 7.0f;
            if (float.IsNaN(moduleSizeEst2))
                return moduleSizeEst1 / 7.0f;
            // Average them, and divide by 7 since we've counted the width of 3 black modules,
            // and 1 white and 1 black module on either side. Ergo, divide sum by 14.
            return (moduleSizeEst1 + moduleSizeEst2) / 14.0f;
        }

        /// <summary>
        ///     See {@link #sizeOfBlackWhiteBlackRun(int, int, int, int)}; computes the total width of
        ///     a finder pattern by looking for a black-white-black run from the center in the direction
        ///     of another point (another finder pattern center), and in the opposite direction too.
        /// </summary>
        private float SizeOfBlackWhiteBlackRunBothWays(int fromX, int fromY, int toX, int toY)
        {
            var result = SizeOfBlackWhiteBlackRun(fromX, fromY, toX, toY);

            // Now count other way -- don't run off image though of course
            var scale = 1.0f;
            var otherToX = fromX - (toX - fromX);
            if (otherToX < 0)
            {
                //UPGRADE_WARNING: Data types in Visual C# might be different.  Verify the accuracy of narrowing conversions. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1042'"
                scale = fromX / (float) (fromX - otherToX);
                otherToX = 0;
            }
            else if (otherToX >= _image.Width)
            {
                //UPGRADE_WARNING: Data types in Visual C# might be different.  Verify the accuracy of narrowing conversions. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1042'"
                scale = (_image.Width - 1 - fromX) / (float) (otherToX - fromX);
                otherToX = _image.Width - 1;
            }
            //UPGRADE_WARNING: Data types in Visual C# might be different.  Verify the accuracy of narrowing conversions. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1042'"
            var otherToY = (int) (fromY - (toY - fromY) * scale);

            scale = 1.0f;
            if (otherToY < 0)
            {
                //UPGRADE_WARNING: Data types in Visual C# might be different.  Verify the accuracy of narrowing conversions. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1042'"
                scale = fromY / (float) (fromY - otherToY);
                otherToY = 0;
            }
            else if (otherToY >= _image.Height)
            {
                //UPGRADE_WARNING: Data types in Visual C# might be different.  Verify the accuracy of narrowing conversions. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1042'"
                scale = (_image.Height - 1 - fromY) / (float) (otherToY - fromY);
                otherToY = _image.Height - 1;
            }
            //UPGRADE_WARNING: Data types in Visual C# might be different.  Verify the accuracy of narrowing conversions. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1042'"
            otherToX = (int) (fromX + (otherToX - fromX) * scale);

            result += SizeOfBlackWhiteBlackRun(fromX, fromY, otherToX, otherToY);
            return result - 1.0f; // -1 because we counted the middle pixel twice
        }

        /// <summary>
        ///     <p>
        ///         This method traces a line from a point in the image, in the direction towards another point.
        ///         It begins in a black region, and keeps going until it finds white, then black, then white again.
        ///         It reports the distance from the start to this point.
        ///     </p>
        ///     <p>
        ///         This is used when figuring out how wide a finder pattern is, when the finder pattern
        ///         may be skewed or rotated.
        ///     </p>
        /// </summary>
        private float SizeOfBlackWhiteBlackRun(int fromX, int fromY, int toX, int toY)
        {
            // Mild variant of Bresenham's algorithm;
            // see http://en.wikipedia.org/wiki/Bresenham's_line_algorithm
            var steep = Math.Abs(toY - fromY) > Math.Abs(toX - fromX);
            if (steep)
            {
                var temp = fromX;
                fromX = fromY;
                fromY = temp;
                temp = toX;
                toX = toY;
                toY = temp;
            }

            var dx = Math.Abs(toX - fromX);
            var dy = Math.Abs(toY - fromY);
            var error = -dx >> 1;
            var xstep = fromX < toX ? 1 : -1;
            var ystep = fromY < toY ? 1 : -1;

            // In black pixels, looking for white, first or second time.
            var state = 0;
            // Loop up until x == toX, but not beyond
            var xLimit = toX + xstep;
            for (int x = fromX, y = fromY; x != xLimit; x += xstep)
            {
                var realX = steep ? y : x;
                var realY = steep ? x : y;

                // Does current pixel mean we have moved white to black or vice versa?
                // Scanning black in state 0,2 and white in state 1, so if we find the wrong
                // color, advance to next state or end if we are in state 2 already
                if (state == 1 == _image[realX, realY])
                {
                    if (state == 2)
                        return MathUtils.Distance(x, y, fromX, fromY);
                    state++;
                }
                error += dy;
                if (error > 0)
                {
                    if (y == toY)
                        break;
                    y += ystep;
                    error -= dx;
                }
            }
            // Found black-white-black; give the benefit of the doubt that the next pixel outside the image
            // is "white" so this last point at (toX+xStep,toY) is the right ending. This is really a
            // small approximation; (toX+xStep,toY+yStep) might be really correct. Ignore this.
            if (state == 2)
                return MathUtils.Distance(toX + xstep, toY, fromX, fromY);
            // else we didn't find even black-white-black; no estimate is really possible
            return float.NaN;
        }

        /// <summary>
        ///     <p>
        ///         Attempts to locate an alignment pattern in a limited region of the image, which is
        ///         guessed to contain it. This method uses {@link AlignmentPattern}.
        ///     </p>
        /// </summary>
        /// <param name="overallEstModuleSize">estimated module size so far</param>
        /// <param name="estAlignmentX">x coordinate of center of area probably containing alignment pattern</param>
        /// <param name="estAlignmentY">y coordinate of above</param>
        /// <param name="allowanceFactor">number of pixels in all directions to search from the center</param>
        /// <returns>
        ///     <see cref="AlignmentPattern" /> if found, or null otherwise
        /// </returns>
        protected AlignmentPattern FindAlignmentInRegion(float overallEstModuleSize, int estAlignmentX,
            int estAlignmentY, float allowanceFactor)
        {
            // Look for an alignment pattern (3 modules in size) around where it
            // should be
            //UPGRADE_WARNING: Data types in Visual C# might be different.  Verify the accuracy of narrowing conversions. "ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?index='!DefaultContextWindowIndex'&keyword='jlca1042'"
            var allowance = (int) (allowanceFactor * overallEstModuleSize);
            var alignmentAreaLeftX = Math.Max(0, estAlignmentX - allowance);
            var alignmentAreaRightX = Math.Min(_image.Width - 1, estAlignmentX + allowance);
            if (alignmentAreaRightX - alignmentAreaLeftX < overallEstModuleSize * 3)
                return null;

            var alignmentAreaTopY = Math.Max(0, estAlignmentY - allowance);
            var alignmentAreaBottomY = Math.Min(_image.Height - 1, estAlignmentY + allowance);

            var alignmentFinder = new AlignmentPatternFinder(
                _image,
                alignmentAreaLeftX,
                alignmentAreaTopY,
                alignmentAreaRightX - alignmentAreaLeftX,
                alignmentAreaBottomY - alignmentAreaTopY,
                overallEstModuleSize,
                _resultPointCallback);

            return alignmentFinder.find();
        }
    }
}
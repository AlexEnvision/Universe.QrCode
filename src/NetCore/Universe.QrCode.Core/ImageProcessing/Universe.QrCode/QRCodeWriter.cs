﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;
using System.Collections.Generic;
using Universe.QrCode.Common.Types;
using Universe.QrCode.Decoder;
using Universe.QrCode.Encoder;
using Universe.QrCode.Models;

namespace Universe.QrCode
{
    /// <summary>
    ///     Этот объект отрисовывает QR-код как двумерный массив BitMatrix со значениями шкалы серого.
    /// <author>dswitkin@google.com (Daniel Switkin)</author>
    /// <author>Alex Envision</author>
    /// </summary>
    public sealed class QRCodeWriter : Writer
    {
        private const int QUIET_ZONE_SIZE = 4;

        /// <summary>
        ///     Кодирует штрих-код, используя настройки по умолчанию.
        /// </summary>
        /// <param name="contents">Содержимое для кодирования в штрих-код</param>
        /// <param name="format">Формат штрих-кода для создания</param>
        /// <param name="width">Предпочтительная ширина в пикселях</param>
        /// <param name="height">Предпочтительная высота в пикселях</param>
        /// <returns>
        ///     Сгенерированный штрих-код как матрица байтов без знака (0 == черный, 255 == белый)
        /// </returns>
        public BitMatrix Encode(string contents, BarcodeFormat format, int width, int height)
        {
            return Encode(contents, format, width, height, null);
        }

        /// <summary>
        ///   Кодирует штрих-код, используя уже конкретные настройки и дополнительные параметры.
        /// </summary>
        /// <param name="contents">Содержимое для кодирования в штрих-код</param>
        /// <param name="format">Формат штрих-кода для создания</param>
        /// <param name="width">Предпочтительная ширина в пикселях</param>
        /// <param name="height">Предпочтительная высота в пикселях</param>
        /// <param name="hints">Additional parameters to supply to the encoder</param>
        /// <returns>
        ///     Сгенерированный штрих-код как матрица байтов без знака (0 == черный, 255 == белый)
        /// </returns>
        public BitMatrix Encode(string contents,
            BarcodeFormat format,
            int width,
            int height,
            IDictionary<EncodeHintType, object> hints)
        {
            if (string.IsNullOrEmpty(contents))
                throw new ArgumentException("Found empty contents");

            if (format != BarcodeFormat.QR_CODE)
                throw new ArgumentException("Can only encode QR_CODE, but got " + format);

            if (width < 0 || height < 0)
                throw new ArgumentException("Requested dimensions are too small: " + width + 'x' + height);

            var errorCorrectionLevel = ErrorCorrectionLevel.L;
            var quietZone = QUIET_ZONE_SIZE;
            if (hints != null)
            {
                if (hints.ContainsKey(EncodeHintType.ERROR_CORRECTION))
                {
                    var requestedEcLevel = hints[EncodeHintType.ERROR_CORRECTION];
                    if (requestedEcLevel != null)
                    {
                        errorCorrectionLevel = requestedEcLevel as ErrorCorrectionLevel;
                        if (errorCorrectionLevel == null)
                            switch (requestedEcLevel.ToString().ToUpper())
                            {
                                case "L":
                                    errorCorrectionLevel = ErrorCorrectionLevel.L;
                                    break;
                                case "M":
                                    errorCorrectionLevel = ErrorCorrectionLevel.M;
                                    break;
                                case "Q":
                                    errorCorrectionLevel = ErrorCorrectionLevel.Q;
                                    break;
                                case "H":
                                    errorCorrectionLevel = ErrorCorrectionLevel.H;
                                    break;
                                default:
                                    errorCorrectionLevel = ErrorCorrectionLevel.L;
                                    break;
                            }
                    }
                }
                if (hints.ContainsKey(EncodeHintType.MARGIN))
                {
                    var quietZoneInt = hints[EncodeHintType.MARGIN];
                    if (quietZoneInt != null)
                        quietZone = Convert.ToInt32(quietZoneInt.ToString());
                }
            }

            var code = Encoder.Encoder.Encode(contents, errorCorrectionLevel, hints);
            return RenderResult(code, width, height, quietZone);
        }

        /// <summary>
        /// Отрисовка результата
        /// Обратить внимание, что входная матрица использует 0 == белый, 1 == черный, 
        /// а выходная матрица использует 0 == черный, 255 == белый (т.е. 8-битное растровое изображение в оттенках серого).
        /// </summary>
        /// <param name="code"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="quietZone"></param>
        /// <returns></returns>
        private static BitMatrix RenderResult(QRCode code, int width, int height, int quietZone)
        {
            var input = code.Matrix;
            if (input == null)
                throw new InvalidOperationException();
            var inputWidth = input.Width;
            var inputHeight = input.Height;
            var qrWidth = inputWidth + (quietZone << 1);
            var qrHeight = inputHeight + (quietZone << 1);
            var outputWidth = Math.Max(width, qrWidth);
            var outputHeight = Math.Max(height, qrHeight);

            var multiple = Math.Min(outputWidth / qrWidth, outputHeight / qrHeight);
            // Заполнение включает в себя как "тихую" зону (the quiet zone),
            // так и дополнительные белые пиксели для соответствия требуемым размерам. 
            // Например, если ввод 25x25, QR будет 33x33, включая "тихую" зону.
            // А если запрошенный размер 200x160, то кратно будет 4 для QR 132x132.
            // Они обрабатывают все отступы от 100x100 (фактический QR) до 200x160.
            var leftPadding = (outputWidth - inputWidth * multiple) / 2;
            var topPadding = (outputHeight - inputHeight * multiple) / 2;

            var output = new BitMatrix(outputWidth, outputHeight);

            for (int inputY = 0, outputY = topPadding; inputY < inputHeight; inputY++, outputY += multiple)
                //Записывае содержимое этой строки штрих-кода
            for (int inputX = 0, outputX = leftPadding; inputX < inputWidth; inputX++, outputX += multiple)
                if (input[inputX, inputY] == 1)
                    output.SetRegion(outputX, outputY, multiple, multiple);

            return output;
        }
    }
}
﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

namespace Universe.QrCode.Common
{
    /// <summary>
    ///     <p>
    ///           Этот класс реализует перспективное преобразование в двух измерениях. 
    ///           Учитывая четыре исходных и четыре конечных точки, он вычислит преобразование, подразумеваемое между ними. 
    ///           Код основан непосредственно на разделе 3.4.2 книги Джорджа Вольберга «Деформация цифрового изображения»(Digital Image Warping); см. страницы 54-56.
    ///     </p>
    /// </summary>
    /// <author>Sean Owen</author>
    /// <author>Alex Envision</author>
    public sealed class PerspectiveTransform
    {
        private readonly float _a11;
        private readonly float _a12;
        private readonly float _a13;
        private readonly float _a21;
        private readonly float _a22;
        private readonly float _a23;
        private readonly float _a31;
        private readonly float _a32;
        private readonly float _a33;

        private PerspectiveTransform(float a11, float a21, float a31, float a12, float a22, float a32, float a13,
            float a23, float a33)
        {
            _a11 = a11;
            _a12 = a12;
            _a13 = a13;
            _a21 = a21;
            _a22 = a22;
            _a23 = a23;
            _a31 = a31;
            _a32 = a32;
            _a33 = a33;
        }

        /// <summary>
        /// </summary>
        /// <param name="x0"></param>
        /// <param name="y0"></param>
        /// <param name="x1"></param>
        /// <param name="y1"></param>
        /// <param name="x2"></param>
        /// <param name="y2"></param>
        /// <param name="x3"></param>
        /// <param name="y3"></param>
        /// <param name="x0P"></param>
        /// <param name="y0P"></param>
        /// <param name="x1P"></param>
        /// <param name="y1P"></param>
        /// <param name="x2P"></param>
        /// <param name="y2P"></param>
        /// <param name="x3P"></param>
        /// <param name="y3P"></param>
        /// <returns></returns>
        public static PerspectiveTransform QuadrilateralToQuadrilateral(float x0, float y0, float x1, float y1,
            float x2, float y2, float x3, float y3, float x0P, float y0P, float x1P, float y1P, float x2P, float y2P,
            float x3P, float y3P)
        {
            var qToS = QuadrilateralToSquare(x0, y0, x1, y1, x2, y2, x3, y3);
            var sToQ = SquareToQuadrilateral(x0P, y0P, x1P, y1P, x2P, y2P, x3P, y3P);
            return sToQ.Times(qToS);
        }

        /// <summary>
        /// </summary>
        /// <param name="points"></param>
        public void TransformPoints(float[] points)
        {
            var a11 = _a11;
            var a12 = _a12;
            var a13 = _a13;
            var a21 = _a21;
            var a22 = _a22;
            var a23 = _a23;
            var a31 = _a31;
            var a32 = _a32;
            var a33 = _a33;
            var maxI = points.Length - 1; // points.length must be even
            for (var i = 0; i < maxI; i += 2)
            {
                var x = points[i];
                var y = points[i + 1];
                var denominator = a13 * x + a23 * y + a33;
                points[i] = (a11 * x + a21 * y + a31) / denominator;
                points[i + 1] = (a12 * x + a22 * y + a32) / denominator;
            }
        }

        /// <summary>Convenience method, not optimized for performance. </summary>
        public void TransformPoints(float[] xValues, float[] yValues)
        {
            var n = xValues.Length;
            for (var i = 0; i < n; i++)
            {
                var x = xValues[i];
                var y = yValues[i];
                var denominator = _a13 * x + _a23 * y + _a33;
                xValues[i] = (_a11 * x + _a21 * y + _a31) / denominator;
                yValues[i] = (_a12 * x + _a22 * y + _a32) / denominator;
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="x0"></param>
        /// <param name="y0"></param>
        /// <param name="x1"></param>
        /// <param name="y1"></param>
        /// <param name="x2"></param>
        /// <param name="y2"></param>
        /// <param name="x3"></param>
        /// <param name="y3"></param>
        /// <returns></returns>
        public static PerspectiveTransform SquareToQuadrilateral(float x0, float y0,
            float x1, float y1,
            float x2, float y2,
            float x3, float y3)
        {
            var dx3 = x0 - x1 + x2 - x3;
            var dy3 = y0 - y1 + y2 - y3;
            if (dx3 == 0.0f && dy3 == 0.0f)
                return new PerspectiveTransform(x1 - x0, x2 - x1, x0,
                    y1 - y0, y2 - y1, y0,
                    0.0f, 0.0f, 1.0f);
            var dx1 = x1 - x2;
            var dx2 = x3 - x2;
            var dy1 = y1 - y2;
            var dy2 = y3 - y2;
            var denominator = dx1 * dy2 - dx2 * dy1;
            var a13 = (dx3 * dy2 - dx2 * dy3) / denominator;
            var a23 = (dx1 * dy3 - dx3 * dy1) / denominator;
            return new PerspectiveTransform(x1 - x0 + a13 * x1, x3 - x0 + a23 * x3, x0,
                y1 - y0 + a13 * y1, y3 - y0 + a23 * y3, y0,
                a13, a23, 1.0f);
        }

        /// <summary>
        /// </summary>
        /// <param name="x0"></param>
        /// <param name="y0"></param>
        /// <param name="x1"></param>
        /// <param name="y1"></param>
        /// <param name="x2"></param>
        /// <param name="y2"></param>
        /// <param name="x3"></param>
        /// <param name="y3"></param>
        /// <returns></returns>
        public static PerspectiveTransform QuadrilateralToSquare(float x0, float y0, float x1, float y1, float x2,
            float y2, float x3, float y3)
        {
            // Here, the adjoint serves as the inverse:
            return SquareToQuadrilateral(x0, y0, x1, y1, x2, y2, x3, y3).BuildAdjoint();
        }

        internal PerspectiveTransform BuildAdjoint()
        {
            // Adjoint is the transpose of the cofactor matrix:
            return new PerspectiveTransform(_a22 * _a33 - _a23 * _a32,
                _a23 * _a31 - _a21 * _a33,
                _a21 * _a32 - _a22 * _a31,
                _a13 * _a32 - _a12 * _a33,
                _a11 * _a33 - _a13 * _a31,
                _a12 * _a31 - _a11 * _a32,
                _a12 * _a23 - _a13 * _a22,
                _a13 * _a21 - _a11 * _a23,
                _a11 * _a22 - _a12 * _a21);
        }

        internal PerspectiveTransform Times(PerspectiveTransform other)
        {
            return new PerspectiveTransform(_a11 * other._a11 + _a21 * other._a12 + _a31 * other._a13,
                _a11 * other._a21 + _a21 * other._a22 + _a31 * other._a23,
                _a11 * other._a31 + _a21 * other._a32 + _a31 * other._a33,
                _a12 * other._a11 + _a22 * other._a12 + _a32 * other._a13,
                _a12 * other._a21 + _a22 * other._a22 + _a32 * other._a23,
                _a12 * other._a31 + _a22 * other._a32 + _a32 * other._a33,
                _a13 * other._a11 + _a23 * other._a12 + _a33 * other._a13,
                _a13 * other._a21 + _a23 * other._a22 + _a33 * other._a23,
                _a13 * other._a31 + _a23 * other._a32 + _a33 * other._a33);
        }
    }
}
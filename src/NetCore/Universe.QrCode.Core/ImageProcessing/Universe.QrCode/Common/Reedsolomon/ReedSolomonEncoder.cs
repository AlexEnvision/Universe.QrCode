﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;
using System.Collections.Generic;

namespace Universe.QrCode.Common.Reedsolomon
{
    /// <summary>
    ///     Реализует кодирование Рида-Соломона, что, собственно, следует из названия.
    /// </summary>
    /// <author>Sean Owen</author>
    /// <author>Alex Envision</author>
    /// <author>William Rucklidge</author>
    /// <author>sanfordsquires</author>
    public sealed class ReedSolomonEncoder
    {
        private readonly IList<GenericGFPoly> _cachedGenerators;
        private readonly GenericGF _field;

        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="field"></param>
        public ReedSolomonEncoder(GenericGF field)
        {
            this._field = field;
            _cachedGenerators = new List<GenericGFPoly>();
            _cachedGenerators.Add(new GenericGFPoly(field, new[] {1}));
        }

        private GenericGFPoly BuildGenerator(int degree)
        {
            if (degree >= _cachedGenerators.Count)
            {
                var lastGenerator = _cachedGenerators[_cachedGenerators.Count - 1];
                for (var d = _cachedGenerators.Count; d <= degree; d++)
                {
                    var nextGenerator =
                        lastGenerator.Multiply(new GenericGFPoly(_field,
                            new[] {1, _field.Exp(d - 1 + _field.GeneratorBase)}));
                    _cachedGenerators.Add(nextGenerator);
                    lastGenerator = nextGenerator;
                }
            }
            return _cachedGenerators[degree];
        }

        /// <summary>
        ///    Кодирование
        /// </summary>
        /// <param name="toEncode"></param>
        /// <param name="ecBytes"></param>
        public void Encode(int[] toEncode, int ecBytes)
        {
            if (ecBytes == 0)
                throw new ArgumentException("No error correction bytes");
            var dataBytes = toEncode.Length - ecBytes;
            if (dataBytes <= 0)
                throw new ArgumentException("No data bytes provided");

            var generator = BuildGenerator(ecBytes);
            var infoCoefficients = new int[dataBytes];
            Array.Copy(toEncode, 0, infoCoefficients, 0, dataBytes);

            var info = new GenericGFPoly(_field, infoCoefficients);
            info = info.MultiplyByMonomial(ecBytes, 1);

            var remainder = info.Divide(generator)[1];
            var coefficients = remainder.Coefficients;
            var numZeroCoefficients = ecBytes - coefficients.Length;
            for (var i = 0; i < numZeroCoefficients; i++)
                toEncode[dataBytes + i] = 0;

            Array.Copy(coefficients, 0, toEncode, dataBytes + numZeroCoefficients, coefficients.Length);
        }
    }
}
﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;
using System.Text;

namespace Universe.QrCode.Common.Reedsolomon
{
    /// <summary>
    ///     <p>
    ///         Представляет собой многочлен, коэффициенты которого являются элементами GF.
    ///         Экземпляры этого класса неизменяемы.
    ///     </p>
    ///     <p>
    ///         Большая заслуга принадлежит Уильяму Раклиджу, поскольку части этого кода являются косвенными
    ///         Это порт его реализации Рида-Соломона на C ++.
    ///     </p>
    /// </summary>
    /// <author>Sean Owen</author>
    /// <author>Alex Envision</author>
    /// <author>William Rucklidge</author>
    /// <author>sanfordsquires</author>
    // ReSharper disable once InconsistentNaming
    internal sealed class GenericGFPoly
    {
        private readonly GenericGF _field;

        /// <summary>
        ///     Инициализирует новый экземпляр класса <see cref="GenericGFPoly" />.
        /// </summary>
        /// <param name="field">
        ///     Экземпляр <see cref="GenericGF" />, представляющий поле для использования
        ///     для выполнения вычислений
        /// </param>
        /// <param name="coefficients">
        ///     Коэффициенты в виде целых чисел, представляющих элементы GF (размер), упорядоченные
        ///     от наиболее значимого (член с наибольшей степенью) до наименее значимого
        /// </param>
        /// <exception cref="ArgumentException">
        ///     Если аргумент нулевой или пустой,
        ///     или если ведущий коэффициент равен 0 и это не
        ///     постоянный многочлен (то есть это не одночлен "0")
        /// </exception>
        internal GenericGFPoly(GenericGF field, int[] coefficients)
        {
            if (coefficients.Length == 0)
                throw new ArgumentException();
            _field = field;
            var coefficientsLength = coefficients.Length;
            if (coefficientsLength > 1 && coefficients[0] == 0)
            {
                // Начальный член должен быть ненулевым ни для чего, кроме постоянного полинома "0"
                var firstNonZero = 1;
                while (firstNonZero < coefficientsLength && coefficients[firstNonZero] == 0)
                    firstNonZero++;
                if (firstNonZero == coefficientsLength)
                {
                    Coefficients = new[] {0};
                }
                else
                {
                    Coefficients = new int[coefficientsLength - firstNonZero];
                    Array.Copy(coefficients,
                        firstNonZero,
                        Coefficients,
                        0,
                        Coefficients.Length);
                }
            }
            else
            {
                Coefficients = coefficients;
            }
        }

        internal int[] Coefficients { get; }

        /// <summary>
        ///     Степень этого многочлена
        /// </summary>
        internal int Degree => Coefficients.Length - 1;

        /// <summary>
        ///     Получает значение, сигнализирующее в случае если <see cref="GenericGFPoly" /> нулевой.
        /// </summary>
        /// <value>true iff this polynomial is the monomial "0"</value>
        internal bool IsZero => Coefficients[0] == 0;

        /// <summary>
        ///     Коэффициент при x^степени члена в этом многочлене
        /// </summary>
        /// <param name="degree">Степень.</param>
        /// <returns>Коэффициент при x^степени члена в этом многочлене</returns>
        internal int GetCoefficient(int degree)
        {
            return Coefficients[Coefficients.Length - 1 - degree];
        }

        /// <summary>
        ///     Оценка этого многочлена в данной точке
        /// </summary>
        /// <param name="a">A.</param>
        /// <returns>Оценка этого многочлена в данной точке</returns>
        internal int EvaluateAt(int a)
        {
            var result = 0;
            if (a == 0)
                return GetCoefficient(0);
            if (a == 1)
            {
                // Просто сумма коэффициентов
                foreach (var coefficient in Coefficients)
                    result = GenericGF.AddOrSubtract(result, coefficient);
                return result;
            }
            result = Coefficients[0];
            var size = Coefficients.Length;
            for (var i = 1; i < size; i++)
                result = GenericGF.AddOrSubtract(_field.Multiply(a, result), Coefficients[i]);
            return result;
        }

        internal GenericGFPoly AddOrSubtract(GenericGFPoly other)
        {
            if (!_field.Equals(other._field))
                throw new ArgumentException("GenericGFPolys do not have same GenericGF field");
            if (IsZero)
                return other;
            if (other.IsZero)
                return this;

            var smallerCoefficients = Coefficients;
            var largerCoefficients = other.Coefficients;
            if (smallerCoefficients.Length > largerCoefficients.Length)
            {
                var temp = smallerCoefficients;
                smallerCoefficients = largerCoefficients;
                largerCoefficients = temp;
            }
            var sumDiff = new int[largerCoefficients.Length];
            var lengthDiff = largerCoefficients.Length - smallerCoefficients.Length;
            // Копирование членов высшего порядка, содержащиеся только в коэффициентах полинома более высокой степени
            Array.Copy(largerCoefficients, 0, sumDiff, 0, lengthDiff);

            for (var i = lengthDiff; i < largerCoefficients.Length; i++)
                sumDiff[i] = GenericGF.AddOrSubtract(smallerCoefficients[i - lengthDiff], largerCoefficients[i]);

            return new GenericGFPoly(_field, sumDiff);
        }

        internal GenericGFPoly Multiply(GenericGFPoly other)
        {
            if (!_field.Equals(other._field))
                throw new ArgumentException("GenericGFPolys do not have same GenericGF field");
            if (IsZero || other.IsZero)
                return _field.Zero;
            var aCoefficients = Coefficients;
            var aLength = aCoefficients.Length;
            var bCoefficients = other.Coefficients;
            var bLength = bCoefficients.Length;
            var product = new int[aLength + bLength - 1];
            for (var i = 0; i < aLength; i++)
            {
                var aCoeff = aCoefficients[i];
                for (var j = 0; j < bLength; j++)
                    product[i + j] = GenericGF.AddOrSubtract(product[i + j],
                        _field.Multiply(aCoeff, bCoefficients[j]));
            }
            return new GenericGFPoly(_field, product);
        }

        internal GenericGFPoly Multiply(int scalar)
        {
            if (scalar == 0)
                return _field.Zero;
            if (scalar == 1)
                return this;
            var size = Coefficients.Length;
            var product = new int[size];
            for (var i = 0; i < size; i++)
                product[i] = _field.Multiply(Coefficients[i], scalar);
            return new GenericGFPoly(_field, product);
        }

        internal GenericGFPoly MultiplyByMonomial(int degree, int coefficient)
        {
            if (degree < 0)
                throw new ArgumentException();
            if (coefficient == 0)
                return _field.Zero;
            var size = Coefficients.Length;
            var product = new int[size + degree];
            for (var i = 0; i < size; i++)
                product[i] = _field.Multiply(Coefficients[i], coefficient);
            return new GenericGFPoly(_field, product);
        }

        internal GenericGFPoly[] Divide(GenericGFPoly other)
        {
            if (!_field.Equals(other._field))
                throw new ArgumentException("GenericGFPolys do not have same GenericGF field");
            if (other.IsZero)
                throw new ArgumentException("Divide by 0");

            var quotient = _field.Zero;
            var remainder = this;

            var denominatorLeadingTerm = other.GetCoefficient(other.Degree);
            var inverseDenominatorLeadingTerm = _field.Inverse(denominatorLeadingTerm);

            while (remainder.Degree >= other.Degree && !remainder.IsZero)
            {
                var degreeDifference = remainder.Degree - other.Degree;
                var scale = _field.Multiply(remainder.GetCoefficient(remainder.Degree), inverseDenominatorLeadingTerm);
                var term = other.MultiplyByMonomial(degreeDifference, scale);
                var iterationQuotient = _field.BuildMonomial(degreeDifference, scale);
                quotient = quotient.AddOrSubtract(iterationQuotient);
                remainder = remainder.AddOrSubtract(term);
            }

            return new[] {quotient, remainder};
        }

        public override string ToString()
        {
            if (IsZero)
                return "0";
            var result = new StringBuilder(8 * Degree);
            for (var degree = Degree; degree >= 0; degree--)
            {
                var coefficient = GetCoefficient(degree);
                if (coefficient != 0)
                {
                    if (coefficient < 0)
                    {
                        if (degree == Degree)
                            result.Append("-");
                        else
                            result.Append(" - ");
                        coefficient = -coefficient;
                    }
                    else
                    {
                        if (result.Length > 0)
                            result.Append(" + ");
                    }
                    if (degree == 0 || coefficient != 1)
                    {
                        var alphaPower = _field.Log(coefficient);
                        if (alphaPower == 0)
                        {
                            result.Append('1');
                        }
                        else if (alphaPower == 1)
                        {
                            result.Append('a');
                        }
                        else
                        {
                            result.Append("a^");
                            result.Append(alphaPower);
                        }
                    }
                    if (degree != 0)
                        if (degree == 1)
                        {
                            result.Append('x');
                        }
                        else
                        {
                            result.Append("x^");
                            result.Append(degree);
                        }
                }
            }
            return result.ToString();
        }
    }
}
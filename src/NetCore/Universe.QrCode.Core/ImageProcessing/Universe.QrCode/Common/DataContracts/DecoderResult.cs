﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;
using System.Collections.Generic;

namespace Universe.QrCode.Common.DataContracts
{
    /// <summary>
    ///     Инкапсулирует результат декодирования матрицы битов. Это обычно
    ///     применяется к форматам 2D штрих-кодов. Пока он содержит полученные необработанные байты,
    ///     а также строковую интерпретацию этих байтов, если применимо.
    ///     <author>Sean Owen</author>
    ///     <author>Alex Envision</author>
    /// </summary>
    public sealed class DecoderResult
    {
        /// <summary>
        ///     initializing constructor
        /// </summary>
        /// <param name="rawBytes"></param>
        /// <param name="text"></param>
        /// <param name="byteSegments"></param>
        /// <param name="ecLevel"></param>
        public DecoderResult(byte[] rawBytes, string text, IList<byte[]> byteSegments, string ecLevel)
            : this(rawBytes, text, byteSegments, ecLevel, -1, -1)
        {
        }

        /// <summary>
        ///     initializing constructor
        /// </summary>
        /// <param name="rawBytes"></param>
        /// <param name="text"></param>
        /// <param name="byteSegments"></param>
        /// <param name="ecLevel"></param>
        /// <param name="saSequence"></param>
        /// <param name="saParity"></param>
        public DecoderResult(byte[] rawBytes, string text, IList<byte[]> byteSegments, string ecLevel, int saSequence,
            int saParity)
            : this(rawBytes, rawBytes == null ? 0 : 8 * rawBytes.Length, text, byteSegments, ecLevel, saSequence,
                saParity)
        {
        }

        /// <summary>
        ///     initializing constructor
        /// </summary>
        /// <param name="rawBytes"></param>
        /// <param name="numBits"></param>
        /// <param name="text"></param>
        /// <param name="byteSegments"></param>
        /// <param name="ecLevel"></param>
        public DecoderResult(byte[] rawBytes, int numBits, string text, IList<byte[]> byteSegments, string ecLevel)
            : this(rawBytes, numBits, text, byteSegments, ecLevel, -1, -1)
        {
        }

        /// <summary>
        ///     initializing constructor
        /// </summary>
        /// <param name="rawBytes"></param>
        /// <param name="numBits"></param>
        /// <param name="text"></param>
        /// <param name="byteSegments"></param>
        /// <param name="ecLevel"></param>
        /// <param name="saSequence"></param>
        /// <param name="saParity"></param>
        public DecoderResult(byte[] rawBytes, int numBits, string text, IList<byte[]> byteSegments, string ecLevel,
            int saSequence, int saParity)
        {
            if (rawBytes == null && text == null)
                throw new ArgumentException();
            RawBytes = rawBytes;
            NumBits = numBits;
            Text = text;
            ByteSegments = byteSegments;
            ECLevel = ecLevel;
            StructuredAppendParity = saParity;
            StructuredAppendSequenceNumber = saSequence;
        }

        /// <summary>
        ///     raw bytes representing the result, or null if not applicable
        /// </summary>
        public byte[] RawBytes { get; }

        /// <summary>
        ///     how many bits of<see cref="RawBytes" /> are valid; typically 8 times its length
        /// </summary>
        public int NumBits { get; }

        /// <summary>
        ///     text representation of the result
        /// </summary>
        public string Text { get; }

        /// <summary>
        ///     list of byte segments in the result, or null if not applicable
        /// </summary>
        public IList<byte[]> ByteSegments { get; }

        /// <summary>
        ///     name of error correction level used, or null if not applicable
        /// </summary>
        public string ECLevel { get; }

        /// <summary>
        ///     gets a value which describe if structure append data was found
        /// </summary>
        public bool StructuredAppend => StructuredAppendParity >= 0 && StructuredAppendSequenceNumber >= 0;

        /// <summary>
        ///     number of errors corrected, or null if not applicable
        /// </summary>
        public int ErrorsCorrected { get; set; }

        /// <summary>
        ///     gives the sequence number of the result if structured append was found
        /// </summary>
        public int StructuredAppendSequenceNumber { get; }

        /// <summary>
        ///     number of erasures corrected, or null if not applicable
        /// </summary>
        public int Erasures { get; set; }

        /// <summary>
        ///     gives the parity information if structured append was found
        /// </summary>
        public int StructuredAppendParity { get; }

        /// <summary>
        ///     Miscellanseous data value for the various decoders
        /// </summary>
        /// <value>The other.</value>
        public object Other { get; set; }
    }
}
﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;
using System.Collections.Generic;
using System.Text;
using Universe.QrCode.Common;
using Universe.QrCode.Common.Reedsolomon;
using Universe.QrCode.Common.Types;
using Universe.QrCode.Decoder;
using Universe.QrCode.Encoder.Exceptions;
using Universe.QrCode.Models;
using Version = Universe.QrCode.Decoder.Version;

namespace Universe.QrCode.Encoder
{
    /// <summary>
    /// Кодировшик
    /// </summary>
    /// <author>satorux@google.com (Satoru Takabayashi) - creator</author>
    /// <author>dswitkin@google.com (Daniel Switkin) - ported from C++</author>
    /// <author>Alex Envision</author>
    public static class Encoder
    {
        // Исходная таблица определена в таблице 5 JISX0510: 2004 (стр.19).
        private static readonly int[] ALPHANUMERIC_TABLE =
        {
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, // 0x00-0x0f
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, // 0x10-0x1f
            36, -1, -1, -1, 37, 38, -1, -1, -1, -1, 39, 40, -1, 41, 42, 43, // 0x20-0x2f
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 44, -1, -1, -1, -1, -1, // 0x30-0x3f
            -1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, // 0x40-0x4f
            25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, -1, -1, -1, -1, -1 // 0x50-0x5f
        };

        internal static string DEFAULT_BYTE_MODE_ENCODING = "ISO-8859-1";

        // Расчет штрафа по маске сложен. Подробности см. В таблице 21 JISX0510: 2004 (стр.45).
        // Обычно он применяет четыре правила и суммирует все штрафы.
        private static int CalculateMaskPenalty(ByteMatrix matrix)
        {
            return MaskUtil.ApplyMaskPenaltyRule1(matrix)
                   + MaskUtil.ApplyMaskPenaltyRule2(matrix)
                   + MaskUtil.ApplyMaskPenaltyRule3(matrix)
                   + MaskUtil.ApplyMaskPenaltyRule4(matrix);
        }

        /// <summary>
        ///  Кодируются «байты» с уровнем коррекции ошибок «ecLevel». Выбирается режим кодирования
        ///  внутренним методом chooseMode(). В случае успеха сохраните результат в qrCode. Рекомендуется использовать QRCode.EC_LEVEL_L(самый низкий уровень) для
        ///  "getECLevel", поскольку наше основное использование - отображение QR-кода на экранах рабочего стола.Для этого нам не нужно очень сильное исправление ошибок.
        ///  Обратить внимание, что нет возможности закодировать байты в MODE_KANJI. Можно добавить EncodeWithMode (), с помощью которого клиенты могут указывать режим кодирования. 
        ///  На данный момент нам этот функционал не нужен.
        /// </summary>
        /// <param name="content">Текст для кодирования</param>
        /// <param name="ecLevel">уровень исправления ошибок для использования</param>
        /// <returns><see cref="QRCode" />Представление закодированного QR-кода</returns>
        public static QRCode Encode(string content, ErrorCorrectionLevel ecLevel)
        {
            return Encode(content, ecLevel, null);
        }

        /// <summary>
        ///     Кодирует указанное содержимое.
        /// </summary>
        /// <param name="content">Содержимое.</param>
        /// <param name="ecLevel">Уровень ec.</param>
        /// <param name="hints">Подсказки.</param>
        /// <returns></returns>
        public static QRCode Encode(string content,
            ErrorCorrectionLevel ecLevel,
            IDictionary<EncodeHintType, object> hints)
        {
            // Определяет, какая кодировка символов была указана вызывающим абонентом, если таковая имеется
            var hasEncodingHint = hints != null && hints.ContainsKey(EncodeHintType.CHARACTER_SET);

#if !SILVERLIGHT || WINDOWS_PHONE
            var encoding = hints == null || !hints.ContainsKey(EncodeHintType.CHARACTER_SET)
                ? null
                : (string) hints[EncodeHintType.CHARACTER_SET];
            if (encoding == null)
                encoding = DEFAULT_BYTE_MODE_ENCODING;
            var generateEci = hasEncodingHint || !DEFAULT_BYTE_MODE_ENCODING.Equals(encoding);
#else // Silverlight из коробки поддерживает только UTF-8 и UTF-16.
         const string encoding = "UTF-8";
         // вызывающий метод может только контролировать, должен ли быть записан сегмент ECI
         // набор символов зафиксирован на UTF-8; но некоторым сканерам не нравится сегмент ECI
         var generateECI = hasEncodingHint;
#endif

            // Выберает режим кодирования, подходящий для содержимого. Обратите внимание, что это не будет пытаться использовать
            // несколько режимов / сегментов, даже если бы они были более эффективными. Было бы хорошо.
            var mode = ChooseMode(content, encoding);

            // Это будет хранить информацию заголовка, такую ​​как режим и длина,
            // а также сегменты «заголовка», такие как сегмент ECI.
            var headerBits = new BitArray();

            // При необходимости добавляет сегмент ECI
            if (mode == Mode.BYTE && generateEci)
            {
                var eci = CharacterSetECI.getCharacterSetECIByName(encoding);
                if (eci != null)
                {
                    var eciIsExplicitDisabled = hints != null && hints.ContainsKey(EncodeHintType.DISABLE_ECI) &&
                                                hints[EncodeHintType.DISABLE_ECI] != null &&
                                                Convert.ToBoolean(hints[EncodeHintType.DISABLE_ECI].ToString());
                    if (!eciIsExplicitDisabled)
                        AppendEci(eci, headerBits);
                }
            }

            // Добавляет заголовок режима FNC1 для данных в формате GS1, если применимо.
            var hasGs1FormatHint = hints != null && hints.ContainsKey(EncodeHintType.GS1_FORMAT);
            if (hasGs1FormatHint && hints[EncodeHintType.GS1_FORMAT] != null &&
                Convert.ToBoolean(hints[EncodeHintType.GS1_FORMAT].ToString()))
                AppendModeInfo(Mode.FNC1_FIRST_POSITION, headerBits);

            // (При установленном ECI) Записывае маркер установленного режима (mode)
            AppendModeInfo(mode, headerBits);

            // Собирает данные в пределах основного сегмента отдельно, чтобы при необходимости подсчитать его размер. 
            // Пока не добавляет его в основную полезную нагрузку.
            var dataBits = new BitArray();
            AppendBytes(content, mode, dataBits, encoding);

            Version version;
            if (hints != null && hints.ContainsKey(EncodeHintType.QR_VERSION))
            {
                var versionNumber = int.Parse(hints[EncodeHintType.QR_VERSION].ToString());
                version = Version.GetVersionForNumber(versionNumber);
                var bitsNeeded = CalculateBitsNeeded(mode, headerBits, dataBits, version);
                if (!WillFit(bitsNeeded, version, ecLevel))
                    throw new WriterException("Data too big for requested version");
            }
            else
            {
                version = RecommendVersion(ecLevel, mode, headerBits, dataBits);
            }

            var headerAndDataBits = new BitArray();
            headerAndDataBits.AppendBitArray(headerBits);
            // Ищет "длину" основного отрезка и записывает его.
            var numLetters = mode == Mode.BYTE ? dataBits.SizeInBytes : content.Length;
            AppendLengthInfo(numLetters, version, mode, headerAndDataBits);
            // Объединяет данные в общую полезную нагрузку
            headerAndDataBits.AppendBitArray(dataBits);

            var ecBlocks = version.GetEcBlocksForLevel(ecLevel);
            var numDataBytes = version.TotalCodewords - ecBlocks.TotalECCodewords;

            // Terminate the bits properly.
            TerminateBits(numDataBytes, headerAndDataBits);

            // Череда битов данных с кодами коррекции ошибок.
            var finalBits = InterleaveWithEcBytes(headerAndDataBits,
                version.TotalCodewords,
                numDataBytes,
                ecBlocks.NumBlocks);

            var qrCode = new QRCode
            {
                ECLevel = ecLevel,
                Mode = mode,
                Version = version
            };

            //  Выбор шаблона маски и установка значения «qrCode».
            var dimension = version.DimensionForVersion;
            var matrix = new ByteMatrix(dimension, dimension);

            // Включение ручного выбора шаблона для использования с помощью подсказки (хинта)
            var maskPattern = -1;
            if (hints != null && hints.ContainsKey(EncodeHintType.QR_MASK_PATTERN))
            {
                var hintMaskPattern = int.Parse(hints[EncodeHintType.QR_MASK_PATTERN].ToString());
                maskPattern = QRCode.IsValidMaskPattern(hintMaskPattern) ? hintMaskPattern : -1;
            }

            if (maskPattern == -1)
                maskPattern = ChooseMaskPattern(finalBits, ecLevel, version, matrix);
            qrCode.MaskPattern = maskPattern;

            // Построение матрицы и установка для нее значения «qrCode».
            MatrixUtil.BuildMatrix(finalBits, ecLevel, version, maskPattern, matrix);
            qrCode.Matrix = matrix;

            return qrCode;
        }

        /// <summary>
        ///     Определяет наименьшую версию QR-кода, которая будет содержать все предоставленные данные.
        /// </summary>
        /// <exception cref="WriterException">if the data cannot fit in any version</exception>
        private static Version RecommendVersion(ErrorCorrectionLevel ecLevel, Mode mode, BitArray headerBits,
            BitArray dataBits)
        {
            // Жестко закодированная часть: нужно знать версию, чтобы знать, сколько бит занимает длина.
            // Но нужно еще знать, сколько бит нужно узнать, чтобы узнать версию. 
            // Сначала мы делаем предположение о версии, предполагая, что версия будет минимальной, 1:
            var provisionalBitsNeeded = CalculateBitsNeeded(mode, headerBits, dataBits, Version.GetVersionForNumber(1));
            var provisionalVersion = ChooseVersion(provisionalBitsNeeded, ecLevel);

            // Используется это предположение, чтобы вычислить правильную версию. До сих пор нет уверенности, что это работает в 100% случаев.
            var bitsNeeded = CalculateBitsNeeded(mode, headerBits, dataBits, provisionalVersion);
            return ChooseVersion(bitsNeeded, ecLevel);
        }

        private static int CalculateBitsNeeded(Mode mode, BitArray headerBits, BitArray dataBits, Version version)
        {
            return headerBits.Size + mode.getCharacterCountBits(version) + dataBits.Size;
        }

        /// <summary>
        ///     Получает буквенно-цифровой код.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <returns>
        ///     кодовая точка таблицы, используемая в буквенно-цифровом режиме или
        ///     -1, если в таблице нет соответствующего кода.
        /// </returns>
        internal static int GetAlphanumericCode(int code)
        {
            if (code < ALPHANUMERIC_TABLE.Length)
                return ALPHANUMERIC_TABLE[code];
            return -1;
        }

        /// <summary>
        ///     Выбирает режим.
        /// </summary>
        /// <param name="content">Содержимое.</param>
        /// <returns></returns>
        public static Mode ChooseMode(string content)
        {
            return ChooseMode(content, null);
        }

        /// <summary>
        ///     Выберите лучший режим, изучив контент. Обратите внимание, что «кодировка» используется как подсказка;
        ///     если это Shift_JIS, а ввод - только двухбайтовые иероглифы, то мы возвращаем {@link Mode # KANJI}.
        /// </summary>
        /// <param name="content">Содержимое.</param>
        /// <param name="encoding">Кодировка.</param>
        /// <returns></returns>
        private static Mode ChooseMode(string content, string encoding)
        {
            if ("Shift_JIS".Equals(encoding) && IsOnlyDoubleByteKanji(content))
                return Mode.KANJI;
            var hasNumeric = false;
            var hasAlphanumeric = false;
            for (var i = 0; i < content.Length; ++i)
            {
                var c = content[i];
                if (c >= '0' && c <= '9')
                    hasNumeric = true;
                else if (GetAlphanumericCode(c) != -1)
                    hasAlphanumeric = true;
                else
                    return Mode.BYTE;
            }
            if (hasAlphanumeric)
                return Mode.ALPHANUMERIC;
            if (hasNumeric)
                return Mode.NUMERIC;
            return Mode.BYTE;
        }

        private static bool IsOnlyDoubleByteKanji(string content)
        {
            byte[] bytes;
            try
            {
                bytes = Encoding.GetEncoding("Shift_JIS").GetBytes(content);
            }
            catch (Exception)
            {
                return false;
            }
            var length = bytes.Length;
            if (length % 2 != 0)
                return false;
            for (var i = 0; i < length; i += 2)
            {
                var byte1 = bytes[i] & 0xFF;
                if ((byte1 < 0x81 || byte1 > 0x9F) && (byte1 < 0xE0 || byte1 > 0xEB))
                    return false;
            }
            return true;
        }

        private static int ChooseMaskPattern(BitArray bits,
            ErrorCorrectionLevel ecLevel,
            Version version,
            ByteMatrix matrix)
        {
            var minPenalty = int.MaxValue; // Чем меньше штраф, тем лучше.
            var bestMaskPattern = -1;
            // Пробуются все шаблоны масок, чтобы выбрать лучший.
            for (var maskPattern = 0; maskPattern < QRCode.NUM_MASK_PATTERNS; maskPattern++)
            {
                MatrixUtil.BuildMatrix(bits, ecLevel, version, maskPattern, matrix);
                var penalty = CalculateMaskPenalty(matrix);
                if (penalty < minPenalty)
                {
                    minPenalty = penalty;
                    bestMaskPattern = maskPattern;
                }
            }
            return bestMaskPattern;
        }

        private static Version ChooseVersion(int numInputBits, ErrorCorrectionLevel ecLevel)
        {
            for (var versionNum = 1; versionNum <= 40; versionNum++)
            {
                var version = Version.GetVersionForNumber(versionNum);
                if (WillFit(numInputBits, version, ecLevel))
                    return version;
            }
            throw new WriterException("Data too big");
        }

        /// <summary></summary>
        /// <returns>true, если количество входных бит уместится в коде с указанной версией и уровнем исправления ошибок.</returns>
        private static bool WillFit(int numInputBits, Version version, ErrorCorrectionLevel ecLevel)
        {
            // В следующих комментариях мы используем номера версии 7-H.
            // numBytes = 196
            var numBytes = version.TotalCodewords;
            // getNumECBytes = 130
            var ecBlocks = version.GetEcBlocksForLevel(ecLevel);
            var numEcBytes = ecBlocks.TotalECCodewords;
            // getNumDataBytes = 196 - 130 = 66
            var numDataBytes = numBytes - numEcBytes;
            var totalInputBytes = (numInputBits + 7) / 8;
            return numDataBytes >= totalInputBytes;
        }

        /// <summary>
        ///     Завершающие биты, как описано в пп. 8.4.8 и 8.4.9 JISX0510: 2004 (стр.24).
        /// </summary>
        /// <param name="numDataBytes">The num data bytes.</param>
        /// <param name="bits">The bits.</param>
        internal static void TerminateBits(int numDataBytes, BitArray bits)
        {
            var capacity = numDataBytes << 3;
            if (bits.Size > capacity)
                throw new WriterException("data bits cannot fit in the QR Code" + bits.Size + " > " +
                                          capacity);
            for (var i = 0; i < 4 && bits.Size < capacity; ++i)
                bits.AppendBit(false);
            // Добавление завершающих битов. Подробнее см. 8.4.8 JISX0510: 2004 (стр.24).
            // Если последний байт не выровнен по 8 битам, мы добавляются биты заполнения.
            var numBitsInLastByte = bits.Size & 0x07;
            if (numBitsInLastByte > 0)
                for (var i = numBitsInLastByte; i < 8; i++)
                    bits.AppendBit(false);
            // Если имеетсе в наличии больше свободного места, то производится заполнение его шаблонами заполнения, определенными в 8.4.9 (стр.24).
            var numPaddingBytes = numDataBytes - bits.SizeInBytes;
            for (var i = 0; i < numPaddingBytes; ++i)
                bits.AppendBits((i & 0x01) == 0 ? 0xEC : 0x11, 8);
            if (bits.Size != capacity)
                throw new WriterException("Bits size does not equal capacity");
        }

        /// <summary>
        ///     Получает количество байтов данных и количество байтов блоков коррекции ошибок для идентификатора блока "blockID".
        ///     Сохраняет результат в «numDataBytesInBlock» и «numECBytesInBlock. См. Таблицу 12 в 8.5.1
        ///     JISX0510:2004 (p.30)
        /// </summary>
        /// <param name="numTotalBytes">Общее количество байтов.</param>
        /// <param name="numDataBytes">Число байтов данных.</param>
        /// <param name="numRsBlocks">Количество блоков RS.</param>
        /// <param name="blockId">Идентификатор блока.</param>
        /// <param name="numDataBytesInBlock">Число байтов данных в блоке</param>
        /// <param name="numEcBytesInBlock">Число байтов EC в блоке.</param>
        internal static void GetNumDataBytesAndNumEcBytesForBlockId(int numTotalBytes,
            int numDataBytes,
            int numRsBlocks,
            int blockId,
            int[] numDataBytesInBlock,
            int[] numEcBytesInBlock)
        {
            if (blockId >= numRsBlocks)
                throw new WriterException("Block ID too large");
            // numRsBlocksInGroup2 = 196 % 5 = 1
            var numRsBlocksInGroup2 = numTotalBytes % numRsBlocks;
            // numRsBlocksInGroup1 = 5 - 1 = 4
            var numRsBlocksInGroup1 = numRsBlocks - numRsBlocksInGroup2;
            // numTotalBytesInGroup1 = 196 / 5 = 39
            var numTotalBytesInGroup1 = numTotalBytes / numRsBlocks;
            // numTotalBytesInGroup2 = 39 + 1 = 40
            var numTotalBytesInGroup2 = numTotalBytesInGroup1 + 1;
            // numDataBytesInGroup1 = 66 / 5 = 13
            var numDataBytesInGroup1 = numDataBytes / numRsBlocks;
            // numDataBytesInGroup2 = 13 + 1 = 14
            var numDataBytesInGroup2 = numDataBytesInGroup1 + 1;
            // numEcBytesInGroup1 = 39 - 13 = 26
            var numEcBytesInGroup1 = numTotalBytesInGroup1 - numDataBytesInGroup1;
            // numEcBytesInGroup2 = 40 - 14 = 26
            var numEcBytesInGroup2 = numTotalBytesInGroup2 - numDataBytesInGroup2;
            // Sanity checks.
            // 26 = 26
            if (numEcBytesInGroup1 != numEcBytesInGroup2)
                throw new WriterException("EC bytes mismatch");
            // 5 = 4 + 1.
            if (numRsBlocks != numRsBlocksInGroup1 + numRsBlocksInGroup2)
                throw new WriterException("RS blocks mismatch");
            // 196 = (13 + 26) * 4 + (14 + 26) * 1
            if (numTotalBytes !=
                (numDataBytesInGroup1 + numEcBytesInGroup1) *
                numRsBlocksInGroup1 +
                (numDataBytesInGroup2 + numEcBytesInGroup2) *
                numRsBlocksInGroup2)
                throw new WriterException("Total bytes mismatch");

            if (blockId < numRsBlocksInGroup1)
            {
                numDataBytesInBlock[0] = numDataBytesInGroup1;
                numEcBytesInBlock[0] = numEcBytesInGroup1;
            }
            else
            {
                numDataBytesInBlock[0] = numDataBytesInGroup2;
                numEcBytesInBlock[0] = numEcBytesInGroup2;
            }
        }

        /// <summary>
        ///     Чередование «битов» с соответствующими байтами коррекции ошибок. В случае успеха сохраняет результат в
        ///     "result". Правило чередования сложное. Подробнее см. 8.6 JISX0510: 2004 (стр.37).
        /// </summary>
        /// <param name="bits">Биты.</param>
        /// <param name="numTotalBytes">Общее количество байтов.</param>
        /// <param name="numDataBytes">Число байтов данных.</param>
        /// <param name="numRsBlocks">Количество блоков RS.</param>
        /// <returns></returns>
        internal static BitArray InterleaveWithEcBytes(BitArray bits,
            int numTotalBytes,
            int numDataBytes,
            int numRsBlocks)
        {
            // "биты" должны иметь байтов данных "getNumDataBytes".
            if (bits.SizeInBytes != numDataBytes)
                throw new WriterException("Number of bits and data bytes does not match");

            // Шаг 1. Разделение байтов данных на блоки и генерация для них байтов исправления/коррекции ошибок. 
            // Сохрание разделенных блоков байтов данных и блоков байтов исправления ошибок в сами «блоки».
            var dataBytesOffset = 0;
            var maxNumDataBytes = 0;
            var maxNumEcBytes = 0;

            // Поскольку известно количество блоков reedsolmon, то можно инициализировать вектор этим самым числом.
            var blocks = new List<BlockPair>(numRsBlocks);

            for (var i = 0; i < numRsBlocks; ++i)
            {
                var numDataBytesInBlock = new int[1];
                var numEcBytesInBlock = new int[1];
                GetNumDataBytesAndNumEcBytesForBlockId(
                    numTotalBytes, numDataBytes, numRsBlocks, i,
                    numDataBytesInBlock, numEcBytesInBlock);

                var size = numDataBytesInBlock[0];
                var dataBytes = new byte[size];
                bits.ToBytes(8 * dataBytesOffset, dataBytes, 0, size);
                var ecBytes = GenerateEcBytes(dataBytes, numEcBytesInBlock[0]);
                blocks.Add(new BlockPair(dataBytes, ecBytes));

                maxNumDataBytes = Math.Max(maxNumDataBytes, size);
                maxNumEcBytes = Math.Max(maxNumEcBytes, ecBytes.Length);
                dataBytesOffset += numDataBytesInBlock[0];
            }
            if (numDataBytes != dataBytesOffset)
                throw new WriterException("Data bytes does not match offset");

            var result = new BitArray();

            // Сначала размещаются блоки данных.
            for (var i = 0; i < maxNumDataBytes; ++i)
                foreach (var block in blocks)
                {
                    var dataBytes = block.DataBytes;
                    if (i < dataBytes.Length)
                        result.AppendBits(dataBytes[i], 8);
                }
            // Затем размещаются блоки исправления/коррекции ошибок.
            for (var i = 0; i < maxNumEcBytes; ++i)
                foreach (var block in blocks)
                {
                    var ecBytes = block.ErrorCorrectionBytes;
                    if (i < ecBytes.Length)
                        result.AppendBits(ecBytes[i], 8);
                }
            if (numTotalBytes != result.SizeInBytes)
                throw new WriterException("Interleaving error: " + numTotalBytes + " and " +
                                          result.SizeInBytes + " differ.");

            return result;
        }

        internal static byte[] GenerateEcBytes(byte[] dataBytes, int numEcBytesInBlock)
        {
            var numDataBytes = dataBytes.Length;
            var toEncode = new int[numDataBytes + numEcBytesInBlock];
            for (var i = 0; i < numDataBytes; i++)
                toEncode[i] = dataBytes[i] & 0xFF;

            new ReedSolomonEncoder(GenericGF.QR_CODE_FIELD_256).Encode(toEncode, numEcBytesInBlock);

            var ecBytes = new byte[numEcBytesInBlock];
            for (var i = 0; i < numEcBytesInBlock; i++)
                ecBytes[i] = (byte) toEncode[numDataBytes + i];
            return ecBytes;
        }

        /// <summary>
        ///    Добавление информации о режиме. В случае успеха результат сохраняется в условных «битах» (Модель BitArray).
        /// </summary>
        /// <param name="mode">Режим.</param>
        /// <param name="bits">Биты.</param>
        internal static void AppendModeInfo(Mode mode, BitArray bits)
        {
            bits.AppendBits(mode.Bits, 4);
        }


        /// <summary>
        ///    Добавление информации о длине. В случае успеха результат сохраняется в условных «битах» (Модель BitArray).
        /// </summary>
        /// <param name="numLetters">Число букв.</param>
        /// <param name="version">Версия.</param>
        /// <param name="mode">Режим.</param>
        /// <param name="bits">Биты.</param>
        internal static void AppendLengthInfo(int numLetters, Version version, Mode mode, BitArray bits)
        {
            var numBits = mode.getCharacterCountBits(version);
            if (numLetters >= 1 << numBits)
                throw new WriterException(numLetters + " is bigger than " + ((1 << numBits) - 1));
            bits.AppendBits(numLetters, numBits);
        }

        /// <summary>
        ///   Добавление «байтов» в «режиме» режима(кодировки) в «биты». В случае успеха результат сохраняется в условных «битах». (Модель BitArray).
        /// </summary>
        /// <param name="content">Содержимое.</param>
        /// <param name="mode">Режим.</param>
        /// <param name="bits">Биты.</param>
        /// <param name="encoding">Кодировка.</param>
        internal static void AppendBytes(string content,
            Mode mode,
            BitArray bits,
            string encoding)
        {
            if (mode.Equals(Mode.NUMERIC))
                AppendNumericBytes(content, bits);
            else if (mode.Equals(Mode.ALPHANUMERIC))
                AppendAlphanumericBytes(content, bits);
            else if (mode.Equals(Mode.BYTE))
                Append8BitBytes(content, bits, encoding);
            else if (mode.Equals(Mode.KANJI))
                AppendKanjiBytes(content, bits);
            else
                throw new WriterException("Invalid mode: " + mode);
        }

        /// <summary>
        /// Функция кодирования числовых значений.
        /// </summary>
        /// <param name="content"></param>
        /// <param name="bits"></param>
        internal static void AppendNumericBytes(string content, BitArray bits)
        {
            var length = content.Length;

            var i = 0;
            while (i < length)
            {
                var num1 = content[i] - '0';
                if (i + 2 < length)
                {
                    // Кодирование трех числовые символов десятью битами. / Encode three numeric letters in ten bits.
                    var num2 = content[i + 1] - '0';
                    var num3 = content[i + 2] - '0';
                    bits.AppendBits(num1 * 100 + num2 * 10 + num3, 10);
                    i += 3;
                }
                else if (i + 1 < length)
                {
                    // Кодирование два числовых символа семью битами. / Encode two numeric letters in seven bits.
                    var num2 = content[i + 1] - '0';
                    bits.AppendBits(num1 * 10 + num2, 7);
                    i += 2;
                }
                else
                {
                    // Кодирование одного числового символа четырьмя битами. / Encode one numeric letter in four bits.
                    bits.AppendBits(num1, 4);
                    i++;
                }
            }
        }

        /// <summary>
        /// Функция кодирования буквенных значений.
        /// </summary>
        /// <param name="content"></param>
        /// <param name="bits"></param>
        internal static void AppendAlphanumericBytes(string content, BitArray bits)
        {
            var length = content.Length;

            var i = 0;
            while (i < length)
            {
                var code1 = GetAlphanumericCode(content[i]);
                if (code1 == -1)
                    throw new WriterException();
                if (i + 1 < length)
                {
                    var code2 = GetAlphanumericCode(content[i + 1]);
                    if (code2 == -1)
                        throw new WriterException();
                    // Кодирование пары буквенно-цифровых символов в 11 бит. / Encode two alphanumeric letters in 11 bits.
                    bits.AppendBits(code1 * 45 + code2, 11);
                    i += 2;
                }
                else
                {
                    // Кодирование одного буквенно-цифрового символа шестью битами. / Encode one alphanumeric letter in six bits.
                    bits.AppendBits(code1, 6);
                    i++;
                }
            }
        }

        internal static void Append8BitBytes(string content, BitArray bits, string encoding)
        {
            byte[] bytes;
            try
            {
                bytes = Encoding.GetEncoding(encoding).GetBytes(content);
            }
#if WindowsCE
         catch (PlatformNotSupportedException)
         {
            try
            {
               // WindowsCE doesn't support all encodings. But it is device depended.
               // So we try here the some different ones
               if (encoding == "ISO-8859-1")
               {
                  bytes = Encoding.GetEncoding(1252).GetBytes(content);
               }
               else
               {
                  bytes = Encoding.GetEncoding("UTF-8").GetBytes(content);
               }
            }
            catch (Exception uee)
            {
               throw new WriterException(uee.Message, uee);
            }
         }
#endif
            catch (Exception uee)
            {
                throw new WriterException(uee.Message, uee);
            }
            foreach (var b in bytes)
                bits.AppendBits(b, 8);
        }

        /// <summary>
        /// Функция кодирования иероглифа
        /// </summary>
        /// <param name="content"></param>
        /// <param name="bits"></param>
        internal static void AppendKanjiBytes(string content, BitArray bits)
        {
            byte[] bytes;
            try
            {
                bytes = Encoding.GetEncoding("Shift_JIS").GetBytes(content);
            }
            catch (Exception uee)
            {
                throw new WriterException(uee.Message, uee);
            }
            if (bytes.Length % 2 != 0)
                throw new WriterException("Kanji byte size not even");
            var maxI = bytes.Length - 1; // bytes.length должен быть четным
            for (var i = 0; i < maxI; i += 2)
            {
                var byte1 = bytes[i] & 0xFF;
                var byte2 = bytes[i + 1] & 0xFF;
                var code = (byte1 << 8) | byte2;
                var subtracted = -1;
                if (code >= 0x8140 && code <= 0x9ffc)
                    subtracted = code - 0x8140;
                else if (code >= 0xe040 && code <= 0xebbf)
                    subtracted = code - 0xc140;
                if (subtracted == -1)
                    throw new WriterException("Invalid byte sequence");
                var encoded = (subtracted >> 8) * 0xc0 + (subtracted & 0xff);
                bits.AppendBits(encoded, 13);
            }
        }

        private static void AppendEci(CharacterSetECI eci, BitArray bits)
        {
            bits.AppendBits(Mode.ECI.Bits, 4);

            // Это верно лишь для значений до 127, и это все, что на данный момент нужно.
            bits.AppendBits(eci.Value, 8);
        }
    }
}
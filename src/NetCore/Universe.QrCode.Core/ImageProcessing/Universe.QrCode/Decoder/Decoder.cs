﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System.Collections.Generic;
using Universe.QrCode.Common.DataContracts;
using Universe.QrCode.Common.Reedsolomon;
using Universe.QrCode.Common.Types;
using Universe.QrCode.Models;

namespace Universe.QrCode.Decoder
{
    /// <summary>
    ///     <p>
    ///         Основной класс, который реализует декодирование QR-кода - в отличие от поиска и извлечения
    ///         QR-кода из изображения.
    ///     </p>
    /// </summary>
    /// <author>
    ///     Sean Owen
    /// </author>
    /// <author>
    ///     Alex Envision
    /// </author>
    public sealed class Decoder
    {
        private readonly ReedSolomonDecoder _rsDecoder;

        /// <summary>
        ///     Initializes a new instance of the <see cref="Decoder" /> class.
        /// </summary>
        public Decoder()
        {
            _rsDecoder = new ReedSolomonDecoder(GenericGF.QR_CODE_FIELD_256);
        }

        /// <summary>
        ///     <p>
        ///         Метод реализованный для удобства, который позволяет декодировать QR-код, представленный как двумерный массив логических значений.
        ///         "истина" означает черный модуль.
        ///     </p>
        /// </summary>
        /// <param name="image">Логические значения, представляющие белые / черные модули QR-кода</param>
        /// <param name="hints">Подсказки по декодированию, которые следует использовать, чтобы повлиять на декодирование</param>
        /// <returns>
        ///     Текст и байты, закодированные в QR-коде
        /// </returns>
        public DecoderResult Decode(bool[][] image, IDictionary<DecodeHintType, object> hints)
        {
            return Decode(BitMatrix.Parse(image), hints);
        }

        /// <summary>
        ///     <p>Расшифровывает QR-код, представленный как <see cref="BitMatrix"/>. 1 или Ђистинаї означает черный модуль.</p>
        /// </summary>
        /// <param name="bits">Логические значени¤, представл¤ющие белые/черные модули QR-кода</param>
        /// <param name="hints">Подсказки по декодированию, которые следует использовать, чтобы повлиять на декодирование</param>
        /// <returns>
        ///     Текст и байты, закодированные в QR-коде
        /// </returns>
        public DecoderResult Decode(BitMatrix bits, IDictionary<DecodeHintType, object> hints)
        {
            // Construct a parser and read version, error-correction level
            var parser = BitMatrixParser.CreateBitMatrixParser(bits);
            if (parser == null)
                return null;

            var result = Decode(parser, hints);
            if (result == null)
            {
                // Revert the bit matrix
                parser.Remask();

                // Will be attempting a mirrored reading of the version and format info.
                parser.SetMirror(true);

                // Preemptively read the version.
                var version = parser.ReadVersion();
                if (version == null)
                    return null;

                // Preemptively read the format information.
                var formatinfo = parser.ReadFormatInformation();
                if (formatinfo == null)
                    return null;

                /*
                  ѕоскольку мы здесь, это означает, что мы успешно обнаружили некоторую информацию 
                  о версии и формате при зеркалировании. Ёто хороший знак, что QR-код может быть зеркальным, 
                  и мы должны попробовать еще раз с зеркальным контентом.
                 */
                // Prepare for a mirrored reading.
                parser.Mirror();

                result = Decode(parser, hints);

                if (result != null)
                    result.Other = new QRCodeDecoderMetaData(true);
            }

            return result;
        }

        private DecoderResult Decode(BitMatrixParser parser, IDictionary<DecodeHintType, object> hints)
        {
            var version = parser.ReadVersion();
            if (version == null)
                return null;
            var formatinfo = parser.ReadFormatInformation();
            if (formatinfo == null)
                return null;
            var ecLevel = formatinfo.ErrorCorrectionLevel;

            // Read codewords
            var codewords = parser.ReadCodewords();
            if (codewords == null)
                return null;
            // Separate into data blocks
            var dataBlocks = DataBlock.getDataBlocks(codewords, version, ecLevel);

            // Count total number of data bytes
            var totalBytes = 0;
            foreach (var dataBlock in dataBlocks)
                totalBytes += dataBlock.NumDataCodewords;
            var resultBytes = new byte[totalBytes];
            var resultOffset = 0;

            // Error-correct and copy data blocks together into a stream of bytes
            foreach (var dataBlock in dataBlocks)
            {
                var codewordBytes = dataBlock.Codewords;
                var numDataCodewords = dataBlock.NumDataCodewords;
                if (!CorrectErrors(codewordBytes, numDataCodewords))
                    return null;
                for (var i = 0; i < numDataCodewords; i++)
                    resultBytes[resultOffset++] = codewordBytes[i];
            }

            // Decode the contents of that stream of bytes
            return DecodedBitStreamParser.Decode(resultBytes, version, ecLevel, hints);
        }

        /// <summary>
        ///     <p>
        ///         Given data and error-correction codewords received, possibly corrupted by errors, attempts to
        ///         correct the errors in-place using Reed-Solomon error correction.
        ///     </p>
        /// </summary>
        /// <param name="codewordBytes">data and error correction codewords</param>
        /// <param name="numDataCodewords">number of codewords that are data bytes</param>
        /// <returns></returns>
        private bool CorrectErrors(byte[] codewordBytes, int numDataCodewords)
        {
            var numCodewords = codewordBytes.Length;
            // First read into an array of ints
            var codewordsInts = new int[numCodewords];
            for (var i = 0; i < numCodewords; i++)
                codewordsInts[i] = codewordBytes[i] & 0xFF;
            var numEcCodewords = codewordBytes.Length - numDataCodewords;

            if (!_rsDecoder.Decode(codewordsInts, numEcCodewords))
                return false;

            // Copy back into array of bytes -- only need to worry about the bytes that were data
            // We don't care about errors in the error-correction codewords
            for (var i = 0; i < numDataCodewords; i++)
                codewordBytes[i] = (byte) codewordsInts[i];

            return true;
        }
    }
}
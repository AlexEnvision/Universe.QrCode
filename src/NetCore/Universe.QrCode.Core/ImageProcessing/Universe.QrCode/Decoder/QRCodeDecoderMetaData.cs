﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using Universe.QrCode.Models;

namespace Universe.QrCode.Decoder
{
    /// <summary>
    ///      Контейнер метаданных для декодирования QR-кода. Экземпляры этого класса могут использоваться для передачи информации обратно в
    ///      расшифровка звонящего. Ожидается, что вызывающие абоненты обработают это.
    /// </summary>
    public sealed class QRCodeDecoderMetaData
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="QRCodeDecoderMetaData" /> class.
        /// </summary>
        /// <param name="mirrored">if set to <c>true</c> [mirrored].</param>
        public QRCodeDecoderMetaData(bool mirrored)
        {
            IsMirrored = mirrored;
        }

        /// <summary>
        ///     True, если QR-код был отзеркален.
        /// </summary>
        public bool IsMirrored { get; }

        /// <summary>
        ///     Примение коррекции порядка результирующих точек из-за зеркального отображения.
        /// </summary>
        /// <param name="points">Array of points to apply mirror correction to.</param>
        public void ApplyMirroredCorrection(ResultPoint[] points)
        {
            if (!IsMirrored || points == null || points.Length < 3)
                return;
            var bottomLeft = points[0];
            points[0] = points[2];
            points[2] = bottomLeft;
            // No need to 'fix' top-left and alignment pattern.
        }
    }
}
﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;
using System.Globalization;
using System.Text;
using Universe.QrCode.Common.Extensions;

namespace Universe.QrCode.Models
{
    /// <summary>
    ///     Инкапсулирует достопримечательность в изображение, содержащее штрих-код. 
    ///     как правило, это, например, расположение шаблона искателя или угол штрих-кода.
    /// </summary>
    /// <author>Sean Owen</author>
	/// <author>Alex Envision</author>
    public class ResultPoint
    {
        private readonly byte[] _bytesX;
        private readonly byte[] _bytesY;
        private readonly float _x;
        private readonly float _y;
        private string _toString;

        /// <summary>
        ///     »нициализирует новый экземпл¤р класса <see cref="ResultPoint" />.
        /// </summary>
        public ResultPoint()
        {
        }

        /// <summary>
        ///     »нициализирует новый экземпл¤р класса <see cref="ResultPoint" />.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        public ResultPoint(float x, float y)
        {
            _x = x;
            _y = y;
            // рассчитать только один раз дл¤ GetHashCode
            _bytesX = BitConverter.GetBytes(x);
            _bytesY = BitConverter.GetBytes(y);
        }

        /// <summary>
        ///     ѕолучить X.
        /// </summary>
        public virtual float X => _x;

        /// <summary>
        ///     ѕолучить Y.
        /// </summary>
        public virtual float Y => _y;

        /// <summary>
        ///     ќпредел¤ет, равен ли указанный <see cref="System.Object" /> этому экземпл¤ру.
        /// </summary>
        /// <param name="other"><see cref="System.Object" /> дл¤ сравнени¤ с этим экземпл¤ром.</param>
        /// <returns>
        ///     <c>true</c> если указанный <see cref="System.Object" /> равно этому экземпл¤ру; иначе, <c>false</c>.
        /// </returns>
        public override bool Equals(object other)
        {
            var otherPoint = other as ResultPoint;
            if (otherPoint == null)
                return false;
            return _x == otherPoint._x && _y == otherPoint._y;
        }

        /// <summary>
        ///     ¬озвращает хэш-код дл¤ этого экземпл¤ра.
        /// </summary>
        /// <returns>
        ///     ’эш-код дл¤ этого экземпл¤ра, подход¤щий дл¤ использовани¤ в алгоритмах хешировани¤ и структурах данных, таких как хеш-таблица.
        /// </returns>
        public override int GetHashCode()
        {
            return 31 * ((_bytesX[0] << 24) + (_bytesX[1] << 16) + (_bytesX[2] << 8) + _bytesX[3]) +
                   (_bytesY[0] << 24) + (_bytesY[1] << 16) + (_bytesY[2] << 8) + _bytesY[3];
        }

        /// <summary>
        ///     ¬озвращает <see cref="System.String" /> представл¤ющий этот экземпл¤р.
        /// </summary>
        /// <returns>
        ///    <see cref="System.String" />, который представл¤ет этот экземпл¤р.
        /// </returns>
        public override string ToString()
        {
            if (_toString == null)
            {
                var result = new StringBuilder(25);
                result.AppendFormat(CultureInfo.CurrentUICulture, "({0}, {1})", _x, _y);
                _toString = result.ToString();
            }
            return _toString;
        }

        /// <summary>
        ///     ”пор¤дочивает массив из трех ResultPoints в пор¤дке [A, B, C] так,
        ///     что AB меньше AC, а BC меньше AC, а угол между BC и BA меньше 180 градусов.
        /// </summary>
        /// <param name="patterns">ћассив из трех <see cref="ResultPoint" /> дл¤ сортировки.</param>
        public static void OrderBestPatterns(ResultPoint[] patterns)
        {
            // »щет рассто¤ние между центрами узоров
            var zeroOneDistance = Distance(patterns[0], patterns[1]);
            var oneTwoDistance = Distance(patterns[1], patterns[2]);
            var zeroTwoDistance = Distance(patterns[0], patterns[2]);

            ResultPoint pointA, pointB, pointC;
            // ѕредпологаетс¤, что один из двух ближайших - это B; и потому A и C сначала будут просто догадками
            if (oneTwoDistance >= zeroOneDistance && oneTwoDistance >= zeroTwoDistance)
            {
                pointB = patterns[0];
                pointA = patterns[1];
                pointC = patterns[2];
            }
            else if (zeroTwoDistance >= oneTwoDistance && zeroTwoDistance >= zeroOneDistance)
            {
                pointB = patterns[1];
                pointA = patterns[0];
                pointC = patterns[2];
            }
            else
            {
                pointB = patterns[2];
                pointA = patterns[0];
                pointC = patterns[1];
            }

            // »спользетс¤ перекрестное-произведение, чтобы вы¤снить, правильны ли A и C или нет. 
            // Ёто вы¤сн¤ет, имеет ли BC x BA положительный компонент z, который мы хотим расположить дл¤ A, B, C. 
            // ≈сли он отрицательный, то мы его перевернули и должны помен¤ть местами A и C.
            if (CrossProductZ(pointA, pointB, pointC) < 0.0f)
            {
                var temp = pointA;
                pointA = pointC;
                pointC = temp;
            }

            patterns[0] = pointA;
            patterns[1] = pointB;
            patterns[2] = pointC;
        }


        /// <summary>
        ///     ¬ычисл¤ет рассто¤ние между двум¤ точками
        /// </summary>
        /// <param name="pattern1">ѕервый паттерн</param>
        /// <param name="pattern2">¬торой паттерн</param>
        /// <returns>
        ///     –ассто¤ние между двум¤ точками
        /// </returns>
        public static float Distance(ResultPoint pattern1, ResultPoint pattern2)
        {
            return MathUtils.Distance(pattern1._x, pattern1._y, pattern2._x, pattern2._y);
        }

        /// <summary>
        ///     ¬озвращает компонент z перекрестного произведени¤ векторов BC и BA.
        /// </summary>
        private static float CrossProductZ(ResultPoint pointA, ResultPoint pointB, ResultPoint pointC)
        {
            var bX = pointB._x;
            var bY = pointB._y;
            return (pointC._x - bX) * (pointA._y - bY) - (pointC._y - bY) * (pointA._x - bX);
        }
    }
}
﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

namespace Universe.QrCode.Models
{
    /// <summary>
    /// These are a set of hints that you may pass to Writers to specify their behavior.
    /// </summary>
    /// <author>dswitkin@google.com (Daniel Switkin)</author>
    /// <author>Alex Envision</author>
    public enum EncodeHintType
    {
        /// <summary>
        /// Specifies the width of the barcode image
        /// type: <see cref="System.Int32" />
        /// </summary>
        WIDTH,

        /// <summary>
        /// Specifies the height of the barcode image
        /// type: <see cref="System.Int32" />
        /// </summary>
        HEIGHT,

        /// <summary>
        /// Specifies what degree of error correction to use, for example in QR Codes.
        /// Type depends on the encoder. For example for QR codes it's type
        /// <see cref="string" />
        /// In all cases, it can also be a <see cref="System" /> representation of the desired value as well.
        /// </summary>
        ERROR_CORRECTION,

        /// <summary>
        /// Don't put the content string into the output image.
        /// type: <see cref="System.Boolean" />
        /// </summary>
        PURE_BARCODE,

        /// <summary>
        /// Specifies what character encoding to use where applicable.
        /// type: <see cref="System.String" />
        /// </summary>
        CHARACTER_SET,

        /// <summary>
        /// Specifies margin, in pixels, to use when generating the barcode. The meaning can vary
        /// by format; for example it controls margin before and after the barcode horizontally for
        /// most 1D formats.
        /// type: <see cref="System.Int32" />, or <see cref="System.String" /> representation of the integer value
        /// </summary>
        MARGIN,

        /// <summary>
        /// Don't append ECI segment.
        /// That is against the specification of QR Code but some
        /// readers have problems if the charset is switched from
        /// ISO-8859-1 (default) to UTF-8 with the necessary ECI segment.
        /// If you set the property to true you can use UTF-8 encoding
        /// and the ECI segment is omitted.
        /// type: <see cref="System.Boolean" />
        /// </summary>
        DISABLE_ECI,

        /// <summary>
        /// Specifies a minimum barcode size (type <see cref="Dimension"/>). Only applicable to Data Matrix now.
        /// </summary>
        MIN_SIZE,

        /// <summary>
        /// Specifies a maximum barcode size (type <see cref="Dimension"/>). Only applicable to Data Matrix now.
        /// </summary>
        MAX_SIZE,

        /// <summary>
        /// if true, don't switch to codeset C for numbers
        /// </summary>
        CODE128_FORCE_CODESET_B,

        /// <summary>
        /// Specifies the exact version of QR code to be encoded.
        /// (Type <see cref="System.Int32" />, or <see cref="System.String" /> representation of the integer value).
        /// </summary>
        QR_VERSION,

        /// <summary>
        /// Specifies whether the data should be encoded to the GS1 standard
        /// type: <see cref="System.Boolean" />, or "true" or "false"
        /// <see cref="System.String" /> value
        /// </summary>
        GS1_FORMAT,

        /// <summary>
        ///  Specifies the QR code mask pattern to be used. Allowed values are
        /// 0..QRCode.NUM_MASK_PATTERNS-1. By default the code will automatically select
        /// the optimal mask pattern.
        /// (Type <see cref="System.Int32" />, or <see cref="System.String" /> representation of the integer value).
        /// </summary>
        QR_MASK_PATTERN,
    }
}
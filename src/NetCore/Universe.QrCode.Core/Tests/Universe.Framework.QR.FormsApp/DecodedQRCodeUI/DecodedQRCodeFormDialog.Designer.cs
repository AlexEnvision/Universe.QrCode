﻿using System.Windows.Forms;

namespace Universe.Framework.QR.FormsApp.DecodedQRCodeUI
{
    partial class DecodedQrCodeFormDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btOK = new System.Windows.Forms.Button();
            this.tbCodeAsBase64String = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbCodeAsStringWithSplitters = new System.Windows.Forms.TextBox();
            this.pbQrCodeImage = new System.Windows.Forms.PictureBox();
            this.cmsQrCodeImg = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.resetZoomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToClipboardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbBottonLeft = new System.Windows.Forms.Label();
            this.lbTopRight = new System.Windows.Forms.Label();
            this.lbTopLeft = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbQrCodeImage)).BeginInit();
            this.cmsQrCodeImg.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btOK
            // 
            this.btOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btOK.Location = new System.Drawing.Point(943, 524);
            this.btOK.Name = "btOK";
            this.btOK.Size = new System.Drawing.Size(101, 33);
            this.btOK.TabIndex = 0;
            this.btOK.Text = "ОК";
            this.btOK.UseVisualStyleBackColor = true;
            this.btOK.Click += new System.EventHandler(this.btOK_Click);
            // 
            // tbCodeAsBase64String
            // 
            this.tbCodeAsBase64String.Location = new System.Drawing.Point(395, 39);
            this.tbCodeAsBase64String.Multiline = true;
            this.tbCodeAsBase64String.Name = "tbCodeAsBase64String";
            this.tbCodeAsBase64String.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbCodeAsBase64String.Size = new System.Drawing.Size(649, 76);
            this.tbCodeAsBase64String.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(395, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(229, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "QR-код прочитанный в виде Base64 строки:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(395, 122);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(178, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "QR-код считанный в виде строки:";
            // 
            // tbCodeAsStringWithSplitters
            // 
            this.tbCodeAsStringWithSplitters.Location = new System.Drawing.Point(395, 146);
            this.tbCodeAsStringWithSplitters.Multiline = true;
            this.tbCodeAsStringWithSplitters.Name = "tbCodeAsStringWithSplitters";
            this.tbCodeAsStringWithSplitters.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbCodeAsStringWithSplitters.Size = new System.Drawing.Size(649, 372);
            this.tbCodeAsStringWithSplitters.TabIndex = 4;
            // 
            // pbQrCodeImage
            // 
            this.pbQrCodeImage.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbQrCodeImage.ContextMenuStrip = this.cmsQrCodeImg;
            this.pbQrCodeImage.Location = new System.Drawing.Point(23, 39);
            this.pbQrCodeImage.Name = "pbQrCodeImage";
            this.pbQrCodeImage.Size = new System.Drawing.Size(350, 350);
            this.pbQrCodeImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbQrCodeImage.TabIndex = 7;
            this.pbQrCodeImage.TabStop = false;
            // 
            // cmsQrCodeImg
            // 
            this.cmsQrCodeImg.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.resetZoomToolStripMenuItem,
            this.copyToClipboardToolStripMenuItem});
            this.cmsQrCodeImg.Name = "cmsDocumentPageImg";
            this.cmsQrCodeImg.Size = new System.Drawing.Size(232, 48);
            // 
            // resetZoomToolStripMenuItem
            // 
            this.resetZoomToolStripMenuItem.Name = "resetZoomToolStripMenuItem";
            this.resetZoomToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.resetZoomToolStripMenuItem.Text = "Сбросить масштаб";
            this.resetZoomToolStripMenuItem.Click += new System.EventHandler(this.resetZoomToolStripMenuItem_Click);
            // 
            // copyToClipboardToolStripMenuItem
            // 
            this.copyToClipboardToolStripMenuItem.Name = "copyToClipboardToolStripMenuItem";
            this.copyToClipboardToolStripMenuItem.Size = new System.Drawing.Size(231, 22);
            this.copyToClipboardToolStripMenuItem.Text = "Копировать в буфер обмена";
            this.copyToClipboardToolStripMenuItem.Click += new System.EventHandler(this.copyToClipboardToolStripMenuItem_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "QR-код фото:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbBottonLeft);
            this.groupBox1.Controls.Add(this.lbTopRight);
            this.groupBox1.Controls.Add(this.lbTopLeft);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(23, 407);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(350, 111);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Координаты кода:";
            // 
            // lbBottonLeft
            // 
            this.lbBottonLeft.AutoSize = true;
            this.lbBottonLeft.Location = new System.Drawing.Point(132, 71);
            this.lbBottonLeft.Name = "lbBottonLeft";
            this.lbBottonLeft.Size = new System.Drawing.Size(16, 13);
            this.lbBottonLeft.TabIndex = 5;
            this.lbBottonLeft.Text = "...";
            // 
            // lbTopRight
            // 
            this.lbTopRight.AutoSize = true;
            this.lbTopRight.Location = new System.Drawing.Point(132, 46);
            this.lbTopRight.Name = "lbTopRight";
            this.lbTopRight.Size = new System.Drawing.Size(16, 13);
            this.lbTopRight.TabIndex = 4;
            this.lbTopRight.Text = "...";
            // 
            // lbTopLeft
            // 
            this.lbTopLeft.AutoSize = true;
            this.lbTopLeft.Location = new System.Drawing.Point(132, 20);
            this.lbTopLeft.Name = "lbTopLeft";
            this.lbTopLeft.Size = new System.Drawing.Size(16, 13);
            this.lbTopLeft.TabIndex = 3;
            this.lbTopLeft.Text = "...";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 71);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Левый нижний угол:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 46);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(119, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Правый верхний угол:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(113, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Левый верхний угол:";
            // 
            // DecodedQrCodeFormDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1056, 569);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pbQrCodeImage);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbCodeAsStringWithSplitters);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbCodeAsBase64String);
            this.Controls.Add(this.btOK);
            this.Name = "DecodedQrCodeFormDialog";
            this.Text = "Расшифрованный QR код";
            this.Load += new System.EventHandler(this.DecodedQrCodeForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbQrCodeImage)).EndInit();
            this.cmsQrCodeImg.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btOK;
        private System.Windows.Forms.TextBox tbCodeAsBase64String;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbCodeAsStringWithSplitters;
        private System.Windows.Forms.PictureBox pbQrCodeImage;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbBottonLeft;
        private System.Windows.Forms.Label lbTopRight;
        private System.Windows.Forms.Label lbTopLeft;
        private System.Windows.Forms.ContextMenuStrip cmsQrCodeImg;
        private System.Windows.Forms.ToolStripMenuItem resetZoomToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToClipboardToolStripMenuItem;
    }
}
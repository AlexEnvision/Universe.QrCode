﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using Universe.PDF.Kernel.Pdf.Canvas.Parser;
using Universe.PDF.Kernel.Pdf.Canvas.Parser.Data;
using Universe.PDF.Kernel.Pdf.Canvas.Parser.Listener;
using Universe.PDF.Kernel.Pdf.Xobject;

namespace Universe.Framework.QR.FormsApp.PdfComponents
{
    public class ImageEventListener : IEventListener
    {
        public void EventOccurred(IEventData data, EventType type)
        {
            if (type != EventType.RENDER_IMAGE)
                return;

            ImageRenderInfo renderInfo = (ImageRenderInfo)data;

            PdfImageXObject image = renderInfo.GetImage();
            if (image == null)
                return;

            try
            {
                var imageByte = image.GetImageBytes(true);
                Bitmap bmp;
                using (var ms = new MemoryStream(imageByte))
                    bmp = new Bitmap(ms);

                OnImageFound(
                    new ImageFoundEventArgs
                    {
                        Bitmap = bmp
                    });
            }
            catch (Exception ex)
            {
                OnImageFoundError(new ImageFoundErrorEventArgs {
                    Exception = ex,
                    Message = ex.Message
                });
            }
        }

        public ICollection<EventType> GetSupportedEvents()
        {
            return new List<EventType> {
                EventType.RENDER_IMAGE
            };
        }

        public event EventHandler<ImageFoundEventArgs> ImageFound;

        public event EventHandler<ImageFoundErrorEventArgs> ImageFoundError;

        protected virtual void OnImageFound(ImageFoundEventArgs e)
        {
            ImageFound?.Invoke(this, e);
        }

        protected virtual void OnImageFoundError(ImageFoundErrorEventArgs e)
        {
            ImageFoundError?.Invoke(this, e);
        }
    }
}
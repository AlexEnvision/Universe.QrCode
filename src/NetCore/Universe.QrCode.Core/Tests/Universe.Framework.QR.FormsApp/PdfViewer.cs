﻿//  ╔═════════════════════════════════════════════════════════════════════════════════╗
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Licensed under the Apache License, Version 2.0 (the "License");               ║
//  ║   you may not use this file except in compliance with the License.              ║
//  ║   You may obtain a copy of the License at                                       ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0                                ║
//  ║                                                                                 ║
//  ║   Unless required by applicable law or agreed to in writing, software           ║
//  ║   distributed under the License is distributed on an "AS IS" BASIS,             ║
//  ║   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.      ║
//  ║   See the License for the specific language governing permissions and           ║
//  ║   limitations under the License.                                                ║
//  ║                                                                                 ║
//  ║                                                                                 ║
//  ║   Copyright 2021 Universe.NetCore.QrCode                                        ║
//  ║                                                                                 ║
//  ║   Лицензировано согласно Лицензии Apache, Версия 2.0 ("Лицензия");              ║
//  ║   вы можете использовать этот файл только в соответствии с Лицензией.           ║
//  ║   Вы можете найти копию Лицензии по адресу                                      ║
//  ║                                                                                 ║
//  ║       http://www.apache.org/licenses/LICENSE-2.0.                               ║
//  ║                                                                                 ║
//  ║   За исключением случаев, когда это регламентировано существующим               ║
//  ║   законодательством или если это не оговорено в письменном соглашении,          ║
//  ║   программное обеспечение распространяемое на условиях данной Лицензии,         ║
//  ║   предоставляется "КАК ЕСТЬ" и любые явные или неявные ГАРАНТИИ ОТВЕРГАЮТСЯ.    ║
//  ║   Информацию об основных правах и ограничениях,                                 ║
//  ║   применяемых к определенному языку согласно Лицензии,                          ║
//  ║   вы можете найти в данной Лицензии.                                            ║
//  ║                                                                                 ║
//  ╚═════════════════════════════════════════════════════════════════════════════════╝

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using Universe.Framework.QR.FormsApp.DataContracts;
using Universe.Framework.QR.FormsApp.PdfComponents;
using Universe.PDF.IO.Image;
using Universe.PDF.Kernel.Geom;
using Universe.PDF.Kernel.Pdf;
using Universe.PDF.Kernel.Pdf.Canvas.Parser;
using Universe.PDF.Layout;
using Universe.PDF.Layout.Element;
using Universe.Types.Event;

namespace Universe.Framework.QR.FormsApp
{
    public class PdfViewer
    {
        public event LogInfoDel LogInfo;

        public event LogErrorDel LogError;

        private int _totalPages;

        public int TotalPages => _totalPages;

        public void Initialize(DocumentViewState documentViewState)
        {
            var filePath = documentViewState.DocumentPath;
            LogInfoMessage($"Selected file: {filePath}");

            _totalPages = GetPageNumbers(filePath);
        }

        public int GetPageNumbers(string filePath)
        {
            if (string.IsNullOrWhiteSpace(filePath))
                throw new ArgumentException(nameof(filePath));

            if (!File.Exists(filePath))
                throw new Exception($"File {filePath} is not exists.");

            var pdfDoc = new PdfDocument(new PdfReader(filePath));
            return pdfDoc.GetNumberOfPages();
        }

        /// <summary>
        /// Следующая страница как изображение
        /// </summary>
        /// <param name="documentViewState"></param>
        /// <returns></returns>
        public PageModel NextPageAsImg(DocumentViewState documentViewState)
        {
            documentViewState.CurrentPage++;

            if (documentViewState.CurrentPage > _totalPages)
                documentViewState.CurrentPage = _totalPages;

            return OpenDocumentAndGetPage(documentViewState.DocumentPath, documentViewState.CurrentPage);
        }

        /// <summary>
        /// Предыдущая страница как изображение
        /// </summary>
        /// <param name="documentViewState"></param>
        /// <returns></returns>
        public PageModel PreviousPageAsImg(DocumentViewState documentViewState)
        {
            documentViewState.CurrentPage--;

            if (documentViewState.CurrentPage < 1)
                documentViewState.CurrentPage = 1;

            return OpenDocumentAndGetPage(documentViewState.DocumentPath, documentViewState.CurrentPage);
        }

        /// <summary>
        /// Открыть документ и получить страницу
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        public PageModel OpenDocumentAndGetPage(string filePath, int pageIndex)
        {            
            if (string.IsNullOrWhiteSpace(filePath))
                throw new ArgumentException(nameof(filePath));

            if (!File.Exists(filePath))
                throw new Exception($"File {filePath} is not exists.");

            var firstPageModel = new PageModel();
            using (var pdfDoc = new PdfDocument(new PdfReader(filePath)))
            {
                try
                {
                    var bitmapPage = GetPageAsBitmap(pdfDoc, pageIndex);
                    if (bitmapPage == null)
                    {
                        LogInfoMessage($"ProcessFile document has 0 pages or hasn't contains pages as image. Exit.");
                        return firstPageModel;
                    }

                    firstPageModel.PageAsBitmap = bitmapPage;
                }
                catch (Exception ex)
                {
                    LogErrorMessage(ex, ex.Message);
                    throw;
                }
            }

            return firstPageModel;
        }

        /// <summary>
        /// Сохранение отредактированных страниц
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="editedPages">Отредактированные страницы</param>
        public void SavePages(string filePath, List<EditedPage> editedPages)
        {
            if (string.IsNullOrWhiteSpace(filePath))
                throw new ArgumentException(nameof(filePath));

            if (editedPages.Count == 0)
                return;

            var firstPage = editedPages.FirstOrDefault()?.EditedPageAsBitmapForSaving;
            if (firstPage == null)
                return;

            using (var pdfDoc = new PdfDocument(new PdfWriter(filePath)))
            using (Document document = new Document(pdfDoc, new PageSize(firstPage.Width, firstPage.Height)))
            {
                try
                {
                    foreach (var editedPage in editedPages)
                    {
                        using (var ms = new MemoryStream())
                        {
                            editedPage.EditedPageAsBitmapForSaving.Save(ms, ImageFormat.Bmp);

                            var pageAsImage = new PDF.Layout.Element.Image(ImageDataFactory.Create(ms.GetBuffer()));
                            var paragraph = new Paragraph()
                                .Add(pageAsImage);

                            document.Add(paragraph);
                        }  
                    }

                    document.Close();
                }
                catch (Exception ex)
                {
                    LogErrorMessage(ex, ex.Message);
                    throw;
                }
            }
        }

        /// <summary>
        /// Получает страницу как изображение
        /// </summary>
        /// <param name="pdfDoc">Модель PDF документа сформироанная iText API</param>
        /// <param name="pageNumber">Номер страницы</param>
        /// <returns></returns>
        protected Bitmap GetPageAsBitmap(PdfDocument pdfDoc, int pageNumber)
        {
            Bitmap bitmapPage = null;

            var imageEventListener = new ImageEventListener();
            imageEventListener.ImageFound += (o, e) =>
            {
                bitmapPage = e.Bitmap;
                if (bitmapPage.Width > bitmapPage.Height)
                    bitmapPage.RotateFlip(RotateFlipType.Rotate270FlipNone);
            };

            imageEventListener.ImageFoundError += (o, e) => {
                LogErrorMessage(e.Exception, $"FindDocuments have error(s) when getting image. The reason: {e.Message}"); // ({_logId})");
            };
            var parser = new PdfCanvasProcessor(imageEventListener);

            var page = pdfDoc.GetPage(pageNumber);
            parser.ProcessPageContent(page);

            return bitmapPage;
        }

        public void LogInfoMessage(string message)
        {
            LogInfo?.Invoke(new LogInfoEventArgs
            {
                AllowReport = true,
                Message = message
            });
        }

        public void LogErrorMessage(Exception ex, string message)
        {
            LogError?.Invoke(new LogErrorEventArgs
            {
                Message = message,
                AllowReport = true,
                Ex = ex
            });
        }
    }
}
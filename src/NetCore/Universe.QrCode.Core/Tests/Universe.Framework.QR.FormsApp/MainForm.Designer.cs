﻿
namespace Universe.Framework.QR.FormsApp
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btLoadImage = new System.Windows.Forms.Button();
            this.btDecode = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.openDocumentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.saveCurrentPageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveResultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveResultAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton2 = new System.Windows.Forms.ToolStripDropDownButton();
            this.btScanCodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btOutLineCodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.createQrCodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cbAutoContrast = new System.Windows.Forms.CheckBox();
            this.cbShowBinarized = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbLog = new System.Windows.Forms.TextBox();
            this.btNextPage = new System.Windows.Forms.Button();
            this.btPrvPage = new System.Windows.Forms.Button();
            this.btOutLineCode = new System.Windows.Forms.Button();
            this.pbDocumentPage = new System.Windows.Forms.PictureBox();
            this.cmsDocumentPageImg = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.resetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetFiltersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.cancelDrawingBorderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cbMedianFilter = new System.Windows.Forms.CheckBox();
            this.cbSharpen = new System.Windows.Forms.CheckBox();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDocumentPage)).BeginInit();
            this.cmsDocumentPageImg.SuspendLayout();
            this.SuspendLayout();
            // 
            // btLoadImage
            // 
            this.btLoadImage.Location = new System.Drawing.Point(14, 885);
            this.btLoadImage.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btLoadImage.Name = "btLoadImage";
            this.btLoadImage.Size = new System.Drawing.Size(105, 40);
            this.btLoadImage.TabIndex = 6;
            this.btLoadImage.Text = "Загрузить документ";
            this.btLoadImage.UseVisualStyleBackColor = true;
            this.btLoadImage.Click += new System.EventHandler(this.btLoadImage_Click);
            // 
            // btDecode
            // 
            this.btDecode.Location = new System.Drawing.Point(141, 885);
            this.btDecode.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btDecode.Name = "btDecode";
            this.btDecode.Size = new System.Drawing.Size(114, 40);
            this.btDecode.TabIndex = 7;
            this.btDecode.Text = "Прочитать QR-код";
            this.btDecode.UseVisualStyleBackColor = true;
            this.btDecode.Click += new System.EventHandler(this.btDecode_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(424, 42);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(147, 15);
            this.label1.TabIndex = 9;
            this.label1.Text = "Изображение документа:";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton1,
            this.toolStripDropDownButton2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1028, 25);
            this.toolStrip1.TabIndex = 10;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openDocumentToolStripMenuItem,
            this.toolStripSeparator1,
            this.saveCurrentPageToolStripMenuItem,
            this.saveResultToolStripMenuItem,
            this.saveResultAsToolStripMenuItem,
            this.toolStripSeparator2,
            this.exitToolStripMenuItem});
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(92, 22);
            this.toolStripDropDownButton1.Text = "Приложение";
            // 
            // openDocumentToolStripMenuItem
            // 
            this.openDocumentToolStripMenuItem.Name = "openDocumentToolStripMenuItem";
            this.openDocumentToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.openDocumentToolStripMenuItem.Text = "Открыть документ";
            this.openDocumentToolStripMenuItem.Click += new System.EventHandler(this.openDocumentToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(236, 6);
            // 
            // saveCurrentPageToolStripMenuItem
            // 
            this.saveCurrentPageToolStripMenuItem.Name = "saveCurrentPageToolStripMenuItem";
            this.saveCurrentPageToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.saveCurrentPageToolStripMenuItem.Text = "Сохранить текущую страницу";
            this.saveCurrentPageToolStripMenuItem.Click += new System.EventHandler(this.saveCurrentPageToolStripMenuItem_Click);
            // 
            // saveResultToolStripMenuItem
            // 
            this.saveResultToolStripMenuItem.Name = "saveResultToolStripMenuItem";
            this.saveResultToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.saveResultToolStripMenuItem.Text = "Сохранить весь документ";
            this.saveResultToolStripMenuItem.Click += new System.EventHandler(this.saveResultToolStripMenuItem_Click);
            // 
            // saveResultAsToolStripMenuItem
            // 
            this.saveResultAsToolStripMenuItem.Name = "saveResultAsToolStripMenuItem";
            this.saveResultAsToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.saveResultAsToolStripMenuItem.Text = "Сохранить весь документ как";
            this.saveResultAsToolStripMenuItem.Click += new System.EventHandler(this.saveResultsAsToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(236, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.exitToolStripMenuItem.Text = "Выход";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // toolStripDropDownButton2
            // 
            this.toolStripDropDownButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btScanCodeToolStripMenuItem,
            this.btOutLineCodeToolStripMenuItem,
            this.toolStripSeparator4,
            this.createQrCodeToolStripMenuItem});
            this.toolStripDropDownButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton2.Name = "toolStripDropDownButton2";
            this.toolStripDropDownButton2.Size = new System.Drawing.Size(60, 22);
            this.toolStripDropDownButton2.Text = "QR-код";
            // 
            // btScanCodeToolStripMenuItem
            // 
            this.btScanCodeToolStripMenuItem.Name = "btScanCodeToolStripMenuItem";
            this.btScanCodeToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.btScanCodeToolStripMenuItem.Text = "Прочитать QR-код";
            this.btScanCodeToolStripMenuItem.Click += new System.EventHandler(this.btScanCodeToolStripMenuItem_Click);
            // 
            // btOutLineCodeToolStripMenuItem
            // 
            this.btOutLineCodeToolStripMenuItem.Name = "btOutLineCodeToolStripMenuItem";
            this.btOutLineCodeToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.btOutLineCodeToolStripMenuItem.Text = "Обвести рамкой QR-код";
            this.btOutLineCodeToolStripMenuItem.Click += new System.EventHandler(this.btOutLineCodeToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(205, 6);
            // 
            // createQrCodeToolStripMenuItem
            // 
            this.createQrCodeToolStripMenuItem.Name = "createQrCodeToolStripMenuItem";
            this.createQrCodeToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.createQrCodeToolStripMenuItem.Text = "Сформировать QR-код";
            this.createQrCodeToolStripMenuItem.Click += new System.EventHandler(this.createQrCodeToolStripMenuItem_Click);
            // 
            // cbAutoContrast
            // 
            this.cbAutoContrast.AutoSize = true;
            this.cbAutoContrast.Location = new System.Drawing.Point(14, 830);
            this.cbAutoContrast.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbAutoContrast.Name = "cbAutoContrast";
            this.cbAutoContrast.Size = new System.Drawing.Size(127, 19);
            this.cbAutoContrast.TabIndex = 22;
            this.cbAutoContrast.Text = "Автоконтрасность";
            this.cbAutoContrast.UseVisualStyleBackColor = true;
            this.cbAutoContrast.CheckedChanged += new System.EventHandler(this.cbAutoContrast_CheckedChanged);
            // 
            // cbShowBinarized
            // 
            this.cbShowBinarized.AutoSize = true;
            this.cbShowBinarized.Location = new System.Drawing.Point(154, 830);
            this.cbShowBinarized.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbShowBinarized.Name = "cbShowBinarized";
            this.cbShowBinarized.Size = new System.Drawing.Size(117, 19);
            this.cbShowBinarized.TabIndex = 23;
            this.cbShowBinarized.Text = "Монохромность";
            this.cbShowBinarized.UseVisualStyleBackColor = true;
            this.cbShowBinarized.CheckedChanged += new System.EventHandler(this.cbShowBinarized_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 42);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 15);
            this.label2.TabIndex = 12;
            this.label2.Text = "Лог:";
            // 
            // tbLog
            // 
            this.tbLog.Location = new System.Drawing.Point(14, 60);
            this.tbLog.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tbLog.Multiline = true;
            this.tbLog.Name = "tbLog";
            this.tbLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbLog.Size = new System.Drawing.Size(389, 762);
            this.tbLog.TabIndex = 11;
            // 
            // btNextPage
            // 
            this.btNextPage.Enabled = false;
            this.btNextPage.Location = new System.Drawing.Point(824, 899);
            this.btNextPage.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btNextPage.Name = "btNextPage";
            this.btNextPage.Size = new System.Drawing.Size(182, 27);
            this.btNextPage.TabIndex = 24;
            this.btNextPage.Text = "Следующая страница";
            this.btNextPage.UseVisualStyleBackColor = true;
            this.btNextPage.Click += new System.EventHandler(this.btNextPage_Click);
            // 
            // btPrvPage
            // 
            this.btPrvPage.Enabled = false;
            this.btPrvPage.Location = new System.Drawing.Point(428, 899);
            this.btPrvPage.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btPrvPage.Name = "btPrvPage";
            this.btPrvPage.Size = new System.Drawing.Size(182, 27);
            this.btPrvPage.TabIndex = 25;
            this.btPrvPage.Text = "Предыдущая страница";
            this.btPrvPage.UseVisualStyleBackColor = true;
            this.btPrvPage.Click += new System.EventHandler(this.btPrvPage_Click);
            // 
            // btOutLineCode
            // 
            this.btOutLineCode.Location = new System.Drawing.Point(280, 885);
            this.btOutLineCode.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btOutLineCode.Name = "btOutLineCode";
            this.btOutLineCode.Size = new System.Drawing.Size(124, 40);
            this.btOutLineCode.TabIndex = 26;
            this.btOutLineCode.Text = "Обвести рамкой QR-код";
            this.btOutLineCode.UseVisualStyleBackColor = true;
            this.btOutLineCode.Click += new System.EventHandler(this.btOutLineCode_Click);
            // 
            // pbDocumentPage
            // 
            this.pbDocumentPage.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbDocumentPage.ContextMenuStrip = this.cmsDocumentPageImg;
            this.pbDocumentPage.Location = new System.Drawing.Point(427, 60);
            this.pbDocumentPage.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pbDocumentPage.Name = "pbDocumentPage";
            this.pbDocumentPage.Size = new System.Drawing.Size(578, 825);
            this.pbDocumentPage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbDocumentPage.TabIndex = 27;
            this.pbDocumentPage.TabStop = false;
            this.pbDocumentPage.DragDrop += new System.Windows.Forms.DragEventHandler(this.pbDocumentPage_DragDrop);
            this.pbDocumentPage.DragEnter += new System.Windows.Forms.DragEventHandler(this.pbDocumentPage_DragEnter);
            this.pbDocumentPage.Paint += new System.Windows.Forms.PaintEventHandler(this.pbDocumentPage_Paint);
            this.pbDocumentPage.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pbDocumentPage_MouseDown);
            this.pbDocumentPage.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pbDocumentPage_MouseMove);
            this.pbDocumentPage.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pbDocumentPage_MouseUp);
            // 
            // cmsDocumentPageImg
            // 
            this.cmsDocumentPageImg.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.resetToolStripMenuItem,
            this.resetFiltersToolStripMenuItem,
            this.toolStripSeparator3,
            this.cancelDrawingBorderToolStripMenuItem});
            this.cmsDocumentPageImg.Name = "cmsDocumentPageImg";
            this.cmsDocumentPageImg.Size = new System.Drawing.Size(291, 76);
            // 
            // resetToolStripMenuItem
            // 
            this.resetToolStripMenuItem.Name = "resetToolStripMenuItem";
            this.resetToolStripMenuItem.Size = new System.Drawing.Size(290, 22);
            this.resetToolStripMenuItem.Text = "Сбросить масштаб";
            this.resetToolStripMenuItem.Click += new System.EventHandler(this.resetToolStripMenuItem_Click);
            // 
            // resetFiltersToolStripMenuItem
            // 
            this.resetFiltersToolStripMenuItem.Name = "resetFiltersToolStripMenuItem";
            this.resetFiltersToolStripMenuItem.Size = new System.Drawing.Size(290, 22);
            this.resetFiltersToolStripMenuItem.Text = "Отменить операции предобработки";
            this.resetFiltersToolStripMenuItem.Click += new System.EventHandler(this.resetFiltersToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(287, 6);
            // 
            // cancelDrawingBorderToolStripMenuItem
            // 
            this.cancelDrawingBorderToolStripMenuItem.Enabled = false;
            this.cancelDrawingBorderToolStripMenuItem.Name = "cancelDrawingBorderToolStripMenuItem";
            this.cancelDrawingBorderToolStripMenuItem.Size = new System.Drawing.Size(290, 22);
            this.cancelDrawingBorderToolStripMenuItem.Text = "Остановить рисование рамки вручную";
            this.cancelDrawingBorderToolStripMenuItem.Click += new System.EventHandler(this.cancelDrawingBorderToolStripMenuItem_Click);
            // 
            // cbMedianFilter
            // 
            this.cbMedianFilter.AutoSize = true;
            this.cbMedianFilter.Location = new System.Drawing.Point(14, 855);
            this.cbMedianFilter.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.cbMedianFilter.Name = "cbMedianFilter";
            this.cbMedianFilter.Size = new System.Drawing.Size(129, 19);
            this.cbMedianFilter.TabIndex = 28;
            this.cbMedianFilter.Text = "Медианый фильтр";
            this.cbMedianFilter.UseVisualStyleBackColor = true;
            this.cbMedianFilter.Visible = false;
            this.cbMedianFilter.CheckedChanged += new System.EventHandler(this.cbMedianFilter_CheckedChanged);
            // 
            // cbSharpen
            // 
            this.cbSharpen.AutoSize = true;
            this.cbSharpen.Location = new System.Drawing.Point(280, 830);
            this.cbSharpen.Name = "cbSharpen";
            this.cbSharpen.Size = new System.Drawing.Size(74, 19);
            this.cbSharpen.TabIndex = 32;
            this.cbSharpen.Text = "Резкость";
            this.cbSharpen.UseVisualStyleBackColor = true;
            this.cbSharpen.CheckedChanged += new System.EventHandler(this.cbSharpen_CheckedChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1028, 935);
            this.Controls.Add(this.cbSharpen);
            this.Controls.Add(this.cbMedianFilter);
            this.Controls.Add(this.pbDocumentPage);
            this.Controls.Add(this.btOutLineCode);
            this.Controls.Add(this.btPrvPage);
            this.Controls.Add(this.btNextPage);
            this.Controls.Add(this.cbAutoContrast);
            this.Controls.Add(this.cbShowBinarized);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbLog);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btDecode);
            this.Controls.Add(this.btLoadImage);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "MainForm";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDocumentPage)).EndInit();
            this.cmsDocumentPageImg.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btLoadImage;
        private System.Windows.Forms.Button btDecode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.CheckBox cbAutoContrast;
        private System.Windows.Forms.CheckBox cbShowBinarized;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbLog;
        private System.Windows.Forms.Button btNextPage;
        private System.Windows.Forms.Button btPrvPage;
        private System.Windows.Forms.Button btOutLineCode;
        private System.Windows.Forms.ToolStripMenuItem saveResultAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openDocumentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveCurrentPageToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.PictureBox pbDocumentPage;
        private System.Windows.Forms.ContextMenuStrip cmsDocumentPageImg;
        private System.Windows.Forms.ToolStripMenuItem resetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetFiltersToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem cancelDrawingBorderToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton2;
        private System.Windows.Forms.ToolStripMenuItem btScanCodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btOutLineCodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveResultToolStripMenuItem;
        private System.Windows.Forms.CheckBox cbMedianFilter;
        private System.Windows.Forms.ToolStripMenuItem createQrCodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.CheckBox cbSharpen;
    }
}

